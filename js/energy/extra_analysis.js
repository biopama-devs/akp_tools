this_app.update_datatables = function (param) {


    console.log('this_app.update_datatables')
    this_app.queried_adm_params['adm0_code'] = this_app.sel_f_admin.adm0_code;


    $.when(this_app.get_ajax_calls.getAdm_tables(this_app.queried_adm_params)).done(function (resp) {
        console.warn(resp)
        /* this_app.all_adm_tables = resp; */
        app_data.datatables_info = {}
        app_data.datatables_info.info = param;
        app_data.datatables_info.data = resp;


    }) //end ajax
}

function sort_by_index(param) {


    return function (a, b) {
        return b[param] > a[param] ? -1 : 1;
    }
}

this_app.plot_modal_datatable = function (resp) {

    if ($('.mapboxgl-popup').length > 0)
        $('.mapboxgl-popup').hide();

    if ($('.analysis_container').length > 0 && $('.analysis_container').is(':visible')) {
        $('.analysis_container').hide();
    }

    $('.dataTables_wrapper,.main_dataTables_wrapper', '#stats_modal').show();


    console.warn(arguments)

    if ($.fn.dataTable.isDataTable('#main_dataTables_wrapper table.dataTable')) {
        var table = $('#main_dataTables_wrapper #main_datatable').DataTable();



        table.destroy();
        //$('.main_dataTables_wrapper table').find('tbody tr,thead tr').empty();


    }

    /* $json['adm_code_value'] = $adm_code_value;
    $json['adm0_code'] = $adm0_code; */
    /* to_query = adm1 & adm_code_value = 1075 & userid = 1732966023959 & adm0_code = 68 */
    var sel_adm_code = resp.adm_code_value;
    var adm_code_name = resp.requested_level + '_name';

    var level = resp.requested_level + '_code';
    //ex:adm0

    if (!resp) {
        debugger
    }
    var data = resp.data_arr;
    /*      if (!this_app.d3_stuff.countries_results) {

             var data = this_app.gaul0_global_info.map(d => { return { ...d, ...d.data } });


             console.warn(data)

         }
         else {
             var data = this_app.d3_stuff.countries_results;
             var sel_adm_code = this_app.d3_stuff.sel_f.adm0_code;
         }
         var adm_name = 'adm0_name';
     }

     else {

         if (resp.data) {
             var data = resp.data.data_arr;
             var adm_code_name = resp.info.adm_code_name;
             var adm_name = adm_code_name.split('_')[0] + '_name';

             var sel_adm_code = resp.info[adm_code_name];
         }
         else {
             var data = resp.data_arr;
             var adm_code_name = this_app.queried_adm_params.to_query + '_code';
             var adm_name = this_app.queried_adm_params.to_query + '_name';


             var sel_adm_code = this_app.queried_adm_params.adm_code_value;
         }

     } */



    function sort_by_index(param) {

        return function (a, b) {
            return b[param] > a[param] ? -1 : 1;
        }
    }




    var buttonCommon = {
        exportOptions: {
            format: {
                header: function (content, columnIdx, html_obj) {
                    console.log(arguments)

                    return this_app.datatables_headers[columnIdx].short;
                },
                body: function (data, row, column, node) {
                    if (column === 6) {
                        console.warn(arguments)
                        var html = $(data);
                        console.log(html)
                    }
                    return data

                }
            }
        }
    }

    var header_html = '';
    var opt = '';
    for (var p in this_app.datatables_headers) {
        var t = this_app.datatables_headers[p];

        header_html += '<th>' + t.short + '</th>';
        if (!t.repeated) {
            if (t.code == 'country')
                opt += '<option value="' + t.code + '" selected>' + t.short + '</option>';
            else
                opt += '<option value="' + t.code + '">' + t.short + '</option>';
        }



    }


    var $datatable = $('#stats_modal .main_dataTables_wrapper #main_datatable');

    function sort_callback(param) {

        var $datatable = $('#stats_modal .main_dataTables_wrapper .dataTable');
        var sort = param.sort;
        var pos = param.pos;
        var sel = param.sel;


        $('#stats_modal .main_dataTables_wrapper .info .sort_asc_desc').text(d => sort == 'asc' ? 'ascending' : 'descending');
        $('#stats_modal .main_dataTables_wrapper .info .sort_param').text(sel.text());

        $datatable.find('th').width(w + 'px');
        $datatable.find('td').css('min-width', w + 'px');
        $datatable.find('.highlighted_datatable_content').removeClass('highlighted_datatable_content');

        var l = $datatable.find('tbody>tr').length;

        console.warn(param, sel)
        if (sel.attr('value') !== 'country') {
            var pos = this_app.datatables_headers.filter(d => !d.repeated).filter(function (d, i) {
                if (d.code == param.code) {
                    d.i2 = i;
                    return d
                }
            })[0].i2;

            $datatable.find('thead th').eq(pos).addClass('highlighted_datatable_content');

            console.log(sel_adm_code)
            if (sel_adm_code)
                $datatable.find(`tbody>tr[_code=${sel_adm_code}]`).addClass('higlighted_datatable_tr');


            $datatable.find('tbody>tr').each(function (i, d) {


                if (sort == 'asc')
                    $(this).find('.adm_td .empty').text(parseInt(l) - i).show();
                else
                    $(this).find('.adm_td .empty').text(parseInt(i) + 1).show();

            });
        }
        else {
            $datatable.find('tbody>tr').each(function (i, d) {

                $(this).find('.adm_td .empty').hide()
            });
        }

    }
    console.info(data)
    var sorted = data.sort(sort_by_index(adm_code_name));


    console.log(header_html)
    //$datatable.find('table thead tr').empty().append(header_html);



    console.warn(data)

    var l = data.length
    $('#stats_modal .datatables_counts').text(l);
    $('#stats_modal .sort_param').text('Administrative unit');
    $('#stats_modal #main_dataTables_wrapper .asc_desc_selection').find('input[value="asc"]').prop('checked', true);

    var w = parseInt($('#stats_modal').width() / 8);
    console.log($datatable)


    this_app.general_data_table_obj = $datatable.DataTable(
        {
            /* retrieve: true, */
            scrollY: "800px",
            dom: 'Bfrtip',
            scrollX: true,
            scrollCollapse: true,
            paging: false,
            fixedColumns: false,
            fixedHeader: true,

            buttons: [
                {
                    extend: 'pdfHtml5',
                    text: 'Export to PDF',
                    exportOptions: {
                        //  columns: ':not(.no-export)' // Exclude columns with the "no-export" class name {
                        columns: function (idx, data, node) {
                            // Exclude columns with the "no-export" class or those set as invisible
                            return $(node).hasClass('no-export') === false && $(node).is(':visible');
                        }
                    },

                    customize: function (doc) {
                        doc.defaultStyle = {
                            font: 'OpenSans',
                            bold: false, // Disable bold text,
                            fontSize: 10
                        };

                        // Remove all bold text explicitly
                        if (doc.styles) {
                            for (var key in doc.styles) {
                                if (doc.styles[key].bold) {
                                    doc.styles[key].bold = false;
                                }
                            }
                        }
                    },

                },
                $.extend(true, {}, buttonCommon, {
                    extend: 'excelHtml5',
                    text: 'Export to Excel',
                    exportOptions: {
                        columns: [0, 2, 3, 4, 5, 7, 8, 10, 12]
                        //columns: 'visible'
                    }
                })



            ],


            data: sorted,
            columnDefs: [
                {
                    targets: [0, 6, 10], // Example: Mark the first column as no-export
                    className: 'no-export'
                },
                {
                    width: w,

                },
                { targets: 0, visible: false, type: 'string' },
                { targets: 1, orderData: 0, type: 'string' },
                { targets: 2, orderData: 2 },
                { targets: 3, orderData: 3 },
                { targets: 4, orderData: 4 },

                { targets: 5, orderData: 5 },

                { targets: 6, visible: false },
                { targets: 7, orderData: 7 },
                { targets: 8, orderData: 8 },
                { targets: 9, orderData: 10 },
                { targets: 10, visible: false },
                { targets: 11, orderData: 12 },
                { targets: 12, visible: false }

            ],


            columns: [{
                data: adm_code_name,
                className: 'adm_td',
                orderable: true,
                'render': function (data, type, row) {
                    console.log(row)

                    /* var sel_adm_code = resp.adm_code_value;
                    var adm_code_name = resp.requested_level + '_name';
                    var level = resp.requested_level + '_code';
                     */
                    console.info(adm_code_name)
                    return row[adm_code_name];
                }

            }, {
                title: 'Name',
                data: adm_code_name,
                className: 'adm_td',
                orderable: true,
                'render': function (data, type, row) {
                    return '<span class="empty"></span><span class="name">' + row[adm_code_name] + '</span>';
                }

            },
            {
                title: 'Schools',
                data: 'schools',
                className: 'schools',
                orderable: true,

                'render': function (data, type, row) {


                    return numberWithCommas(row.schools)
                }

            },
            {
                title: 'Power plants',
                data: 'power_plants',
                className: 'power_plants',
                orderable: true,

                'render': function (data, type, row) {


                    return numberWithCommas(row.power_plants)
                }

            }, {
                'title': 'Health centres in-grid',
                data: 'hc_in_grid',
                className: 'hc_in_grid',
                orderable: true,
                'render': function (data, type, row) {


                    return numberWithCommas(row.hc_in_grid)
                }

            }, {
                'title': 'Health centres out-grid',
                data: 'hc_out_grid',
                className: 'hc_out_grid',
                orderable: true,
                'render': function (data, type, row) {


                    return numberWithCommas(row.hc_out_grid);
                }

            },
            {
                data: 'hc_out_grid',
                className: 'hc_out_grid',
                orderable: true,
                'render': function (data, type, row) {


                    return row.hc_out_grid;
                }

            },
            {
                title: 'Refugee camps',
                data: 'refugee_camps',
                className: 'refugee_camps',
                orderable: true,
                'render': function (data, type, row) {


                    return numberWithCommas(row.refugee_camps)
                }

            },
            {
                title: 'Electric grid',
                data: 'electric_grid',
                className: 'electric_grid',
                orderable: true,
                'render': function (data, type, row) {

                    return row.electric_grid
                }

            },
            {
                title: 'Density',
                data: 'ghs_density',
                className: 'ghs_density',
                orderable: true,
                'render': function (data, type, row) {


                    return +numberWithCommas(row.ghs_density)
                }

            },
            {
                data: 'ghs_density',
                className: 'ghs_density',
                orderable: true,
                'render': function (data, type, row) {


                    return row.ghs_density;
                }

            },

            {
                title: 'Population',
                data: 'ghs_sum_pop',
                className: 'ghs_sum_pop',
                orderable: true,
                'render': function (data, type, row) {

                    return numberWithCommas(row.ghs_sum_pop);
                }


            },
            {
                data: 'ghs_sum_pop',
                className: 'ghs_sum_pop',
                orderable: true,
                'render': function (data, type, row) {

                    return row.ghs_sum_pop;
                }

            },








            ]



        }
    ); //end datatable



    /*
        $datatable.find('th').width(w + 'px');


     */
    $('#stats_modal .dataTables_info_btn').on('click', function () {

        if ($(this).parents().closest('.main_dataTables_wrapper').length > 0)
            $('#stats_modal').find('.buttons-excel').eq(0).click();
        else
            $('#stats_modal').find('.buttons-excel').eq(1).click();
    })

    /*    setTimeout(function () {
           alert($datatable.find('.dt-scroll-body thead').length)
           $datatable.find('.dt-scroll-body thead').remove();

       }, 100) */
    $datatable.find('td').css('min-width', w + 'px');

    $('#stats_modal .main_dataTables_wrapper .sort_param_select').append(opt);

    $('.main_dataTables_wrapper select.sort_param_select').unbind('change');
    $('.main_dataTables_wrapper select.sort_param_select').on('change', function () {
        var param = {};
        console.warn(data_table_obj)

        param.sel = $(this).find('option:selected');
        var id = param.sel.attr('value');


        param.pos = this_app.datatables_headers.filter(d => d.code == id && !d.repeated)[0].i;
        param.sort = $('#stats_modal .main_dataTables_wrapper .asc_desc_selection').find('input:checked').attr('value');
        param.code = id;
        if (param.code == 'country') {
            param.pos = 0;

            data_table_obj
                .order([param.pos, param.sort])

                .draw();
        }
        else {

            this_app.general_data_table_obj
                .order([param.pos, param.sort])
                .draw();


        }
        sort_callback(param);

    });

    $('.asc_desc_selection input', '.main_dataTables_wrapper').unbind('change');
    $('.asc_desc_selection input', '.main_dataTables_wrapper').on('change', function () {


        var param = {};

        param.sel = $('#stats_modal .main_dataTables_wrapper .sort_param_select').find('option:selected');
        var id = param.sel.attr('value');

        console.warn(this_app.datatables_headers.filter(d => d.code == id && !d.repeated))
        param.pos = this_app.datatables_headers.filter(d => d.code == id && !d.repeated)[0].i;
        param.sort = $('#stats_modal .main_dataTables_wrapper .asc_desc_selection').find('input:checked').attr('value');
        param.code = id;
        console.log(param)
        if (id == 'country') {

            this_app.general_data_table_obj
                .order([0, param.sort])
                .draw();
        } else {
            this_app.general_data_table_obj
                .order([param.pos, param.sort])
                .draw();


        }
        sort_callback(param);

    })




    $datatable.find('tbody tr.higlighted_datatable_tr').removeClass('higlighted_datatable_tr')

    /*  if (sel_adm_code) {
         console.warn(sorted)

         console.log(sel_adm_code)


         console.log($datatable.find('tbody tr').length)
         console.log($datatable.find('tbody tr'))
         $datatable.find('tbody tr').each(function (i, d) {

             if (!sorted[i]) // && !sorted[i]['adm0_code'])
                 debugger
             $(this).attr('_code', sorted[i]['adm0_code']);
         });

         $datatable.find(`tbody>tr[_code=${sel_adm_code}]`).addClass('higlighted_datatable_tr');
     } */


    var param = {};
    var id;
    $('#stats_modal .main_dataTables_wrapper .sort_param_select option').each(function () {

        if ($(this).attr('selected')) {
            param.sel = $(this);
            param.code = $(this).attr('value');
        }
    });

    console.warn(param)
    console.log(this_app.datatables_headers)
    /*   param.pos = this_app.datatables_headers.filter(d => d.code == param.code && !d.repeated)[0].i;
      param.sort = $('#stats_modal .asc_desc_selection').find('input:checked').attr('value');
      console.log(param)
      console.info($('#stats_modal .main_dataTables_wrapper .dataTable thead tr th')) */

    // sort_callback(param);





}



this_app.plot_hc_modal_datatable = function (resp) {


    if ($.fn.dataTable.isDataTable('#hc_dataTables_wrapper table.dataTable')) {
        var table = $('#hc_dataTables_wrapper #hc_datatable').DataTable();



        table.destroy();



    }
    /*     if (resp.data) {
            var data = resp.data.data_arr;
            var adm_code_name = resp.info.adm_code_name;
            var adm_name = adm_code_name.split('_')[0] + '_name';

            var sel_adm_code = resp.info[adm_code_name];
        }
        else {
            var data = resp.data_arr;
            var adm_code_name = this_app.queried_adm_params.to_query + '_code';
            var adm_name = this_app.queried_adm_params.to_query + '_name';


            var sel_adm_code = this_app.queried_adm_params.adm_code_value;
        } */
    var data = resp.data_arr;
    var sel_adm_code = resp.adm_code_value;
    var adm_code_name = resp.requested_level + '_name';

    var level = resp.requested_level + '_code';
    console.warn(data)

    var sorted = data.sort(sort_by_index(adm_code_name))
    /*         .map(d => {
                //add adm_name to each object


                return { ...d, ...d.data }
            }); */
    console.log(sorted)

    $('#hc_dataTables_wrapper .asc_desc_selection').find('input[value="asc"]').prop('checked', true);

    var td_w = parseInt($('#stats_modal').width() / 8);

    function sort_callback(param) {
        debugger
        console.warn(param);
        var sort = param.sort;
        var pos = param.pos;
        var sel = param.sel;
        var table_type = param.table_type;

        if (table_type == 'hc_out') {

            $wrapper = $('#stats_modal .hc_dataTables_wrapper');
            $datatable = $wrapper.find('.dataTable');
            var headers = this_app.out_hc_datatables_headers;

        } else {
            $wrapper = $('.dataTables_wrapper');
            $datatable = $wrapper.find('.dataTable');
            var headers = this_app.datatables_headers;
        }

        $wrapper.find('.info .sort_asc_desc').text(d => sort == 'asc' ? 'ascending' : 'descending');
        $wrapper.find('.info .sort_param').text(sel.text());

        var $datatable = $('#stats_modal .hc_dataTables_wrapper #hc_datatable');

        $datatable.find('.highlighted_datatable_content').removeClass('highlighted_datatable_content');

        var l = $datatable.find('tbody>tr').length;

        console.warn(param, sel)
        if (sel.attr('value') !== 'country') {
            var pos = headers.filter(d => !d.repeated).filter(function (d, i) {
                if (d.code == param.code) {
                    d.i2 = i;
                    return d
                }
            })[0].i2;

            $datatable.find('thead th').eq(pos).addClass('highlighted_datatable_content');
            $datatable.find('tbody>tr').each(function (i, d) {
                $(this).find('td').eq(pos).addClass('highlighted_datatable_content');

                if (sort == 'asc')
                    $(this).find('.adm_td .empty').text(parseInt(l) - i)
                else
                    $(this).find('.adm_td .empty').text(parseInt(i) + 1)

            });
        }
        var w = parseInt($('#stats_modal').width() / 8);

        $wrapper.find(".dataTable").css("width", "100%");

        $datatable.find('th').width(w + 'px');


        $datatable.find('td').css('min-width', w + 'px');
        $datatable.find('.highlighted_datatable_content').removeClass('highlighted_datatable_content');

        /*  setTimeout(function () {

             var $datatable = $('#stats_modal .hc_dataTables_wrapper table.extra_info_datatable')
             var td_w = parseInt($('#stats_modal').width() / 8);
             $datatable.find('thead th').css('width', td_w + 'px!important');
             $datatable.find('thead th').attr('style', 'width:' + td_w + 'px!important');
             $datatable.find('td').css('min-width', td_w + 'px');
         }, 500) */

    }
    function sort_callback_p(param) {

        console.warn(param);
        var sort = param.sort;
        var pos = param.pos;
        var sel = param.sel;
        var table_type = param.table_type;

        if (table_type == 'hc_out') {
            $wrapper = $('#stats_modal .hc_dataTables_wrapper');
            $datatable = $wrapper.find('.dataTable');
            var headers = this_app.out_hc_datatables_headers;

        } else {
            $wrapper = $('.dataTables_wrapper');
            $datatable = $wrapper.find('.dataTable');
            var headers = this_app.datatables_headers;
        }

        $wrapper.find('.info .sort_asc_desc').text(d => sort == 'asc' ? 'ascending' : 'descending');
        $wrapper.find('.info .sort_param').text(sel.text());



        $datatable.find('.highlighted_datatable_content').removeClass('highlighted_datatable_content');

        var l = $datatable.find('tbody>tr').length;

        console.warn(param, sel)
        if (sel.attr('value') !== 'country') {
            var pos = headers.filter(d => !d.repeated).filter(function (d, i) {
                if (d.code == param.code) {
                    d.i2 = i;
                    return d
                }
            })[0].i2;

            $datatable.find('thead th').eq(pos).addClass('highlighted_datatable_content');
            $datatable.find('tbody>tr').each(function (i, d) {
                $(this).find('td').eq(pos).addClass('highlighted_datatable_content');

                if (sort == 'asc')
                    $(this).find('.adm_td .empty').text(parseInt(l) - i)
                else
                    $(this).find('.adm_td .empty').text(parseInt(i) + 1)

            });
        }

        setTimeout(function () {
            /* var $datatable = $('#stats_modal .hc_dataTables_wrapper .dataTable'); */
            var $datatable = $('#stats_modal .hc_dataTables_wrapper table.dataTable')
            var td_w = parseInt($('#stats_modal').width() / 8);
            $datatable.find('thead th').css('width', td_w + 'px!important');
            $datatable.find('thead th').attr('style', 'width:' + td_w + 'px!important');
            $datatable.find('td').css('min-width', td_w + 'px');
        }, 500)

    }

    var $datatable = $('#stats_modal .hc_dataTables_wrapper #hc_datatable');
    $datatable.show();
    var $wrapper = $('#stats_modal .hc_dataTables_wrapper');



    var t = app_data.layers_info.filter(function (d) {
        if (d.id == 'hospitals')
            return d
    })[0]
    var params_symbology = t.params_symbology;
    var params_description = t.params_description;
    this_app.out_hc_datatables_headers = [

        {
            code: 'country',
            text: 'Adm. Name',
            short: 'Adm. Name',

            i: 0

        },
        {
            code: 'country',
            text: 'Adm. Name',
            short: 'Adm. Name',
            repeated: true,

            i: 1
        },
        {
            code: 'hc_out_grid',
            text: 'Out-grid health centres',
            short: 'Out-grid health centres',
            i: 2
        },
        {
            code: 'hc_out_grid',
            text: 'Out-grid health centres',
            short: 'Out-grid health centres',
            i: 3,
            repeated: true
        },
        {
            code: 'pv',
            text: 'PV (kW)',
            short: 'Required PV [kWp]',
            long: 'Required PV capacity [kWp]',

            i: 4
        },
        {
            code: 'pv_value',

            text: 'PV avg',
            short: 'PV val',
            repeated: true,
            i: 5
        },
        {
            code: 'bat',
            text: 'BAT',
            short: 'Battery size to install [kWh]',
            long: 'Battery size to install [kWh]',

            i: 6
        },
        {
            code: 'bat_value',
            text: 'BAT',
            short: 'BAT',
            repeated: true,
            i: 7
        },
        {
            code: 'eldemand',
            text: 'eldemand',
            short: 'Electricity demand [kWh/year]',
            long: 'Annual electricity demand per facility per year [kWh/year]',

            i: 8
        },
        {
            code: 'eldemand_value',
            text: 'eldemand',
            short: 'eldemand',
            repeated: true,
            i: 9
        },

        {
            code: 'lcoeh_soft',
            text: 'lcoeh_soft',
            short: 'Levelised cost of electricity [EUR/kWh]',
            long: 'Levelised cost of electricity [EUR/kWh]',

            i: 10
        },
        {
            code: 'lcoeh_soft_value',
            text: 'lcoeh_soft',
            short: 'lcoeh_soft',
            repeated: true,
            i: 11
        },
        {
            code: 'capex_soft',
            text: 'capex_soft',
            short: 'Up-front cost [EUR]',
            long: 'Up-front cost [EUR]: capital costs [EUR] of PV installation (including storage and BOS)',
            i: 12
        },
        {
            code: 'capex_soft_value',
            text: 'capex_soft',
            short: 'capex_soft',
            repeated: true,
            i: 13
        }
    ]


    if ($wrapper.find('.sort_param_select option').length == 0) {



        $('.hc_dataTables_wrapper .change_sort_param').on('click', function () {

            $('#stats_modal .hc_dataTables_wrapper .change_sort_param_tool').show();

        })


        $('input.extend_hc_info').on('change', function () {

            //check if checked

            if ($('input.extend_hc_info').is(':checked')) {
                $('#stats_modal .hc_dataTables_wrapper').find('.val_div_container').show();
            } else {
                $('#stats_modal .hc_dataTables_wrapper').find('.val_div_container').hide();
            }

        })





        var to_f = params_symbology.filter(d => d.param_name !== 'category_l')
        for (var p in to_f) {

            var _this = to_f[p];
            var html = '<div class="params_description_container"><div class="col-content"><div class="title">' + this_app.out_hc_datatables_headers.filter(d => d.code == _this.param_name && !d.repeated)[0].long + '</div>';
            html += '<table><thead><tr><th><th></tr></thead><tbody>';

            /*    if (_this.param_name == 'pv' || _this.param_name == 'eldemand') {
                   var s = 's3';
               } else {
                   var s = 's5';
               } */
            html += _this.legend.map(function (d, i) {
                if (!d.val.includes('data')) {


                    /* if (_this.param_name == 'lcoeh_soft') {

                        var val = d.val.split('//')[1];
                    } else {

                    } */
                    var val = d.val;
                    if (!val) console.log(d, _this)
                    return '<tr><td><span>' + val + '</span></td><td><span class="cat" style="background:' + d.color + '">' + d.text + '</span></td></tr>'

                }
            }).join('')
            html += '</tbody></table></div>';
            console.info(html)
            $('.params_description2 .row').eq(0).append(html)

        }
        $('.show_hide_params_description2_btn').on('click', function () {
            if ($(this).hasClass('active') == false) {
                $('.params_description2').show();
            } else {
                $('.params_description2').hide();
            }
            $(this).toggleClass('active')
        })


    } //end first time plotted

    var header_html = '';
    var opt = '';
    for (var p in this_app.out_hc_datatables_headers) {

        var t = this_app.out_hc_datatables_headers[p];
        header_html += '<th>' + t.short + '</th>';

        if (p == 0) {

            opt += '<option value="' + t.code + '" selected>' + t.short + '</option>';
        } else {
            if (!t.repeated)
                opt += '<option value="' + t.code + '">' + t.short + '</option>';
        }



    }
    //   $datatable.find('thead tr').empty().append(header_html);

    $('#stats_modal .hc_dataTables_wrapper .sort_param_select').empty().append(opt);



    $('#stats_modal .hc_dataTables_wrapper select.sort_param_select').unbind('change');
    $('#stats_modal .hc_dataTables_wrapper select.sort_param_select').on('change', function () {
        var param = {};

        param.sel = $(this).find('option:selected');
        var id = param.sel.attr('value');

        param.pos = this_app.out_hc_datatables_headers.filter(d => d.code == id && !d.repeated)[0].i;
        param.sort = $('#stats_modal .hc_dataTables_wrapper .asc_desc_selection').find('input:checked').attr('value');
        param.code = id;
        param.table_type = 'hc_out';
        data_table_obj
            .order([param.pos, param.sort])
            .draw();
        sort_callback(param);
    })

    $('.asc_desc_selection input', '.hc_dataTables_wrapper').unbind('change');
    $('.asc_desc_selection input', '.hc_dataTables_wrapper').on('change', function () {


        var param = {};

        param.sel = $wrapper.find('.sort_param_select').find('option:selected');
        var id = param.sel.attr('value');
        console.warn(this_app.out_hc_datatables_headers.filter(d => d.code == id && !d.repeated))
        param.pos = this_app.out_hc_datatables_headers.filter(d => d.code == id && !d.repeated)[0].i;
        param.sort = $(this).attr('value');
        param.table_type = 'hc_out';

        console.log(param)
        console.info(data_table_obj)
        param.code = id;
        data_table_obj
            .order([param.pos, param.sort])
            .draw();
        sort_callback(param);


    });




    var lcoeh_symbology = params_symbology.filter(function (d) {
        return d.param_name == 'lcoeh_soft'
    })[0].legend;

    var bat_symbology = params_symbology.filter(function (d) {
        return d.param_name == 'bat'
    })[0].legend;

    var eldemand_symbology = params_symbology.filter(function (d) {
        return d.param_name == 'eldemand'
    })[0].legend;

    var pv_symbology = params_symbology.filter(function (d) {
        return d.param_name == 'pv'
    })[0].legend;

    var capex_symbology = params_symbology.filter(function (d) {
        return d.param_name == 'capex_soft'
    })[0].legend;

    var param = {};
    param.sel = $('#stats_modal .hc_dataTables_wrapper .sort_param_select').find('option:selected');
    var id = param.sel.attr('value');

    param.code = id;
    //param.pos = this_app.out_hc_datatables_headers.filter(d => d.code == id && !d.repeated)[0].i;
    //param.sort = $('#stats_modal .hc_dataTables_wrapper .asc_desc_selection').find('input:checked').attr('value');
    param.table_type = 'hc_out';


    console.log(sorted)
    var data_table_obj = $datatable.DataTable({
        scrollY: "800px",
        /* bSort: false, */
        dom: 'Bfrtip',
        scrollX: true,
        scrollCollapse: true,
        paging: false,
        fixedColumns: false,
        fixedHeader: true,
        buttons: [
            {
                extend: 'pdfHtml5',
                text: 'Export to PDF',
                exportOptions: {
                    //  columns: ':not(.no-export)' // Exclude columns with the "no-export" class name {
                    columns: function (idx, data, node) {
                        // Exclude columns with the "no-export" class or those set as invisible
                        return $(node).hasClass('no-export') === false && $(node).is(':visible');
                    }
                },

                customize: function (doc) {
                    doc.defaultStyle = {
                        font: 'OpenSans',
                        bold: false, // Disable bold text,
                        fontSize: 10
                    };

                    // Remove all bold text explicitly
                    if (doc.styles) {
                        for (var key in doc.styles) {
                            if (doc.styles[key].bold) {
                                doc.styles[key].bold = false;
                            }
                        }
                    }
                },

            },
            $.extend(true, {}, buttonCommon, {
                extend: 'excelHtml5',
                text: 'Export to Excel',
                exportOptions: {
                    columns: [1, 3, 5, 7, 9, 11, 13]
                    //columns: 'visible'
                }
            })



        ],
        /*   buttons: [
              $.extend(true, {}, buttonCommon('out_hc'), {
                  extend: 'excelHtml5',
                  exportOptions: {
                      columns: [1, 3, 5, 7, 9, 11, 13]
                  }
              }),

          ], */


        data: sorted,
        columnDefs: [{
            width: td_w
        },
        {
            targets: [0, 2, 4, 6, 8, 10, 13], // Example: Mark the first column as no-export
            className: 'no-export'
        },
        { targets: 1, visible: false },
        { targets: 0, orderData: 1 },
        { targets: 3, visible: false },
        { targets: 2, orderData: 3 },
        { targets: 4, orderData: 5 },
        { targets: 5, visible: false },
        { targets: 6, orderData: 7 },

        { targets: 7, visible: false },

        { targets: 8, orderData: 9 },
        { targets: 9, visible: false },

        { targets: 10, orderData: 11 },
        { targets: 11, visible: false },

        { targets: 12, orderData: 13 },
        { targets: 13, visible: false },
        ],


        columns: [{
            title: 'Name',
            data: adm_code_name,
            className: 'adm_td',
            orderable: true,
            'render': function (data, type, row) {

                return '<span class="empty"></span><span class="name">' + row[adm_code_name] + '</span>';
            }

        }, {
            data: adm_code_name,
            className: 'adm_td',
            orderable: true,
            'render': function (data, type, row) {
                return row[adm_code_name];
            }

        },
        {
            title: 'Out-grid health centres',
            data: 'hc_out_grid',
            className: 'hc_out_grid',
            orderable: true,

            'render': function (data, type, row) {


                return numberWithCommas(row.hc_out_grid);
            }

        },
        {
            data: 'hc_out_grid',
            className: 'hc_out_grid',
            orderable: false,

            'render': function (data, type, row) {


                return row.hc_out_grid;
            }

        },
        {
            title: 'PV',
            data: 'pv',
            className: 'pv',
            orderable: true,
            'render': function (data, type, row) {


                var cat = row.pv.cat;


                return '<div class="cat_div">' +
                    '<div class="cat_container"><span class="cat" style="background: ' + pv_symbology[cat].color + '">' +
                    pv_symbology[cat].text + '</span></div></div>' +
                    '<div>Average:' + row.pv.avg + '</div><div class="val_div_container"><span class="val">' + row.pv.max + '</span><span class="percent">  (' + row.pv.percent + '%)</span></div>';
            }

        },
        {

            data: 'pv',
            className: 'pv',
            orderable: true,
            'render': function (data, type, row) {


                return row.pv.avg;
            }

        },


        {
            title: 'BAT',
            data: 'bat',
            className: 'bat',
            orderable: true,
            'render': function (data, type, row) {

                var cat = row.bat.cat;
                return '<div class="cat_div">' +
                    '<div class="cat_container"><span class="cat" style="background: ' + bat_symbology[cat].color + '">' +
                    bat_symbology[cat].text + '</span></div></div>' +

                    '<div> Average: ' + row.bat.avg + '</div> <div class="val_div_container"><span class="val">' + row.bat.max + '</span><span class="percent">  (' + row.bat.percent + '%)</span></div>';


            }

        },
        {

            data: 'bat',
            className: 'bat',
            orderable: true,
            'render': function (data, type, row) {


                return row.bat.avg;
            }

        },




        {

            title: 'Electricity demand',
            data: 'eldemand',
            className: 'eldemand',
            orderable: true,
            'render': function (data, type, row) {

                var cat = row.eldemand.cat;

                return '<div class="cat_div">' +
                    '<div class="cat_container"><span class="cat" style="background: ' + eldemand_symbology[cat].color + '">' +
                    eldemand_symbology[cat].text + '</span></div></div>' +
                    '<div> Average: ' + row.eldemand.avg + '</div>' +
                    '<div class="val_div_container"><span class="val">' + row.eldemand.max + '</span><span class="percent">  (' + row.eldemand.percent + '%)</span></div>';

            }

        },
        {

            data: 'eldemand',
            className: 'eldemand',
            orderable: true,
            'render': function (data, type, row) {


                return row.eldemand.avg;
            }

        },


        {
            title: 'LCOEH',
            data: 'lcoeh_soft',
            className: 'lcoeh_soft',
            orderable: true,
            'render': function (data, type, row) {

                var cat = row.lcoeh_soft.cat;

                return '<div class="cat_div">' +
                    '<div class="cat_container"><span class="cat" style="background: ' + lcoeh_symbology[cat].color + '">' +
                    lcoeh_symbology[cat].text + '</span></div></div>' +
                    '<div> Average: ' + row.lcoeh_soft.avg + '</div>' +
                    '<div class="val_div_container"><span class="val">' + row.lcoeh_soft.max + '</span><span class="percent">  (' + row.lcoeh_soft.percent + '%)</span></div>';
            }

        },
        {

            data: 'lcoeh_soft',
            className: 'lcoeh_soft',
            orderable: true,
            'render': function (data, type, row) {


                return row.lcoeh_soft.avg;
            }

        },


        {
            title: 'Capex',

            data: 'capex_soft',
            className: 'capex_soft',
            orderable: true,
            'render': function (data, type, row) {

                var cat = row.capex_soft.cat;

                return '<div class="cat_div">' +
                    '<div class="cat_container"><span class="cat" style="background: ' + capex_symbology[cat].color + '">' +
                    capex_symbology[cat].text + '</span></div></div>' +
                    '<div> Average: ' + row.capex_soft.avg + '</div>' +
                    '<div class="val_div_container"><span class="val">' + row.capex_soft.max + '</span><span class="percent">  (' + row.capex_soft.percent + '%)</span></div>';
            }


        },
        {

            data: 'capex_soft',
            className: 'capex_soft',
            orderable: true,
            'render': function (data, type, row) {


                return row.capex_soft.avg;
            }

        },



        ]


    }); //end datatable

    console.warn($datatable)
    // sort_callback(param);

    function buttonCommon(table) {

        if (table == 'out_hc')
            var headers = this_app.out_hc_datatables_headers;

        return {
            exportOptions: {
                format: {
                    header: function (content, columnIdx, html_obj) {

                        console.log(arguments, headers)

                        return headers[columnIdx].short;

                    },
                    body: function (data, row, column, node) {

                        return data


                    }
                }
            }
        }
    }


}

this_app.create_d3_legend_analysis = function (type, layer_id, param_name) {

    console.warn(arguments)

    if (param_name == 'gen_type')
        return false;

    console.log(this_app.analysis_data)
    console.info(layer_id);
    console.info(app_data.layers_info);
    var this_data = this_app.analysis_data.filter(function (d) {
        return d.layer_name == layer_id && d.param == param_name
    })[0];

    if (layer_id == 'hospitals') {
        var name = 'Health facilities';
    }
    if (layer_id == 'power_plants') {
        var name = 'Power plants';
    }
    if (layer_id == 'ref_settlements') {
        var name = 'Refugee settlements';

    }

    console.warn(this_data)

    console.info(this_data.data.sort(d => d.cat))
    var t_layer = app_data.layers_info.filter(function (d) {
        return d.id == layer_id;
    })[0];

    var params = t_layer.params_description;
    console.warn(t_layer)
    console.warn(arguments)

    var svg = d3.select('.analysis_container .card.' + layer_id + ' svg.circles_svg.' + param_name);

    console.log(svg)
    svg.style('display', 'block');

    var t_symbol = t_layer.params_symbology.filter(function (d) {
        return d.param_name == param_name
    });
    console.log(t_symbol)
    var t = this_data.data.sort(d => d.cat)

    if (t)
        console.info(t)

    var t_arr_cat = t.map(d => d.cat);
    var scale_val = t_symbol[0].legend;



    var scale_val = t_symbol[0].legend.map(function (d, i) {
        // Filter data matching the current category
        var filtered = t.filter(item => item.cat === i);

        var t_counts = filtered[0] ? filtered[0].counts : 0;
        var text = d.val + '//' + t_counts;

        if (i === 0)
            text += ' records';
        // Prepare the data object
        var data = {
            val: text,
            color: d.color,
            text: d.text
        };



        return data;
    });


    console.warn(scale_val)

    var params_description = params.filter(function (d) {
        return d.param_name == param_name
    })[0];

    if (params_description && params_description.type_param == 'range' || t_layer._type == 'marker') {
        console.log(params_description)
        var width = $('.card.' + layer_id).width();
        var buckets_l = scale_val.length;

        var unit_w = parseInt(width / buckets_l);
        var foreignObject_height = 25;

        if (t_layer._type == 'marker') {

            if (t_symbol[0].symbol_type == 'square')
                var _symbol = d3.symbolSquare;


            if (t_symbol[0].symbol_type == 'diamond')
                var _symbol = d3.symbolDiamond;
        }

        if (params_description._domain) {
            var _domain = params_description._domain;
            console.log(_domain)
        } else {
            console.warn('no need of linearsize, no domain...etc')
        }
        console.warn(t_symbol)


        $('.analysis_container .card.' + layer_id).find('.d3_legend').css('display', 'block!important');

        if (t_symbol[0].orienation == 'vertical') {



            $('.analysis_container .card.' + layer_id).find('.d3_legend').height(buckets_l * foreignObject_height);

            var unit_w = parseInt(width / buckets_l);
            var shape_height = 15;
        } else {

            svg.attr("height", 70);
            var unit_w = 70;
            var shape_height = 15;
        }
        svg.append("g")
            .attr("class", "circles_g")

            .attr("transform", "translate(30, 10)");





        if (t_layer._type == 'line')
            var _symbol = d3.symbolSquare;

        if (t_layer._type == 'circle') {
            var linearSize = d3.scaleQuantize().domain(_domain).range(params_description._range);
            console.warn(params_description._range)
            var _symbol = d3.symbolCircle;
            var t_shape = 'circle';

            var legendSize = d3.legendSize()
                .scale(linearSize);

        }


        if (t_layer._type == 'marker') {
            console.log(t_symbol)
            if (t_symbol[0].symbol_type == 'square')
                var _symbol = d3.symbolSquare;

            if (t_symbol[0].symbol_type == 'diamond')
                var _symbol = d3.symbolDiamond;

            var legendSize = d3.legendColor();

        }
        if (t_shape == 'circle') {
            legendSize.shape('circle')
                .shapePadding(8)
                .labelOffset(20)
                .orient(t_symbol[0].orienation);
        } else {
            legendSize.shape("path", d3.symbol().type(_symbol).size(150)())
                .shapePadding(8)
                .labelOffset(20)
                .orient(t_symbol[0].orienation);
        }

        if (this_data.param == 'lcoeh_soft' && this_data.layer_name == 'hospitals')
            debugger

        legendSize
            .labelFormat('.03f')
            .labels(scale_val.map(function (d) {
                console.info(d)
                return d.val
            }))

        console.log(legendSize)
        svg.select(".circles_g")
            .call(legendSize);

        if (t_shape == 'circle') {
            var a = svg.selectAll('.legendCells circle')
        } else {
            var a = svg.selectAll('.legendCells path')
        }

        a.attr('fill', function (d, i) {

            if (!scale_val[i]) {
                d3.select(this.parentNode).remove();

            } else {
                return scale_val[i].color;
            }
        });
        svg.selectAll('.legendCells text')
            .attr('fill', function (d, i) {
                if (!scale_val[i]) {
                    return '#fff';

                } else {
                    return scale_val[i].color;
                }
            })

        if (t_layer.border) {
            console.info('.card.' + t_layer.id + ' .svg_circle.' + param_name + ' circle')
            d3.selectAll('.card.' + t_layer.id + ' .svg_circle.' + param_name + ' circle')
                .style('stroke', t_layer.border)
                .style('stroke-width', '2.5')
        }
    }

}



function sort_by_value(param) {

    return function (a, b) {
        console.info(b)
        return a.param < b.param ? -1 : 1;
    }

}


this_app.update_ghs_analysis = function (data) {

    console.info(this_app.this_pdf)
     $('.row.analysis_container').append(`</div><div class="row second_row d-flex nojustify-content-between"><div class="card-container col-md-3 ghs_africa_250m_bigger_2" _layer="ghs_africa_250m_bigger_2"><div class="analysis_card card"><div class="card-content white-text"> <div class="general_info">
        <div class="card-title">Population</div><div class="ghs_counts_container"></div></div><div class="info" style="display:none">

        <div class="row d-flex">
        <div class="col-10">Click any property to extend information</div>
        <div class="col-2 text-end">
    <i class="bi bi-x close_analysis_params"></i>
    </div>
    </div>

        <div class="row row_header"> <div class="col s6">Classification km<sup>2</sup></div><div class="col s3">Inhabitants</div> <div class="col s3">Hab./km<sup>2</sup></div> </div> <div class="rows_container"> </div></div></div><div class="card-action"> <a class="vis_by_param" href="#">Extend information</a></div> </div> </div></div>`);


    var v_domain = [2, 50, 300, 1500, 300000].map(d => parseInt(d / 16));
    var c_scale2 = ['#eef78d', '#ff9800', '#b2f705', '#4caf50', '#31f7f7', '#6716f2', '#FF1764', '#57266D'];
    var c_scale = ['#f2e16e', '#7fd32c', '#4d81f4', '#c91d67'];

    var c_scale_cat = d3.scaleThreshold()
        .domain(v_domain)
        .range([0, 1, 2, 3]);

    var c_scale_cat_km2 = d3.scaleThreshold()
        .domain([3, 18, 93, 3500])
        .range([0, 1, 2, 3]);


    console.log(data)
    var sorted_color_arr = c_scale.map(function (d, i) {

        var color = d3.rgb(c_scale[i]).formatHex();

        if (i == c_scale.length - 1) {
            var label = '> ' + v_domain[i]
        } else {
            var label = v_domain[i] + '-' + v_domain[i + 1];
        }

        switch (i) {
            case 0:
                label += ' Dispersed density';
                var s_label = 'Dispersed density';
                var colors_label = '<div class="gradient grad_0"></div>';
                break;
            case 1:
                label += ' Low density';
                var s_label = 'Low density';
                var colors_label = '<div class="gradient grad_1"></div>';

                break;
            case 2:
                label += ' Moderate density';
                var s_label = 'Moderate density';
                var colors_label = '<div class="gradient grad_2"></div>';
                break;
            case 3:
                label += ' High density';
                var s_label = 'High density';
                var colors_label = '<div class="gradient grad_3"></div>';
                break;
        }
        return {
            rank: v_domain[i],
            code: v_domain[i],
            label: label,
            simple_label: s_label,
            label_html: '<div class="row"><div class="col s6">' + colors_label + label + '</div>',
            rank_color: color
        }
    })
    console.log(sorted_color_arr)
    var sum_population = data.sum_total;

    var count_px = data.count_px;
    var data = data.ghs_data;

    var table_info = {
        table: {


            widths: ['*', '*', '*'],
            layout: {
                fillColor: function (rowIndex, node, columnIndex) {
                    return (rowIndex % 2 === 0) ? '#cec9c9' : null;
                }
            },
            body: [
                [{ text: 'Category', style: 'tableHeader' }, { text: 'Inhabitants', style: 'tableHeader' }, { text: 'Hab/Sqkm', style: 'tableHeader' }]
            ]
        },
        layout: 'lightHorizontalLines',
        "margin": [0, 0, 20, 5]
    };

    this_app.this_pdf.content.push({
        width: '*',
        text: 'Affected population',
        style: 'feature_name',
        alignment: 'center'


    })

    if (data.length == 0) {
        $('.analysis_container .ghs_africa_250m_bigger_2').append('<h5>No population here</h5>');
        this_app.this_pdf.content.push({
            width: '*',
            text: 'No population here',
            style: 'alert',
            alignment: 'center'
        })

    } else {
        var total_sum = 0;
        var sum_density = 0;
        var total_pop_px = 0;

        sorted_color_arr.forEach(function (d, i) {
            console.info(d)
            for (var p in data) {


                total_sum += data[p].sum;
                total_pop_px += data[p].num_px;


            }
        });

        var x = 0;
        var total_num_px = 0;
        sorted_color_arr.forEach(function (d, i) {

            for (var p in data) {
                if (data[p].px_class == i) {
                    var info = data[p];
                    total_sum += info.sum;
                    var density = (info.sum / (info.num_px * 16));
                    sum_density += (density);
                    total_num_px += info.num_px;

                    data[p].class_density = density;
                    x += (data[p].num_px * data[p]['class_density']);

                }

            }

            if (!info) {
                var info = {
                    num_px: 'None',
                    sum: '0'
                }
                var density = 0;
            }

            var t_html = d.label_html;
            t_html += '<div class="col s3">' + info.sum + '</div>'
            t_html += '<div class="col s3">' + parseFloat(density).toFixed(2) + '</div>'

            t_html += '</div></div>';

            table_info.table.body.push([d.label, numberWithCommas(info.sum), parseFloat(density).toFixed(2)]);
            $('.analysis_container .ghs_africa_250m_bigger_2 .rows_container').append(t_html)

            $('.analysis_container .ghs_africa_250m_bigger_2 .rows_container').find('.gradient').each(function (i, d) {
                console.log(i)
                $(this).css('background', function () {
                    if (i == 0) {
                        return 'linear-gradient(90deg, ' + c_scale2[0] + ' 0%, ' + c_scale2[1] + ' 100%)'
                    } else {
                        return 'linear-gradient(90deg, ' + c_scale2[i * 2] + ' 0%, ' + c_scale2[(i * 2) + 1] + ' 100%)'
                    }

                })
            });

        });






        this_app.this_pdf.content.push(table_info);
        var avg_pop_sqkm = numberWithCommas(parseFloat(x / total_num_px).toFixed(2));

        this_app.this_pdf.counts.push({
            feature: 'population',
            data: [
                {
                    "sum_population": numberWithCommas(sum_population)


                },
                { "avg_pop_sqkm": avg_pop_sqkm }

            ]
        });


        this_app.this_pdf.content.push({
            width: '*',
            text: [
                { "text": 'Average Hab/sqKm: ' }, { "text": avg_pop_sqkm, style: 'counts' }
            ]

        })
        console.warn(avg_pop_sqkm)
        this_app.this_pdf.content.push({
            width: '*',
            text: [{ text: 'Total population: ' }, { text: numberWithCommas(sum_population), style: 'counts' }]
        })

        var o = {
            layout: 'noBorders',
            layer: 'population',
            table: {
                layout: 'noBorders',
                body: [

                    [
                        { text: 'Affected population', style: 'filled_cell' }
                    ],
                    [
                        [{
                            "text": numberWithCommas(sum_population),
                            style: "counts"

                        },
                        {
                            "text": ' Habitants off-the-grid',
                            "alignment": "center"


                        },
                        { text: avg_pop_sqkm, style: "counts" }, { text: ' hab/km2', "alignment": "center" }
                        ]

                    ]
                ]
            }
        };
        console.info(o)








        var cat = c_scale_cat_km2(avg_pop_sqkm);

        $('.card-container.ghs_africa_250m_bigger_2 .ghs_counts_container').html('<div class="ghs_counts">' + avg_pop_sqkm + ' hab/Km<sup>2</sup></div> <div class="ghs_description">Population density for <span style="color:gold">non-electrified areas</span> <span class="color_span" style="background:' + sorted_color_arr[cat].rank_color + '">' + sorted_color_arr[cat].simple_label + '</div><div class="sum_pop">Total population ' + numberWithCommas(sum_population) + '</div>')

    }
}



this_app.update_table_analysis = function (data, layer_name, bbox) {
    console.info(data)




    switch (layer_name) {

        case 'schools':

            var _data = data.categories_data;
            console.info(_data)
            var layer_counts_txt = data.counts;
            var t_counts = data.counts;
            var name = 'Schools';
            var num_rows_params = 1;
            break;

        case 'hospitals':

            var _data = data.categories_data;
            console.info(_data)
            var layer_counts_txt = data.counts;
            var t_counts = data.out_grid_hc_counts;
            var name = 'Health facilities';
            var num_rows_params = 1;
            if (bbox == true) {

                var ids = data.ids;
                var p_filter = ids.reduce(function (memo, feature) {

                    memo.circle_opacity_expression.push(feature, 1)
                    return memo;

                }, {
                    circle_opacity_expression: ["match", ["get", "id"]]
                });
                p_filter.circle_opacity_expression.push(0.1);
                console.log(p_filter)
                this_app.sel_f_admin.setFilter_data[0].filter = function () {
                    return map.setPaintProperty('hospitals', "circle-opacity", p_filter.circle_opacity_expression);
                }
            }
            break;
        case 'power_plants':
            console.log(data)
            var t_counts = data.counts;
            if (bbox == true) {

                var ids = data.ids;
                var p_filter = ids.reduce(function (memo, feature) {

                    memo.circle_opacity_expression.push(feature, 1)
                    return memo;

                }, {
                    circle_opacity_expression: ["match", ["get", "id"]]
                });
                p_filter.circle_opacity_expression.push(0.1);
                this_app.sel_f_admin.setFilter_data[1].filter = function () {
                    return map.setPaintProperty('power_plants', "circle-opacity", p_filter.circle_opacity_expression);
                }
            }
            var _data = data.categories_data;
            var layer_counts_txt = data.counts;
            var name = 'Power plants';
            var num_rows_params = 1;
            break;
        case 'refugee_camps':
            console.log(data)
            var t_counts = data.counts;
            if (bbox == true) {

                var ids = data.ids;
                var p_filter = ids.reduce(function (memo, feature) {

                    memo.circle_opacity_expression.push(feature, 1)
                    return memo;

                }, {
                    circle_opacity_expression: ["match", ["get", "id"]]
                });
                p_filter.circle_opacity_expression.push(0.1);
                this_app.sel_f_admin.setFilter_data[2].filter = function () {
                    return map.setPaintProperty('refugee_settlements', "circle-opacity", p_filter.circle_opacity_expression);
                }
            }
            var _data = data.categories_data;
            var layer_counts_txt = data.counts;
            var name = 'Refugee settlements';
            var num_rows_params = 1;
            break;
        case 'electric_grid':

            var _data = data.categories_data;
            console.info(data, _data);


            var layer_counts_txt = data.total_length;
            var t_counts = data.total_length;
            var name = 'Electric grid';
            var num_rows_params = 1;
            if (bbox == true) {

                var ids = data.ids;
                var p_filter = ids.reduce(function (memo, feature) {

                    memo.exp.push(feature, 1)
                    return memo;

                }, {
                    exp: ["match", ["get", "id"]]
                });
                p_filter.exp.push(0.1);
                console.log(p_filter)
                this_app.sel_f_admin.setFilter_data[3].filter = function () {
                    return map.setPaintProperty('electric_grid', "line-opacity", p_filter.exp);
                }
            }
            break;
        default:
            break;
    }
    if (layer_name == 'hospitals')
        var h = '<div class="row first_row d-flex">';
    else
        var h = '';

    if (layer_name == 'refugee_camps') {
        h += '<div class="card-container col-md-3 ' + layer_name + '" _layer="ref_settlements"> <div class="analysis_card card ref_settlements" _layer="ref_settlements"> <div class="card-content white-text"> <div class="general_info"> <span class="card-title">' + name + '</span>';
    }

    else {
        if (layer_name !== 'taf_layers') {
            h += '<div class="card-container col-md-3 ' + layer_name + '" _layer="' + layer_name + '"> <div class="analysis_card card ' + layer_name + '" _layer="' + layer_name + '"> <div class="card-content white-text"> <div class="general_info"> <span class="card-title">' + name + '</span>';
        }
        else {
            h += '<div class="card-container col-md-3 taf_layers"> <div class="analysis_card card"> <div class="card-content white-text"> <div class="general_info"> <span class="card-title">CMP – Sector Support Studies</span>';
        }
    }

    if (layer_name == 'hospitals') {
        h += '<span class="layer_counts hc_off_grid">' + numberWithCommas(data.out_grid_hc_counts) + '</span><span class="layer_counts hc_in_grid">' + numberWithCommas(data.in_grid_hc_counts) + '</span>';
    } else {
        if (layer_name !== 'electric_grid') {
            if (layer_name !== 'taf_layers') {
                h += '<span class="layer_counts">' + numberWithCommas(layer_counts_txt) + '</span>';
            }
            else {

                var taf_info = data.taf_layers.filter((d) => {
                    return d.counts > 0
                })

                console.warn(taf_info);

                if (taf_info.length > 0) {
                    h += '<ul>';
                    taf_info.forEach(function (d, i) {

                        console.log(d)
                        var title = app_data.layers_info.filter((dd) => { return dd.id == d.layer_id })[0].title;
                        console.log(d)
                        h += '<li><div class="row"><div class="col s9"><span>' + title + '</span></div><div class="col s3"><span class="layer_counts">' + numberWithCommas(d.counts) + '</span></div></div></li>'

                    })
                    h += '</ul>';
                }
                else {
                    h += '<span class="layer_counts">No CMP studies in the area</span>'
                }
            }
        }
        else {
            h += '<span class="layer_counts">' + numberWithCommas(parseFloat(data.total_length).toFixed(1)) + '</span> Km';
        }


    }


    if (layer_name !== 'taf_layers') {

        h += `</div><div class="info" style="display:none">
        <div class="row d-flex">
             <div class="col-10">Click any property to extend information</div>
                <div class="col-2 text-end">
                    <i class="bi bi-x close_analysis_params"></i>
                </div>

        </div>
    <ul> </ul> </div > </div > <div class="row card-action"> <div class="col-10">`;
        if (t_counts > 0)
            h += '<a class="vis_by_param" href="#">Visualize per parameter</a>';


        if (t_counts > 0 && !bbox)
            h += `
        <div class="form-check form-switch d-flex align-items-center">
  <input class="form-check-input switch_input" type="checkbox" id="flexSwitchCheckDefault">
  <label class="form-check-label" for="flexSwitchCheckDefault">Show only point data into selected adm. Unit</label>
</div>

        `
        //'<div class="row switch only_inside_pol_data"> <div class="text">Show only point data into selected adm. Unit</div><label> Off <input type="checkbox"> <span class="lever"></span> On </label></div>';

        if (t_counts > 0)
            h += '</div><div class="col-2 text-end"><i class="bi bi-file-earmark-arrow-down-fill download_excel tooltiped" placement="bottom" title="Download as excell"></i><i class="bi bi-cloud-download-fill download_shp tooltiped" placement="bottom" title="Download as shapefile"></i></div></div> </div> ';

        h += '</div>';
    }
    else {
        h += '</div></div> </div></div>';

        console.warn(h)
    }

    if (layer_name == 'hospitals') {
        $('.row.analysis_container').append(h);
    } else {
        if (layer_name !== 'electric_grid' && layer_name !== 'taf_layers') {
            console.warn(layer_name)
            $('.row.analysis_container').find('.row.first_row').append(h);
        }

        else {

            $('.row.analysis_container').find('.row.second_row').append(h);
        }

    }

    if (layer_name == 'taf_layers')
        return false;

    if (data.counts == 0) {
        return false;
    }
    if (layer_name == 'refugee_camps')
        var layer_name = 'ref_settlements';

    var t_layer = app_data.layers_info.filter(function (d) {
        return d.id == layer_name;
    })[0];
    console.info(t_layer)
    var arr = [];
    var n_json = [];
    if (layer_name == 'power_plants') {
        var rows = [{
            columns: [

            ]
        }];
        _data.forEach(function (d, i) {

            if (arr.indexOf(d.param) == -1) {
                arr.push(d.param)
                n_json.push(d)
            }
        });
    } else {
        _data.forEach(function (d, i) {

            if (arr.indexOf(d.param) == -1) {
                arr.push(d.param)
                n_json.push(d)
            }
        });
    }
    if (layer_name == 'hospitals') {
        var summary_counts = [{
            "text": numberWithCommas(data.in_grid_hc_counts),
            style: "counts"

        },
        {
            text: 'inside grid',
            style: 'summary_counts'
        },
        {
            "text": numberWithCommas(data.out_grid_hc_counts),
            style: "counts"

        },
        {
            text: 'outside grid',
            style: 'summary_counts'
        }
        ]

        var summary_counts2 = {
            feature: layer_name,
            data: [{
                "in_grid_hc_counts": numberWithCommas(data.in_grid_hc_counts),
            },
            {
                "out_grid_hc_counts": numberWithCommas(data.out_grid_hc_counts)

            }
            ]

        }
    }
    if (layer_name == 'power_plants' || layer_name == 'ref_settlements' || layer_name == 'schools') {
        var summary_counts = [{
            "text": numberWithCommas(data.counts),
            style: "counts"

        }]

        var summary_counts2 = {
            feature: layer_name,
            data: { "counts": numberWithCommas(data.counts) }



        }
    }

    if (layer_name == 'electric_grid') {

        var summary_counts = [{
            "text": numberWithCommas(parseInt(data.total_length)) + ' Km',
            style: "counts"

        }]

        var summary_counts2 = {
            feature: layer_name,
            data: {
                "counts": numberWithCommas(parseInt(data.total_length))
            }


        }
    }
    console.log(n_json)
    console.info(summary_counts2)
    this_app.this_pdf.counts.push(summary_counts2);
    var t_column = {
        layout: 'noBorders',
        layer: layer_name,




        table: {
            body: [

                [
                    { text: name, style: 'filled_cell' }
                ],
                [
                    summary_counts
                ]
            ]
        }
    };
    console.info(t_column)

    if (layer_name == 'power_plants') {


        this_app.this_pdf.content.push({
            width: '*',
            text: name,
            style: 'feature_name',
            alignment: 'center'


        });
        this_app.this_pdf.content.push({
            "columns_layer_name": layer_name,

            "columns": [

            ]
        })
    } else {
        this_app.this_pdf.content.push({
            width: '*',
            text: name,
            style: 'feature_name',
            alignment: 'center',



        });

    }


    n_json.forEach(function (d, i) {
        d.total_sum = 0;
        d.total_avg = 0;
        d.total_features = 0;
        var param = d.param;

        var params = t_layer.params_description;
        console.log(params, param)
        var this_param_description = params.filter(function (d2) {
            return d2.param_name == param
        });

        console.warn(d)
        console.log(this_param_description)
        d.layer_name = layer_name;
        d.data.forEach(function (d2, i2) {
            n_json[i].total_sum += d2.sum;
            if (this_param_description.type_param !== 'categorical' || this_param_description.type_param !== 'categorical_index') {
                n_json[i]['data'][i2].avg = d2.sum / d2.counts;
                n_json[i].total_features += d2.counts;
                n_json[i]['data'][i2].all_values = 'emptied';


            }
        })

    })

    console.info(n_json)


    n_json.forEach(function (d, i) {
        var param = d.param;

        var params = t_layer.params_description;
        console.log(params, param)
        var this_param_description = params.filter(function (d2) {
            return d2.param_name == param
        });

        if (layer_name == 'schools') {
            console.log(this_param_description)
        }

        if (this_param_description.type_param !== 'categorical' || this_param_description.type_param !== 'categorical_index') {
            n_json[i].total_avg = (n_json[i].total_sum / n_json[i].total_features);

        }
    });




    var html = '';
    var hospitals_rows = [{

        "columns": [
            [],
            []

        ]

    },
    {

        "columns": [
            [],
            [],
            []
        ]

    },
    {

        "columns": [
            [],
            []

        ]

    }
    ];

    /*     if (layer_name == 'schools')
            debugger */

    //we have 3 rows for schools
    //for 7 parameters to plot
    var schools_rows = [{

        "columns": [
            [],
            []

        ]

    },
    {

        "columns": [
            [],
            [],
            []
        ]

    },
    {

        "columns": [
            [],
            []


        ]

    }
    ];
    var power_rows = [{

        "columns": [
            [],
            [],
            []

        ]

    }];

    var ref_camps_rows = [{

        "columns": [
            [],
            []

        ]

    }];

    var electric_grid_rows = [{

        "columns": [
            [],
            []

        ]

    }];
    console.warn(layer_name, t_layer, n_json)
    n_json.forEach(function (d, i) {

        var layer_name = d.layer_name;
        console.warn(t_layer, param)

        var index_param = i;
        var param_name = d.param;

        var t_symbol = t_layer.params_symbology.filter(function (info) {
            return info.param_name == d.param;
        })[0];


        var scale_val = t_symbol.legend;

        if (layer_name == 'schools')
            console.warn(scale_val)


        var params = t_layer.params_description;

        var params_description = params.filter(function (d2) {
            return d2.param_name == param_name
        })[0];

        if (params_description) {

            var table_info = {
                layout: 'lightHorizontalLines',
                "margin": [0, 0, 20, 5],
                table: {
                    widths: [50, 50, 30],
                }
            };

            if (params_description.type_param !== 'categorical' && params_description.type_param !== 'categorical_index') {
                console.warn('NON categorical for ' + layer_name);

                table_info.table.body = [
                    [{ text: 'Category', style: 'tableHeader' }, { text: 'Average', style: 'tableHeader' }, { text: 'Counts', style: 'tableHeader' }]

                ];


            } else {
                console.warn('YES categorical for ' + layer_name);
                if (layer_name == 'electric_grid') {

                    table_info.table.body = [
                        [{ text: 'Category', style: 'tableHeader' }, { text: 'Length', style: 'tableHeader' }]
                    ];

                } else {

                    table_info.table.body = [
                        [{ text: 'Category', style: 'tableHeader' }, { text: 'Counts', style: 'tableHeader' }]
                    ];
                }
            }
            console.info(params_description)

            var _domain = params._domain;
            console.info(d)

            this_app.analysis_data.push(d);
            var this_data = d.data;
            var l = this_data.length;

            console.info(this_data)

            var l = this_data.length;
            if (l == 0) {

            } else {


                var sorted = d.data.sort(function (a, b) {
                    return a.counts > b.counts ? -1 : 1
                });
                console.info(JSON.stringify(sorted));

                var t = sorted[0];
                html += '<li class="' + param_name + '"><div class="row li_info_container">'

                html += '<div class="col-9 analysis_param_title_container tooltiped" data-tooltip="Extend parameters" ><span class="title">' + params_description.short_title +
                    '</span>';
                console.warn(params_description, param_name)
                console.info(t)
                if (param_name == 'lcoeh_soft' || param_name == 'lcoe_usdc_kwh_10') {

                    console.log(t, t.avg)

                    html += '<span class="span_avg_sum">Average in most abundant category: ' + numberWithCommas(t.avg) + '</span>';
                    html += '<span class="span_avg_sum">Total average: ' + numberWithCommas(d.total_avg) + '</span>';
                    t_column.table.body[1][0].push({
                        "text": params_description.short_title,
                        margin: [0, 2, 0, 0],
                        fontSize: 8,
                        "font": "OpenSans",
                        OpenSansBolded: true,
                        color: '#c88319'
                    }, {
                        "text": 'Average: ' + t.avg.toFixed(2)
                    }, {
                        "text": '\t' + scale_val[t.cat].text + '\t',
                        style: "cat",
                        background: scale_val[t.cat].color,

                    });

                } else {
                    if (params_description.type_param !== 'categorical' &&
                        params_description.type_param !== 'categorical_index') {



                        html += '<span class="span_avg_sum">Sum in most abundant category: ' + numberWithCommas(t.sum) + '</span>';
                        html += '<span class="span_avg_sum">Total sum: ' + numberWithCommas(d.total_sum) + '</span>';

                        t_column.table.body[1][0].push({
                            "text": params_description.short_title,
                            margin: [0, 2, 0, 0],
                            fontSize: 8,
                            "font": "OpenSans",
                            OpenSansBolded: true,
                            color: '#c88319'
                        }, {

                            "text": 'Sum: ' + t.sum.toFixed(2)
                        }, {
                            "text": '\t' + scale_val[t.cat].text + '\t',
                            style: "cat",
                            background: scale_val[t.cat].color,

                        });

                    } else {
                        var diff_types = sorted.length;


                        html += '<div class="span_avg_sum"><span style="font-size:1.4rem">' + diff_types + '</span> different categories</div>';

                        console.log(params_description.short_title)

                        t_column.table.body[1][0].push({
                            "text": params_description.short_title,
                            margin: [0, 2, 0, 0],
                            fontSize: 8,
                            "font": "OpenSans",
                            OpenSansBolded: true,
                            color: '#c88319'
                        }, {
                            "text": [{ text: diff_types, fontSize: 10 }, { text: ' different categories' }]

                        });
                        if (layer_name == 'hospitals') {
                            var param = 'category_l';
                        }

                        if (layer_name == 'schools') {

                            var param = 'category';
                        }

                        if (layer_name == 'power_plants') {
                            var param = 'gen_type_name';
                        }

                        if (layer_name == 'electric_grid') {
                            var param = 'status';
                        }

                        console.log(sorted)

                        for (var p in sorted) {
                            var t = sorted[+p];
                            console.info(sorted[3])

                            if (layer_name == 'electric_grid') {
                                console.log(t)
                                console.log(scale_val[1])
                                console.log(scale_val[t.cat].val)

                            }
                            if (!scale_val[t.cat] || !scale_val[t.cat].val) {

                                console.warn(scale_val)
                                console.warn(t)
                                console.info(sorted, p, sorted[p])
                            }
                            t_column.table.body[1][0].push({
                                "text": scale_val[t.cat].val + ' (' + t.counts + ')',
                                margin: [10, 2, 0, 2]

                            })


                        }


                        console.info(t_column.table.body[1][0])

                    }
                }
                html += "</div>";
                if (params_description.type_param !== 'categorical' && params_description.type_param !== 'categorical_index') {
                    html += '<div class="col-3"><span class="color_span title_attr center" style="background:' + scale_val[t.cat].color + '">' +
                        scale_val[t.cat].text + '</span></div>';



                    for (var p in sorted) {
                        var t = sorted[p];
                        if (scale_val[t.cat]) {
                            if (layer_name == 'electric_grid') {
                                if (p == 0)
                                    table_info.table.body.push([{ text: scale_val[t.cat].text, style: 'bigger_value' }, { text: t.sum_length.toFixed(2), style: 'bigger_value' }]);
                                else
                                    table_info.table.body.push([scale_val[t.cat].text, t.sum_length.toFixed(2)]);

                            } else {
                                if (p == 0) {
                                    table_info.table.body.push([{ text: scale_val[t.cat].text, style: 'bigger_value' },
                                    { text: t.avg.toFixed(2), style: 'bigger_value' },
                                    { text: t.counts, style: 'bigger_value' }
                                    ]);
                                } else {
                                    table_info.table.body.push([scale_val[t.cat].text, t.avg.toFixed(2), t.counts])
                                }


                            }

                        } else {
                            console.warn(t)

                        }

                    }
                } else {

                    if (layer_name == 'hospitals') {
                        if (param_name == 'category_l')
                            var param = 'category_l';
                        else
                            var param = param_name;

                    } else {

                        if (layer_name == 'schools') {
                            if (param_name == 'category')
                                var param = 'category';
                            else
                                var param = param_name;

                            //var param = 'category';

                        }

                        if (layer_name == 'power_plants')
                            var param = 'gen_type_name';

                        if (layer_name == 'refugee_camps')
                            var param = 'gen_type_name';

                        if (layer_name == 'electrid_grid') {
                            var param = 'status';
                        }



                    }
                    /*   if (layer_name == 'schools' || layer_name == 'schools')
                          debugger */
                    console.log(sorted)

                    console.log(t, param, t[param]) //ex: { cat: 1, category: "secondary", counts: 239, avg: NaN, all_values: "emptied" } category secondary

                    for (var p in sorted) {
                        var t = sorted[p];


                        if (layer_name == 'electric_grid') {


                            var v = scale_val[t.cat].val;

                            table_info.table.body.push([v, t.sum_length]);

                        } else {


                            if (t.cat.toString()) {
                                //table_info.table.body.push([t[param], t.counts]);
                                table_info.table.body.push([param, t.counts]);
                            }
                            else {
                                //  table_info.table.body.push([t[param], t.counts]);
                                table_info.table.body.push([param, t.counts]);
                            }
                        }

                    }

                    if (layer_name == 'hospitals' || layer_name == 'schools')
                        console.warn(table_info.table.body)

                }





                if (layer_name == 'power_plants' || layer_name == 'electric_grid') {


                    if (layer_name == 'electric_grid') {
                        var rows = electric_grid_rows;
                    }

                    if (layer_name == 'schools') {
                        var rows = schools_rows;
                        console.log(rows)
                    }

                    if (layer_name == 'power_plants') {
                        var rows = power_rows;
                    }

                    var first_col = rows[0].columns;

                    first_col[index_param].push({
                        text: params_description.short_title,
                        alignment: 'left',
                        style: "short_title"
                    })

                    first_col[index_param].push(table_info);
                    first_col[index_param].push({
                        text: params_description.long_title_txt,
                        alignment: 'left',
                        style: "long_title"
                    });
                    if (index_param == 1) {

                        for (var p in this_app.this_pdf.counts) {
                            var o = this_app.this_pdf.counts[p];
                            if (layer_name == 'power_plants' && o.feature == layer_name) {
                                this_app.this_pdf.content.push({ text: [{ text: o.data.counts, style: 'counts' }, { text: ' Power plants' }] })
                            }

                            if (layer_name == 'schools' && o.feature == layer_name) {
                                this_app.this_pdf.content.push({ text: [{ text: o.data.counts, style: 'counts' }, { text: ' Schools' }] })
                            }

                            if (layer_name == 'electric_grid' && o.feature == layer_name) {
                                this_app.this_pdf.content.push({ text: [{ text: o.data.counts, style: 'counts' }, { text: ' Km' }] });
                            }
                        }

                        this_app.this_pdf.content.push(rows[0]);
                    }
                    console.info(first_col)
                    console.log(this_app.this_pdf.content)



                } else {

                    console.log(layer_name, index_param)

                    if (layer_name == 'schools')
                        var rows = schools_rows;
                    else
                        var rows = hospitals_rows;




                    console.log(rows)
                    console.log(index_param, layer_name)
                    console.log(this_app.this_pdf)
                    var first_col = rows[0].columns;
                    console.warn(first_col)
                    var sec_col = rows[1].columns;

                    var third_col = rows[2].columns;
                    if (index_param == 1) {

                        for (var p in this_app.this_pdf.counts) {
                            var o = this_app.this_pdf.counts[p];
                            if (o.feature == 'hospitals' && o.feature == layer_name) {
                                this_app.this_pdf.content.push({ text: [{ text: o.data[0].in_grid_hc_counts, style: 'counts' }, { text: ' in grid ' }, { text: o.data[1].out_grid_hc_counts, style: 'counts', "margin": [30, 0, 0, 0] }, { text: ' outside grid' }] });

                            }

                            if (o.feature == 'ref_settlements' && o.feature == layer_name) {
                                this_app.this_pdf.content.push({ text: [{ text: o.data.counts, style: 'counts' }, { text: ' refugee settlements' }] });

                            }

                            if (o.feature == 'schools' && o.feature == layer_name) {
                                this_app.this_pdf.content.push({ text: [{ text: o.data.counts, style: 'counts' }, { text: ' schools' }] });

                            }


                        }
                    }

                    if (index_param < 2) {

                        first_col[index_param].push({
                            text: params_description.short_title,
                            alignment: 'left',
                            style: "short_title"
                        })
                        first_col[index_param].push(table_info);
                        first_col[index_param].push({
                            text: params_description.long_title_txt,
                            alignment: 'left',
                            style: "long_title"
                        });


                        if (index_param == 1) {

                            this_app.this_pdf.content.push(rows[0]);

                        }

                    } else {

                        if (index_param < 5) {
                            console.log('pushing second column')
                            sec_col[index_param - 2].push({
                                text: params_description.short_title,
                                alignment: 'left',
                                style: "short_title"
                            });
                            sec_col[index_param - 2].push(table_info);

                            sec_col[index_param - 2].push({
                                text: params_description.long_title_txt,
                                alignment: 'left',
                                style: "long_title"
                            });
                            console.log(d.param.length, index_param)
                            if (index_param == 4) {

                                this_app.this_pdf.content.push(rows[1]);


                            }
                        }

                        if (index_param >= 5) {


                            console.log('pushing third column', layer_name, index_param)
                            third_col[index_param - 5].push({
                                text: params_description.short_title,
                                alignment: 'left',
                                style: "short_title"
                            });
                            third_col[index_param - 5].push(table_info);

                            third_col[index_param - 5].push({
                                text: params_description.long_title_txt,
                                alignment: 'left',
                                style: "long_title"
                            });
                            console.log(d.param.length, index_param)
                            this_app.this_pdf.content.push(rows[2]);



                        }

                    }
                    console.log(rows)


                }
                if (layer_name !== 'schools' && (params_description.type_param !== 'categorical' && params_description.type_param !== 'categorical_index')) {
                    html += '</div>';

                    html += '<div style="display:none" class="' + param_name + ' row"><div class="row"><span class="col-4"></span></div><div class="col-8"><div class="d3_legend">' +
                        '<svg class="circles_svg ' + param_name + '"></svg></div></div><div class="col-4 counts_col"><ul></ul></div></div></li>';
                } else {

                    if (params_description.type_param !== 'categorical' && params_description.type_param !== 'categorical_index') {
                        html += '</div>';

                        html += '<div style="display:none" class="' + param_name + ' row"><div class="row"><span class="col-4"></span></div><div class="col-8"><div class="d3_legend">' +
                            '<svg class="circles_svg ' + param_name + '"></svg></div></div><div class="col-4 counts_col"><ul></ul></div></div></li>';
                    } else {
                        html += '</div>';

                        html += '<div style="display:none" class="' + param_name + ' row"><div class="row"><span class="col-4 to_right"></span></div><div class="col-12 counts_col"><ul class="collection categorical ' + param_name + '">';


                        for (var p in sorted) {
                            var t = sorted[p];


                            html += '<li class="collection-item"><span class="name">' + scale_val[t.cat].val + '</span><span class="badge" style="margin-right: 10px;color:black;background-color:' + scale_val[t.cat].color + '!important">' + numberWithCommas(t.counts) + '</span></li>';

                        }
                        html += '</ul></div></div></li>';




                    }
                }

            }
        }



    });
    console.warn(layer_name)
    console.log(t_column)
    console.info(this_app.this_pdf.extra)
    console.info(this_app.this_pdf.extra[0].columns)
    $('.analysis_container .' + layer_name).find('.info ul').append(html);



}