// labeler.js
(function () {
  d3.labeler = function () {
    var lab = [],
      anc = [],
      w = 1, // box width
      h = 1, // box width
      labeler = {};

    var max_move = 5.0,
      max_angle = 0.5,
      acc = 0;
    rej = 0;

    // weights
    var w_len = 0.2, // leader line length 
      w_inter = 1.0, // leader line intersection
      w_lab2 = 30.0, // label-label overlap
      w_lab_anc = 30.0; // label-anchor overlap
    w_orient = 3.0; // orientation bias

    // booleans for user defined functions
    var user_energy = false,
      user_schedule = false;

    var user_defined_energy,
      user_defined_schedule;

    energy = function (index) {
      // energy function, tailored for label placement

      var m = lab.length,
        ener = 0,
        dx = lab[index].x - anc[index].x,
        dy = anc[index].y - lab[index].y,
        dist = Math.sqrt(dx * dx + dy * dy),
        overlap = true,
        amount = 0
      theta = 0;

      // penalty for length of leader line
      if (dist > 0) ener += dist * w_len;

      // label orientation bias
      dx /= dist;
      dy /= dist;
      if (dx > 0 && dy > 0) {
        ener += 0 * w_orient;
      } else if (dx < 0 && dy > 0) {
        ener += 1 * w_orient;
      } else if (dx < 0 && dy < 0) {
        ener += 2 * w_orient;
      } else {
        ener += 3 * w_orient;
      }

      var x21 = lab[index].x,
        y21 = lab[index].y - lab[index].height + 2.0,
        x22 = lab[index].x + lab[index].width,
        y22 = lab[index].y + 2.0;
      var x11, x12, y11, y12, x_overlap, y_overlap, overlap_area;

      for (var i = 0; i < m; i++) {
        if (i != index) {
          // penalty for intersection of leader lines
          overlap = intersect(anc[index].x, lab[index].x, anc[i].x, lab[i].x,
            anc[index].y, lab[index].y, anc[i].y, lab[i].y);
          if (overlap) ener += w_inter;

          // penalty for label-label overlap
          x11 = lab[i].x;
          y11 = lab[i].y - lab[i].height + 2.0;
          x12 = lab[i].x + lab[i].width;
          y12 = lab[i].y + 2.0;
          x_overlap = Math.max(0, Math.min(x12, x22) - Math.max(x11, x21));
          y_overlap = Math.max(0, Math.min(y12, y22) - Math.max(y11, y21));
          overlap_area = x_overlap * y_overlap;
          ener += (overlap_area * w_lab2);
        }

        // penalty for label-anchor overlap
        x11 = anc[i].x - anc[i].r;
        y11 = anc[i].y - anc[i].r;
        x12 = anc[i].x + anc[i].r;
        y12 = anc[i].y + anc[i].r;
        x_overlap = Math.max(0, Math.min(x12, x22) - Math.max(x11, x21));
        y_overlap = Math.max(0, Math.min(y12, y22) - Math.max(y11, y21));
        overlap_area = x_overlap * y_overlap;
        ener += (overlap_area * w_lab_anc);
      }
      return ener;
    };

    mcmove = function (currT) {
      // Monte Carlo translation move

      // select a random label
      var i = Math.floor(Math.random() * lab.length);

      // save old coordinates
      var x_old = lab[i].x;
      var y_old = lab[i].y;

      // old energy
      var old_energy;
      if (user_energy) {
        old_energy = user_defined_energy(i, lab, anc)
      } else {
        old_energy = energy(i)
      }

      // random translation
      lab[i].x += (Math.random() - 0.5) * max_move;
      lab[i].y += (Math.random() - 0.5) * max_move;

      // hard wall boundaries
      if (lab[i].x > w) lab[i].x = x_old;
      if (lab[i].x < 0) lab[i].x = x_old;
      if (lab[i].y > h) lab[i].y = y_old;
      if (lab[i].y < 0) lab[i].y = y_old;

      // new energy
      var new_energy;
      if (user_energy) {
        new_energy = user_defined_energy(i, lab, anc)
      } else {
        new_energy = energy(i)
      }

      // delta E
      var delta_energy = new_energy - old_energy;

      if (Math.random() < Math.exp(-delta_energy / currT)) {
        acc += 1;
      } else {
        // move back to old coordinates
        lab[i].x = x_old;
        lab[i].y = y_old;
        rej += 1;
      }
    };

    mcrotate = function (currT) {
      // Monte Carlo rotation move

      // select a random label
      var i = Math.floor(Math.random() * lab.length);

      // save old coordinates
      var x_old = lab[i].x;
      var y_old = lab[i].y;

      // old energy
      var old_energy;
      if (user_energy) {
        old_energy = user_defined_energy(i, lab, anc)
      } else {
        old_energy = energy(i)
      }

      // random angle
      var angle = (Math.random() - 0.5) * max_angle;

      var s = Math.sin(angle);
      var c = Math.cos(angle);

      // translate label (relative to anchor at origin):
      lab[i].x -= anc[i].x
      lab[i].y -= anc[i].y

      // rotate label
      var x_new = lab[i].x * c - lab[i].y * s,
        y_new = lab[i].x * s + lab[i].y * c;

      // translate label back
      lab[i].x = x_new + anc[i].x
      lab[i].y = y_new + anc[i].y

      // hard wall boundaries
      if (lab[i].x > w) lab[i].x = x_old;
      if (lab[i].x < 0) lab[i].x = x_old;
      if (lab[i].y > h) lab[i].y = y_old;
      if (lab[i].y < 0) lab[i].y = y_old;

      // new energy
      var new_energy;
      if (user_energy) {
        new_energy = user_defined_energy(i, lab, anc)
      } else {
        new_energy = energy(i)
      }

      // delta E
      var delta_energy = new_energy - old_energy;

      if (Math.random() < Math.exp(-delta_energy / currT)) {
        acc += 1;
      } else {
        // move back to old coordinates
        lab[i].x = x_old;
        lab[i].y = y_old;
        rej += 1;
      }
    };

    intersect = function (x1, x2, x3, x4, y1, y2, y3, y4) {
      // returns true if two lines intersect, else false
      // from http://paulbourke.net/geometry/lineline2d/

      var mua, mub;
      var denom, numera, numerb;

      denom = (y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1);
      numera = (x4 - x3) * (y1 - y3) - (y4 - y3) * (x1 - x3);
      numerb = (x2 - x1) * (y1 - y3) - (y2 - y1) * (x1 - x3);

      /* Is the intersection along the the segments */
      mua = numera / denom;
      mub = numerb / denom;
      if (!(mua < 0 || mua > 1 || mub < 0 || mub > 1)) {
        return true;
      }
      return false;
    }

    cooling_schedule = function (currT, initialT, nsweeps) {
      // linear cooling
      return (currT - (initialT / nsweeps));
    }

    labeler.start = function (nsweeps) {
      // main simulated annealing function
      var m = lab.length,
        currT = 1.0,
        initialT = 1.0;

      for (var i = 0; i < nsweeps; i++) {
        for (var j = 0; j < m; j++) {
          if (Math.random() < 0.5) {
            mcmove(currT);
          } else {
            mcrotate(currT);
          }
        }
        currT = cooling_schedule(currT, initialT, nsweeps);
      }
    };

    labeler.width = function (x) {
      // users insert graph width
      if (!arguments.length) return w;
      w = x;
      return labeler;
    };

    labeler.height = function (x) {
      // users insert graph height
      if (!arguments.length) return h;
      h = x;
      return labeler;
    };

    labeler.label = function (x) {
      // users insert label positions
      if (!arguments.length) return lab;
      lab = x;
      return labeler;
    };

    labeler.anchor = function (x) {
      // users insert anchor positions
      if (!arguments.length) return anc;
      anc = x;
      return labeler;
    };

    labeler.alt_energy = function (x) {
      // user defined energy
      if (!arguments.length) return energy;
      user_defined_energy = x;
      user_energy = true;
      return labeler;
    };

    labeler.alt_schedule = function (x) {
      // user defined cooling_schedule
      if (!arguments.length) return cooling_schedule;
      user_defined_schedule = x;
      user_schedule = true;
      return labeler;
    };

    return labeler;
  };
})();

// Global variables
var anchor_array = [],
  label_array = [];
var bubble_data = {
  plotted_info: {},
  filtered_label: null,
  chart_options: {
    visible_labels: true,
    circle_border: 0,
    opacity: 0.8,
    labels: true,
    gridLines: false

  }
}

function create_guides(element, r, fixed, copy) {
  d3.selectAll('.guide.temp').remove();
  var x = +element.attr("cx"),
    y = +element.attr("cy"),
    color = element.style("fill");
  //vertical line
  bubble_data.main_g
    .append("line")
    .attr("class", function () {
      if (fixed)
        return 'guide fixed'
      else
        return 'guide temp'
    })
    .attr("x1", x)
    .attr("x2", x)
    .attr("y1", y)
    // .attr("y2", height + 20)
    .attr("y2", bubble_data.height + 10)
    .style("stroke", color)
    .style("stroke-width", '2px')
    .style("opacity", 0)
    .transition().duration(200)
    .style("opacity", 1);

  bubble_data.main_g
    .append("text")
    .attr("class", function () {
      if (fixed)
        return 'guide fixed';
      else
        return 'guide temp';
    })
    .attr("x", x - 50)
    .attr("y", y)
    .style("fill", color)
    .style("opacity", 0)
    .style("text-anchor", "end")
    .text(function () {
      if (r['y_val'] == 0.00000001) {
        return 'No data'
      } else {
        return numberWithCommas(format_num(r.y_val));
        //return format_num(r['y_val'])
      }
    })
    .transition().duration(200)
    .style("opacity", 1);
  // .attr("transform",
  //     "translate(" + (-bubble_data.margin.left) + "," + (-bubble_data.margin.top) + ")");

  /*
    params
    code: "BDI"
color_title: "Performance"
r: "co2_tonnes_minigrid"
r_title: "GHG emissions avoided per year (ktons CO2eq)"
x: "lcoe_average"
x_title: "LCOE (EUR/kWh)"
y: "pop_noelect_minigrid"

*/
  //Value on the X axis
  bubble_data.main_g
    .append("text")
    .attr("class", function () {
      if (fixed)
        return 'guide fixed'
      else
        return 'guide temp'
    })
    .attr("x", x + 35)
    .attr("y", y + 38)
    .style("fill", color)
    .style("opacity", 0)
    .style("text-anchor", "end")
    .text(function () {
      if (r['x_val'] == 0.00000001)
        return 'No data'
      else
        //return format_num(r['x_val'])
        return numberWithCommas(format_num(r.x_val));
    })
    //.text( "$ " + d3.format(".2s")(d.GDP_perCapita) )
    .transition().duration(200)
    .style("opacity", 1);

  //horizontal line
  bubble_data.main_g
    .append("line")
    .attr("class", function () {
      if (fixed)
        return 'guide fixed'
      else
        return 'guide temp'
    })
    .attr("x1", x)
    //.attr("x2", -20)
    .attr("x2", 0)
    .attr("y1", y)
    .attr("y2", y)
    .style("stroke", color)
    .style("opacity", 0)
    .transition().duration(200)
    .style("opacity", 0.5);
  //  copy.call(transition_circle, bubble_data.minRadius, bubble_data.maxRadius, true, copy);
}

function clone(selector) {
  var node = d3.select(selector).node();
  return d3.select(node.parentNode.insertBefore(node.cloneNode(true),
    node.nextSibling));
}

//currently missing
// y: 'pop_noelect_minigrid',
//co2_tonnes_minigrid

// x: 'lcoe_average',

// r: 'co2_tonnes_minigrid',
// x_title: 'LCOE (EUR/kWh)',
// y_title: 'Market size: decentralized technologies',
var facts_list = [
  // {
  //     info_container: 'status',
  //     code: 'er_2019',
  //     long_txt: 'Electrification rate'
  // },
  // {
  //     info_container: 'status',
  //     code: 'er_2019',
  //     long_txt: 'Electrification rate'
  // },
  // {
  //     info_container: 'status',
  //     code: 'er_2019',
  //     long_txt: 'Electrification rate'
  // },
  // {
  //     info_container: 'status',
  //     code: 'er_2019',
  //     long_txt: 'Electrification rate'
  // },
  {
    info_container: 'status',
    code: 'er_2019',
    long_txt: 'Electrification rate'
  },
  {
    info_container: 'status',
    code: 'rer_2019',
    long_txt: 'Rural electrification rate'
  },
  {
    info_container: 'status',
    code: 're_2014',
    long_txt: 'Renewable energy share in the total final energy consumption'
  },
  {
    info_container: 'impacts',
    code: 'pop_noelect_minigrid',
    long_txt: 'Market size: number of new customers'
  },
  {
    info_container: 'impacts',
    code: 'pop_minigrid',
    long_txt: 'PV mini-grid market size)'
  },
  {
    info_container: 'impacts',
    code: 'pop_shs',
    long_txt: 'Market size by SHS or nano-solar'
  },
  {
    info_container: 'financial',
    code: 'npv_sum',
    long_txt: 'Total investment (EUR) 20 years lifetime'
  },
  {
    info_container: 'financial',
    code: 'lcoe_average',
    long_txt: 'Average Levelized cost of electricity per country [EUR/kWh]'
  },
  {
    info_container: 'financial',
    code: 'npv_cap',
    long_txt: 'cost/capita [EUR/person]'
  }
];


function on_bubble_btn() {
  if ($('body').width() < 1900) {
    var mw = "90%";
  } else {
    var mw = "80%";
  }
  $("#country_modal").animate({
    top: "2%",
    width: mw,
    height: "90vh",
    //height: "toggle"
  }, 1000, function () {
    // Animation complete.

    $('#bubble_selects_div').show();
    $('#bubble_selects_div select').empty();

    var bubble_sel_options = '';
    for (var p in facts_list) {
      var o = facts_list[p];
      bubble_sel_options += '<option value="' + o.code + '">' + o.long_txt + '</option>';
    }
    console.warn(bubble_sel_options);
    $('#x_bubble_select').append(bubble_sel_options);
    $('#y_bubble_select').append(bubble_sel_options);

    $('#bubble_selects_div select').formSelect();

    $('#bubble_selects_div select').bind('change', function (e) {
      var params = bubble_data.plotted_info.params;

      var sel = $(this).find('option:selected');
      if ($(this).attr('id') == 'x_bubble_select') {
        params.x = sel.attr('value');
        params['x_title'] = sel[0].label;
        $('.collapsible.x_axis_c .collapsible-header').find('span').text(sel[0].label);
      } else {
        params.y = sel.attr('value');
        params['y_title'] = sel[0].label;

        $('.collapsible.y_axis_c .collapsible-header').find('span').text(sel[0].label);
      }

      bubble_data.plotted_info.params = params;

      bubble_chart(params, false);
    });
    $('.fixed_bubble_popup').on('click', function (e) {
      if ($(e.target).hasClass('show_more')) {
        if ($(e.target).text() == 'Show more') {
          $(e.target).text('Show less');
          $('.more_info_container').show();
        } else {
          $(e.target).text('Show more');
          $('.more_info_container').hide();
        }
      }
    })

    $('.bubble_download').on('click', function () {
      var a = document.createElement('a');
      a.href = 'https://africa-knowledge-platform.ec.europa.eu/energy_json/indicators/downloads/simple_results.xlsx';
      // a.href = './downloads/simple_results.xlsx';
      //a.download = "copernicus download";
      document.body.appendChild(a);

      a.click();
      document.body.removeChild(a);
    })
    $('.bubble_border_div input').on('click', function () {
      if ($('.bubble_border_div input:checked').length == 0) {
        d3.selectAll('.bubbles').style('stroke-width', 0);
      } else {
        d3.selectAll('.bubbles').style('stroke-width', 2);
      }
    })
    $('.bubble_line_div input').on('click', function () {
      if ($('.bubble_line_div input:checked').length == 0) {
        d3.selectAll('.tick line').style('opacity', 0);
        bubble_data.chart_options.gridLines = false;
      } else {
        d3.selectAll('.tick line').style('opacity', 0.6);
        bubble_data.chart_options.gridLines = true;
      }
    })

    $('.bubble_labels_div input').on('click', function () {
      if ($('.bubble_labels_div input:checked').length == 0) {
        bubble_data.chart_options.labels = false;
        d3.selectAll('.labeler,.link').style('opacity', 0);
      } else {
        bubble_data.chart_options.labels = true;
        d3.selectAll('.labeler,.link').style('opacity', .6);
      }
    })

    $('.chart_options .title').on('click', function () {
      if (!$(this).hasClass('on')) {
        $(this).find('i').removeClass('fa-angle-down').addClass('fa-angle-up');
        $('.chart_options .all_options_div').show()
      } else {
        $(this).find('i').removeClass('fa-angle-up').addClass('fa-angle-down');
        $('.chart_options .all_options_div').hide();
      }
      $(this).toggleClass('on')
    })

    /*
    x--> lcoe (electricity per country)  
    y--> market size
    tamany cercle--> avoided Co2 emission. min and max
    color cercle --> main index (1-5) --> ordinal scale
      [{"country":"Afghanistan","continent":"Asia","lifeExp":","pop":"31889923","gdpPercap":"974.5803384"},
    {
    "code":"AGO",
    "rank_index":33,
    "country":"Angola",
    "data_class":{
    "main_index_class":1,

    attr("cx", function(d) {
                return x(d.gdpPercap); // lcoe_average  LCOE (EUR/kWh) 
            })
            .attr("cy", function(d) {
                return y(d.lifeExp); //pop_noelect_minigrid Market size: decentralized technologies ('000 people)
            })
            .attr("r", function(d) {
                return z(d.pop); //co2_tonnes_minigrid GHG emissions avoided per year (ktons CO2eq)

                 lcoe_average: {value: facts.lcoe_average,title:'LCOE (EUR/kWh)'},
            pop_noelect_minigrid: {value:facts.pop_noelect_minigrid,title:'Market size: decentralized technologies (people)'},
            co2_tonnes_minigrid: {value:facts.co2_tonnes_minigrid,title:''
        */

    var initial_params = {
      x: 'lcoe_average',
      //y: 'pop_noelect_minigrid',
      y: 'pop_minigrid',
      r: 'co2_tonnes_minigrid',
      x_title: 'LCOE (EUR/kWh)',
      //  y_title: 'Market size: decentralized technologies',
      y_title: 'PV mini-grid market size',
      // (\'000 people)
      code: app_state.country_facts_sel.code
    }
    $.when(bubble_chart(initial_params, true)).done(function () {
      var params = initial_params;

      $('.bubble_opacity input[type="number"]').niceNumber({
        // auto resize the number input
        autoSize: true,

        // the number of extra character
        autoSizeBuffer: 1,

        // custom button text
        buttonDecrement: '-',
        buttonIncrement: "+",

        // 'around', 'left', or 'right'
        buttonPosition: 'around',
        /**
            callbackFunction
            @param {$input} currentInput - the input running the callback
            @param {number} amount - the amount after increase/decrease
            @param {object} settings - the passed niceNumber settings
        **/
        onDecrement: function ($currentInput, amount, settings) {
          d3.selectAll('.bubbles').style('opacity', amount)
        },
        onIncrement: function ($currentInput, amount, settings) {
          //amount "0.7"
          d3.selectAll('.bubbles').style('opacity', amount);
        }
      });
      $('.collapsible.x_axis_c .collapsible-header').find('span').text(initial_params['x_title']);
      $('.collapsible.y_axis_c .collapsible-header').find('span').text(initial_params['y_title']);

      var r = bubble_data.reduced.countries_data.filter(function (info) {
        return info.code == params.code
      })[0];

      var f = app_data.all_data.filter(function (info) {
        return info.code == params.code
      })[0];

      $('.fixed_bubble_popup').css('max-height', $('#bubble_chart').height() + 'px');

      bubble_info(f, r, params, true);

      var _this = '.bubbles[code=' + r.code + ']';

      d3.selectAll('.temporary_transition').remove();
      var element = d3.select(_this);

      var copy = clone(_this);

      create_guides(element, r, true, copy);

      if ($('#circle-legend').find('span').length == 0) {
        var c = $('#circle-legend');
        var h = '';
        app.scale_values.forEach(function (d) {
          h += '<span class="circle_span" style="background:' + d.color + ';"></span><span class="circle_title">' + d.title + '</span>';
        });
        c.append(h)
      }
      var width = $('.circle_legend_container').width();
      // Add legend: circles
      //{x:'lcoe_average' , y:'pop_noelect_minigrid' ,r:'co2_tonnes_minigrid' }
      var valuesToShow = [+bubble_data.min_r, +(bubble_data.max_r / 2).toFixed(2), +bubble_data.max_r.toFixed(2)];
      var max_r = bubble_data.z(bubble_data.max_r);
      var max_circle = max_r + 20;
      var xLabel = width - 150;
      $('.circle_legend_container svg').height((max_r * 3));

      var circle_legend_container = d3.select('.circle_legend_container svg g');
      circle_legend_container
        .attr("transform",
          "translate(0,10)")

      console.warn(circle_legend_container);
      circle_legend_container
        .selectAll("legend")
        .data(valuesToShow)
        .enter()
        .append("circle")
        .attr("cx", max_circle)
        .attr("cy", function (d, i) {
          //being 30 the max radius(?)
          return bubble_data.z(d);
        })
        .attr("r", function (d) {
          return bubble_data.z(d);
        })
        .style("fill", "none")

      // Add legend: segments
      circle_legend_container
        .selectAll("legend")
        .data(valuesToShow)
        .enter()
        .append("line")
        .attr('x1', function (d) {
          return max_circle + bubble_data.z(d);
        })
        .attr('x2', function (d) {
          return max_circle + bubble_data.z(d) + 45
        })
        .attr('y1', function (d, i) {
          if (i == valuesToShow.length - 1) {
            return bubble_data.z(d) + 10
          } else {
            return bubble_data.z(d)
          }
          //return bubble_data.z(d)
        })
        .attr('y2', function (d, i) {
          if (i == valuesToShow.length - 1) {
            return bubble_data.z(d) + 10
          } else {
            return bubble_data.z(d)
          }
        })
        .style('stroke-dasharray', ('2,2'))

      $('.circle_legend .title').text('GHG emissions avoided per year (ktons CO2eq)');

      circle_legend_container
        .selectAll("legend")
        .data(valuesToShow)
        .enter()
        .append("text")
        .attr('x', function (d) {
          return max_circle + bubble_data.z(d) + 45
        })
        .attr('y', function (d, i) {
          if (i == valuesToShow.length - 1) {
            return bubble_data.z(d) + 10
          } else {
            return bubble_data.z(d)
          }
        })
        .text(function (d) {
          return d
          //+ ' avoided Co2 emission';
        })
        .style("font-size", '1rem')
        .style("fill", "#525252")
        .attr('alignment-baseline', 'middle');

      /*
                  bubble_data.reduced
          
                  main_index_class:
          arr: (5) [1, 3, 5, 4, 2]
          data: Array(5)
          0:
          countries: Array(8)
          0: {name: "Angola", code: "AGO"}
          1: {name: "Central African Republic", code: "CAF"}
          2: {name: "Democratic Republic of the Congo", code: "COD"}
          3: {name: "Congo", code: "COG"}
          4: {name: "Ghana", code: "GHA"}
          5: {name: "Madagascar", code: "MDG"}
          6: {name: "Sierra Leone", code: "SLE"}
          */
      var html = '<ul class="collapsible">'
      app.scale_values.forEach(function (d) {
        var pos = bubble_data.reduced.main_index_class.arr.indexOf(d.value);
        if (pos > -1) var els = bubble_data.reduced.main_index_class.data[pos];

        var sorted_countries = els.countries.sort(function (a, b) {
          return a.index_val - b.index_val;
        })

        html += '<li><div class="collapsible-header" style="background-color:' + d.color + ';">' + d.title +
          '<span class="new badge" data-badge-caption="' + els.counts + '"></span></div><div class="collapsible-body">';
        sorted_countries.forEach(function (info) {
          //   index_val:d.data_val.main_index_val
          html += '<div class="row bubble_popup_row"><div class="col s9">' + info.name + ' </div><div class="col s3">' + info.index_val + '</div></div>'
        })

        html += '</div></li>';
      })
      html += '</ul>'

      $('#bubble_color_legend').append(html);
      $('#bubble_color_legend .collapsible').collapsible();
    })
  }) //end  animation

  $('.btn_bubble').hide();
}


function transition_circle(element, start, end, temporary) {
  //  return false

  if (temporary == true) {
    var color = '#ef940d';
    element.attr('class', 'temporary_transition');
  } else {
    element.attr('class', 'fixed_transition');
    var color = '#10adeb';
  }

  element
    .attr('r', 8)
    .style('opacity', 0.9)
    .attr('fill', d => color)
    .attr('stroke', d => 'white')
    .transition()
    .delay(80)
    .duration(800)
    .attr('r', function (d) {
      return 35
    })
    .style('opacity', 0.5)
    .on("end", function (d) {
      //  transition_circle(element, end, start)
      //d3.select(this).call(transition, end, start);
      //  element.call($(this), end, start); 
    });
}

function format_num(num) {
  if (!num) {
    return 'No data'
  } else {
    if (typeof num === 'string') {
      if (num == '0.00000001') {
        return 'No data'
      } else {
        var num = parseFloat(num); //.toFixed(2);
      }
    }
    if (num < 1) {
      return (num).toFixed(4);
    } else {
      if (num.toString().indexOf(".") == -1) {
        //integer
        return num
      } else {
        return (num).toFixed(2);
      }
    }
  }
}

function bubble_chart(params, first_plot) {
  d3.selectAll('.guide,.temporary_transition,.fixed_transition').remove();
  var label_array = [];
  var anchor_array = [];

  bubble_data.plotted_info.params = params;
  bubble_data.reduced = app_data.all_data.reduce(function (memo, d, i) {
    // console.info(i)
    //memo.countries.push(d.country)
    var main_index_class = d.data_class['main_index_class'];
    var facts = d.country_facts[0];

    //{x:'lcoe_average' , y:'pop_noelect_minigrid' ,r:'co2_tonnes_minigrid' }

    var x_param_name = params['x'];
    var y_param_name = params['y'];
    var r_param_name = params['r'];

    /*
          },
    {
        info_container: 'impacts',
        code: 'pop_noelect_minigrid',
        long_txt: 'Market size: number of new customers (000 people)'
    },
    {
        info_container: 'impacts',
        code: 'pop_minigrid',
        long_txt: 'PV mini-grid market size (000 people)'
    },
 {
        info_container: 'impacts',
        code: 'pop_shs',
        long_txt: 'Market size by SHS or nano-solar (000 people)'
    },
    */

    memo.x_arr.push(facts[x_param_name]);
    memo.y_arr.push(facts[y_param_name]);
    memo.r_arr.push(facts[r_param_name]);

    var index_pos = memo.main_index_class.arr.indexOf(main_index_class);
    if (index_pos == -1) {
      memo.main_index_class.arr.push(main_index_class);
      memo.main_index_class.data.push({
        index: main_index_class,
        counts: 1,
        countries: [{
          name: d.country,
          code: d.code,
          index_val: d.data_val.main_index_val
        }]
      });
    } else {
      memo.main_index_class.data[index_pos].counts++;
      memo.main_index_class.data[index_pos].countries.push({
        name: d.country,
        code: d.code,
        index_val: d.data_val.main_index_val
      });
    }

    //there is no data, we have to solve it in some way...
    // if (!facts[x_param_name])
    //     facts[x_param_name] = 0.00000001;

    // if (!facts[y_param_name])
    //     facts[y_param_name] = 0.00000001;

    // if (!facts[r_param_name])
    //    facts[r_param_name] = 0.00000001;
    if (facts[x_param_name] && facts[y_param_name] && facts[r_param_name]) {
      memo.countries_data.push({
        code: d.code,
        country: d.country,
        rank_index: d.rank_index,
        main_index_class: main_index_class,
        x_val: facts[x_param_name],
        y_val: facts[y_param_name],
        r_val: facts[r_param_name]
        // x_val: format_num(facts[x_param_name]),
        // y_val: format_num(facts[y_param_name] / 1000),
        // r_val: format_num(facts[r_param_name])
      })
    }

    return memo;
  }, {
    countries_data: [],
    x_arr: [],
    y_arr: [],
    r_arr: [],
    main_index_class: {
      arr: [],
      data: []
    }
  });

  //     var sorted_code_color_arr = bubble_data.reduced.main_index_class.sort(function(a, b) {
  //     return a.rank - b.rank;
  // })

  // $('#bubble_chart g').empty();
  params.r_title = 'GHG emissions avoided per year (ktons CO2eq)';
  params.color_title = 'Performance';

  //{x:'lcoe_average' , y:'pop_noelect_minigrid' ,r:'co2_tonnes_minigrid' }

  bubble_data.max_x = Math.max.apply(Math, bubble_data.reduced.x_arr);
  bubble_data.min_x = Math.min.apply(Math, bubble_data.reduced.x_arr);

  bubble_data.max_y = Math.max.apply(Math, bubble_data.reduced.y_arr);
  bubble_data.min_y = Math.min.apply(Math, bubble_data.reduced.y_arr);

  bubble_data.max_r = Math.max.apply(Math, bubble_data.reduced.r_arr);

  bubble_data.min_r = Math.min.apply(Math, bubble_data.reduced.r_arr);

  // height: 245px;
  // width:815px;
  //var div_h = $('#bubble_chart').height();
  var div_h = 500;
  var div_w = ($('h3.country_name').width() * 75) / 100;

  bubble_data.margin = {
    top: 20,
    right: 20,
    bottom: 40,
    // left: 90
    left: 60
  };
  bubble_data.width = div_w - bubble_data.margin.left - bubble_data.margin.right;
  bubble_data.height = div_h - bubble_data.margin.top - bubble_data.margin.bottom;

  var svg = d3.select("#bubble_chart").select('svg');
  console.warn(svg.size(), first_plot)
  if (svg.size() == 0) {
    // var first_plot = true;
    d3.select("#bubble_chart").append("svg")
      .attr("width", (bubble_data.width + bubble_data.margin.left + bubble_data.margin.right) - 30)
      .attr("height", bubble_data.height + bubble_data.margin.top + bubble_data.margin.bottom)
      .append("g")
      .attr('class', 'main_g')
      .attr("transform",
        "translate(" + bubble_data.margin.left + "," + bubble_data.margin.top + ")");
  } else {
    d3.selectAll('.guide,.temporary_transition').remove();
  }

  // d3.select('.main_g').append('g')
  //     .attr("transform",
  //         "translate(" + (bubble_data.width - 50) + ",40)")
  //     .attr('height', '25px')
  //     .attr('fill', 'white')
  //     .append('text')
  //     .attr('fill', 'red')
  //     .text('Click on a circle')

  $('.bubble_container').show();
  var main_g = d3.select('g.main_g');
  bubble_data.main_g = main_g;

  var z = d3.scaleSqrt()
    .domain([bubble_data.min_r, bubble_data.max_r])
    .range([2, 20]);

  bubble_data.z = z;

  // Add X axis
  var x = d3.scaleLinear()
    .domain([bubble_data.min_x, bubble_data.max_x])
    .range([0, bubble_data.width - z(bubble_data.max_r)]);

  // Add X axis

  var Xaxis = d3.axisBottom(x)
    .tickFormat(function (d, i) {
      if ((d / 1000000) >= 1) {
        console.info(d);
        d = d / 1000000 + "M";
        return d;
      } else {
        if ((d / 1000) >= 1) {
          d = d / 1000 + "K";
          return d;
        } else {
          return d;
        }
      }
    });

  //.tickFormat(function(d){ return d.x;});
  // var xAxis_grid = d3.axisBottom()
  //     .scale(x)

  // .tickSizeInner(-bubble_data.height)
  //     .tickSizeOuter(0)
  //     .tickPadding(10);

  // svg.append("g")
  //     .attr("class", "x2 axis2")
  //     .attr("transform", "translate(0," + bubble_data.height + ")")
  //     .call(xAxis_grid);

  var g_bottom = d3.select('g.axisBottom');

  if (g_bottom.size() == 0) {
    main_g.append("g")
      .attr("transform", "translate(0," + bubble_data.height + ")")
      .attr('class', 'axisBottom')

    main_g.append('g')
      .attr('fill', 'black')
      .attr("class", 'g_data');
    //  debugger
  }
  var g_bottom = d3.select('g.axisBottom');
  g_bottom.call(Xaxis)
  //

  g_bottom.select('.x_scale_title').remove();
  // Add X axis label:
  g_bottom.append("text")
    .attr('class', 'x_scale_title')
    .attr("text-anchor", "start")
    .attr("x", 20)
    //more or less the height of the x axis numbers..
    .attr("y", 30)
    .text(params.x_title)

  $('.chart_title .x_param').text(params.x_title);
  $('.chart_title .y_param').text(params.y_title);

  // Add Y axis
  //{x:'lcoe_average' , y:'pop_noelect_minigrid' ,r:'co2_tonnes_minigrid' }
  var y = d3.scaleLinear()
    .domain([bubble_data.min_y, bubble_data.max_y])
    .range([bubble_data.height, 0]);

  var Leftaxis = d3.axisLeft(y)
    .tickFormat(function (d, i) {
      if ((d / 1000000) >= 1) {
        console.info(d)
        d = d / 1000000 + "M";
        return d;
      } else {
        if ((d / 1000) >= 1) {
          d = d / 1000 + "K";
          return d;
        } else {
          return d;
        }
      }
    })
    .ticks(5);

  var left_g = d3.select('.axisLeft');
  if (left_g.size() == 0) {
    main_g.append("g")
      .attr('class', 'axisLeft');
  }
  var left_g = d3.select('.axisLeft');

  //left_g.call(d3.axisLeft(y)
  left_g.call(Leftaxis)

  d3.selectAll("g.axisLeft g.tick")
    .append("line")
    .attr("class", "gridline")
    .attr("x1", 0)
    .attr("y1", 0)
    .attr("x2", bubble_data.width)
    .attr("y2", 0)
    .style('stroke-dasharray', '2 2')
    .style("stroke", '#6c6868')
    .style('opacity', function () {
      if (bubble_data.chart_options.gridLines == true)
        return 0.6
      else
        return 0
    })

  d3.selectAll("g.axisBottom g.tick")
    .append("line")
    .attr("class", "gridline")
    .attr("x1", 0)
    .attr("y1", -bubble_data.height)
    .attr("x2", 0)
    .attr("y2", 0)
    .style("stroke", '#6c6868')
    .style('stroke-dasharray', '2 2')
    .style('opacity', function (d, i) {
      if (bubble_data.chart_options.gridLines == true) {
        if (i > 0)
          return 0.6;
        else
          return 0;
      } else {
        return 0;
      }
    })

  // Add Y axis label:
  // svg.append("text")
  //     .attr("text-anchor", "end")
  //     .attr("x", 0)
  //     //.attr("y", -20)
  //     .text(params.y_title)
  //     .attr("text-anchor", "start")
  //     .attr('font-size', '10px')

  // .attr("transform", "translate(-50," + (height - 10) + ") rotate(-90)")

  left_g.select('.y_scale_title').remove();
  left_g.append("text")
    .attr('class', 'y_scale_title')
    .attr("text-anchor", "end")
    .attr("x", 0)
    //.attr("y", -20)
    .text(params.y_title)
    .attr("text-anchor", "start")
    .attr("transform", "translate(-50," + (bubble_data.height - 10) + ") rotate(-90)");

  // Add a scale for bubble size
  //{x:'lcoe_average' , y:'pop_noelect_minigrid' ,r:'co2_tonnes_minigrid' }

  var g_data = d3.select('svg g.g_data');

  var circles = g_data.selectAll("circle")
    .data(bubble_data.reduced.countries_data)
  circles.exit(); //.remove();

  console.warn('first plot ' + first_plot);
  var max_cx_pos = 0;
  if (first_plot === true) {
    bubble_data.minRadius = 10,
      bubble_data.maxRadius = 100;

    var duration = 800;

    circles.enter()
      .append("circle")
      .attr('code', function (d) {
        return d.code;
      })
      .attr("class", function (d) {
        return "bubbles main_index_class_" + d.main_index_class;
      })
      .style("fill", function (d) {
        //return myColor(d.continent);
        return app.scale_values.filter(function (d2) {
          return d2.value == d.main_index_class;
        })[0].color;
      })
      .attr("stroke", "white")
      .attr("cx", function (d, i) {
        console.info(d);
        if (x(d.x_val) > max_cx_pos) {
          max_cx_pos = x(d.x_val);
        }
        anchor_array.push({
          code: d.code,
          x: x(d.x_val),
          y: y(d.y_val),
          r: z(d.r_val),
          name: d.country
        });
        label_array.push({
          code: d.code,
          x: x(d.x_val),
          y: y(d.y_val),
          r: z(d.r_val),
          //name: "Node " + String(i),
          name: d.country
        });
        return x(d.x_val); //- z(d.r_val); // lcoe_average  LCOE (EUR/kWh) 
      })
      .attr("cy", function (d) {
        return y(d.y_val); //pop_noelect_minigrid Market size: decentralized technologies ('000 people)
      })
      .attr("r", function (d) {
        return z(d.r_val); //co2_tonnes_minigrid GHG emissions avoided per year (ktons CO2eq)
      })
      .style('opacity', bubble_data.chart_options.opacity)
      .style('stroke-width', bubble_data.chart_options.circle_border + 'px')
    var _this = '.bubbles[code=' + params.code + ']';
    var copy = clone(_this);
    copy.call(transition_circle, bubble_data.minRadius, bubble_data.maxRadius, false);
  } else {
    circles.transition()
      .ease(d3.easeSin)
      .duration(1800)
      .attr("delay", function (d, i) {
        return 500 * i;
      })
      .attr("cx", function (d, i) {
        if (x(d.x_val) > max_cx_pos) {
          max_cx_pos = x(d.x_val);
        }

        anchor_array.push({
          code: d.code,
          x: x(d.x_val),
          y: y(d.y_val),
          r: z(d.r_val),
          name: d.country
        });
        label_array.push({
          code: d.code,
          x: x(d.x_val),
          y: y(d.y_val),
          r: z(d.r_val),
          //name: "Node " + String(i),
          name: d.country
        });

        return x(d.x_val);
      })
      .attr("cy", function (d) {
        return y(d.y_val); //pop_noelect_minigrid Market size: decentralized technologies ('000 people)
      })
      .on("end", function (d) {
        if (d.code == params.code) {
          console.info('cloning ' + d.code)
          var _this = '.bubbles[code=' + params.code + ']';
          var copy = clone(_this);

          copy.call(transition_circle, bubble_data.minRadius, bubble_data.maxRadius, false);
        }
        // d3.select(_this)
        //     //d3.select(this)
        //     .dispatch('mouseenter');
        //var copy = clone(_this);
        //d3.select(this)
        //copy.call(transition_circle, bubble_data.minRadius, bubble_data.maxRadius, false);
      });
  }

  //var circles = g_data.selectAll("circle");

  console.info(bubble_data.reduced.countries_data);

  // main_g.selectAll(null)
  //     .data(bubble_data.reduced.countries_data)
  //     .enter()
  //     .append('text')
  //     .text(d => d.country)
  //     .attr('color', 'white')
  //     .attr('font-size', 11)
  //     .attr("cx", function(d, i) {
  //         console.warn(d)
  //         return x(d.x_val);

  //     })
  //     .attr("cy", function(d) {
  //         return y(d.y_val); //pop_noelect_minigrid Market size: decentralized technologies ('000 people)
  //     })
  //     .attr("x", function(d, i) {
  //         console.warn(d)
  //         return x(d.x_val);

  //     })
  //     .attr("y", function(d) {
  //         return y(d.y_val); //pop_noelect_minigrid Market size: decentralized technologies ('000 people)
  //     })

  // var ticked = function(d) {
  //     console.info(d)
  //     d.attr('cx', function(data) {
  //             return data.x
  //         })
  //         .attr('cy', (data) => {
  //             return data.y
  //         });

  //     d.attr('x', (data) => {
  //             return data.x
  //         })
  //         .attr('y', (data) => {
  //             return data.y
  //         });
  // }


  g_data.selectAll(".link").data([]).exit().remove();
  g_data.selectAll(".labeler").data([]).exit().remove();

  var r_arr = label_array.map(d => d.r);

  var max_r = Math.max.apply(Math, r_arr);
  var min_r = Math.min.apply(Math, r_arr);
  var svg_width = $('#bubble_chart svg').attr('width')
  var f_size_scale = d3.scaleSqrt()
    .domain([min_r, max_r])
    .range([8, 17]);

  labels = g_data.selectAll(".labeler")
    .data(label_array)
    .enter()
    .append("text")
    .attr("class", function (d) {
      return "labeler " + d.code
    })
    .attr('text-anchor', 'start')
    .text(function (d) {
      return d.name;
    })
    .attr("x", function (d) {
      if (d.x) {
        console.warn(d, d.x)
      }
      if (d.x > (max_cx_pos - 20)) {
        return ((d.x) - 75);
      } else {
        return d.x + 5;
      }
    })
    .attr("y", function (d) {
      if (!d.y) {
        console.warn(d, d.y)
      }
      return (d.y);
    })
    .style('opacity', 0)
    .style("font-size", function (d) {
      return f_size_scale(d.r) + 'px'
      //return d.r + 'px';
    })
    .attr("pointer-events", "none")

  if (bubble_data.chart_options.labels == true) {
    labels.style('opacity', 1)
  }
  console.info(labels)

  // Size of each label
  var index = 0;
  labels.each(function () {
    label_array[index].width = this.getBBox().width;
    label_array[index].height = this.getBBox().height;
    index += 1;
  });
  var offset = 4;
  // Draw links
  links = g_data.selectAll(".link")
    .data(label_array)
    .enter()
    .append("line")
    .attr("class", function (d) {
      return "link " + d.code
    })
    .attr("x1", function (d) {
      return (d.x);
    })
    .attr("y1", function (d) {
      return (d.y);
    })
    .attr("x2", function (d) {
      if (d.x > (max_cx_pos - 20)) {
        return ((d.x) - 75);
      } else {
        return d.x;
      }
    })
    .attr("y2", function (d) {
      return (d.y);
    })
    .style('opacity', 0)
    .attr("stroke-width", 1.1)
    .attr("stroke", "grey")
    .attr("pointer-events", "none");

  if (bubble_data.chart_options.labels == true) {
    console.warn(links)
    links.style('opacity', .6)
  }
  var sim_ann = d3.labeler()
    .label(label_array)
    .anchor(anchor_array)
    .width(bubble_data.width)
    .height(bubble_data.height)
  sim_ann.start(1000);
  console.warn(labels.size())
  labels
    .transition()
    .duration(800)
    .attr("x", function (d) {
      if (d.x > (max_cx_pos - 20)) {
        return ((d.x) - 75);
      } else {
        return d.x + 5;
      }
      //return (d.x);
    })
    .attr("y", function (d) {
      return (d.y);
    })

  links
    .transition()
    .duration(800)
    .attr("x2", function (d) {
      return (d.x);
    })
    .attr("y2", function (d) {
      return (d.y);
    })
  var circles = g_data.selectAll("circle.bubbles");
  circles.on("mouseenter",
    //     showTooltip
    // )
    // -3- Trigger the functions for hover
    // .on("mouseover", 
    // showTooltip
    function (r) {
      $('html').css('cursor', 'pointer');

      //showTooltip(d, this, elem)

      var f = app_data.all_data.filter(function (info) {
        return info.code == r.code;
      })[0];
      bubble_info(f, r, params, false);
      console.info(r)
      d3.selectAll('.temporary_transition').remove();
      var _this = '.bubbles[code=' + r.code + ']';
      var copy = clone(_this)
      copy.attr('class', 'temporary_transition');
      var element = d3.select(this);

      //var ps={x:x,y:y,color:color}
      //Append lines to bubbles that will be used to show the precise data points
      //d3.selectAll('.guide.temp').remove(element);
      create_guides(element, r, false, copy);
      if (bubble_data.chart_options.labels == true)
        d3.selectAll('.labeler').style('opacity', 0.1);

      var filtered_label = d3.selectAll('.labeler').filter(function (d) {
        return d.code == r.code;
      });

      bubble_data.filtered_label = filtered_label;
      filtered_label.style('opacity', 1).classed('filtered_label', true);

      setTimeout(function () {
        if (bubble_data.chart_options.labels == true)
          d3.selectAll('.labeler')
          .transition()
          .delay(80)
          .duration(800)
          .style('opacity', 0.9);
      }, 1600)
    }
  ).on('mouseleave', function (r) {
    d3.selectAll('.filtered_label').classed('filtered_label', false);
    bubble_data.filtered_label.classed('filtered_label', true);
    //$('html').css('cursor', 'unset');
  })
}

function bubble_info(f, r, params, fixed) {
  //all_data , reduced, params
  var country_facts = f.country_facts[0];
  if (fixed == true)
    i = 1;
  else
    i = 2;

  $('.fixed_bubble_popup .tooltip_container>.row').each(function () {
    var c = $(this).attr('class').split(' ')[1];
    console.info(r);
    switch(c) {
      case 'countries':
        $(this).find('.col-bubble').eq(i).text(f.country);
        break;
      case 'x_row':
        if (r.x_val == 0.001)
          $(this).find('.col-bubble').eq(i).text('No data');
        else
          $(this).find('.col-bubble').eq(i).text(numberWithCommas(format_num(r.x_val)));
        break;
      case 'y_row':
        if (r.y_val == 0.00000001)
          $(this).find('.col-bubble').eq(i).text('No data');
        else
          //  $(this).find('.col').eq(i).text(r.y_val);
          $(this).find('.col-bubble').eq(i).text(numberWithCommas(format_num(r.y_val)));
        break;
      case 'r_row':
        if (r.r_val == 0.00000001)
          $(this).find('.col-bubble').eq(i).text('No data');
        else
          $(this).find('.col-bubble').eq(i).text(numberWithCommas(format_num(r.r_val)));
        break;
      case 'color_row':
        var performance = app.scale_values.filter(function (d) {
          return d.value == r.main_index_class;
        })[0];
        $(this).find('.col-bubble').eq(i).text(performance.title);
        $(this).find('.col-bubble').eq(i).css('border-bottom', '4px solid ' + performance.color);
        $(this).find('.col-bubble').eq(i).css('padding', 'unset');
        break;
      default:
        break;
    }
  })

  if (fixed == false) {
    $('.tooltip_container').find('.col').show();
  }

  var params_code_arr = [params['x'], params['y'], params['r']];
  if (fixed == true) {
    var html = '';

    for (var p in facts_list) {
      var o = facts_list[p];
      html += '<div class="row more_info ' + o.code + ' valign-wrapper"> <div class="col s6 t_title ex_tooltiped" data-position="top" data-tooltip="' + o.long_txt + '">' + o.long_txt +
        '</div><div class="col s3">' +
        numberWithCommas(format_num(country_facts[o.code])) + '</div><div class="col s3"></div></div>';
    }

    $('.more_info_container').empty().append(html);
    $('.more_info_container').find('.tooltiped').tooltip();
  } else {
    // for (var p in facts_list) {
    //     var o = facts_list[p];
    //     if (params_code_arr.indexOf(o.code) > -1) {
    $('.more_info_container .row.more_info').each(function () {
      var attr = $(this).attr('class').split(' ')[2];
      $(this).find('.col').eq(2).text(numberWithCommas(format_num(country_facts[attr])));
      //  $(this).find('.col').eq(2).text(numberWithCommas(format_num(country_facts[attr])));
    })
    //     }
    //
    // }
  }
}

$('.apply_bubble').click(function () {
  var params = {};
  params['x'] = $('#x_bubble_select').find('option:selected').attr('value');
  params['x_title'] = $('.x_axis_c').find('.select-dropdown li.selected span').text();

  $('.collapsible.x_axis_c .collapsible-header').find('span').text(params['x_title']);

  params['y'] = $('#y_bubble_select').find('option:selected').attr('value');
  params['y_title'] = $('.y_axis_c').find('.select-dropdown li.selected span').text();
  $('.collapsible.y_axis_c .collapsible-header').find('span').text(params['y_title']);

  params['r'] = 'co2_tonnes_minigrid';
  params['code'] = app_state.country_facts_sel.code;
  console.warn(params)

  bubble_data.plotted_info.params = params;
  bubble_chart(params, false);

  $('.fixed_bubble_popup .tooltip_container>.row').each(function (i, d) {
    $(this).find('.col').eq(2).hide();
    switch (i) {
      //0, first time is the country

      case 1:
        $(this).find('.col').eq(0).text(params['x_title']);
        break;
      case 2:
        $(this).find('.col').eq(0).text(params['y_title']);
        break;
      case 3:
        $(this).find('.col').eq(0).text(params['r']);
        break;
      default:
        break;
    }
  });

  // var facts_list = [{
  //     info_container: 'status',
  //     code: 'er_2019',
  //     long_txt: 'Electrification rate'
  // },
  // {

  /*
   <div class="more_info"> <div class="col s4 t_title">o.long_text</div><div class="col s4"></div><div class="col s4"></div></div>
  */
});