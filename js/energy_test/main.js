
(function (H) {
    H.wrap(H.Chart.prototype, 'setSize', function (proceed, width, height, animate) {
        if ((typeof width === 'string') && (typeof height === 'string')) {
            var oldWidth = this.containerWidth,
                oldHeight = this.containerHeight;
            if (width.indexOf("%") !== -1) {
                var percentWidth = parseFloat(width.slice(0, width.indexOf("%"))),
                    percentHeight = parseFloat(height.slice(0, height.indexOf("%")));
            } else {
                var percentWidth = parseFloat(width),
                    percentHeight = parseFloat(height);
            }
            var width = oldWidth * percentWidth * 0.01,
                height = oldHeight * percentHeight * 0.01;
        }
        proceed.apply(this, Array.prototype.slice.call(arguments, 1));
    });
}(Highcharts));
$.fn.once = function (processed_class) {
    if (typeof processed_class == 'undefined') {
        processed_class = 'processed';
    }
    return this.not('.' + processed_class).addClass(processed_class);
};
let clickTimeout;
var flying = false;
this_app.debounce = function (fn, delay) {
    var timer = null;
    return function () {
        var context = this,
            args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
            fn.apply(context, args);
        }, delay);
    };
}
this_app.throttle = function (fn, threshhold, scope) {
    threshhold || (threshhold = 1000);
    var last,
        deferTimer;
    return function () {
        var context = scope || this;

        var now = +new Date,
            args = arguments;
        if (last && now < last + threshhold) {
            clearTimeout(deferTimer);
            deferTimer = setTimeout(function () {
                last = now;
                fn.apply(context, args);
            }, threshhold);
        } else {
            last = now;
            fn.apply(context, args);
        }
    };
}
this_app.scale_r = d3.scaleLinear()
    .domain([0, 12])
    .range([3, 16]);


function numberWithCommas(x) {

    if (!x) {





        return 0

    }
    if (x > 10) {

        var d = Math.floor(x);
        var t = d.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

        if (t.includes(',') && t.split(',').length > 2) {

            return t
        } else {
            return t
        }

    } else {


        if (x.toString().includes('.')) {

            var d = parseFloat(x).toFixed(2)
        } else {
            var d = x;
        }
        return d
    }


}
$(document).ready(function () {

    document.querySelectorAll('input').forEach(input => {
        input.setAttribute('autocomplete', 'off');
    });

    window.addEventListener('resize', () => {
        console.log('Window resized!');
    });



    $(document).on('shown.bs.tab', '.stats_modal_tables_wrapper a[data-bs-toggle="tab"]', function (e) {
        var id = $(e.target).attr("id") // activated tab

        console.log(id)
        $('#stats_modal .tab-pane').hide();
        if (id == 'nav-main_dataTables_wrapper-tab') {
            $('#main_dataTables_wrapper').show();
            window.dispatchEvent(new Event('resize'));

        } else if (id == 'nav-hc_dataTables_wrapper-tab') {

            $('#hc_dataTables_wrapper').show();
            window.dispatchEvent(new Event('resize'));
        }
    })
    var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'));

    var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
        return new bootstrap.Tooltip(tooltipTriggerEl)
    })

    /*     document.addEventListener('DOMContentLoaded', function () {
            const tooltips = document.querySelectorAll('.tooltiped');
            alert(tooltips.length)

            // Initialize MaterializeCSS tooltips
            M.Tooltip.init(tooltips, {
                onOpenStart: function (tooltipElement) {
                    console.log('Tooltip is opening:', tooltipElement);
                    debugger; // Triggers the debugger
                }
            });
        }); */

    console.log(this_app)

    attach_analysis_events();
    this_app.get_ajax_calls['get_download_info']();

    $(".test_centered_toast").html(`<span>Explore</span> layers using lateral menu or <span>double-click</span> on a country to see its statistics`)
    $(".test_centered_toast").addClass('main');

    $('.test_centered_toast').animate({ 'opacity': 1, 'top': '30%' }, 400)


    setTimeout(function () {

        $('.test_centered_toast').animate({ 'opacity': 0, 'top': '0%' }, 400);
        $('.test_centered_toast').removeClass('main');

    }, 5500)


    console.log($('[data-toggle="tooltip"]').length)

    var introModal = new bootstrap.Modal($('#intro_modal').eq(0));
    var $introModal = $('#intro_modal');

    $('#main-navigation,.globan-center').hide();
    $('.panels-flexible-region-73-center').css('width', '100%');

    //$('html,body').attr('style', "font-family:unset")
    var p = "'Open Sans', sans-serif, \"Helvetica Neue\", Helvetica, Arial";
    $('html,body').attr('style', "font-family:" + p);




    app_data.layers_info.filter(d => d._type == 'circle').forEach(function (d, i) {


        console.log(d.id)

        if (d.id == 'taf_pumps_down' || d.not_styled || d.id === 'ref_unhcr_merged_simple'
            || d.id === 'schools')

            return false;

        new XNColorPicker({
            color: d.border ? d.border : 'red',
            selector: ".notype." + d.id,
            showprecolor: true, //显示预制颜色
            prevcolors: ['#EF534F', '#BA69C8', '#FFD54F', '#81C784', '#7FDEEA', '#90CAF9', '#F44436', '#AB47BC', '#FFC106', '#66BB6A', '#25C6DA', '#4EC3F7', '#E53934', '#9D27B0', '#FFA726', '#4CAF50', '#00ACC1', '#29B6F6', '#D32E30', '#8F24AA', '#FB8C01', '#378E3C', '#0097A7', '#02AAF4', '#C62928', '#7B1FA2', '#F57C02', '#2F7D31', '#00838F', '#029BE5', '#B71B1C', '#6A1B9A', '#EF6C00', '#34691D', '#006164', '#0388D1', '#980A0B', '#4A148C', '#E65100', '#1A5E20', '#004D41', '#01579B', 'rgba(0,0,0,0)', '#FFFFFF', '#DBDBDB', '#979797', '#606060', '#000000'],
            showhistorycolor: false, //显示历史
            historycolornum: 16, //历史条数
            format: 'hsla', //rgba hex hsla,初始颜色类型
            showPalette: true, //显示色盘
            show: false, //初始化显示
            lang: 'en', // cn 、en
            colorTypeOption: 'single', //
            autoConfirm: true,
            onError: function (e) {

            },
            onCancel: function (color) {
                console.log("cancel", color)
            },
            onChange: function (color) {
                console.log(d.id)

                console.warn(d3.selectAll('.analysis_layer.' + d.id + ' svg.circles_svg path'))

                d3.selectAll('.analysis_layer.' + d.id + ' svg.circles_svg path').style('stroke', color.color.hex);
                d3.selectAll('.analysis_layer.' + d.id + ' svg.circles_svg circle').style('stroke', color.color.hex);

                map.setPaintProperty(
                    d.id,
                    "circle-stroke-color", color.color.hex)
            },
            onConfirm: function (color) {

                d3.selectAll('.analysis_layer.' + d.id + ' svg.circles_svg path').style('stroke', color.color.hex)
                d3.selectAll('.analysis_layer.' + d.id + ' svg.circles_svg circle').style('stroke', color.color.hex);

                map.setPaintProperty(
                    d.id,
                    "circle-stroke-color", color.color.hex)

            }
        });
    })
    $('.panels-flexible-region-61-center').css('width', '100%');
    $('#content-wrapper .tabs').eq(0).remove();
    $('.container').removeClass('container');
    $('#intro_modal').css('top', '0%')
    $('body').addClass('ceat_body');

    function setup_initial() {


        if (map.getSource('gaul_level_0_source')) {



            //$('.materialize_form_radio input').eq(0).prop('checked', true);
            $('.materialize_form_radio').eq(0).trigger('change');

            console.log('tabs ready')

            console.log('ready initial')



            setTimeout(function () {

                $('#map,nav,.sidenav').css('opacity', 1);
                $('.sidenav .collapsible .queryable_li .collapsible-header').trigger('click')
                $('.sidenav').sidenav();
                $('.tabs').tabs();



            }, 500)

            clearInterval(setup_initial_timer)


        }
        else {

        }






    }

    if ($('.tabs').length > 0) {


    }

    var setup_initial_timer = setInterval(setup_initial, 50);
    this_app.roundIfNecessary = function (val, withCommas) {
        console.info(val, typeof val)
        if (typeof val !== 'number') {
            return val;
        }

        if (Number.isInteger(val)) {
            if (!withCommas)
                return val;
            else
                return numberWithCommas(val);
        } else {
            return numberWithCommas(Math.round(val * 100) / 100);
        }
    }
    this_app.userid = Date.now();
    this_app.popup_active_counts = 0;

    this_app.calculation_busy_option = {
        spinner: "accordion",
        text: 'Calculating...',
        background: "#101010",
        textMargin: '2rem',
        fontSize: '3rem',
        textColor: "white",
        color: "#ff9800"

    }
    $.busyLoadSetup({
        spinner: "accordion",
        text: 'Calculating...',
        background: "#101010",
        textMargin: '2rem',
        fontSize: '3rem',
        textColor: "white",
        color: "#ff9800"
    });

    this_app.generating_file_busy_option = {
        spinner: "accordion",
        text: 'Generating file...',
        background: "#101010",
        textMargin: '2rem',
        fontSize: '3rem',
        textColor: "white",
        color: "#ff9800"

    }


    function in_distance(val, layer_name) {
        var layer_info = app_data.layers_info.filter(function (d) {
            return d.id == layer_name;
        })[0];
        var layer = layer_info._layer;


        var features = map.querySourceFeatures(layer.id, {
            sourceLayer: 'hospitals_source',
            filter: ['<', ['get', 'dist_grid'], 15]
        });
        console.log(features)

        var features = map.queryRenderedFeatures({
            layers: [layer.id]
        });
        console.log(features)
        var filtered = features.filter(function (feature) {
            return feature.properties.dist_grid < parseInt(val)
        });
        console.log(filtered)

        if (features.length) {
            console.warn(val)

            var p_filter = filtered.reduce(function (memo, feature) {
                var prop = feature.properties;
                memo.circle_opacity_expression.push(prop.row_id, 1)
                return memo;

            }, {
                circle_opacity_expression: ["match", ["get", "row_id"]]
            });
            p_filter.circle_opacity_expression.push(0.2);
            console.warn(p_filter.circle_opacity_expression)

            map.setPaintProperty('hospitals', "circle-opacity", p_filter.circle_opacity_expression);
        }
    }

    function setup_distance(val) {

        console.log(val)
        map.removeLayer('hospitals');
        console.log('removed hospit')
        hospitals.filter = ["all", ["<=", ["get", "dist_grid"], parseInt(val)]];
        setTimeout(function () {
            map.addLayer(hospitals);
        }, 1500)



    }
    var opacity = null;
    $('li.analysis_layer').each(function () {

        var t = $(this);
        var layer_name = t.attr('class').split(' ')[1];
        if (t.find('.range-wrap.opacity_range').length > 0) {

            var range_opacity_container = t.find('.range-wrap.opacity_range');
            var rangeV_opacity = range_opacity_container.find('.rangeV.rangeV_opacity');
            var range_opacity = range_opacity_container.find('.range_z_opacity');


            console.warn('dealing with opacities on ' + layer_name)
            console.info(rangeV_opacity)


            document.addEventListener("DOMContentLoaded", setValue(range_opacity, rangeV_opacity));
            range_opacity.on('input', function (e) {

                setValue($(e.target), rangeV_opacity);

                opacity = null;
                console.warn('changing with opacities')

                this_app.throttle(change_opacity(e.target.value, layer_name, false), 300)

            })

        }

        if (t.find('.range-wrap.stroke_range').length > 0) {

            var range_stroke_container = t.find('.range-wrap.stroke_range');
            var rangeV_stroke = range_stroke_container.find('.rangeV.rangeV_stroke');
            var range_stroke = range_stroke_container.find('.range_z_stroke');
            console.warn('dealing with strokes on ' + layer_name)



            document.addEventListener("DOMContentLoaded", setValue_stroke(range_stroke, rangeV_stroke));
            range_stroke.on('input', function (e) {

                setValue_stroke($(e.target), rangeV_stroke);
                console.warn('changing with stroke layer ' + layer_name, 3 * (e.target.value / 100))

                if (layer_name == 'electric_grid') {
                    map.setPaintProperty(
                        layer_name,
                        "line-width", 3 * (e.target.value / 100)
                    );
                } else {
                    d3.selectAll('.analysis_card.' + layer_name + ' .cell circle').style('stroke-width', 3 * (e.target.value / 100));
                    d3.selectAll('.analysis_layer.' + layer_name + ' .cell path').style('stroke-width', 3 * (e.target.value / 100))


                    map.setPaintProperty(
                        layer_name,
                        "circle-stroke-width", 3 * (e.target.value / 100)
                    );
                }

            })

        }


        if (t.find('.range-wrap.radius_range').length > 0) {

            var range_radius_container = t.find('.range-wrap.radius_range');
            var rangeV_radius = range_radius_container.find('.rangeV.rangeV_radius');
            var range_radius = range_radius_container.find('.range_z_radius');
            console.warn('dealing with opacities on ' + layer_name)
            console.info(rangeV_radius)


            document.addEventListener("DOMContentLoaded", setValue_radius(range_radius, rangeV_radius));
            range_radius.on('input', function (e) {

                setValue_radius($(e.target), rangeV_radius);

                console.warn('changing with radius on ' + layer_name)

                var layer = app_data.layers_info.filter(function (d) {
                    return d.id == layer_name
                })[0];
                console.log(layer)
                if (layer_name == 'refugee_camps') {
                    console.warn('refugee camps, we have to re-symbolize 2 layers');
                    var new_size = this_app.get_radius_by_param(layer, layer.active_param, e.target.value)
                    console.log(new_size)
                    map.setPaintProperty(layer_name, "circle-radius", new_size.circle_scale);

                    d3.selectAll('.marker.refugee_camps g').attr('transform', 'scale(' + new_size.marker_scale + ')');

                } else {
                    this_app.throttle(change_radius(e.target.value, layer_name, 500))
                }

            })



        }


        if (t.find('.range-wrap.distance_range').length > 0) {
            var range_container_2 = t.find('.range-wrap.distance_range');

            var rangeV2 = range_container_2.find('.rangeV.rangeV_distance');
            var range2 = range_container_2.find('.range_z_distance');

            document.addEventListener("DOMContentLoaded", setValue(range2, rangeV2));
            range2.on('input', function (e) {
                console.warn('changing with distance')
                setValue($(e.target), rangeV2);

            })
            range2.on('change', function (e) {
                console.log('distance is ' + e.target.value)
                this_app.throttle(setup_distance(e.target.value), 500)

            })

        }


    });

    function change_radius(val, layer_name) {
        console.log(arguments)
        var layer = app_data.layers_info.filter(function (d) {
            return d.id == layer_name;
        })[0];

        if (!layer.active_param) {
            var param = layer.default_param;
        } else {
            var param = layer.active_param;
        }




        map.setPaintProperty(layer_name, "circle-radius", this_app.get_radius_by_param(layer, param, 3 * (val / 100)));
    }

    function change_opacity(val = parseInt(val), layer_name, africa_platform) {

        console.info(arguments)

        if (africa_platform == true) {

            var l = africa_platform_layers.filter(function (d) {
                return d.layer_id == layer_name
            })[0];

            if (l) {
                var layer = l._layer;
                map.setPaintProperty(
                    layer_name,
                    'raster-opacity',
                    parseInt(val, 10) / 100
                );
            }
        } else {

            if (layer_name == 'time_travel') {
                console.warn($('li.analysis_layer.time_travel').attr('active_layer'));
                var layer_name = $('li.analysis_layer.time_travel').attr('active_layer');

            }
            if (!layer_name) return false;

            console.warn('chang opacity to ' + val + ' for ' + layer_name)
            var layer_info = app_data.layers_info.filter(function (d) {
                return d.id == layer_name;
            })[0];
            var container = $('.analysis_layer.' + layer_name);
            var val = parseInt(val, 10) / 100

            container.find('.range-wrap-container.opacity>span b').text(parseInt(val * 100) + '%')

            switch (layer_info._layer.type) {
                case 'circle':




                    map.setPaintProperty(
                        layer_name,
                        'circle-opacity',
                        val
                    );

                    map.setPaintProperty(
                        layer_name,
                        "circle-stroke-width",
                        val
                    );

                    break;
                case 'raster':

                    if (layer_name.includes('travel')) {

                        var time_layers = ['travel_hc_access_minimum_all_motorized', 'travel_hc_access_minimum_all_walking',
                            'travel_time_primary_all_walking', 'travel_time_primary_all_motorized'
                        ];
                        time_layers.forEach(function (item) {
                            var l = app_data.layers_info.filter(d => d.id == item)[0]._layer;
                            l.paint['raster-opacity'] = val;
                        });


                    }

                    map.setPaintProperty(
                        layer_name,
                        'raster-opacity',
                        val
                    );




                    break;

                case 'line':
                    map.setPaintProperty(
                        layer_name,
                        'line-opacity',
                        val
                    );
                    break;
                case 'fill':
                    map.setPaintProperty(
                        layer_name,
                        'fill-opacity',
                        val
                    );
                    break;
                default:
                    break;
            }
        }

    }

    function create_tables(data) {
        console.warn(arguments)
        $('.list.analysis_results').show();
        console.info(data)
        if (data.electric_grid_data.geo.length > 0) // && data.electric_grid_data.geo[0].electric_categories.counts > 0) {
        {
            $(".table_analysis li.electric_grid").show();
            var electric_data = data.electric_grid_data.geo[0].electric_categories;
            console.warn(electric_data)
            $(".table_analysis li.electric_grid .rows_container").empty();

            $(".table_analysis li.electric_grid .badge").attr(
                "data-badge-caption",
                electric_data.counts
            );
            electric_data.categories_data.forEach(function (d) {
                console.log(d)
                var l = d.sum_length;
                if (l < 1) {
                    var length = parseFloat(l).toFixed(4);
                } else {
                    var length = parseFloat(l).toFixed(2);
                }
                var symbol = $("svg.electric_grid .legendCells .cell")
                    .eq(d.category)
                    .clone(true);

                $(".table_analysis li.electric_grid .rows_container").append(
                    '<div class="row"><div class="col s8" code="' +
                    d.category +
                    '"><div class="symbol electric_grid_' +
                    d.category +
                    '"><svg></svg></div>' +
                    '</div><div class="col s4">' +
                    length +
                    " Km</div></div>"
                );

                symbol.appendTo(".electric_grid_" + d.category + " svg");

                $(".electric_grid_" + d.category + " .cell").attr(
                    "transform",
                    "translate(15,25)"
                );
            });
        } else {
            $(".table_analysis li.electric_grid").hide();
        }

        if (data.hospitals_categories) {

            $(".table_analysis li.hospitals").show();
            $(".table_analysis li.hospitals .badge").attr(
                "data-badge-caption",
                data.hospitals_categories.counts
            );
            $(".table_analysis li.hospitals .rows_container").empty();
            data.hospitals_categories.categories_data.forEach(function (d) {
                console.log(d)
                var symbol = $("svg.hospitals .legendCells .cell")
                    .eq(d.category)
                    .clone(true);

                $(".table_analysis li.hospitals .rows_container").append(
                    '<div class="row"><div class="col s8" code="' +
                    d.category +
                    '"><div class="symbol hospitals_' +
                    d.category +
                    '"><svg></svg></div>' +
                    '</div><div class="col s4">' +
                    d.count +
                    "</div></div>"
                );

                symbol.appendTo(".hospitals_" + d.category + " svg");

                $(".hospitals_" + d.category + " .cell").attr(
                    "transform",
                    "translate(15,25)"
                );
            });

        } else {

            $(".table_analysis li.hospitals").hide();

        }


        if (data.power_plants) {

            $(".table_analysis li.power_plants").show();
            $(".table_analysis li.power_plants .badge").attr(
                "data-badge-caption",
                data.power_plants.counts
            );
            $(".table_analysis li.power_plants .rows_container").empty();
            data.power_plants.data.forEach(function (d) {
                console.log(d)
                var symbol = $("svg.power_plants .legendCells .cell")
                    .eq(d.category)
                    .clone(true);

                $(".table_analysis li.power_plants .rows_container").append(
                    '<div class="row"><div class="col s8" code="' +
                    d.category +
                    '"><div class="symbol power_plants_' +
                    d.category +
                    '"><svg></svg></div>' +
                    '</div><div class="col s4">' +
                    d.count +
                    "</div></div>"
                );

                symbol.appendTo(".power_plants_" + d.category + " svg");

                $(".power_plants_" + d.category + " .cell").attr(
                    "transform",
                    "translate(15,25)"
                );
            });
        } else {
            $(".table_analysis li.power_plants").hide();
        }

        $('.analysis_li .collapsible-body:first').animate({
            scrollTop: $(".table_analysis li:last").offset().top + 150
        }, 500);

    }

    function setValue_stroke(range, rangeV) {
        console.warn(arguments)

        var val = range[0]['value']; /// 2;
        var newValue = Number((val - range[0]['min']) * 100 / (range[0]['max'] -
            range[0]['min']));

        var newPosition = 10 - (newValue * 0.2);

        var rV = rangeV[0];


        rV.innerHTML = '<span>' + (3 * (val / 100)).toFixed(1) + '</span>';
        rV.style.top = '-5px';
        rV.style.left = 'calc(' + val / 2 + '% + (' + newPosition + 'px))';

    };

    function setValue_radius(range, rangeV) {

        var val = range[0]['value'];
        var newValue = Number((val - range[0]['min']) * 100 / (range[0]['max'] -
            range[0]['min']));

        var newPosition = 10 - (newValue * 0.2);

        var rV = rangeV[0];
        console.warn('calc(' + val + '% + (' + newPosition + 'px))')

        rV.innerHTML = '<span>' + (3 * (val / 100)).toFixed(1) + '</span>';
        rV.style.top = '-18px';
        rV.style.left = 'calc(' + val + '% + (' + newPosition + 'px))';

    };

    function setValue(range, rangeV) {

        var newValue = Number((range[0]['value'] - range[0]['min']) * 100 / (range[0]['max'] -
            range[0]['min']));

        var newPosition = 3 - (newValue * 0.2);

        var rV = rangeV[0];
        rV.innerHTML = '<span>' + range[0]['value'] + '%</span>';
        rV.style.top = '-14px';
        rV.style.left = range[0]['value'] + '%';
        opacity = range[0]['value']
    };

    var gaul_0_simple = {
        "id": "gaul_0_simple",
        "source": "gaul_level_0_line_source",
        "source-layer": "gaul_0_simplified",
        "type": "fill",
        'layout': {
            'visibility': 'visible'

        },
        'paint': {
            "fill-color": "#a146bd",
            "fill-outline-color": "#87989c",
            "fill-opacity": 0

        }
    }


    var bounds = [

        [-85.759, -41.9178], // Southwest coordinates
        [90.8, 34.389] // Northeast coordinates
    ];


    var bounds = [

        [-78.80852513562333, -43.657374316390175], // Southwest coordinates
        [144.28318031551646, 48.84800] // Northeast coordinates
    ];

    map = new mapboxgl.Map({
        container: 'map', // container id
        maxBounds: bounds,
        showTileBoundaries: false,
        style: {
            "glyphs": "mapbox://fonts/openmaptiles/{fontstack}/{range}.pbf",

            "version": 8,
            "sources": {
                "gaul_level_0_line_source": {
                    "type": "vector",
                    "tiles": [
                        "https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/wmts?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&LAYER=hotspots:gaul_level_0_africa3&STYLE=&TILEMATRIX=EPSG:900913:{z}&TILEMATRIXSET=EPSG:900913&FORMAT=application/vnd.mapbox-vector-tile&TILECOL={x}&TILEROW={y}"
                    ],
                }
            },
            "layers": [gaul_0_simple]
        },
        preserveDrawingBuffer: true,
        //center: [2.525, -4.7892],

        /* {
            "lng": 32.73732758994663,
            "lat": 3.754681461566065
          } */
        zoom: 2.25,

        attributionControl: false
    });




    $(".mapboxgl-map").css('background-color', "#131313");
    map.doubleClickZoom.disable()

    map.addControl(new mapboxgl.NavigationControl());

    map.dragRotate.disable();
    map.touchZoomRotate.disableRotation();


    function projectPoint(lon, lat) {
        var point = map.project(new mapboxgl.LngLat(lon, lat));
        this.stream.point(point.x, point.y);
    }
    projection = d3.geoEquirectangular().rotate([-10.5, 0]);
    var container = map.getCanvasContainer();

    var svg = d3.select(container).append("svg");
    var transform = d3.geoTransform({
        point: projectPoint
    });



    svg.append("rect")
        .classed('rect_container', true)
        .attr("width", $('#map').width() + 'px')
        .attr("height", $('#map').height() + 'px')
        .attr("fill", 'rgba(0,0,0,0.01)');



    svg.append('g').classed('points_container', true);


    class map_legend_control {
        onAdd(map) {
            this.map = map;
            this.container = document.createElement('div');
            this.container.className = 'mapboxgl-ctrl mapboxgl-ctrl-group print_and_layers_container';

            return this.container;
        }
        onRemove() {
            this.container.parentNode.removeChild(this.container);
            this.map = undefined;
        }
    }
    var legend_control_ctrl = new map_legend_control();
    map.addControl(legend_control_ctrl, 'top-right');

    $('.print_and_layers_container').html('<div class="btn intro_modal_btn d-flex align-items-center">Info</div><div class="btn d-flex align-items-center fullscreen tooltiped" data-tooltip="Fullscreen" ><i class="bi bi-fullscreen"></i></div><div class="btn d-flex align-items-center stats_info tooltiped" data-tooltip="General country statistics"><i class="bi bi-bar-chart-line-fill"></i></div><button id="popper-button-countries" class="mapboxgl-ctrl-icon explore_countries" type="button"><i class="bi bi-geo-alt-fill"></i></button><button class="mapboxgl-ctrl-icon print_map tooltiped" type="button" data-tooltip="Print map" aria-label="Print map"><i class="bi bi-printer-fill"></i></button>' +
        '<button class="mapboxgl-ctrl-icon change_basemap tooltiped" type="button" data-tooltip="Add/remove basemaps" aria-label="Change basemap"><i class="bi bi-map-fill"></i></button>' +
        '<button class="mapboxgl-ctrl-icon distance_tool tooltiped" type="button" data-tooltip="Measure distances"aria-label="Measure distance"><i class="bi bi-bezier"></i></button>');
    $('.print_and_layers_container .tooltiped').tooltip_materialize();

    var h = $('.mapboxgl-ctrl-top-right').outerHeight() + 10 + 'px';

    $('#menu').css('top', h);


    $('.btn.intro_modal_btn').on('click', function () {
        if ($(this).hasClass('selected') == false) {
            introModal.show();
        } else {
            introModal.hide();
        }
        $(this).toggleClass('selected');
    })


    $('.btn.fullscreen').on('click', function () {
        if ($(this).hasClass('selected') == false) {
            screenfull.request($('body')[0]);
        } else {
            screenfull.exit();
        }
        $(this).toggleClass('selected');
    })


    $('.change_basemap').click(function () {


        var $div = $('.mapboxgl-ctrl-top-right')
        var t = $div.offset().top;
        var h = $div.outerHeight();
        $('#menu').css('top', t + h + 10 + 'px');

        if ($(this).hasClass('on') == false) {


            $('#menu').show();
            if ($('.map_results_control_container').is(':visible')) {
                $('.map_results_control_container').find('.close_popup').trigger('click')
            }
            $(this).addClass('on');
        } else {
            $('#menu').hide();
            $(this).removeClass('on')

        }
    });

    $('.mapboxgl-ctrl.print_and_layers_container .stats_info').on('click', function () {
        $("body").busyLoad("show");




        $.when(this_app.get_ajax_calls.getAdm_tables({

            'to_query': 'adm0'
        }).then(function (resp) {
            $('#stats_modal').modal_materialize('open');

            this_app.plot_modal_datatable(resp);


            this_app.plot_hc_modal_datatable(resp);
            $('#stats_modal .nav-link.active').removeClass('active');
            var profileTab = new bootstrap.Tab(document.getElementById('nav-main_dataTables_wrapper-tab'));
            profileTab.show();
        }))



        $("body").busyLoad("hide");
    })
    $('.explore_countries_menu').on('click', function (e) {

        if ($(e.target).hasClass('country_ranking')) {

            $("body").busyLoad("show", this_app.generating_file_busy_option);
            $('#stats_modal').modal_materialize('open');




            $('#stats_modal .chosen-single').text('Administrative Units');
            console.log(this_app)
            console.log(this_app.get_ajax_calls)
            var obj = {
                'adm0_code': parseInt($('.explore_countries_menu select').find('option:selected').attr('_code')),
                'to_query': 'adm0'
            }
            this_app.get_ajax_calls.getAdm_tables(obj).then(function (resp) {
                console.log(resp)
                this_app.plot_modal_datatable(resp);
                this_app.plot_hc_modal_datatable(resp)
                $('#stats_modal .nav-link.active').removeClass('active');
                var profileTab = new bootstrap.Tab(document.getElementById('nav-main_dataTables_wrapper-tab'));
                profileTab.show();
            })



            $('#stats_modal .tabs').tabs();
            $('#stats_modal .tabs').eq(1).show();

            $("body").busyLoad("hide");

        }

        if ($(e.target).hasClass('set_explore_country')) {
            var ls = ['hospitals', 'power_plants', 'ref_settlements', 'electric_grid', 'ref_unhcr_merged_simple', 'ref_unhcr_merged_simple'].concat(['bess', 'geotermal_grouped_label', 'geotermal_grouped',
                'geotermal_ungrouped', 'hydrogen_ungrouped', 'hydrogen_grouped', 'pumps_down', 'pumps_lines', 'pumps_up', 'solar', 'wind'].map((d) => 'taf_' + d))
            console.warn(ls)
            var id = parseInt($('.explore_countries_menu select').find('option:selected').attr('_code'));
            if (!$(e.target).hasClass('on')) {


                this_app.active_admin_filter.code_name = 'adm0_code';
                this_app.active_admin_filter.code_val = parseInt(id);

                if (map.getLayer('schools')) {
                    this_app.set_raster_cql(app_data.layers_info.filter(function (d) {
                        return d.id == 'schools';
                    })[0]
                    )
                }

                var sel_bbox_coordinates = JSON.parse(this_app.gaul0_global_info.filter(function (d) { return d.adm0_code == id })[0].bbox)
                    .coordinates[0];
                console.log(sel_bbox_coordinates)

                map.fitBounds([
                    sel_bbox_coordinates[3],
                    sel_bbox_coordinates[1]
                ]);
                $('.svg_opacity_container').show();


                var l = map.getStyle().layers.length;



                if (!map.getLayer('gaul_level_0_background')) {


                    map.addLayer(gaul_level_0_background, map.getStyle().layers[l - 1].id);
                }



                map.setFilter('gaul_level_0_background', ['!=', 'adm0_code', id]);
                map.setPaintProperty('gaul_level_0_background', 'fill-opacity', 1);




                for (var p in app_data.layers_info) {
                    var l = app_data.layers_info[p];
                    if (ls.indexOf(l.id) > -1) {
                        l.active_filters[2]['admin_filter'] = ['in', 'adm0_code', id];
                        if (map.getLayer(l.id)) {

                            var all_filters = ["all"]

                            for (var p in l.active_filters) {
                                var key = Object.keys(l.active_filters[p]);
                                if (l.active_filters[p][key]) {
                                    all_filters.push(l.active_filters[p][key])
                                }

                            }
                            map.setFilter(l.id, all_filters)
                            if (map.getLayer(l.id + '_labels'))
                                map.setFilter(l.id + '_labels', all_filters);
                        }

                    }
                }
                $(e.target).text('Reset');
            }
            else {

                this_app.active_admin_filter.code_name = null;
                this_app.active_admin_filter.code_val = null;

                this_app.set_raster_cql(app_data.layers_info.filter(function (d) {
                    return d.id == 'schools';
                })[0]
                )

                for (var p in app_data.layers_info) {
                    var l = app_data.layers_info[p];
                    if (ls.indexOf(l.id) > -1) {
                        l.active_filters[2]['admin_filter'] = null;
                        if (map.getLayer(l.id)) {

                            var all_filters = ["all"]

                            for (var p in l.active_filters) {
                                var key = Object.keys(l.active_filters[p]);
                                if (l.active_filters[p][key]) {
                                    all_filters.push(l.active_filters[p][key])
                                }

                            }
                            map.setFilter(l.id, all_filters);

                            if (map.getLayer(l.id + '_labels'))
                                map.setFilter(l.id + '_labels', all_filters);
                        }

                    }
                }
                $(e.target).text('Focus');

                if (map.getLayer('gaul_level_0_background')) {
                    map.setPaintProperty('gaul_level_0_background', 'fill-opacity', 0);
                    map.removeLayer('gaul_level_0_background');
                }
            }
            $(e.target).toggleClass('on');


        }


    })






    $('.explore_countries').click(function () {

        $('#popper-popup-countries').hide();
        if ($(this).hasClass('on') == false) {
            $('.tooltiped', '.explore_countries_menu').hide();
            if ($('.from_all_admin_simple_analysis').is(':visible')) {
                $('.from_all_admin_simple_analysis .close_popup').trigger('click');
            }
            $('.explore_countries_menu .tooltiped').tooltip_materialize()
            if ($('.explore_countries_menu select option').length == 1) {
                var focus_opacity_slider = document.getElementById('focus_opacity_slider');
                noUiSlider.create(focus_opacity_slider, {
                    animate: true,
                    start: [.7],
                    tooltips: true, // no tooltip
                    behaviour: 'hover-snap',
                    direction: 'ltr',
                    connect: true,
                    step: 0.1,
                    range: {
                        'min': 0,
                        'max': 1
                    }

                });

                var focus_opacity_slider_fx = _.throttle(set_focus_opacity_slider_interval, 1000);

                function set_focus_opacity_slider_interval() {


                    map.setPaintProperty('gaul_level_0_background', 'fill-opacity', this_app.temp_focus_opacity);
                    $('.svg_opacity_container .hover-val').text((this_app.temp_focus_opacity * 100) + ' %')

                }
                focus_opacity_slider.noUiSlider.on('update', function (value) {
                    console.info(value)
                    console.warn(+value[0])
                    this_app.temp_focus_opacity = +value[0];
                    if (map.getLayer('gaul_level_0_background'))
                        focus_opacity_slider_fx();
                });


                this_app.get_ajax_calls.getCountries_select().then(function (d) {


                    console.log('this_app.get_ajax_calls.getCountries_select')

                    var sel_bboxes = d.map(function (item) {
                        return {
                            'adm0_code': item.adm0_code,
                            'bbox': item.bbox,
                        }
                    })

                    var options = '';
                    for (var p in d) {

                        var d2 = d[p];
                        options += "<option value='" + d2.adm0_code + "' _code='" + d2.adm0_code + "'>" + d2.adm0_name + "</option>";
                    }
                    console.warn(options)
                    var $select = $('.explore_countries_menu select');
                    $select.append(options);

                    var y = $('.mapboxgl-ctrl-top-right').outerHeight() + $('.mapboxgl-ctrl-top-right').height() + 30;
                    $('.explore_countries_menu').css('top', y + 'px');
                    setTimeout(function () {
                        $('.explore_countries_menu').show();
                    }, 500)

                    $select.on('change', function () {
                        $('.extend_stats',
                            '.explore_countries_menu').hide();

                        $('.focus_container').show();

                        var id = $(this).find('option:selected').attr('_code');


                        var sel_bbox = JSON.parse(this_app.gaul0_global_info.filter(function (d) { return d.adm0_code == id })[0].bbox);

                        if (!this_app.all_adm_tables) {
                            //if (!this_app.d3_stuff.countries_results) {
                            this_app.get_ajax_calls.getAdm_tables({
                                'adm0_code': id,
                                'to_query': 'adm0'
                            }).then(function (d) {

                                //this_app.d3_stuff.countries_results = d.data_arr;
                                this_app.all_adm_tables = d;


                                this_app.fx.fillCountry_stats_lateralBar(id);
                            });
                        }


                        else {
                            this_app.fx.fillCountry_stats_lateralBar(id);
                        }


                        $('.explore_countries_menu .tooltiped,.btn.set_explore_country').show();
                        $('.btn.set_explore_country').text('Focus').removeClass('on');




                    });



                })
            }
            $('.explore_countries_menu').show();
            $(this).addClass('on');
            $('.explore_countries_menu').removeClass('active');
        } else {
            $('.explore_countries_menu').hide();
            $(this).removeClass('on');
            $('.explore_countries_menu').removeClass('active');

        }
    });


    setup_layer_styling();
    this_app.measuring_line = false;




    $('.distance_tool').on('click', function () {
        console.warn(this_app.measuring_line)
        if ($(this).hasClass('on')) {
            console.log('remove prev lines')
            this_app.measuring_line = false;
            line_geojson.features = [];
            linestring.coordinates = [];
            map.getSource('line_geojson_source').setData(line_geojson);
            if (map.getLayer('measure-points')) {

            }

            $('.distance-container').hide();

            map.getCanvas().style.cursor = 'pointer';
            $(this).removeClass('on');
        } else {
            this_app.measuring_line = true;
            $(this).addClass('on');
            map.moveLayer('measure-points');
            map.moveLayer('measure-lines');

        }
    });

    $('.print_map').on('click', function () {

        $('.mapboxgl-control-container').hide();

        if ($('.analysis_container').is(':visible')) {
            var vis_analysis_container = true;
            var mapCanvas = document.getElementsByClassName('mapboxgl-canvas')[0];
            var mapDiv = document.getElementById('map');
        }


        html2canvas($("#map")[0]).then(canvas => {
            console.warn('printing map')
            console.warn(canvas)
            var imgageData = canvas.toDataURL("image/png");
            console.info(this_app.print_on_pdf)
            var newData = imgageData.replace(/^data:image\/png/, "data:application/octet-stream");

            if (this_app.print_on_pdf == false) {
                $("<a>", {
                    href: newData,
                    download: "ceat_print.png",
                    on: {
                        click: function () {
                            $(this).remove()
                        }
                    }
                })
                    .appendTo("body")[0].click();
            } else {

                if (this_app.this_pdf.with_map) {
                    console.info(this_app.this_pdf.content)
                    for (var p in this_app.this_pdf.content) {

                        if (this_app.this_pdf.content[p].map)
                            this_app.this_pdf.content.splice(p, 1);

                    }
                    console.info(this_app.this_pdf.content)

                }
                console.log(newData)
                pdfMake.pageLayout = {
                    height: 792,
                    width: 612,
                    margins: [40, 20, 40, 20]
                }

                this_app.this_pdf.pageMargins = pdfMake.pageLayout.margins;
                this_app.this_pdf.content.push({

                    width: (pdfMake.pageLayout.width - pdfMake.pageLayout.margins[0] -
                        pdfMake.pageLayout.margins[2]),
                    image: newData,
                    map: true
                });

                this_app.this_pdf.with_map = true;

                $('.mapboxgl-popup.after_analysis_popup,.mapboxgl-control-container').show();

                console.log(JSON.stringify(this_app.this_pdf))

                setTimeout(function () {


                    pdfMake.createPdf(this_app.this_pdf).download();
                    $("body").busyLoad("hide", this_app.generating_file_busy_option);
                    this_app.print_on_pdf = false;


                    if (vis_analysis_container == true)
                        $('.analysis_container').show();
                }, 2000)
            }

        })
        $('.mapboxgl-control-container').show();
    });

    var distanceContainer = document.getElementById('distance');
    var line_geojson = {
        'type': 'FeatureCollection',
        'features': []
    };
    var linestring = {
        'type': 'Feature',
        'geometry': {
            'type': 'LineString',
            'coordinates': []
        }
    };



    $('.activate_dist_grid').click(function () {
        if ($(this).hasClass('on')) {
            $(this).text('Visualize distance to grid');
            map.removeLayer('sel_hospitals_grid_dist_lines');
            map.getSource('sel_hospitals_grid_dist_lines_source').setData(null);

        } else {
            var features = this_app.hospitals_dist_grid_data.geo[0];

            map.getSource('sel_hospitals_grid_dist_lines_source').setData(features);


            if (!map.getLayer('sel_hospitals_grid_dist_lines')) {

                map.addLayer(sel_hospitals_grid_dist_lines)
            }

            map.setPaintProperty(
                'sel_hospitals_grid_dist_lines',
                'line-opacity', 1);
            $(this).text('Hide distance to grid')
        }
        $(this).toggleClass('on')
    })

    function plot_adm_analysis(params) {

        console.log(arguments)


        $('body').busyLoad("show", this_app.calculation_busy_option);

        $('.analysis_container').empty();

        $.ajax({
            //url: 'https://hsm.land.copernicus.eu/php/akp_energy/intersects_analysis_download_test_new_taf4_new.php',
            url: 'https://hsm.land.copernicus.eu/php/akp_energy/intersects_analysis_download_test_new_taf4_new.php',

            type: 'GET',

            data: params,

            dataType: 'json',
            success: function (response) {
                console.log(response)

                if (response.counts == 0) {
                    alert('It seems there is nothing here!')
                    return false
                }
                this_app.sel_f_admin_extended = response;
                var adm_code_value = params.adm_code_value;

                this_app.sel_f_admin_extended.setFilter_data = [{
                    layer_name: 'hospitals'
                }, {
                    layer_name: 'power_plants'
                },
                {
                    layer_name: 'refugee_camps'
                },
                {
                    layer_name: 'electric_grid'
                }
                ];


                for (var p in this_app.sel_f_admin_extended.setFilter_data) {
                    this_app.sel_f_admin_extended.setFilter_data[p].adm_filter_activated = false;
                    this_app.sel_f_admin_extended.setFilter_data[p].bbox_filter_activated = true;
                }

                if (params.adm_code_name == 'adm0_code') {
                    var stats_pdf_txt = 'Country statistics';
                    this_app.this_pdf.stats_pdf_style = 'stats_pdf_style_0';
                    var adm_full_text = response['adm0_name'];

                    this_app.sel_f_admin_extended.iso2_code = response.iso2_code;


                    var adm_style = 'adm0_style';
                    var html =
                        `<div class="d-flex">
                        <div class="adm_name flex-grow-1 center">
                            <span class="adm0">
                            ${params.adm0_name}
                            </span>
                            <span class="adm_area">${numberWithCommas(response.adm_area)} km<sup>2</sup></span>
                        </div>

                          <div class="">
                            <div class="row align-items-end">



                                <i class="col bi bi-bar-chart-line-fill to_stats_modal tooltiped" data-bs-toggle="tooltip" data-bs-placement="bottom" aria-label="Compare to other administrative units" data-bs-original-title="Compare to other administrative units"></i>

                                <i class="col bi bi-x-lg close_analysis_container"></i>
                                <i class="col bi bi-fullscreen expand_analysis_container"></i>
                                <i class="col bi bi-filetype-pdf print_pdf"></i>

                                </div>
                            </div>
                          </div>


                        `

                    $('.analysis_container').append(html)

                }

                if (params.adm_code_name == 'adm1_code') {
                    var adm_full_text = response.adm0_name + '/' + response.adm1_name;
                    this_app.this_pdf.stats_pdf_style = 'stats_pdf_style_1';

                    var stats_pdf_txt = 'Adm. Units level 1 statistics for ' + response.adm0_name;
                    var adm_style = 'adm1_style';

                    var html =
                        `<div class="d-flex">
                    <div class="adm_name flex-grow-1 center">
                        `+
                        '<span class="adm0">' + response.adm0_name + '</span> / <span class="adm1">' + response.adm1_name + '</span>'
                        + `
                        </span>
                        <span class="adm_area">${numberWithCommas(response.adm_area)} km<sup>2</sup></span>
                    </div>

                      <div class="">
                        <div class="row align-items-end">



                            <i class="col bi bi-bar-chart-line-fill to_stats_modal tooltiped" data-bs-toggle="tooltip" data-bs-placement="bottom" aria-label="Compare to other administrative units" data-bs-original-title="Compare to other administrative units"></i>

                            <i class="col bi bi-x-lg close_analysis_container"></i>
                            <i class="col bi bi-fullscreen expand_analysis_container"></i>
                            <i class="col bi bi-filetype-pdf print_pdf"></i>

                            </div>
                        </div>
                      </div>`



                    /*  var html = '<div class="adm_name center row"><div class="col-9"><span class="adm0">' + this_app.sel_f_admin_extended.adm0_name + '</span> / <span class="adm1">' + this_app.sel_f_admin.adm1_name + '</span> <span class="adm_area">' + numberWithCommas(response.adm_area) + ' km<sup>2</sup></span></div><div class="col-3"><i class="material-icons right close_analysis_container" >close</i ><i class="material-icons right expand_analysis_container">fullscreen</i><i class="material-icons right print_pdf">picture_as_pdf</i>' +
                         '</div></div>' */
                    $('.analysis_container').append(html)

                }

                if (params.adm_code_name == 'adm2_code') {
                    var stats_pdf_txt = 'Adm. Units level 2 statistics for ' + response.adm0_name;
                    this_app.this_pdf.stats_pdf_style = 'stats_pdf_style_2';

                    var adm_full_text = response.adm0_name + '/' + response.adm1_name + '/' + response.adm2_name;
                    var adm_style = 'adm2_style';


                    var html = `<div class="d-flex">
                    <div class="adm_name flex-grow-1 center">
                        `+
                        '<span class="adm0">' + response.adm0_name + '</span> / <span class="adm1">' + response.adm1_name + '</span>/ <span class="adm2">' + response.adm2_name + '</span>'
                        + `
                        </span>
                        <span class="adm_area">${numberWithCommas(response.adm_area)} km<sup>2</sup></span>
                    </div>

                      <div class="">
                        <div class="row align-items-end">



                            <i class="col bi bi-bar-chart-line-fill to_stats_modal tooltiped" data-bs-toggle="tooltip" data-bs-placement="bottom" aria-label="Compare to other administrative units" data-bs-original-title="Compare to other administrative units"></i>

                            <i class="col bi bi-x-lg close_analysis_container"></i>
                            <i class="col bi bi-fullscreen expand_analysis_container"></i>
                            <i class="col bi bi-filetype-pdf print_pdf"></i>

                            </div>
                        </div>
                      </div>`

                    /* var html = '<div class="adm_name center row"><div class="col-9"><span class="adm0">' + this_app.sel_f_admin_extended.adm0_name + '</span> / <span class="adm1">' + this_app.sel_f_admin.adm1_name + '</span> / <span class="adm2">' + response.adm2_name + '</span><span class="adm_area">' + numberWithCommas(response.adm_area) + ' km<sup>2</sup></span></div><div class="col-3"> <span class="to_stats_modal">Compare to other Admin. units</span><i class="material-icons right close_analysis_container"></i><i class="material-icons right expand_analysis_container" >fullscreen</i ><i class="material-icons right print_pdf">picture_as_pdf</i>' +
                        '</div></div>' */
                    $('.analysis_container').append(html);


                }

                console.info(this_app)
                this_app.this_pdf.stats_pdf_txt = stats_pdf_txt;
                this_app.this_pdf.adm_style = adm_style;


                this_app.this_pdf.content = [];

                this_app.this_pdf.adm_full_text = adm_full_text;



                $.when(this_app.update_table_analysis(response.hospitals_categories, 'hospitals'), false)
                    .then(this_app.update_table_analysis(response.schools_categories, 'schools', true))
                    .then(
                        this_app.update_table_analysis(response.power_categories, 'power_plants', false)


                    ).then(
                        this_app.update_table_analysis(response.refugees_categories, 'refugee_camps', false)
                    )

                    .then(
                        this_app.update_ghs_analysis(response)
                    )
                    .then(
                        this_app.update_table_analysis(response.electric_grid_categories, 'electric_grid', false)
                    )
                    .then(
                        this_app.update_table_analysis(response, 'taf_layers', false)
                    )
                    //.then(this_app.update_datatables(response))

                    .then(function () {



                        $('.analysis_container .tooltiped').tooltip();


                        $('.analysis_container').unbind('mouseleave');
                        $('.analysis_container').on('mouseleave', function () {
                            map.scrollZoom.disable();
                            setTimeout(function () {
                                map.scrollZoom.enable();
                            }, 3000)
                            console.log('leving')
                        });



                        setTimeout(function () {
                            var mapCanvas = document.getElementsByClassName('mapboxgl-canvas')[0];
                            var mapDiv = document.getElementById('map');
                            $('body').css('overflow-y', 'auto!important');
                            $('.sidenav').css('position', 'absolute');


                            mapDiv.style.height = '70vh';
                            mapCanvas.style.height = '70vh';
                            /* var h = $('nav.ceat_nav').height();
                            app_data.sidenav_css = {
                                top: (h + 10) + 'px',
                                height: $('.sidenav').css('height')
                            } */
                            $('.sidenav').css('max-height', '60vh');
                            $('body').css('overflow-y', 'visible');
                            map.resize();

                            $('#map').addClass('bordered');
                            console.log(mapDiv.style.height + ' is the NEW mapdiv height')


                            $('body').busyLoad("hide");

                            $('.analysis_container').addClass('active_analysis')
                            $('.analysis_container').show();

                            $('html').animate({
                                scrollTop: $(".analysis_container").offset().top
                            }, 2000);

                        }, 1500)

                        $('.analysis_container .card-container').unbind('click');
                        $('.analysis_container .card-container').bind('click', function (e) {
                            var layer_id = $(this).attr('_layer');


                            var t_layer = app_data.layers_info.filter(function (d) {
                                return d.id == layer_id
                            })[0];
                            console.info($(e.target))
                            if ($(e.target).is('input')) {
                                console.info(this_app.sel_f_admin)
                                var code_name = this_app.sel_f_admin_extended['adm_code_name'];
                                var val = this_app.sel_f_admin_extended[code_name];
                                var switcher = $(this).find('.only_inside_pol_data.switch')

                                var status = $(e.target).prop('checked');
                                console.log(t_layer)
                                if (status == false) {

                                    t_layer.active_filters[2]['admin_filter'] = null;


                                    this_app.active_admin_filter.code_name = null;
                                    this_app.active_admin_filter.code_val = null;

                                    for (var p in this_app.sel_f_admin_extended.setFilter_data) {
                                        if (this_app.sel_f_admin_extended.setFilter_data.layer_name == t_layer.id)
                                            this_app.sel_f_admin_extended.setFilter_data[i].adm_filter_activated = false;
                                    }

                                } else {
                                    for (var p in this_app.sel_f_admin_extended.setFilter_data) {
                                        if (this_app.sel_f_admin_extended.setFilter_data.layer_name == t_layer.id)
                                            this_app.sel_f_admin_extended.setFilter_data[p].adm_filter_activated = true;
                                    }


                                    this_app.active_admin_filter.code_name = code_name
                                    this_app.active_admin_filter.code_val = val;

                                    if (t_layer._type !== 'marker') {
                                        t_layer.active_filters[2]['admin_filter'] = ["in", code_name, val];
                                    } else {
                                        t_layer.active_filters[2]['admin_filter'] = { 'code_name': code_name, 'code_val': val }
                                    }


                                }

                                if (!map.getLayer(layer_id)) {
                                    $('.analysis_layer.' + layer_id).find('.highlight').trigger('click');
                                    var all_filters = ["all"]

                                    for (var p in t_layer.active_filters) {
                                        var key = Object.keys(t_layer.active_filters[p]);
                                        if (t_layer.active_filters[p][key]) {
                                            all_filters.push(t_layer.active_filters[p][key])
                                        }

                                    }
                                    console.log(all_filters)

                                    if (!t_layer.marker_join) {


                                        map.setFilter(layer_id, all_filters);


                                    } else {
                                        map.setFilter(layer_id, all_filters);
                                        console.warn('adm val is ' + val);
                                        setTimeout(function () {

                                            console.warn($('.marker.refugee_camps').length)

                                            $('.marker.refugee_camps').each(function (i, d) {
                                                var adm_codes = $(this).data('adm_codes');
                                                console.warn(adm_codes)
                                                var c = adm_codes[0][code_name];
                                                if (c !== val)
                                                    $(this).hide();

                                            })

                                        }, 2000)
                                    }
                                } else {
                                    var all_filters = ["all"]

                                    for (var p in t_layer.active_filters) {
                                        var key = Object.keys(t_layer.active_filters[p]);
                                        if (t_layer.active_filters[p][key]) {
                                            all_filters.push(t_layer.active_filters[p][key])
                                        }
                                    }
                                    console.log(all_filters)
                                    map.setFilter(layer_id, all_filters)

                                    if (t_layer.marker_join) {

                                        $('.marker.refugee_camps').each(function (i, d) {
                                            var adm_codes = $(this).data('adm_codes');
                                            console.warn(adm_codes)
                                            var c = adm_codes[0][code_name];
                                            if (c !== val)
                                                $(this).hide();

                                        })
                                    }

                                }
                            }
                            if ($(e.target).hasClass('download_excel') == true ||
                                $(e.target).hasClass('download_shp') == true) {


                                if (this_app.queried_bbox_params !== null) {
                                    var params = this_app.queried_bbox_params;
                                } else {
                                    if (this_app.queried_adm_params !== null)
                                        var params = this_app.queried_adm_params

                                }
                                if ($(e.target).hasClass('download_excel')) {
                                    params.req_format = 'xlsx';
                                } else {
                                    params.req_format = 'shp';
                                }
                                params.adm_code_name = this_app.queried_adm_params.to_query + '_code';
                                console.warn(params);
                                params.layer_id = layer_id;
                                $("body").busyLoad("show", this_app.generating_file_busy_option);
                                $.ajax({

                                    // url: 'https://hsm.land.copernicus.eu/php/akp_energy/new_intersects_only_download.php',
                                    //url: 'https://hsm.land.copernicus.eu/php/akp_energy/downloads_new4.php',
                                    url: 'https://hsm.land.copernicus.eu/php/akp_energy/downloads_new4.php',

                                    type: 'GET',

                                    data: params,

                                    dataType: 'json',
                                    success: function (response) {

                                        $("body").busyLoad("hide", this_app.generating_file_busy_option);

                                        console.log('https://hsm.land.copernicus.eu/php/akp_energy/downloads/' + this_app.userid + '/zipped/' + layer_id + '_' + params.adm_code_value + '.zip')




                                        if (response.success == true) {


                                            var a = document.createElement('a');

                                            a.href = 'https://hsm.land.copernicus.eu/php/akp_energy/downloads/' + this_app.userid + '/zipped/' + layer_id + '_' + params.adm_code_value + '.zip';
                                            document.body.appendChild(a);

                                            a.click();
                                            document.body.removeChild(a);
                                        }
                                    }

                                })


                            }
                            if ($(e.target).hasClass('vis_by_param')) {
                                e.preventDefault();
                                console.log($(this))
                                console.warn($(this).find('.info'))
                                $(this).find('.info').show();

                            } else {
                                if ($(e.target).hasClass('close_analysis_params')) {
                                    $(this).find('.info').hide();
                                    $(this).find('.general_info,.card-action').show();
                                }
                                if ($(e.target).is('li')) {
                                    console.log($(e.target).next())

                                    if ($(e.target).next().hasClass('li_info_container') == false) {


                                        console.log('no class')
                                        return false
                                    }
                                    var _class = $(e.target).attr('class');


                                    if ($(this).find('div.' + _class).is(':visible'))
                                        $(this).find('div.' + _class).hide();
                                    else
                                        $(this).find('div.' + _class).show();


                                    if ($('.card.' + layer_name + ' svg.circles_svg.' + _class).length == 0) {

                                        this_app.create_d3_legend_analysis('analysis', layer_id, _class);
                                    }
                                }
                                if ($(e.target).parents().closest('.li_info_container').length > 0) {

                                    console.log('create legend from title in card analysis')
                                    var _class = $(e.target).parents().closest('li').attr('class');

                                    if ($(this).find('div.' + _class).is(':visible')) {
                                        $(this).find('div.' + _class).hide();
                                    } else {
                                        $(this).find('div.' + _class).show();
                                        $(this).find('div.' + _class + ' .d3_legend').show();
                                    }

                                    if ($('.card.' + layer_id + ' svg.circles_svg.' + _class + ' g').length == 0) {

                                        this_app.create_d3_legend_analysis('analysis', layer_id, _class);
                                    }

                                }
                            }
                        })
                    })



            }
        }) //end ajax
    }
    map.on('load', function () {

        mapboxgl.accessToken = 'pk.eyJ1IjoicGVyaWt1dCIsImEiOiJVNzBjMl9FIn0.38swLsSY2ao8E8rU8FYgyw';
        addSources(map);

        map.addLayer(gaul_level_0_all);



        app_data.layers_mouseover_arr = app_data.layers_info.filter(d => d._type === 'circle').map(function (d) { return d.id });

        this_app.d3_stuff = {
            initial_region_style: {
                "stroke": "#ffff",
                "fill": "black",
                "opacity": 0.1,
                'stroke-width': 0.4
            },
            geojson_mask: null,
            projectPoint: function (lon, lat) {
                var point = map.project(new mapboxgl.LngLat(lon, lat));
                this.stream.point(point.x, point.y);
            },
            transform: d3.geoTransform({
                point: projectPoint
            }),
            update: function () {
                console.log(this)

                console.warn('updateing geom')
                if (this.featureElement) {
                    this.featureElement.attr("d", this.path)
                }


            },
            draw_mask: function () {

                var container = map.getCanvasContainer();
                var svg = d3.select('.mapboxgl-canvas-container svg');

                svg.selectAll("path.mask").remove();
                this.path = d3.geoPath().projection(transform);
                console.log(this)
                this.featureElement = svg.selectAll("path.region")
                    .data([this.geojson_mask])
                    .enter()
                    .append("path")
                    .classed('mask', true)



                this.featureElement.attrs(this.initial_region_style);
                this.featureElement.attr("d", this.path);

                this.featureElement.transition().duration(1000)
                    .attr('opacity', .7)
            }

        };











        function checkStatus() {
            console.log('checking status')
            console.log(map.getZoom())
            if (map.getZoom() < 3) {
            }
            if (map.loaded()) {
                console.warn(map.getStyle().layers);
                console.log('ok')
                cancelAnimationFrame(checkStatus);


                $('#map').busyLoad("hide");

            } else {
                console.log('map not loaded??')

            }

        }

        requestAnimationFrame(checkStatus);

        function onSourceData(e) {
            if (e.isSourceLoaded) {
                $('#map').busyLoad("hide")
                cancelAnimationFrame(checkStatus);
            }
        };

        map.on('idle', function () {





            if (!app_data.temp_layer) {
                return false
            }


            if (app_data.temp_layer) // && !map.getLayer('gsn_'+app_data.temp_active_param.id))
            {
                console.log('ok idle')
                app_data.temp_layer = null;
                map.off('sourcedata')
            }


        })



        map.on('styledata', function () {
            if (!app_data || !app_data.temp_layer)
                return false

            console.log('A styledataloading event occurred on layer ' + app_data.temp_layer.id);

            var loaded = false;
            map.on('sourcedata', ({ isSourceLoaded, sourceId }) => {

                if (loaded == true || !app_data.temp_layer)
                    return false;


                var l = map.getStyle().layers.filter(d => d.id == app_data.temp_layer.id)

                var layer_name = l[0].id;

                var this_layer = app_data.layers_info.filter(d => d.id == layer_name)[0];

                if (sourceId == layer_name + "_source") {

                    if (!isSourceLoaded) {





                    }
                    else {
                        console.log('hide')

                        $(".test_centered_toast").empty();
                        var html = '<span>Layer <span class="highlighted">' + this_layer.title + '</span> has been added';
                        if (this_layer._type == 'circle') {
                            html += '<small>Click on a feature to check values</small>'
                        }
                        html += '</span>';
                        $(".test_centered_toast").html(html);

                        $('.test_centered_toast').animate({ 'opacity': 1, 'top': '10%' }, 400)


                        setTimeout(function () {

                            $('.test_centered_toast').animate({ 'opacity': 0, 'top': '0%' }, 400);

                        }, 4500)

                        loaded = true;
                        console.log('ok isSourceLoaded')



                    }
                }

            })

        });

        map.on('sourcedata', onSourceData);

        var loaded = false;

        var general_mousemove_debounced = _.debounce(all_mousemove_debounced, 250, { 'maxWait': 1000 });
        function all_mousemove_debounced(e) {


            var present_layers = map.getStyle().layers.filter(d => app_data.layers_mouseover_arr.includes(d.id))
                .map(function (d) { return d.id });

            console.info(map.queryRenderedFeatures(e.point), present_layers);
            var features = map.queryRenderedFeatures(e.point).filter(d => present_layers.includes(d.layer.id))



            console.log(features,
                features.length == 0)



            if (!features || features.length == 0) {
                if (map.getLayer(layer_id + '_sel'))
                    map.setLayoutProperty(layer_id + '_sel', "visibility", "none");

                return false
            }


            var f = features[0];
            var layer_id = f.layer.id;
            var _id = f.properties.gid ? f.properties.gid : f.properties.id;
            var field = f.properties.gid ? 'gid' : 'id';





            if (_id && map.getLayer(layer_id + '_sel')) {
                map.setFilter(layer_id, ["match", ["get", field], _id, true, false]);
                map.setLayoutProperty(layer_id + '_sel', "visibility", "visible");
            }

        }

        map.on('moveend', function () {


            this_app.update_popup_circle();
        });



        map.on('zoom', function () {
            var zoom = map.getZoom();
            var b_layer = map.getStyle().layers[0];
            if (zoom < 4) {
                map.setPaintProperty(b_layer.id, "raster-opacity", 0);
            } else {
                map.setPaintProperty(b_layer.id, "raster-opacity", 1);
            }

            if (this_app.d3_stuff.geojson_mask) {

                svg.classed("hidden", true);

            }
            if (map.getLayer('a2ei_points')) {
                map.setPaintProperty('a2ei_points', 'circle-radius', this_app.scale_r(map.getZoom()))
            }
        })

        $('ul.hxxide-on-med-and-down li:first').hide();





        var refugee_camps_geojson = {
            'type': 'FeatureCollection',
            'features': []
        };
        map.addSource('line_geojson_source', {
            'type': 'geojson',
            'data': line_geojson
        });
        map.addLayer({
            id: 'measure-points',
            type: 'circle',
            source: 'line_geojson_source',
            paint: {
                'circle-radius': 10,
                'circle-color': '#46ddaa',
                'circle-opacity': 0.5
            },
            filter: ['in', '$type', 'Point']
        });

        map.addLayer({
            id: 'measure-lines',
            type: 'line',
            source: 'line_geojson_source',
            layout: {
                'line-cap': 'round',
                'line-join': 'round'
            },
            paint: {
                'line-color': '#1e9f74',
                'line-width': 3.5
            },
            filter: ['in', '$type', 'LineString']
        });

        var a2ei_geojson = {
            'type': 'FeatureCollection',
            'features': []
        };

        map.addSource('a2ei_geojson_source', {
            'type': 'geojson',
            'data': a2ei_geojson
        });
        this_app.feature_popup = new mapboxgl.Popup({
            closeButton: true,
            closeOnClick: true,
            className: 'feature_map_popup',
            offset: 15
        });

        this_app.after_analysis_popup = new mapboxgl.Popup({
            closeButton: true,
            closeOnClick: false,
            className: 'after_analysis_popup',
            offset: {
                'bottom': [0, -20],
                'top': [0, 15],
                'top-left': [0, 0], //[linearOffset, (markerHeight - markerRadius - linearOffset) * -1],
                'top-right': [0, 0], //[-linearOffset, (markerHeight - markerRadius - linearOffset) * -1],

            }
        });
        var replaceSVGwithCanvas = function (callback) {
            var $container = $('.d3_legend');
            var svgElements = $container.find('svg');
            svgElements.each(function () {
                var canvas, xml;
                canvas = document.createElement("canvas");
                canvas.className = "screenShotTempCanvas";
                xml = (new XMLSerializer()).serializeToString(this);
                xml = xml.replace(/xmlns=\"http:\/\/www\.w3\.org\/2000\/svg\"/, '');
                canvg(canvas, xml);
                $('.canvas_legend').append(canvas)
            });
            callback(); //to be called after the process in finished
        };

        var drawing_pol = null;

        var area_popup = new mapboxgl.Popup({
            className: 'area_popup',
        })


        setTimeout(function () {

            base_layer = generate_level_0('#e6691a', 1);
            base_layer.active = true;
            console.log(base_layer)
            base_layers.push(base_layer);






            $('#baselayers_switcher input').eq(1).trigger('click');
            map.fire('zoom');
            map.resize();

        }, 1000)

        var STYLES_DRAW = [




        ];
        var modes = MapboxDraw.modes;
        modes.draw_rectangle = DrawRectangle.default;

        var draw = new MapboxDraw({

            displayControlsDefault: false,
            controls: {
                polygon: true,
                trash: true
            },
            //styles: STYLES_DRAW
        });
        map.addControl(draw);



        class map_results_control {
            onAdd(map) {
                this.map = map;
                this.container = document.createElement('div');
                this.container.className = 'map_results_control_container';

                return this.container;
            }
            onRemove() {
                this.container.parentNode.removeChild(this.container);
                this.map = undefined;
            }
        }
        var results_control_ctrl = new map_results_control();
        map.addControl(results_control_ctrl, 'bottom-right');
        $('.map_results_control_container').addClass('mapboxgl-ctrl mapboxgl-ctrl-group').html(`
            <div data-position="left" data-tooltip="Show statistics table" class="slide_btn tooltiped">
               <a class="btn-floating btn-small waves-effect waves-light">
                <i class="material-icons">navigate_before</i></a>
            </div>
            <div class="from_all_admin_simple_analysis">
               <div class="row title">
                <div class="col-1"><i data-position="top" data-tooltip="Hide statistics table"  class="tooltiped slide_table bi bi-arrow-bar-up left"></i></div>
            <div class="col-11 ">Administrative statistics</div>
            </div>

            <div class="datatable_legend">
            <span class="info">Choose a parameter to map</span>
            <select autocomplete="off" class="form-select to_map_select">

                                <option disabled value="" selected>Select a feature to map</option>
                                <option value="none">None</option>
                          </select>
            <div class="fullscreen_table row">Extend info</div>
                          </div>
  <table id="from_all_admin_simple_analysis_table" class="dataTable"><thead><tr></tr></thead><tbody></tbody></table>
</div>

            </div>
             `);
        //<div class="row"><span class="download_excell_simple_analysis col-6">Download Excell</span>

        $('.map_results_control_container .fullscreen_table').on('click', function () {



            var params = {
                adm0_code: this_app.map_results_control_container_params.adm0_code,
                adm_code_name: this_app.map_results_control_container_params.adm_code_name,
                /*   adm_code_value: this_app.map_results_control_container.sel_f_admin[code_name], */
                userid: this_app.userid,
                adm_code_value: this_app.map_results_control_container_params.adm_code_value,
                to_query: this_app.map_results_control_container_params.adm_code_name.split('_')[0]
            };
            console.info(params)

            $.ajax({

                //url: 'https://hsm.land.copernicus.eu/php/akp_energy/adm_tables_pdf_info_dev_3_taf.php',
                url: 'https://hsm.land.copernicus.eu/php/akp_energy/adm_tables_pdf_info_dev_3_taf.php',


                type: 'GET',
                async: true,
                data: params,

                dataType: 'json',
                success: function (resp) {


                    console.warn(resp)


                    $('#stats_modal').modal_materialize('open');
                    /* $('#stats_modal .tabs').tabs();//'select', 'main_dataTables_wrapper');
                    $('#stats_modal .tabs').eq(1).show(); */

                    var t = $('#stats_modal table.dataTable')


                    var tabTrigger = new bootstrap.Tab(document.querySelector('#nav-main_dataTables_wrapper-tab'));
                    tabTrigger.show();

                    this_app.plot_modal_datatable(resp);
                    this_app.plot_hc_modal_datatable(resp);








                }
            }) //end ajax
        })

        $('.map_results_control_container .slide_table').on('click', function () {
            $('.from_all_admin_simple_analysis').animate({
                width: "toggle"
            });

            $('.map_results_control_container').addClass('inactive');
            $('.slide_btn').css('display', 'block');
        });
        $('.slide_btn').on('click', function () {
            $('.from_all_admin_simple_analysis').animate({
                width: "toggle"
            });
            $('.map_results_control_container').removeClass('inactive');
            $('.slide_btn').hide()
        });
        $('.map_results_control_container .tooltiped').tooltip_materialize();





        $('.btn.draw_rec_custom').click(function () {





            if ($(this).hasClass('on') == false) {
                $(this).addClass('on');
                if (!map.getLayer('drawn_pol'))
                    map.addLayer(drawn_pol);
                this_app.drawing_rect = true;
                map.setPaintProperty('drawn_pol', 'line-width', 0);
                $('.mapbox-gl-draw_trash').click();
                $('.mapbox-gl-draw_polygon').click();
                setTimeout(function () {
                    draw.changeMode('draw_rectangle');

                }, 300);
            } else {
                $(this).removeClass('on');
                if (map.getLayer('drawn_pol'))
                    map.removeLayer('drawn_pol');
                this_app.drawing_rect = false;
                $('.mapboxgl-popup.area_popup').hide();

            }


        });

        $('.draw_freehand_pol').click(function () {
            this_app.drawing_rect = true;

            $('.mapbox-gl-draw_polygon').click();

        });
        $('.repeat_polygon').click(function () {
            after_creating_pol(this_app.userPolygon);
        })

        $('.clean_custom').click(function () {
            this_app.drawing_rect = false;
            this_app.userPolygon = null;
            this_app.hospitals_dist_grid_data = null;

            $('.analysis_container').empty().hide();

            $('#map').removeClass('bordered');

            var mapCanvas = document.getElementsByClassName('mapboxgl-canvas')[0];
            var mapDiv = document.getElementById('map');
            mapDiv.style.height = '100vh';
            mapCanvas.style.height = '100vh';
            $('body').css('overflow-y', 'hidden');
            $('.sidenav').css('top', '8%');
            $('.sidenav').css('position', 'fixed');
            map.resize();
            $("body,html,document").animate({
                scrollTop: $('.nav-wrapper').height() - 50
            });

            $('.btn.draw_rec_custom').removeClass('on');
            $('.repeat_polygon,.clean_custom').hide();
            $('.analysis_results').hide();
            $('.sidenav').css('height', '89vh');
            map.removeLayer('drawn_pol');


        })



        $('.buffer').click(function () {
            if (!this_app.sel_electric_grid)
                alert('no electric grid')
            else
                create_line_buffer(this_app.sel_electric_grid)
        })



        function create_line_buffer(features) {
            console.log(features);
            var rr_crds = []

            for (i = 0; i < features.features.length; i++) {
                rr_crds.push(features.features[i]);
            }
            console.log(rr_crds)

            var rr_coords = turf.multiLineString(rr_crds);
            console.log(rr_coords)
            var buffer = turf.buffer(features, 10, {
                units: 'kilometers'
            })
            console.info(buffer)
            map.getSource('sel_lines_buffer_source').setData(buffer);

            map.addLayer(sel_grid_buffer);

        }

        function sort_by_value(param) {

            return function (a, b) {
                console.info(b)
                return a.param < b.param ? -1 : 1;
            }

        }



        function all_admin_simple_analysis_datatables(info) {

            $("body").busyLoad("show", this_app.calculation_busy_option);
            console.warn(info)

            //var params = { 'selected_code': this_app.sel_f_admin.selected_code, 'adm0_code': this_app.sel_f_admin['adm0_code'] };
            var params = { 'selected_code': info.selected_code, 'adm0_code': info['adm0_code'] };

            $.ajax({
                //url: 'https://hsm.land.copernicus.eu/php/akp_energy/all_admin_simple_analysis2.php',
                url: 'https://hsm.land.copernicus.eu/php/akp_energy/all_admin_simple_analysis2.php',

                type: 'GET',

                data: params,
                async: true,
                dataType: 'json',
                success: function (resp) {



                    this_app.sel_f_admin.response = resp;
                    console.log(resp)
                    this_app.map_results_control_container_params = {
                        adm0_code: info['adm0_code'],
                        adm_code_value: info[info.selected_code],
                        //ex:adm1_code
                        adm_code_name: info.selected_code,
                        /* adm_code_value: this_app.map_results_control_container.sel_f_admin[code_name], */
                        userid: this_app.userid,

                        //adm0
                        to_query: info.selected_code.split('_')[0]
                    };
                    $('.from_all_admin_simple_analysis .datatable_info').empty();
                    var headers = [{
                        code: 'country',
                        text: 'Name',
                        short: 'Name',
                        i: 0
                    },
                    {
                        code: 'schools',
                        text: 'Schools',
                        short: 'Schools',
                        i: 1
                    },
                    {
                        code: 'power_plants',
                        text: 'Power plants',
                        short: 'P.P',
                        i: 2
                    },
                    {
                        code: 'hc_in_grid',
                        text: 'In-grid health centres',
                        short: 'IN H.C',
                        i: 3
                    },
                    {
                        code: 'hc_out_grid',
                        text: 'Out-grid health centres',
                        short: 'OUT H.C',
                        i: 4
                    },
                    {
                        code: 'ref_settlements',
                        text: 'Refugee settlements',
                        short: 'R.S',
                        i: 5
                    },
                    {
                        code: 'electric_grid',
                        text: 'Electric grid (km)',
                        short: 'EL.G',
                        i: 6
                    },
                    {
                        code: 'density',
                        text: 'Density (Hab/sqKm)',
                        short: 'DENSITY',
                        i: 7
                    }
                    ];

                    if ($('.mapboxgl-ctrl-bottom-right').hasClass('with_table') == false) {
                        if (!this_app.gradients) {
                            this_app.gradients = [
                                {
                                    layer_name: 'schools',
                                    color_range: ['#fdc830', '#f37335'],
                                    color_label: '#3d3e40',
                                    color_border: 'white',
                                    map_label: '#3d3e40'
                                },
                                {
                                    layer_name: 'power_plants',
                                    color_range: ['#fdc830', '#f37335'],
                                    color_label: '#3d3e40',
                                    color_border: 'white',
                                    map_label: '#3d3e40'
                                },
                                {
                                    layer_name: 'ref_settlements',
                                    color_range: ['#f3904f', '#3b4371'],
                                    color_label: '#fcfffd',
                                    color_border: 'white',
                                    map_label: '#fcfffd'
                                },
                                {
                                    layer_name: 'hc_out_grid',
                                    color_range: ['#71b280', '#134e5e'],
                                    color_label: '#d7e0da',
                                    color_border: 'white',
                                    map_label: '#d7e0da'
                                },
                                {
                                    layer_name: 'hc_in_grid',
                                    color_range: ['#44a08d', '#093637'],
                                    color_label: 'white',
                                    color_border: 'white',
                                    map_label: 'white'
                                },
                                {
                                    layer_name: 'density',
                                    color_range: ["#fced97", '#f53722'],
                                    color_label: 'black',

                                    color_border: 'black',
                                    map_label: 'black'
                                },
                                {
                                    layer_name: 'electric_grid',
                                    color_range: ['#4776e6', '#8e54e9'],
                                    color_label: 'white',
                                    color_border: 'white',
                                    map_label: 'white'
                                }

                            ];
                        }
                        $('.mapboxgl-ctrl-bottom-right').addClass('with_table');

                        var header_html = '';

                        var info_html_new = '<div class="maps_container">';
                        var select_html = '';
                        for (var p in headers) {
                            var t = headers[p];

                            if (t.code !== 'country')
                                var color_range = this_app.gradients.filter(d => d.layer_name == t.code)[0].color_range;

                            headers
                            if (p == 0) {
                                header_html += '<th class="adm_th"><span>' + t.short + '</span></th>';
                            } else {


                                select_html += `<option value="${t.code}">${t.text}</option>`;

                                info_html_new += `<div style="" class="map_legend_container ${t.code}">
                                <div class="row g-0">
                                  <div class="min col-sm-3 col-md-3"></div>
                                  <div class="col-sm-6 col-md-6"></div>
                                  <div class="max col-sm-3 col-md-3"></div>
                                </div>`
                                info_html_new += `<div class="row g-0 gradient grad_0" style="background: linear-gradient(90deg, ${color_range[0]} 0%, ${color_range[1]} 100%);"></div>`;




                                info_html_new += '<div class="row g-0" style="margin-top: 1rem;">';

                                info_html_new += `<div class="col-sm-6 col-md-6 map_table_interaction">
                                <form autocomplete="off">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                                        <label class="form-check-label" for="flexCheckDefault">
                                                        Map/table interaction
                                                        </label>
                                                    </div>
                                                    </form>
                                                </div>`;
                                info_html_new += `<div class="col-sm-6 col-md-6 map_table_circles">
                                <form autocomplete="off">
                                                    <div class="form-check">
                                                            <input class="form-check-input" type="checkbox" value="" id="map_table_circles_${p}">
                                                            <label class="form-check-label" for="map_table_circles_${p}">
                                                            Plot info in circle
                                                            </label>
                                                    </div>
                                                    </form>
                                                </div>`;


                                info_html_new += '</div>';

                                //map_legend_container
                                info_html_new += '</div>';



                            }


                        }

                        //maps_container
                        info_html_new += '</div>';
                        info_html_new += `<div class="custom-toolbar">
                        <input type="text"  class="find-input form-control bs-find" placeholder="Search a feature in the table"/>

                        <i class="pdf-button bi bi-file-earmark-pdf-fill"></i>
                        </div>`;

                        if ($('.datatable_legend .maps_container').length == 0) {

                            $('.datatable_legend').append(info_html_new);
                            $('.datatable_legend select.to_map_select').append(select_html);




                        }
                        console.warn(info_html_new)

                        //$('.from_all_admin_simple_analysis table thead tr').append(header_html);





                    } else {


                        var table = $('#from_all_admin_simple_analysis_table').DataTable();
                        $('.map_results_control_container').show();

                        table.destroy();

                        //  $('.from_all_admin_simple_analysis table.dataTable tbody').empty();
                    }
                    var headers_arr = headers.map(d => d.code)




                    // $('.from_all_admin_simple_analysis table thead th').show()
                    var adm_name_code = this_app.sel_f_admin.selected_code.split('_code')[0] + '_name';
                    switch (this_app.sel_f_admin.selected_code) {

                        case 'adm0_code':
                            var adm_name_code = "adm0_name";
                            var txt = "Statistics per country";
                            break;
                        case 'adm1_code':
                            var adm_name_code = "adm1_name";
                            var txt = "<div class='title'>Statistics per Adm. Unit level 1 in <span>" + resp['data'][0]['adm0_name'] + "</span></div>";
                            break;
                        case 'adm2_code':
                            var adm_name_code = "adm2_name";
                            var txt = "<div class='title'>Statistics per Adm. Unit level 2 in <span>" + resp['data'][0]['adm0_name'] + "</span></div>";
                            break;
                        default:
                            break;
                    }
                    $('.from_all_admin_simple_analysis .title .s10').html(txt);


                    setTimeout(function () {
                        map.setPaintProperty(this_app.sel_f_admin.highlight_params.highlight_layer.id, "fill-opacity", 0);

                    }, 1200)


                    function sort_by_index(param) {

                        return function (a, b) {
                            return b[param] > a[param] ? -1 : 1;
                        }
                    }


                    var sorted = resp['data'];
                    console.warn(sorted)

                    var l = sorted.length
                    //var h = $('#map').height() - 80;
                    this_app.data_table_obj = $('#from_all_admin_simple_analysis_table').DataTable({
                        //  scrollY: h + 'px',
                        /* height: '800px', */
                        scrollY: 'calc(-350px + 100vh)',
                        scrollX: true,
                        dom: 'Bfrtip',

                        scrollCollapse: true,
                        paging: false,
                        fixedColumns: false,
                        fixedHeader: true,
                        data: sorted,

                        buttons: [

                            {
                                extend: 'pdfHtml5',
                                text: 'Export to PDF',
                                className: 'd-none', // Hide the button from DataTables default toolbar
                                customize: function (doc) {
                                    doc.defaultStyle = {
                                        font: 'OpenSans',
                                        bold: false // Disable bold text
                                    };

                                    // Remove all bold text explicitly
                                    if (doc.styles) {
                                        for (var key in doc.styles) {
                                            if (doc.styles[key].bold) {
                                                doc.styles[key].bold = false;
                                            }
                                        }
                                    }
                                }
                            }
                            /* 'copy',       // Copy to clipboard
                            'csv',        // Download as CSV
                            'excel',      // Download as Excel
                            'pdf',        // Download as PDF
                            'print'  */      // Print view
                        ],

                        columnDefs: [
                            { width: '25%', targets: 0 }
                        ],
                        columns: [
                            {
                                title: 'Name',
                                data: adm_name_code,
                                className: 'adm_td',
                                orderable: true,
                                'render': function (data, type, row) {
                                    var h = '<div class="row" new_code="' + row[this_app.sel_f_admin.selected_code] + '"><span class="col-10 d-flex align-items-center">' + row[adm_name_code] + '</span><span class="col-2 d-flex align-items-center"><i class="bi adm_td_switch bi-three-dots-vertical"></i></span></div>';
                                    h += '<div class="row adm_td_actions"><span class="col col-2"><i data-position="left" data-tooltip="Zoom to" class="bi right adm_td_zoom_in bi-zoom-in"></i></span><span class="col col-2"><i data-position="left" data-tooltip="Expand feature information"  class="bi right adm_td_view_table bi-list-columns-reverse"></i></span><span class="col col-2"><i data-position="left" data-tooltip="Highlight on map"  class="bi right adm_td_highlight_table bi-lightbulb-fill"></i></span></div>';
                                    return h

                                }


                            },
                            {
                                title: 'Schools',
                                data: 'schools',
                                className: 'schools',
                                orderable: true,

                                'render': function (data, type, row) {


                                    return numberWithCommas(row.counts.schools)
                                }

                            },
                            {
                                title: 'Power plants',
                                data: 'power_plants',
                                className: 'power_plants',
                                orderable: true,

                                'render': function (data, type, row) {


                                    return numberWithCommas(row.counts.power_plants)
                                }

                            }, {
                                title: 'In-grid health centres',
                                data: 'hc_in_grid',
                                className: 'hc_in_grid',
                                orderable: true,
                                'render': function (data, type, row) {


                                    return numberWithCommas(row.counts.hospitals.in_grid_hc_counts)
                                }

                            }, {
                                title: 'Out-grid health centres',
                                data: 'hc_out_grid',
                                className: 'hc_out_grid',
                                orderable: true,
                                'render': function (data, type, row) {


                                    return numberWithCommas(row.counts.hospitals.out_grid_hc_counts)
                                }

                            },
                            {
                                title: 'Refugee settlements',
                                data: 'ref_settlements',
                                className: 'ref_settlements',
                                orderable: true,
                                'render': function (data, type, row) {


                                    return numberWithCommas(row.counts.refugee_camps)
                                }

                            },
                            {
                                title: 'Electric grid (km)',
                                data: 'electric_grid',
                                className: 'electric_grid',
                                orderable: true,
                                'render': function (data, type, row) {


                                    return numberWithCommas(row.counts.electric_grid)
                                }


                            },
                            {
                                title: 'Density',
                                data: 'density',
                                className: 'density',
                                orderable: true,
                                'render': function (data, type, row) {


                                    return row.counts.density
                                }


                            }
                        ]
                    });

                    window.dispatchEvent(new Event('resize'));
                    //  $('.from_all_admin_simple_analysis .dataTables_scrollHeadInner').width($('.from_all_admin_simple_analysis .dataTables_wrapper').width())

                    var h = $('.print_and_layers_container').outerHeight() + 10 + 'px';
                    console.warn('height is ' + h)

                    setTimeout(function () {

                        $('.pdf-button', '.custom-toolbar').on('click', function () {

                            var table = $('#from_all_admin_simple_analysis_table').DataTable();
                            table.button('.buttons-pdf').trigger();
                        });

                        // Add custom search functionality
                        $('.find-input', '.custom-toolbar').on('keyup', function () {
                            var table = $('#from_all_admin_simple_analysis_table').DataTable();
                            table.search(this.value).draw();
                        });

                    }, 1500)

                    $('.mapboxgl-ctrl-bottom-right.with_table').css('top', h);


                    $('.datatable_legend select.to_map_select').on('change', function () {

                        var layer = $(this).val();

                        $('.maps_container >div').hide();
                        $('.maps_container >div input:checked').prop('checked', '');

                        $('.animated-icon', '#map').remove();

                        if (map.getLayer('gaul_level_0_analysis')) {
                            console.warn('removing gaul level 0 analysis')
                            map.removeLayer('gaul_level_0_analysis')
                        }


                        if (map.getLayer('gaul_level_1_analysis'))
                            map.removeLayer('gaul_level_1_analysis')

                        if (map.getLayer('gaul_level_2_analysis'))
                            map.removeLayer('gaul_level_2_analysis')


                        if (layer == 'none') {



                            return false;
                        }

                        //this_app.gradients.filter(d => d.layer_name == val)[0];
                        this_app.active_hover = { code: this_app.sel_f_admin.selected_code, layer: layer };

                        $('.map_legend_container.' + layer).show();
                        var obj = {
                            total_f: 0,
                            min: 0,
                            max: 0,
                            adm_code_name: this_app.sel_f_admin.selected_code,
                            'feature': layer,
                            data: []
                        };
                        var ordered_rows = [];
                        this_app.markers_obj_arr = [];
                        this_app.markers_obj_arr_ids = [];

                        this_app.data_table_obj.rows().every(function (rowIdx, tableLoop, rowLoop) {
                            console.log(arguments)

                            console.info(sorted[rowIdx])
                            if (layer == 'hc_in_grid' || layer == 'hc_out_grid' || layer == 'ref_settlements') {
                                if (layer == 'ref_settlements') {
                                    var val = this.data().counts.refugee_camps;
                                }

                                if (layer == 'hc_in_grid') {
                                    var val = this.data().counts.hospitals['in_grid_hc_counts'];
                                }
                                if (layer == 'hc_out_grid') {
                                    var val = this.data().counts.hospitals['out_grid_hc_counts'];
                                }
                            } else {
                                var val = this.data().counts[layer]
                            }

                            if (!val) {
                                var val = 0;
                            }

                            var n = this_app.sel_f_admin.selected_code.split('_')[0] + '_name';
                            var name = sorted[rowIdx][n];
                            if (val > 0) {
                                this_app.markers_obj_arr_ids.push(sorted[rowIdx][this_app.sel_f_admin.selected_code]);
                                this_app.markers_obj_arr.push({
                                    centroid: [sorted[rowIdx].lng, sorted[rowIdx].lat],
                                    code: sorted[rowIdx][this_app.sel_f_admin.selected_code],
                                    value: val
                                });
                                obj.data.push({ name: name, code: this.data()[obj.adm_code_name], val: val });
                            }

                        });




                        var vals_arr = obj.data.map(d => d.val).sort((a, b) => a - b)


                        obj.min = Math.min.apply(Math, vals_arr);
                        obj.max = Math.max.apply(Math, vals_arr);
                        $('.map_legend_container.' + layer).find('.min').html('<span>Min: ' + numberWithCommas(obj.min) + '</span>');
                        $('.map_legend_container.' + layer).find('.max').html('<span>Max: ' + numberWithCommas(obj.max) + '</span>');

                        obj.total_f = vals_arr.length;
                        console.log(obj)
                        var this_gradients_i = this_app.gradients.filter(d => d.layer_name == layer)[0];
                        var color_range = this_gradients_i.color_range;


                        if (layer == 'schools') {


                            var _scale = d3.scaleLinear()
                                .domain([obj.min, obj.max])
                                .range(color_range);
                        }
                        if (layer == 'power_plants') {


                            var _scale = d3.scaleLinear()
                                .domain([obj.min, obj.max])
                                .range(color_range);
                        }
                        if (layer == 'ref_settlements') {


                            var _scale = d3.scaleLinear()
                                .domain([obj.min, obj.max])
                                .range(color_range);
                        }

                        if (layer == 'hc_out_grid') {

                            var _scale = d3.scaleLinear()
                                .domain([obj.min, obj.max])
                                .range(color_range);
                        }

                        if (layer == 'hc_in_grid') {

                            var _scale = d3.scaleLinear()
                                .domain([obj.min, obj.max])
                                .range(color_range);
                        }

                        if (layer == 'density') {


                            var _scale = d3.scaleLinear()
                                .domain([obj.min, obj.max])
                                .range(color_range);
                        }


                        if (layer == 'electric_grid') {

                            var _scale = d3.scaleLinear()
                                .domain([obj.min, obj.max])
                                .range(color_range);
                        }

                        this_app.markers_obj_arr_scale = _scale;
                        var rscale = d3.scaleLinear()
                            .domain([obj.min, obj.max])
                            .range([35, 60])

                        this_app.markers_obj_arr_rscale = rscale;


                        /*  if (prev_circles == true) {
                             $('.map_legend_container.' + layer).find('.map_table_circles input').trigger('click');
                         } */
                        var arr = [];
                        this_app.from_all_admin_simple_analysis_reduced = { layer_name: layer, data: [] }

                        var red = obj.data.filter(d => d.val > 0).reduce(function (memo, feature) {

                            if (arr.indexOf(feature.code) == -1) {


                                arr.push(feature.code);

                                this_app.from_all_admin_simple_analysis_reduced.data.push({
                                    name: feature.name,
                                    code: feature.code,
                                    value: feature.val,
                                    color: obj.max === 0 ? color_range[0] : _scale(feature.val)


                                })
                                memo.opacity.push(feature.code, 1)
                                memo.color.push(feature.code, obj.max === 0 ? color_range[0] : _scale(feature.val))

                            }
                            return memo;

                        }, {

                            color: ["match", ["get", obj.adm_code_name]],
                            opacity: ["match", ["get", obj.adm_code_name]]

                        });




                        var temp = this_app.from_all_admin_simple_analysis_reduced.data.sort((a, b) => (a.value < b.value) ? 1 : -1);
                        console.log(temp)
                        this_app.from_all_admin_simple_analysis_reduced.data = temp.map(function (d, i) {
                            d.position = i + 1;
                            return d;
                        })


                        console.info(this_app.from_all_admin_simple_analysis_reduced)
                        red.opacity.push(0);
                        red.color.push('#222423');
                        console.log(JSON.stringify(red))







                        if (obj.adm_code_name == 'adm0_code') {

                            map.setPaintProperty('gaul_0_labels', 'text-color', this_gradients_i.map_label);
                            var gaul_analysis = 'gaul_level_0_analysis';
                            map.addLayer(gaul_level_0_analysis, 'gaul_level_0_line')
                        }
                        if (obj.adm_code_name == 'adm1_code') {
                            var gaul_analysis = 'gaul_level_1_analysis';
                            //  map.addLayer(gaul_level_1_analysis, 'gaul_level_1');
                            map.addLayer(gaul_level_1_analysis, 'gaul_level_0_line');
                            map.setPaintProperty('gaul_1_labels', 'text-color', this_gradients_i.map_label);
                        }
                        if (obj.adm_code_name == 'adm2_code') {
                            var gaul_analysis = 'gaul_level_2_analysis';
                            //map.addLayer(gaul_level_2_analysis, 'gaul_level_2');
                            map.addLayer(gaul_level_2_analysis, 'gaul_level_0_line');
                            map.setPaintProperty('gaul_2_labels', 'text-color', this_gradients_i.map_label);
                        }
                        console.log(gaul_analysis)
                        if (this_app.from_all_admin_simple_analysis_reduced.data.length > 0) {
                            map.setPaintProperty(gaul_analysis, "fill-opacity", red.opacity);
                            map.setPaintProperty(gaul_analysis, "fill-color", red.color);
                            console.log(gaul_analysis)
                        } else {
                            map.setPaintProperty(gaul_analysis, "fill-opacity", 0);
                        }



                        $('#map').busyLoad("hide");
                    })


                    $('.datatable_legend i.bi').unbind('click')
                    $('.datatable_legend i.bi').on('click', function (e) {

                        var parent = $(this).parents().closest('.param_container');
                        var pos = parent.attr('index');
                        var layer = parent.attr('_layer');

                        var t = $(e.target);


                        if (t.hasClass('map_it')) {
                            var $vis = $('.datatable_legend .map_legend_container:visible')

                            if ($vis.length > 0) {


                                $vis.hide();
                                $vis.find('input:checked').prop('checked', '');

                            }



                            if (!t.hasClass('active')) {
                                $('#map').busyLoad("show");
                                map.off('mousemove', debounced);
                                $('.analysis_hover_info').hide();
                                $('.animated-icon').remove();

                                $('.datatable_legend').find('i.active').removeClass('active');
                                this_app.active_hover = { code: this_app.sel_f_admin.selected_code, layer: layer };
                                console.log($('.map_legend_container:visible'))
                                var circles_input = $('.map_legend_container:visible').find('.map_table_circles input:checked');
                                if (circles_input.length > 0) {
                                    var prev_circles = true;
                                    circles_input.prop('checked', '');
                                } else {
                                    var prev_circles = false;
                                }


                                var labels_input = $('.map_legend_container:visible').find('.map_table_labels input:checked');
                                if (labels_input.length > 0) {
                                    var prev_labels = true;
                                    labels_input.prop('checked', '');
                                } else {
                                    var prev_labels = false;
                                }

                                $('.map_legend_container').hide();
                                $('.datatable_legend .map_it').removeClass('active');
                                t.addClass('active');
                                parent.parent().find('.map_legend_container').show()

                                var obj = {
                                    total_f: 0,
                                    min: 0,
                                    max: 0,
                                    adm_code_name: this_app.sel_f_admin.selected_code,
                                    'feature': layer,
                                    data: []
                                };
                                var ordered_rows = [];
                                this_app.markers_obj_arr = [];
                                this_app.markers_obj_arr_ids = [];

                                this_app.data_table_obj.rows().every(function (rowIdx, tableLoop, rowLoop) {
                                    console.log(arguments)

                                    console.info(sorted[rowIdx])
                                    if (layer == 'hc_in_grid' || layer == 'hc_out_grid' || layer == 'ref_settlements') {
                                        if (layer == 'ref_settlements') {
                                            var val = this.data().counts.refugee_camps;
                                        }

                                        if (layer == 'hc_in_grid') {
                                            var val = this.data().counts.hospitals['in_grid_hc_counts'];
                                        }
                                        if (layer == 'hc_out_grid') {
                                            var val = this.data().counts.hospitals['out_grid_hc_counts'];
                                        }
                                    } else {
                                        var val = this.data().counts[layer]
                                    }

                                    if (!val) {
                                        var val = 0;
                                    }

                                    var n = this_app.sel_f_admin.selected_code.split('_')[0] + '_name';
                                    var name = sorted[rowIdx][n];
                                    if (val > 0) {
                                        this_app.markers_obj_arr_ids.push(sorted[rowIdx][this_app.sel_f_admin.selected_code]);
                                        this_app.markers_obj_arr.push({
                                            centroid: [sorted[rowIdx].lng, sorted[rowIdx].lat],
                                            code: sorted[rowIdx][this_app.sel_f_admin.selected_code],
                                            value: val
                                        });
                                        obj.data.push({ name: name, code: this.data()[obj.adm_code_name], val: val });
                                    }

                                });




                                var vals_arr = obj.data.map(d => d.val).sort((a, b) => a - b)


                                obj.min = Math.min.apply(Math, vals_arr);
                                obj.max = Math.max.apply(Math, vals_arr);
                                $('.map_legend_container.' + layer).find('.min').html('<span>Min: ' + numberWithCommas(obj.min) + '</span>');
                                $('.map_legend_container.' + layer).find('.max').html('<span>Max: ' + numberWithCommas(obj.max) + '</span>');

                                obj.total_f = vals_arr.length;
                                console.log(obj)
                                var this_gradients_i = this_app.gradients.filter(d => d.layer_name == layer)[0];
                                var color_range = this_gradients_i.color_range;


                                if (layer == 'schools') {


                                    var _scale = d3.scaleLinear()
                                        .domain([obj.min, obj.max])
                                        .range(color_range);
                                }
                                if (layer == 'power_plants') {


                                    var _scale = d3.scaleLinear()
                                        .domain([obj.min, obj.max])
                                        .range(color_range);
                                }
                                if (layer == 'ref_settlements') {


                                    var _scale = d3.scaleLinear()
                                        .domain([obj.min, obj.max])
                                        .range(color_range);
                                }

                                if (layer == 'hc_out_grid') {

                                    var _scale = d3.scaleLinear()
                                        .domain([obj.min, obj.max])
                                        .range(color_range);
                                }

                                if (layer == 'hc_in_grid') {

                                    var _scale = d3.scaleLinear()
                                        .domain([obj.min, obj.max])
                                        .range(color_range);
                                }

                                if (layer == 'density') {


                                    var _scale = d3.scaleLinear()
                                        .domain([obj.min, obj.max])
                                        .range(color_range);
                                }


                                if (layer == 'electric_grid') {

                                    var _scale = d3.scaleLinear()
                                        .domain([obj.min, obj.max])
                                        .range(color_range);
                                }

                                this_app.markers_obj_arr_scale = _scale;
                                var rscale = d3.scaleLinear()
                                    .domain([obj.min, obj.max])
                                    .range([35, 60])

                                this_app.markers_obj_arr_rscale = rscale;


                                if (prev_circles == true) {
                                    $('.map_legend_container.' + layer).find('.map_table_circles input').trigger('click');
                                }
                                var arr = [];
                                this_app.from_all_admin_simple_analysis_reduced = { layer_name: layer, data: [] }

                                var red = obj.data.filter(d => d.val > 0).reduce(function (memo, feature) {

                                    if (arr.indexOf(feature.code) == -1) {


                                        arr.push(feature.code);

                                        this_app.from_all_admin_simple_analysis_reduced.data.push({
                                            name: feature.name,
                                            code: feature.code,
                                            value: feature.val,
                                            color: obj.max === 0 ? color_range[0] : _scale(feature.val)


                                        })
                                        memo.opacity.push(feature.code, 1)
                                        memo.color.push(feature.code, obj.max === 0 ? color_range[0] : _scale(feature.val))

                                    }
                                    return memo;

                                }, {

                                    color: ["match", ["get", obj.adm_code_name]],
                                    opacity: ["match", ["get", obj.adm_code_name]]

                                });




                                var temp = this_app.from_all_admin_simple_analysis_reduced.data.sort((a, b) => (a.value < b.value) ? 1 : -1);
                                console.log(temp)
                                this_app.from_all_admin_simple_analysis_reduced.data = temp.map(function (d, i) {
                                    d.position = i + 1;
                                    return d;
                                })


                                console.info(this_app.from_all_admin_simple_analysis_reduced)
                                red.opacity.push(0);
                                red.color.push('#222423');
                                console.log(JSON.stringify(red))



                                if (map.getLayer('gaul_level_0_analysis')) {
                                    console.warn('removing gaul level 0 analysis')
                                    map.removeLayer('gaul_level_0_analysis')
                                }


                                if (map.getLayer('gaul_level_1_analysis'))
                                    map.removeLayer('gaul_level_1_analysis')

                                if (map.getLayer('gaul_level_2_analysis'))
                                    map.removeLayer('gaul_level_2_analysis')



                                if (obj.adm_code_name == 'adm0_code') {

                                    map.setPaintProperty('gaul_0_labels', 'text-color', this_gradients_i.map_label);
                                    var gaul_analysis = 'gaul_level_0_analysis';
                                    map.addLayer(gaul_level_0_analysis, 'gaul_level_0_line')
                                }
                                if (obj.adm_code_name == 'adm1_code') {
                                    var gaul_analysis = 'gaul_level_1_analysis';
                                    map.addLayer(gaul_level_1_analysis, 'gaul_level_1');
                                    map.setPaintProperty('gaul_1_labels', 'text-color', this_gradients_i.map_label);
                                }
                                if (obj.adm_code_name == 'adm2_code') {
                                    var gaul_analysis = 'gaul_level_2_analysis';
                                    map.addLayer(gaul_level_2_analysis, 'gaul_level_2');
                                    map.setPaintProperty('gaul_2_labels', 'text-color', this_gradients_i.map_label);
                                }
                                console.log(gaul_analysis)
                                if (this_app.from_all_admin_simple_analysis_reduced.data.length > 0) {
                                    map.setPaintProperty(gaul_analysis, "fill-opacity", red.opacity);
                                    map.setPaintProperty(gaul_analysis, "fill-color", red.color);
                                    console.log(gaul_analysis)
                                } else {
                                    map.setPaintProperty(gaul_analysis, "fill-opacity", 0);
                                }

                                if (prev_labels == true) {
                                    $('.map_legend_container.' + layer).find('.map_table_labels input').trigger('click');
                                }

                                $('#map').busyLoad("hide");

                            } else {
                                console.log('remove active')
                                $('.animated-icon').remove();
                                this_app.active_hover = null;
                                t.removeClass('active');
                                $('#from_all_admin_simple_analysis_table tr.temp_higlighted_datatable_tr').removeClass('temp_higlighted_datatable_tr')
                                $('.analysis_hover_info').hide();

                                if (map.getLayer('gaul_level_0_analysis'))
                                    map.removeLayer('gaul_level_0_analysis')

                                if (map.getLayer('gaul_level_1_analysis'))
                                    map.removeLayer('gaul_level_1_analysis')

                                if (map.getLayer('gaul_level_2_analysis'))
                                    map.removeLayer('gaul_level_2_analysis')
                            }
                        }



                    })
                    var adm_code_name = this_app.sel_f_admin.selected_code;



                    function tr_mouseenter_debounced(adm_value) {
                        console.info('debounced')
                    }
                    $('.from_all_admin_simple_analysis .dataTable tbody .adm_td_actions i').tooltip_materialize({
                        delay: 50,
                        delayOut: 50000,
                        position: 'top',
                        html: "<div class='my-material-tooltip'>Tooltip</div>"
                    })
                    function mousemove_tr_debounced() {

                        alert('debounced')
                        console.log(arguments[0], this, this._DT_RowIndex)
                        console.log(arguments[0].currentTarget._DT_RowIndex, arguments[0].currentTarget.rowIndex)
                        var tr_data = this_app.data_table_obj.row(arguments[0].currentTarget._DT_RowIndex).data();

                        $(this).addClass('temp_higlighted_datatable_tr');


                        var adm_code_name = this_app.sel_f_admin.selected_code;
                        var opacity_expression = ["match", ["get", this_app.sel_f_admin.selected_code], tr_data[adm_code_name], 1, 0];

                        var line_expression = ["match", ["get", this_app.sel_f_admin.selected_code], tr_data[adm_code_name], 2, 0];

                        var highlighted_line_name = this_app.sel_f_admin.highlight_params.highlight_layer.id + '_line';
                        console.log(highlighted_line_name)

                        if (!map.getLayer(highlighted_line_name))
                            map.addLayer(highlighted_line_name)


                        if (adm_code_name == 'adm0_code') {
                            var highlighted_line_name = 'gaul_level_0_line'
                            map.setPaintProperty('gaul_level_0_line', "line-opacity", 0);
                        }


                        if (adm_code_name == 'adm1_code')
                            map.setPaintProperty(highlighted_line_name, "line-opacity", 0);

                        if (adm_code_name == 'adm2_code')
                            map.setPaintProperty(highlighted_line_name, "line-opacity", 0);


                        map.setPaintProperty(highlighted_line_name, "line-width", line_expression);

                        map.setPaintProperty(this_app.sel_f_admin.highlight_params.highlight_layer.id, "fill-opacity", opacity_expression);

                        setTimeout(function () {
                            if (map.getLayer(this_app.sel_f_admin.highlight_params.highlight_layer.id))
                                map.setPaintProperty(this_app.sel_f_admin.highlight_params.highlight_layer.id, "fill-opacity", 0);

                            if (adm_code_name == 'adm0_code')
                                map.setPaintProperty(highlighted_line_name, "line-opacity", 1);

                            if (adm_code_name == 'adm1_code')
                                map.setPaintProperty(highlighted_line_name, "line-opacity", 1);

                            if (adm_code_name == 'adm2_code')
                                map.setPaintProperty(highlighted_line_name, "line-opacity", 1);
                        }, 700)


                    }
                    var tr_debounced = _.debounce(mousemove_tr_debounced, 250, { 'maxWait': 1000 });


                    setTimeout(function () {
                        console.info('making sure scrollto is posible for selected row')
                        /*  var f = $('.from_all_admin_simple_analysis .dataTable tbody .adm_td div[new_code="' + this_app.sel_f_admin[adm_code_name] + '"]');
                         console.warn(f)
                         var parent_tr = f.parents().closest('tr')
                         parent_tr.addClass('higlighted_datatable_tr');
                         $('.from_all_admin_simple_analysis .dataTables_scrollBody').scrollTop(0);
                         var scroll_to = parseInt(parent_tr.position().top);
                         $('.from_all_admin_simple_analysis .dataTables_scrollBody').animate({
                             scrollTop: scroll_to
                         }, 500); */

                        console.warn('hide busyload for all_admin_simple_analysis_datatables')
                        $("body").busyLoad("hide", this_app.calculation_busy_option);


                    }, 800)

                    $('#from_all_admin_simple_analysis_table tbody').off('click')
                    $('#from_all_admin_simple_analysis_table tbody').on('click', function (e) {
                        var target = $(e.target);

                        var adm_code_name = this_app.sel_f_admin.selected_code;
                        var adm_code_value = this_app.sel_f_admin[adm_code_name];
                        console.info(this_app.sel_f_admin, adm_code_name, adm_code_value);

                        if (target.hasClass('adm_td_highlight_table')) {
                            console.log('highlight')
                            switch (adm_code_name) {
                                case 'adm0_code':
                                    var layer_name = 'gaul_level_0';
                                    var label_layer = 'gaul_0_labels';
                                    var zoom = 5;
                                    break;
                                case 'adm1_code':
                                    var layer_name = 'gaul_level_1';
                                    var label_layer = 'gaul_1_labels';
                                    var zoom = 7;
                                    break;
                                case 'adm2_code':
                                    var layer_name = 'gaul_level_2';
                                    var label_layer = 'gaul_2_labels';
                                    var zoom = 8;
                                    break;

                                default:
                                    break;
                            }

                            var highlight_layer = layer_name + '_highlighted';
                            var highlight_layer_line = layer_name + '_highlighted_line';

                            console.log(highlight_layer);
                            var tr = target.parents().closest('.adm_td').find('div.row').eq(0);
                            var adm_value = parseInt(tr.attr('new_code'));



                            if (map.getLayer(highlight_layer)) {




                            } else {

                            }
                            if (layer_name == 'gaul_level_0') {
                                var prev_line_paint = window['gaul_level_0_line'].paint;

                                //map.setPaintProperty('gaul_level_0_line', 'line-opacity', 0);

                            } else {
                                var prev_line_paint = window[layer_name].paint;
                                //map.setPaintProperty(layer_name, 'line-opacity', 0);
                            }


                            console.log(prev_line_paint)


                            var color_expression = ["match", ["get", adm_code_name], adm_value, '#ffd617', '#0f0f0f'];
                            var opacity_expression = ["match", ["get", adm_code_name], adm_value, 1, 0];

                            map.setPaintProperty(highlight_layer, "fill-color", color_expression);
                            map.setPaintProperty(highlight_layer, 'fill-opacity', opacity_expression);

                            setTimeout(function () {
                                map.setPaintProperty(highlight_layer, 'fill-opacity', 0);
                            }, 800)
                            var line_color_expression = ["match", ["get", adm_code_name], adm_value, '#ff7c17', '#7d7272'];
                            map.setPaintProperty(highlight_layer_line, "line-color", line_color_expression);

                            var line_opacity_expression = ["match", ["get", adm_code_name], adm_value, 1, 0];
                            map.setPaintProperty(highlight_layer_line, "line-opacity", line_opacity_expression);

                            setTimeout(function () {
                                map.setPaintProperty(highlight_layer_line, 'line-opacity', 0);


                            }, 7500)

                            /*      var label_color_expression = ["match", ["get", adm_code_name], adm_value, '#ffd617', '#ffffff'];
                                 var label_opacity_expression = ["match", ["get", adm_code_name], adm_value, 1, 0.2];


                                 var prev_label_paint = window[label_layer].paint;

                                 map.setPaintProperty(label_layer, "text-color", label_color_expression);
                                 map.setPaintProperty(label_layer, "text-opacity", label_opacity_expression);
                                 setTimeout(function () {
                                     console.log('setting to previous paint style before clicking adm_td_highlight_table')
                                     if (layer_name == 'gaul_level_0') {
                                         map.setLayoutProperty('gaul_level_0_line', 'visibility', 'visible');
                                         map.setPaintProperty('gaul_level_0_line', 'line-opacity', prev_line_paint['line-opacity']);
                                     } else {


                                         map.setPaintProperty(layer_name, 'line-opacity', prev_line_paint['line-opacity']);
                                     }
                                     map.setPaintProperty(label_layer, "text-color", prev_label_paint['text-color']);
                                     map.setPaintProperty(label_layer, "text-opacity", prev_label_paint['text-opacity']);

                                     map.setPaintProperty(highlight_layer_line, 'line-opacity', 0);

                                 }, 5500) */



                        }

                        if (target.hasClass('adm_td_zoom_in')) {

                            var tr = target.parents().closest('.adm_td').find('div.row').eq(0);
                            var tr_code = tr.attr('new_code')
                            console.info(tr_code)
                            var tr_data = sorted.filter(function (d) {

                                if (parseInt(d[adm_code_name]) == parseInt(tr_code))
                                    return d;
                            })[0];

                            var adm_value = parseInt(adm_code_value);
                            console.warn(adm_code_name, adm_value)
                            var opacity_expression = ["match", ["get", adm_code_name], adm_value, 1, 0];

                            console.log(opacity_expression)

                            var line_expression = ["match", ["get", adm_code_name], adm_value, 2, 0];

                            var bbox = JSON.parse(tr_data['bbox']);
                            if (!bbox) {
                                alert('Missing data to zoom in');
                                return false;
                            }


                            var bounds = new mapboxgl.LngLatBounds();
                            bounds.extend(bbox.coordinates[0]);

                            var c = bounds.getCenter();
                            console.info(c)
                            switch (adm_code_name) {
                                case 'adm0_code':
                                    var layer_name = 'gaul_level_0';
                                    var zoom = 5;
                                    break;
                                case 'adm1_code':
                                    var layer_name = 'gaul_level_1';
                                    var zoom = 7;
                                    break;
                                case 'adm2_code':
                                    var layer_name = 'gaul_level_2';
                                    var zoom = 9;
                                    break;
                                default:
                                    break;
                            }


                            console.log(JSON.stringify([c.lng, c.lat]))

                            map.flyTo({
                                center: [c.lng, c.lat],
                                zoom: zoom,
                                bearing: 0,
                                speed: 0.8, // make the flying slow
                                curve: 3, // change the speed at which it zooms out
                                offset: [-200, 0],
                                easing: function (t) {
                                    return t;
                                },
                                essential: true
                            });




                        }
                        if (target.hasClass('adm_td_view_table')) {
                            $('.from_all_admin_simple_analysis .dataTable .higlighted_datatable_tr').removeClass('higlighted_datatable_tr');
                            var tr = target.parents().closest('.mdc-data-table__row');
                            tr.addClass('higlighted_datatable_tr');
                            var tr = target.parents().closest('.adm_td').find('div.row').eq(0);
                            var tr_code = tr.attr('new_code')
                            console.info(tr_code)
                            var tr_data = sorted.filter(function (d) {
                                console.log(d, adm_code_name)
                                if (parseInt(d[adm_code_name]) == parseInt(tr_code))
                                    return d;
                            })[0];
                            var adm_code_name = tr_data['selected_code'];
                            var a = adm_code_name.substr(0, 4);
                            var adm_code_val = tr_data[adm_code_name];

                            var params = {
                                adm_code_name: adm_code_name,
                                adm_code_value: adm_code_val,
                                userid: this_app.userid,
                                to_query: a
                            };
                            console.warn(params);

                            plot_adm_analysis(params)

                        }
                        if (target.hasClass('adm_td_switch') || target.parent().hasClass('adm_td_switch')) {

                            var td_actions = target.parents().closest('.adm_td').find('.adm_td_actions');
                            console.log(td_actions)
                            if (td_actions.is(':visible')) {
                                td_actions.attr('style', 'display:none!important');
                            } else {
                                td_actions.attr('style', 'display:block!important');
                            }


                        }
                    })



                }

            })
        }


        this_app.after_selecting_adm_unit = function (adm_code_value, f_to_query, lngLat) {

            console.log('this_app.after_selecting_adm_unit is applied on...')
            console.info(arguments)
            /*   var mapCanvas = document.getElementsByClassName('mapboxgl-canvas')[0];
              var mapDiv = document.getElementById('map'); */

            this_app.analysis_data = [];



            this_app.queried_adm_params = {

                to_query: f_to_query,
                adm_code_value: adm_code_value,
                userid: this_app['userid']

            };

            data.userid = this_app.userid;
            this_app.queried_bbox_params = null;

            switch (f_to_query) {
                case 'adm0':

                    var highlight_layer = gaul_level_0_highlighted;
                    var adm_code_name = 'adm0_code';
                    var adm_code_name_name = 'adm0_name';

                    break;

                case 'adm1':

                    var highlight_layer = gaul_level_1_highlighted;
                    var adm_code_name = 'adm1_code';
                    var adm_code_name_name = 'adm1_name';
                    break;

                case 'adm2':

                    var highlight_layer = gaul_level_2_highlighted;
                    var adm_code_name = 'adm2_code';
                    var adm_code_name_name = 'adm2_name';
                    break;
                default:
                    break;
            }

            console.warn(highlight_layer)
            this_app.sel_f_admin.highlight_params = { highlight_layer: highlight_layer, adm_code_name: adm_code_name, adm_code_name_name: adm_code_name_name }


            $.ajax({
                //url: 'https://hsm.land.copernicus.eu/php/akp_energy/simple_popup_analysis_taf2.php',
                url: 'https://hsm.land.copernicus.eu/php/akp_energy/simple_popup_analysis_taf2.php',
                // url: 'https://pere.gis-ninja.eu/akp_energy/simple_popup_analysis_taf2.php',

                type: 'GET',
                async: true,
                data: this_app['queried_adm_params'],


                dataType: 'json',
                success: function (d) {
                    var counts = d.counts;
                    console.log(d)
                    var name = d[d.adm_code_name + '_name'];

                    //refugee_camps
                    var par = ['hc_off_grid', 'hc_in_grid', 'schools', 'power_plants', 'refugee_camps'];
                    var template = `<div _code="${adm_code_value}" class="after_analysis_popup_html"><div class="adm_title">${name}</div>`
                    for (var p in par) {

                        var d = par[p];
                        switch (d) {
                            case 'hc_off_grid':
                                template += `<div class="health_centres"></span>Health centres</span>`;
                                var this_counts = counts.hospitals.out_grid_hc_counts;
                                var title = 'off the grid';
                                break;

                            case 'hc_in_grid':
                                var this_counts = counts.hospitals.in_grid_hc_counts;
                                var title = 'in the grid';
                                break;

                            case 'schools':
                                var this_counts = counts.schools;
                                var title = 'Schools';
                                break;

                            case 'power_plants':
                                var this_counts = counts.power_plants;
                                var title = 'Power Plants';
                                break;

                            case 'refugee_camps':
                                var this_counts = counts.refugee_camps;
                                var title = 'Refugee settlements';
                                break;
                            default: break;
                        }




                        template += `<div class="row">
                                <span class="col-8 card-title">${title}</span><span class="col-4 layer_counts">${numberWithCommas(this_counts)}</span>
                        </div>`


                        if (d == 'hc_in_grid')

                            template += '</div>'



                    }

                    template += `<div class="row">
                        <span class="col-8 card-title">Electric grid</span><span class="col-4 layer_counts">${numberWithCommas(counts.electric_grid)} Km</span>
                </div>`
                    template += `<div class="population">
                <span>Population in non-electrified areas</span>
                <div class="row">
                <span class="col-8 card-title">Population density</span><span class="col-4 layer_counts">${numberWithCommas(counts.density)} hab/250 m<sup>2</sup> </span>
                </div>
                <div class="row">
                <span class="col-8 card-title">Total population</span><span class="col-4 layer_counts">${numberWithCommas(counts.sum_pop)}</span>
                </div>
        </div>`


                    //<div class="card-container hospitals"><div class="analysis_card card"><div class="card-content white-text"><div class="adm_title">' + d[adm_code_name_name] + '</div><div class="card-container schools"><div class="analysis_card card"><div class="card-content white-text"><span class="card-title">Schools</span>    <span class="layer_counts">' + numberWithCommas(counts.schools) + '</span></div></div>    </div><span class="card-title">Health facilities</span>    <span class="layer_counts hc_off_grid">' + numberWithCommas(counts.hospitals.out_grid_hc_counts) + '</span>    <span class="layer_counts hc_in_grid">' + numberWithCommas(counts.hospitals.in_grid_hc_counts) + '</span></div></div>    </div>    <div class="card-container power_plants"><div class="analysis_card card"><div class="card-content white-text"><span class="card-title">Power plants</span>    <span class="layer_counts">' + numberWithCommas(counts.power_plants) + '</span></div></div>    </div>    <div class="card-container refugee_camps"><div class="analysis_card card"><div class="card-content white-text"><span class="card-title">Refugee settlements</span>    <span class="layer_counts">' + counts.c + '</span></div></div>    </div>    <div class="card-container undefined"><div class="analysis_card card"><div class="card-content white-text">    <div class="card-title">Population</div>    <div class="ghs_counts_container"><div class="ghs_counts">' + counts.density + ' hab/Km<sup>2</sup></div><div class="ghs_description">Population density for <span style="color:gold">non-electrified areas</span> <span class="color_span" style="border-color: #7fd32c;">Low density</span></div><div class="sum_pop">Total population ' + numberWithCommas(counts.sum_pop) + '</div>    </div></div></div>    </div>    <div class="card-container electric_grid"><div class="analysis_card card"><div class="card-content white-text"><span class="card-title">Electric grid</span>    <span class="layer_counts">' + numberWithCommas(counts.electric_grid) + '</span> Km</div></div>    </div>';

                    if (this_app['queried_adm_params'].to_query === 'adm0') {
                        var taf_layers = ['bess', 'pumps_up', 'solar', 'wind', 'geotermal_ungrouped', 'hydrogen_grouped']
                    }
                    else {
                        var taf_layers = ['pumps_up', 'solar', 'wind', 'geotermal_ungrouped']
                    }

                    taf_layers.map((d) => 'taf_' + d).forEach((d, i) => {
                        console.info(d);
                        if (i == 0) {
                            template += '<div class="taf_container">';
                        }
                        var title = app_data.layers_info.filter((dd) => dd.id === d)[0].title;

                        var this_counts = numberWithCommas(counts[d]);
                        if (this_counts > 0)
                            //template += `<div class="card-container ${d}"><div class="analysis_card card"><div class="card-content white-text row d-flex justify-content-center align-items-center"><span class="col-9 card-title">${title}</span>    <span class="col-3 layer_counts">${this_counts}</span></div></div>    </div>`
                            template += `<div class="row">
                                <span class="col-8 card-title">${title}</span><span class="col-4 layer_counts">${this_counts}</span>
                        </div>`

                        if (i == taf_layers.length - 1) {
                            template += '</div>';
                        }


                    })
                    template += '<div class="row g-0 btn_container align-items-center"><span class="center col m-1"><a data-position="bottom" data-tooltip="View detailed information on selected feature" class="extend_popup_analysis_info align-items-center">Detailed info</a></span><span class="center col m-1"><a data-position="bottom" data-tooltip="Compare statistics over other administrative units"  class="compare_from_popup align-items-center">Compare and map</a></span></div></div>';
                    var latLng = {
                        lat: d.lat,
                        lng: d.lng
                    }
                    new mapboxgl.Popup({
                        closeButton: true,
                        closeOnClick: false,
                        className: 'after_analysis_popup',
                        offset: {
                            'bottom': [0, -20],
                            'top': [0, 15],
                            'top-left': [0, 0], //[linearOffset, (markerHeight - markerRadius - linearOffset) * -1],
                            'top-right': [0, 0], //[-linearOffset, (markerHeight - markerRadius - linearOffset) * -1],

                        }
                    })
                        .setLngLat(lngLat)
                        .setHTML(template).addTo(map);

                    $('.compare_from_popup,.extend_popup_analysis_info').tooltip_materialize();

                    $('.after_analysis_popup_html[_code=' + adm_code_value + ']').data('info', this_app.sel_f_admin)


                    console.log($('.after_analysis_popup_html[_code=' + adm_code_value + ']').data('info'))

                    var adm_name_code = this_app.sel_f_admin.selected_code.split('_code')[0] + '_name';






                }
            })



        } //end after_selecting_adm_unit
        function after_creating_pol(userPolygon) {
            $('.draw_rec_custom').removeClass('on');
            $('.mapboxgl-popup.area_popup').remove();


            var f = draw.getAll().features;

            if (f.length > 0) {
                var f_id = f[0];
                console.info(f_id)
                draw.deleteAll().getAll();

            }
            if (userPolygon) {



                $('.drawn_area').empty().html('Analysed area: ' + this_app.drawn_area + ' km<sup>2</sup>');
                var polygonBoundingBox = turf.bbox(userPolygon);
                var southWest = [polygonBoundingBox[0], polygonBoundingBox[1]];
                var northEast = [polygonBoundingBox[2], polygonBoundingBox[3]];
                var bbox = {
                    sw: southWest,
                    ne: northEast
                };
                this_app.bbox = bbox;

                var northEastPointPixel = map.project(northEast);
                var southWestPointPixel = map.project(southWest);



            } else {
                var bbox = this_app.bbox;

            }

            this_app.analysis_data = [];
            this_app.queried_adm_params = null;

            this_app.queried_bbox_params = {
                userid: this_app.userid,
                query_layers: app_data.query_layers,
                bbox: bbox,
                to_query: 'bbox',
            }

            $('#map').busyLoad("show", this_app.calculation_busy_option);
            $.ajax({
                //url: 'https://hsm.land.copernicus.eu/php/akp_energy/intersects_analysis_download_test_new_taf4_new.php',
                url: 'https://hsm.land.copernicus.eu/php/akp_energy/intersects_analysis_download_test_new_taf4_new.php',
                type: 'GET',

                data: {
                    userid: this_app.userid,
                    query_layers: app_data.query_layers,
                    bbox: bbox,
                    to_query: 'bbox',

                },

                dataType: 'json',
                success: function (response) {

                    $("#map").busyLoad("hide", this_app.calculation_busy_option);
                    console.log(response)
                    if (response.counts == 0) {
                        alert('It seems there is nothing here!')
                        return false
                    }

                    $('#map').addClass('bordered');
                    this_app.this_pdf.content = [];
                    this_app.this_pdf.counts = [];
                    this_app.this_pdf.with_map = false;
                    this_app.sel_f_admin = {
                        setFilter_data: [{
                            layer_name: 'hospitals'
                        }, {
                            layer_name: 'power_plants'
                        },
                        {
                            layer_name: 'refugee_camps'
                        },
                        {
                            layer_name: 'electric_grid'
                        }
                        ]
                    };

                    for (var p in this_app.sel_f_admin.setFilter_data) {
                        this_app.sel_f_admin.setFilter_data[p].adm_filter_activated = false;
                        this_app.sel_f_admin.setFilter_data[p].bbox_filter_activated = true;
                    }






                    $('.analysis_container').empty();
                    //   $('.analysis_container').append('<div class="row"><div class="col-9 drawn_area center">' + this_app.drawn_area + ' km<sup>2</sup></div><div class="col-3"><i class="material-icons  right close_analysis_container"></i><i class="material-icons  right expand_analysis_container">fullscreen</i><i class="material-icons  right print_pdf">picture_as_pdf</i></div>');

                    var html = `<div class="d-flex">
                    <div class="adm_name flex-grow-1 center">
                       <div class="drawn_area">${this_app.drawn_area} km<sup>2</sup></div>
                    </div>

                      <div class="">
                        <div class="row align-items-end">





                            <i class="col bi bi-x-lg close_analysis_container"></i>

                            <i class="col bi bi-filetype-pdf print_pdf"></i>

                            </div>
                        </div>
                      </div>`
                    $('.analysis_container').append(html);

                    $.when(this_app.update_table_analysis(response.hospitals_categories, 'hospitals', true))
                        .then(
                            this_app.update_table_analysis(response.schools_categories, 'schools', true)
                        )
                        .then(
                            this_app.update_table_analysis(response.power_categories, 'power_plants', true)
                        ).
                        then(
                            this_app.update_table_analysis(response.refugees_categories, 'refugee_camps', true)
                        )

                        .then(
                            this_app.update_ghs_analysis(response)
                        )
                        .then(
                            this_app.update_table_analysis(response.electric_grid_categories, 'electric_grid', true)
                        )
                        .then(
                            this_app.update_table_analysis(response, 'taf_layers', false)
                        )
                        .then(function () {

                            $('.analysis_container .tooltiped').tooltip_materialize();

                            console.log(this_app.sel_f_admin)
                            this_app.this_pdf.content.unshift(this_app.this_pdf.extra[0]);

                            this_app.this_pdf.content.unshift({
                                width: '*',
                                text: this_app.drawn_area + ' sqKm',
                                style: 'adm0_style',
                                alignment: 'center'
                            });

                            $("#map").busyLoad("hide", this_app.calculation_busy_option);
                            console.info(this_app.this_pdf)
                            setTimeout(function () {
                                $('.mapboxgl-popup').show();
                                $('.mapboxgl-popup.after_analysis_popup').show();
                            }, 500)

                            var mapCanvas = document.getElementsByClassName('mapboxgl-canvas')[0];
                            var mapDiv = document.getElementById('map');

                            $('body').css('overflow-y', 'auto!important');
                            $('.sidenav').css('max-height', '66vh');

                            mapDiv.style.height = '70vh';
                            mapCanvas.style.height = '70vh';

                            $('.sidenav').css('position', 'absolute');
                            $('body').css('overflow-y', 'visible');
                            map.resize();
                            console.log(mapDiv.style.height + ' is the NEW mapdiv height')


                            $('.analysis_container').addClass('active_analysis')
                            $('.analysis_container').show();

                            $('.card-container.hospitals,.card-container.power_plants').each(function () {
                                var h = $(this).height();
                                $(this).find('.info').css({ 'overflow-y': 'auto', 'max-height': '700px' })
                            })
                            $('#map').busyLoad("hide");

                            $('html').animate({
                                scrollTop: $(".analysis_container").offset().top
                            }, 2000);

                            $('.analysis_container .print_pdf').on('click', function () {
                                $('.mapboxgl-popup.after_analysis_popup,.mapboxgl-control-container').hide();
                                $("body").busyLoad("hide", this_app.generating_file_busy_option);
                                this_app.print_on_pdf = true;

                                $('html').animate({
                                    scrollTop: $("#map").offset().top
                                }, 10);
                                $('.print_map').trigger('click');
                            })


                            $('.analysis_container').unbind('mouseleave');
                            $('.analysis_container').on('mouseleave', function () {
                                map.scrollZoom.disable();
                                setTimeout(function () {
                                    map.scrollZoom.enable();
                                }, 3000)
                                console.log('leving')
                            });
                            $('.analysis_container .card-container').unbind('click');

                            $('.analysis_container .close_analysis_container').on('click', function () {
                                $('.analysis_container').hide();

                                if (map.getLayer("drawn_pol")) {
                                    map.removeLayer("drawn_pol")
                                }
                                mapDiv.style.height = '100vh';
                                mapCanvas.style.height = '100vh';

                                $('.sidenav').css('position', 'fixed');
                                $('.sidenav').css('max-height', 'unset');

                                $('body').css('overflow-y', 'hidden');
                                map.resize();

                                $("body,html,document").animate({
                                    scrollTop: $('.nav-wrapper').height() - 50
                                });
                            });

                            $('.expand_analysis_container').on('click', function () {

                                if ($(this).hasClass('on')) {
                                    screenfull.exit();

                                } else {

                                    screenfull.request($('.analysis_container')[0]);

                                }

                                $(this).toggleClass('on');

                            })

                            $('.analysis_container .card-container').bind('click', function (e) {
                                var layer_name = $(this).attr('_layer');
                                console.info($(e.target))
                                if ($(e.target).hasClass('download_excel') == true ||
                                    $(e.target).hasClass('download_shp') == true) {


                                    if (this_app.queried_bbox_params !== null) {
                                        var params = this_app.queried_bbox_params;
                                    } else {
                                        if (this_app.queried_adm_params !== null)
                                            var params = this_app.queried_adm_params

                                    }
                                    if ($(e.target).hasClass('download_excel')) {
                                        params.req_format = 'xlsx';
                                    } else {
                                        params.req_format = 'shp';
                                    }
                                    console.warn(params);
                                    params.layer_id = layer_name;
                                    params.to_query = 'bbox';

                                    $("body").busyLoad("show", this_app.generating_file_busy_option);
                                    $.ajax({

                                        // url: 'https://hsm.land.copernicus.eu/php/akp_energy/new_intersects_only_download.php',
                                        //url: 'https://hsm.land.copernicus.eu/php/akp_energy/downloads_new4.php',
                                        url: 'https://hsm.land.copernicus.eu/php/akp_energy/downloads_new4.php',

                                        type: 'GET',

                                        data: params,

                                        dataType: 'json',
                                        success: function (response) {

                                            $("body").busyLoad("hide", this_app.generating_file_busy_option);
                                            if (response.success == true) {
                                                var a = document.createElement('a');
                                                console.log('https://hsm.land.copernicus.eu/php/akp_energy/downloads/' + this_app.userid + '/zipped/' + params.layer_id + '_bbox.zip')
                                                a.href = 'https://hsm.land.copernicus.eu/php/akp_energy/downloads/' + this_app.userid + '/zipped/' + params.layer_id + '_bbox.zip';
                                                document.body.appendChild(a);

                                                a.click();
                                                document.body.removeChild(a);
                                            }

                                        }

                                    })


                                }
                                if ($(e.target).hasClass('vis_by_param')) {
                                    e.preventDefault();
                                    $(this).find('.info').show();

                                } else {
                                    if ($(e.target).hasClass('close_analysis_params')) {
                                        $(this).find('.info').hide();
                                        $(this).find('.general_info').show();
                                    }
                                    if ($(e.target).is('li')) {
                                        var _class = $(e.target).attr('class');


                                        if ($(this).find('div.' + _class).is(':visible'))
                                            $(this).find('div.' + _class).hide();
                                        else
                                            $(this).find('div.' + _class).show();


                                        if ($('.card.' + layer_name + ' svg.circles_svg.' + _class).length == 0) {

                                            this_app.create_d3_legend_analysis('analysis', layer_name, _class);
                                        }
                                    }

                                    if ($(e.target).parents().closest('.li_info_container').length > 0) {
                                        var _class = $(e.target).parents().closest('li').attr('class');

                                        if ($(this).find('div.' + _class).is(':visible')) {
                                            $(this).find('div.' + _class).hide();
                                        } else {
                                            $(this).find('div.' + _class).show();
                                        }

                                        if ($('.card.' + layer_name + ' svg.circles_svg.' + _class + ' g').length == 0) {

                                            this_app.create_d3_legend_analysis('analysis', layer_name, _class);
                                        }

                                    }
                                }
                            })

                        })


                    if (map.getLayer('electric_grid')) {

                        if (response.electric_grid_data) {


                            map.setPaintProperty(
                                'electric_grid',
                                'line-opacity', 0);

                            var features = response.electric_grid_data.geo[0];

                            this_app.sel_electric_grid = features;

                            if (!map.getLayer('sel_elect')) {

                                map.addLayer(sel_elect)

                            }

                            if (features) {
                                map.getSource('elec_lines_source').setData(features);
                                map.setPaintProperty(
                                    'sel_elect',
                                    'line-opacity', 1);
                            }
                        }


                    }

                }
            }) //end ajax
        } //end after_creation_draw

        map.on('draw.create', function (e) {


            this_app.drawing_rect = true;
            map.setPaintProperty('drawn_pol', 'line-width', 2)
            setTimeout(function () {
                this_app.drawing_rect = false;
                $('.btn.clean_custom,.btn.repeat_polygon').show();
            }, 300)
            console.log(e.features)

            this_app.this_pdf.content = [];
            this_app.this_pdf.counts = [];
            this_app.this_pdf.extra = [{
                columns: [

                ]
            }]
            $("#map").busyLoad("show", this_app.calculation_busy_option);
            $('.mapboxgl-popup.feature_map_popup').remove();
            this_app.userPolygon = e.features[0];

            after_creating_pol(this_app.userPolygon)

        })



        var test = 0;
        var debounced = _.debounce(mousemove_debounced, 250, { 'maxWait': 1000 });
        this_app.mouseovered_code = null;

        function mousemove_debounced(e) {
            if (this_app.active_hover) {
                switch (this_app.active_hover.code) {


                    case 'adm0_code':

                        var l_name = 'gaul_level_0_analysis';
                        var code_name = 'adm0_code';
                        break;

                    case 'adm1_code':
                        var l_name = 'gaul_level_1_analysis';
                        var code_name = 'adm1_code';

                        break;
                    case 'adm2_code':

                        var l_name = 'gaul_level_2_analysis';
                        var code_name = 'adm2_code';
                        break;
                    default:
                        break;
                }





                switch (this_app.active_hover.layer) {


                    case 'hc_in_grid':

                        var txt = 'In-grid health centres';

                        break;

                    case 'hc_out_grid':
                        var txt = 'Off-grid health centres';

                        break;
                    case 'ref_settlements':
                        var txt = 'Refugee settlements';

                        break;
                    case 'power_plants':
                        var txt = 'Power plants';

                        break;
                    case 'electric_grid':
                        var txt = 'Electric grid (Km)';

                        break;
                    case 'density':
                        var txt = 'Off-grid population density';

                        break;
                    default:
                        break;
                }

                var features = map.queryRenderedFeatures(e.point);
                console.warn(features)

                if (features.length > 0) {


                    var feature = map.queryRenderedFeatures(e.point, { layers: [l_name] });
                    if (feature && feature.length > 0) {

                        var layer_name = this_app.from_all_admin_simple_analysis_reduced.layer_name;

                        if (feature[0].properties[code_name] == this_app.mouseovered_code) {
                            console.warn('we are still in same code ' + this_app.mouseovered_code)
                            return false;
                        } else {

                            this_app.mouseovered_code = feature[0].properties[code_name];
                            $('.map_results_control_container #from_all_admin_simple_analysis_table .temp_higlighted_datatable_tr').removeClass('temp_higlighted_datatable_tr');
                            console.warn('we are in different code ' + this_app.mouseovered_code)
                        }


                        var i = 0;
                        this_app.data_table_obj.rows().every(function (rowIdx, tableLoop, rowLoop) {
                            i++;
                            if (this.data()[code_name] == feature[0].properties[code_name]) {


                                var sel = this_app.from_all_admin_simple_analysis_reduced.data.filter(d => d.code == this.data()[code_name])[0];
                                console.log(sel)

                                if (i == 0) {
                                    sel.row_position = 0;

                                } else {
                                    sel.row_position = i - 1;

                                }

                                if (sel) {
                                    console.log(sel)
                                    //$('.map_results_control_container #from_all_admin_simple_analysis_table .temp_higlighted_datatable_tr')
                                    $('#from_all_admin_simple_analysis_table tr').eq(sel.row_position).addClass('temp_higlighted_datatable_tr')
                                    var html = '<h6>' + sel.name + '</h6><span class="value" style="color:white">' + sel.value +
                                        ' </span><span class="elem">' + txt + '</span><span class="elem2">Position ' + sel.position + ' of ' + this_app.from_all_admin_simple_analysis_reduced.data.length + '</span>';

                                    $('.analysis_hover_info').show();
                                    $('.analysis_hover_info').html(html);
                                    $(".analysis_hover_info").animate({ "background-color": sel.color }, 500);



                                    $('#from_all_admin_simple_analysis_table .dataTables_wrapper').scrollTop(0);
                                    var scroll_to = parseInt($('#from_all_admin_simple_analysis_table .temp_higlighted_datatable_tr').position().top);
                                    console.log(scroll_to)
                                    $('.from_all_admin_simple_analysis .dataTables_wrapper').animate({
                                        scrollTop: scroll_to
                                    }, 500);

                                } else {
                                    $('.analysis_hover_info').hide();
                                }

                            }
                        });
                    } else {
                        $('.analysis_hover_info').hide();
                    }
                } else {
                    console.log('not a feature')
                    $('.analysis_hover_info').hide();
                    $('.temp_higlighted_datatable_tr').removeClass('temp_higlighted_datatable_tr');
                }

            }


            if (this_app.measuring_line == true || this_app.drawing_rect == true) {

                var features = map.queryRenderedFeatures(e.point, {
                    layers: ['measure-points']
                });
                map.getCanvas().style.cursor = 'crosshair';
            } else {
            }

            if (drawn_test.area) {

                area_popup
                    .setLngLat(drawn_test.popup_pos)
                    .setHTML(drawn_test.area + ' km<sup>2</sup>')
                    .addTo(map);

                $('.drawn_area').empty().html('Analysed area: ' + drawn_test.area + ' km<sup>2</sup>')
                return false;
            }
        }






        function get_colors(params) {
            console.info(params)

            var colorInterpolator = d3.interpolate("green", "red");
            var steps = 8;
            var colorArray = d3.range(0, (1 + 1 / steps), 1 / (steps - 1)).map(function (d) {
                return colorInterpolator(d)
            });
            console.warn(d3.range(0, (1 + 1 / steps), 1 / (steps - 1)))
            var scale = d3.scaleQuantize()
                .domain([params.min, params.max])
                .range(colorArray)
            console.warn(scale.domain())
            console.warn(scale.range())
            return scale;
        }

        map.on('dblclick', function (e) {
            console.info('double click')
            if (this_app.measuring_line == true) {
                this_app.measuring_line = false;
                $('.distance-container').hide();
                map.getCanvas().style.cursor = 'pointer';
                e.preventDefault();
                return false
            }

            this_app.clickType = 'double';
            clearTimeout(clickTimeout);


            console.log("Double click detected!", e.lngLat);

            if (this_app.get_analysis_by_adm !== null) {
                this_app.this_pdf.content = [];
                this_app.this_pdf.counts = [];
                this_app.this_pdf.extra = [{
                    columns: [

                    ]
                }]

                $('.mapboxgl-popup.feature_map_popup').remove();

                if ($('.analysis_container').is(':visible')) {
                    $('.analysis_container').hide();
                    $('.analysis_container').removeClass('active_analysis');
                    $('#map').removeClass('bordered');
                    var mapCanvas = document.getElementsByClassName('mapboxgl-canvas')[0];
                    var mapDiv = document.getElementById('map');
                    mapDiv.style.height = '100vh';
                    mapCanvas.style.height = '100vh';
                    $('.sidenav').css('top', '8%');
                    $('.sidenav').css('position', 'fixed');
                    $('body').css('overflow-y', 'hidden');
                    map.resize();
                }

                this_app.selected_e = e;
                get_analysis_by_adm(e);
            }

            /*  setTimeout(function () {
                 console.log(this_app.clickType)
                 this_app.clickType = null;
             }, 500) */



        });


        var minRadius = 10,
            maxRadius = 130;

        var duration = 800;

        function transition_circle(element, start, end) {

            console.log('transition_circle')
            element
                .attr('r', 8)
                .style('opacity', 0.8)
                .attr('cx', function (d) {

                    if (d.centroid) {
                        return point_project(d.centroid).x;
                    } else {
                        return d.x
                    }
                })
                .attr('cy', function (d) {

                    if (d.centroid) {
                        return point_project(d.centroid).y;
                    } else {
                        return d.y;
                    }
                })

                .attr('fill', d => '#ef940d')
                .attr('stroke', d => 'white')
                .transition()
                .delay(800)
                .duration(duration)
                .attr('r', function (d) {
                    return 35
                })
                .style('opacity', 0)
        }

        function plot_radius() {

            var _scale = this_app.markers_obj_arr_scale;

            var non_active_centroids = $('.animated-icon:visible').filter(function () {
                var id = parseInt($(this).attr('id').split('_')[1]);
                return this_app.markers_obj_arr_ids.indexOf(id) == -1
            })
            non_active_centroids.remove();

            var gradients_info = this_app.gradients.filter(d => d.layer_name == this_app.from_all_admin_simple_analysis_reduced.layer_name)[0];


            this_app.markers_obj_arr.forEach(function (d) {
                if (d.value == 0) {
                    if ($('#centroid_' + d.code, '#map').length > 0) {
                        $('#centroid_' + d.code, '#map').remove();

                    }
                }

                if (d.value > 0) {
                    if ($('#centroid_' + d.code, '#map').length == 0) {
                        var el = document.createElement('div');
                        el.className = 'animated-icon my-icon';
                        el.id = 'centroid_' + d.code;
                        new mapboxgl.Marker(el, {
                            offset: [0, 20]
                        })
                            .setLngLat(d.centroid)
                            .addTo(map);
                    }

                    var $centroid = $('#centroid_' + d.code, '#map');

                    if ($centroid.find('.inner_circle').length == 0) {
                        $centroid.append('<span class="inner_circle">' + numberWithCommas(d.value) + '</span>');
                    } else {
                        $centroid.find('.inner_circle').text(numberWithCommas(d.value));
                    }
                    $centroid.find('.inner_circle').css('color', gradients_info.color_label);

                    var sel_radius = this_app.markers_obj_arr_rscale(d.value);
                    $centroid.animate({
                        width: sel_radius + 'px',
                        height: sel_radius + 'px'
                    }, 800

                    ).css(
                        'background-color', _scale(d.value)
                    ).css('display', 'block')

                    $centroid.find('.inner_circle').animate({
                        'line-height': sel_radius + 'px',
                        'font-size': sel_radius / 3.5
                    })


                }

            })
        }
        $('#map').on('click', function (e) {


            if ($(e.target).is(':checkbox') && $(e.target).parents().closest('.map_table_circles').length > 0) {


                console.warn($(e.target))
                if ($(e.target).is(':checkbox'))
                    var input = $(e.target)
                else
                    var input = $(e.target).parent().find('input:checkbox')


                var container = input.parents().closest('.map_legend_container')
                console.log(container)
                console.warn(container.find('.map_table_circles input'))


                setTimeout(function () {


                    if (input.is(':checked')) {
                        plot_radius();

                        console.log('on radius')
                        container.find('.map_table_circles input').prop('checked', true);



                    } else {
                        $('.animated-icon').hide();
                        console.log('off radius or update??')
                        container.find('.map_table_circles input').prop('checked', false);
                    }
                }, 100)
            }

            console.warn($(e.target))

            //mapboxgl - popup - close - button
            if ($(e.target).hasClass('mapboxgl-popup-close-button') && $(e.target).parent().find('.after_analysis_popup_html')) {


                var info = $(e.target).parent().find('.after_analysis_popup_html').data('info')

                if (!info) return false;
                //we are not on an admin feature popup
                console.info(info[info.selected_code])


                this_app.sel_highlighted_arr = this_app.sel_highlighted_arr.filter(d => d !== info[info.selected_code])
                this_app.fx.highlight_adm_unit(info.selected_code, info[info.selected_code], false);



            }
            /*     if ($(e.target).parents().closest('.after_analysis_popup').length > 0) {

                    var popup = $(e.target).parents().closest('.after_analysis_popup');
                    console.log(popup)
                    debugger
                } */
            if ($(e.target).parents().closest('.feature_map_popup').length > 0) {

                var $popup = $(e.target).parents().closest('.feature_map_popup');
                var target = $(e.target);
                if (target.hasClass('mapboxgl-popup-close-button')) {
                    $popup.remove();

                }
                if (target.hasClass('a2ei_chart')) {

                    $popup.addClass('with_a2ei_info')
                    $popup.find('.popup_info_container.ceat').hide();


                    $.when($popup.find('.mapboxgl-popup-content').show().animate({

                        height: 'auto'
                    }, 600)).then(function () {
                        $popup.find(".mapboxgl-popup-content").busyLoad("show");
                        var a2ei_id = parseInt($popup.find('.a2ei').attr('a2ei_id'));

                        this_app.a2ei.id = a2ei_id;
                        this_app.a2ei.range = 'day';

                        var today = moment().format("YYYY-MM-DD");
                        $popup.find('.a2ei,.popup_info_container_a2ei').show();

                        function promises_ajax(id, range) {

                            var params = {
                                id: id,

                                range: range
                            }


                            return $.ajax({
                                //url: "https://hsm.land.copernicus.eu/php/akp_energy/a2ei/a2ei_4_dates_new2.php",
                                //url: "https://hsm.land.copernicus.eu/php/akp_energy/a2ei_4_dates_new2.php",
                                url: "https://pere.gis-ninja.eu/akp_energy/a2ei/a2ei_4_dates_new2.php",
                                type: "GET",
                                data: params,
                                dataType: "json",
                                async: true,
                                success: function (data) {

                                    console.log(data)
                                    data.range = params.range;
                                    if (!data.status) {
                                        var d = data.map(function (d) {
                                            if (!d.averagePower)
                                                return 'null'
                                            else
                                                return d.averagePower
                                        });

                                        if (data.range == 'day') {



                                            this_app.a2ei.series.push({ name: data.range, avg_data: d })
                                        }
                                        if (data.range == 'week') {

                                        }
                                        if (data.range == 'month') {
                                            var ranges_data = data.map(function (d) {
                                                return [d.averagePower + d.stdDevPower, d.averagePower - d.stdDevPower]
                                            });

                                            this_app.a2ei.series.push({ name: data.range, avg_data: d, ranges_data: ranges_data })

                                        }
                                        if (data.range == 'year') {

                                        }



                                    }


                                },
                                error: function () {
                                    alert('some query failed, results may be incomplete')
                                }

                            });

                        }


                        var promises_arr = []
                        var ranges_arr = ['day', 'month']

                        for (var p in ranges_arr) {
                            promises_arr.push(promises_ajax(this_app.a2ei.id, ranges_arr[p]))
                        }


                        Promise.all(promises_arr).then(() => {

                            var extra = {
                                "hour": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
                                "wp_primary": [412, 412, 412, 412, 756, 756, 756, 756, 756, 1581, 1581, 825, 1306, 1306, 825, 687, 687, 1306, 1306, 1306, 619, 412, 412, 412],
                                "wp_secondary": [825, 825, 825, 825, 1512, 1512, 1512, 1512, 1512, 3162, 3162, 1649, 2612, 2612, 1649, 1375, 1375, 2612, 2612, 2612, 1237, 825, 825, 825],
                                "wp_tertiary": [5155, 5155, 5155, 5155, 9450, 9450, 9450, 9450, 9450, 19759, 19759, 10309, 16323, 16323, 10309, 8591, 8591, 16323, 16323, 16323, 7732, 5155, 5155, 5155],
                                "daily_demand": [0.0206185567, 0.0206185567, 0.0206185567, 0.0206185567, 0.0378006873, 0.0378006873, 0.0378006873, 0.0378006873, 0.0378006873, 0.0790378007, 0.0790378007, 0.0412371134, 0.0652920962, 0.0652920962, 0.0412371134, 0.0343642612, 0.0343642612, 0.0652920962, 0.0652920962, 0.0652920962, 0.0309278351, 0.0206185567, 0.0206185567, 0.0206185567],
                                "wp_first": [103, 103, 103, 103, 189, 189, 189, 189, 189, 395, 395, 206, 326, 326, 206, 172, 172, 326, 326, 326, 155, 103, 103, 103]
                            }
                            console.log(this_app.a2ei.series)

                            var monthly = this_app.a2ei.series.filter(d => d.name == 'month')[0];
                            var monthly_nulls = monthly.avg_data.filter(d => d == 'null');

                            var series = [];
                            if (monthly_nulls.length == monthly.avg_data.length) {
                                this_app.a2ei.monthly_data = false;
                            }
                            else {
                                this_app.a2ei.monthly_data = true;
                                series.push({
                                    name: 'Monthly Averages',
                                    data: this_app.a2ei.series.filter(d => d.name == 'month')[0].avg_data,
                                    zIndex: 1,
                                    marker: {
                                        fillColor: 'white',
                                        lineWidth: 1,
                                        lineColor: '#b9097e'
                                    }
                                },
                                    {
                                        name: 'Monthly Range (Std. Desviation)',
                                        data: this_app.a2ei.series.filter(d => d.name == 'month')[0].ranges_data,
                                        type: 'arearange',
                                        lineWidth: 0,
                                        linkedTo: ':previous',
                                        color: '#b82c3a',
                                        fillOpacity: 0.7,
                                        zIndex: 0,
                                        marker: {
                                            enabled: false
                                        }
                                    })
                            }
                            var daily = this_app.a2ei.series.filter(d => d.name == 'day')[0];
                            var daily_nulls = daily.avg_data.filter(d => d == 'null');

                            if (daily_nulls.length == daily.avg_data.length) {

                            }
                            else {
                                series.push({
                                    name: 'Hourly consumption:  live tracking',
                                    data: daily.avg_data,
                                    zIndex: 1,
                                    marker: {
                                        fillColor: '#f2cb1d',
                                        lineWidth: 1,
                                        lineColor: '#f84b1a'
                                    }
                                });
                            }

                            console.info(series)

                            /* this_app.a2ei.series.forEach(item => {
                                if (item.name === "day") {
                                    item.avg_data = item.avg_data.map((d) => {
                                        if (d == 'null') return 0
                                        else
                                            return d
                                    }); // Replace each value with 0
                                }
                            }); */

                            // if (this_app.a2ei.monthly_data = false) {
                            if (series.length == 0) {
                                $popup.find('.highcharts_all .highcharts').empty().hide();
                                $popup.find('.a2ei_chart_description .withoutChart').show();
                            }
                            else {

                                /*  if (daily_nulls.length == this_app.a2ei.series[0].avg_data.length) {


                                     this_app.a2ei.series.forEach(item => {
                                         if (item.name === "day") {
                                             item.avg_data = item.avg_data.map(() => 0); // Replace each value with 0
                                         }
                                     });

                                     $popup.find('.highcharts_all .highcharts').empty().hide();
                                     $popup.find('.a2ei_chart_description .withoutChart').show();
                                     console.warn('No daily data available')


                                 } else { */
                                this_app.a2ei.daily_data = true;
                                $popup.find('.highcharts_all .highcharts').show();
                                $popup.find('.a2ei_chart_description .withChart').hide();
                                Highcharts.theme = {
                                    colors: ['#058DC7', '#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572',
                                        '#FF9655', '#FFF263', '#6AF9C4'
                                    ],
                                    chart: {
                                        backgroundColor: '#0c0c0c',
                                    },
                                    title: {
                                        style: {
                                            color: '#ff9733',
                                            font: 'sans-serif'
                                        }
                                    },
                                    subtitle: {
                                        style: {
                                            color: '#666666',
                                            font: 'bold 12px "Trebuchet MS", Verdana, sans-serif'
                                        }
                                    },
                                    legend: {
                                        itemStyle: {
                                            font: '9pt Trebuchet MS, Verdana, sans-serif',
                                            color: '#ff9733'
                                        },
                                        itemHoverStyle: {
                                            color: 'gray'
                                        }
                                    }
                                };
                                Highcharts.setOptions(Highcharts.theme);



                                $popup.find('.highcharts_all .highcharts').highcharts({

                                    title:
                                    {
                                        text: 'Electricity demand (monitored site)'
                                    },
                                    yAxis: [{
                                        title: {
                                            text: 'Average Power [W]'
                                        }
                                    },

                                    ],
                                    xAxis: {
                                        title: {
                                            text: 'Hour of the day'
                                        },
                                        labels: {
                                            formatter: function () {

                                                return this.value + 'h';
                                            }
                                        }
                                    },


                                    plotOptions: {
                                        series: {
                                            label: {
                                                connectorAllowed: false
                                            },
                                            pointStart: 0
                                        }
                                    },
                                    legend: {
                                        layout: 'horizontal',
                                        align: 'center',
                                        verticalAlign: 'bottom'
                                    },
                                    series: series,


                                    tooltip: {
                                        enabled: true,
                                        valueDecimals: 2
                                    },
                                    responsive: {
                                        rules: [{
                                            condition: {
                                                maxWidth: 500
                                            },
                                        }]
                                    }

                                })


                            }



                            $popup.find(".mapboxgl-popup-content").busyLoad("hide");
                            $(window).on('resize', function () {
                                console.warn('window resize event')

                                charts = Highcharts.charts;

                                var w = $popup.find('.highcharts_all').width();
                                $popup.find('.highcharts_all .highcharts').width(w)
                                console.log('resizing')

                                charts.forEach(function (chart, index) {

                                    chart.setSize("100%", "100%", true);
                                    chart.reflow();

                                });


                            })

                        })



                    })
                }



            }

            if ($(e.target).is(':checkbox') && $(e.target).parents().closest('.map_table_labels').length > 0) {
                debugger

                switch (this_app.sel_f_admin.selected_code) {
                    case 'adm0_code':
                        var label_layer = 'gaul_0_labels';

                        break;
                    case 'adm1_code':
                        var label_layer = 'gaul_1_labels';
                        break;
                    case 'adm2_code':
                        var label_layer = 'gaul_2_labels';
                        break;
                    default:
                        break;
                }
                var input = $(e.target);
                if (input.is(':checked')) {
                    map.setFilter(label_layer, ['==', 'adm0_code', this_app.sel_f_admin.adm0_code]);





                } else {


                    map.setFilter(label_layer, null);
                }

            }


            if ($(e.target).parents().closest('.map_table_interaction').length > 0) {

                console.warn($(e.target).parents().closest('.map_table_interaction'))
                if ($(e.target).is(':checkbox')) {
                    var input = $(e.target)

                    setTimeout(function () {


                        if (input.is(':checked')) {

                            map.on('mousemove', debounced)
                            input.prop('checked', true);
                        } else {
                            $('.analysis_hover_info').hide()
                            $('#from_all_admin_simple_analysis_table .temp_higlighted_datatable_tr').removeClass('temp_higlighted_datatable_tr');
                            map.off('mousemove', debounced)
                            input.prop('checked', false);
                        }
                    }, 100)
                }
            }


            if ($(e.target).hasClass('download_excell_simple_analysis')) {
                $("body").busyLoad("show", this_app.generating_file_busy_option);
                $("body").busyLoad("show", this_app.generating_file_busy_option);
                var params = {

                    adm0_code: this_app['sel_f_admin'].adm0_code,
                    userid: this_app.userid,
                    selected_code: this_app['sel_f_admin'].selected_code

                };


                params.download = 'true';
                $.ajax({

                    //url: 'https://hsm.land.copernicus.eu/php/akp_energy/all_admin_simple_analysis_download.php',
                    url: 'https://hsm.land.copernicus.eu/php/akp_energy/all_admin_simple_analysis_download.php',

                    type: 'GET',
                    data: params,

                    dataType: 'json',
                    success: function (response) {

                        $("body").busyLoad("hide", this_app.generating_file_busy_option);
                        if (response.created_files == true) {


                            window.location = 'https://hsm.land.copernicus.eu/php/akp_energy/downloads/' + this_app.userid + '/zipped/' + this_app.userid + '.zip';

                        }
                    }
                })


            }
            if ($(e.target).hasClass('compare_from_popup')) {
                console.log(this_app)
                $('.map_legend_container').find('input').prop('checked', '');



                if (debounced) {
                    console.warn('debounced from hovering has been declared?')
                    map.off('mousemove', debounced);
                } else {
                    console.warn('debounced from hovering has still NOT been declared?')
                }
                $('.map_results_control_container').removeClass('inactive')
                $('.map_results_control_container').find('.map_legend_container').hide();
                var popup = $(e.target).parents().closest('.after_analysis_popup_html');

                var info = popup.data('info');


                if (this_app.prev_sel_f_admin) {
                    if ($('.from_all_admin_simple_analysis table.dataTable tbody tr').length == 0) {
                        console.log('we can have prev_sel_f_admin but no compared still!! TO BE IMPROVED, when coming back to a previous popup...')
                        all_admin_simple_analysis_datatables(info);
                        $('.mapboxgl-ctrl-bottom-right').show();
                        $('.from_all_admin_simple_analysis').show();
                        return false
                    }
                    if (info.selected_code != this_app.prev_sel_f_admin.selected_code) {

                        if ($('.map_it.active').length > 0)
                            $('.map_it.active').trigger('click');

                        if ($('.animated-icon', '#map').length > 0)
                            $('.animated-icon', '#map').remove()
                        console.log('querying new... ' + this_app.sel_f_admin.selected_code);
                        debugger
                        var adm_code_name = this_app.sel_f_admin.selected_code;
                        if (this_app.prev_sel_f_admin[adm_code_name] == this_app.sel_f_admin[adm_code_name]) {

                            console.log('querying again... ' + this_app.sel_f_admin.selected_code + ' and same value, ' + this_app.sel_f_admin[adm_code_name]);
                            setTimeout(function () {
                                console.warn('not really used this_app.sel_f_admin.highlight_params.highlight_layer...')
                                map.setPaintProperty(this_app.sel_f_admin.highlight_params.highlight_layer.id, "fill-opacity", 0);

                            }, 2800)

                            $('.from_all_admin_simple_analysis .dataTable tbody tr.higlighted_datatable_tr').removeClass('higlighted_datatable_tr');


                            $('.from_all_admin_simple_analysis .dataTable tbody tr').each(function (i, d) {

                                if (parseInt($(this).attr('_code')) == parseInt(this_app.sel_f_admin[this_app.sel_f_admin.selected_code])) {
                                    $(this).addClass('higlighted_datatable_tr')
                                }

                            });
                        } else {
                            all_admin_simple_analysis_datatables(info);
                        }
                    } else {
                        console.log(this_app.sel_f_admin);

                        console.warn('table already there for code ' + info[info.selected_code])
                        var code_name = info.selected_code;
                        var code_val = info[info.selected_code];
                        console.log(this_app.prev_sel_f_admin[code_name], code_val, this_app.prev_sel_f_admin[info.selected_code] == code_val)

                        $('.map_results_control_container').show();
                        if (info.selected_code !== 'adm0_code') {
                            if (info['adm0_code'] == this_app.prev_sel_f_admin['adm0_code']) {

                                setTimeout(function () {
                                    var f = $('.from_all_admin_simple_analysis .dataTable tbody .adm_td div[new_code="' + code_val + '"]');
                                    $('.from_all_admin_simple_analysis .dataTable tbody .higlighted_datatable_tr').removeClass('higlighted_datatable_tr')

                                    var parent_tr = f.parents().closest('.mdc-data-table__row')
                                    parent_tr.addClass('higlighted_datatable_tr');
                                    $('.from_all_admin_simple_analysis .dataTables_scrollBody').scrollTop(0);
                                    var scroll_to = parseInt(parent_tr.position().top);
                                    $('.from_all_admin_simple_analysis .dataTables_scrollBody').animate({
                                        scrollTop: scroll_to
                                    }, 500);
                                }, 800)
                            } else {
                                console.warn('querying again... on different country?')
                                all_admin_simple_analysis_datatables(info)
                            }
                        } else {
                            setTimeout(function () {
                                var f = $('.from_all_admin_simple_analysis .dataTable tbody .adm_td div[new_code="' + code_val + '"]');
                                $('.from_all_admin_simple_analysis .dataTable tbody .higlighted_datatable_tr').removeClass('higlighted_datatable_tr')
                                console.warn(f)
                                var parent_tr = f.parents().closest('.mdc-data-table__row')
                                parent_tr.addClass('higlighted_datatable_tr');
                                $('.from_all_admin_simple_analysis .dataTables_scrollBody').scrollTop(0);
                                var scroll_to = parseInt(parent_tr.position().top);
                                $('.from_all_admin_simple_analysis .dataTables_scrollBody').animate({
                                    scrollTop: scroll_to
                                }, 500);
                            }, 800)
                        }


                    }
                } else {
                    all_admin_simple_analysis_datatables(info)
                }

                $('.mapboxgl-ctrl-bottom-right').show();
                $('.from_all_admin_simple_analysis').show();
            }

            if ($(e.target).hasClass('extend_popup_analysis_info')) {
                var t = $(e.target)

                console.log(t.parents().closest('.after_analysis_popup_html'))
                console.info(t.parents().closest('.after_analysis_popup_html').data())
                var params = t.parents().closest('.after_analysis_popup_html').data('info');

                switch (params.selected_code) {
                    case 'adm0_code':
                        var to_query = 'adm0';
                        break;
                    case 'adm1_code':
                        var to_query = 'adm1';
                        break;
                    case 'adm2_code':
                        var to_query = 'adm2';
                        break;
                    default:
                        break;
                }


                var params = {
                    adm_code_name: params.selected_code,
                    adm_code_value: params[params.selected_code],
                    userid: this_app.userid,
                    to_query: to_query
                };
                console.warn(params)

                plot_adm_analysis(params)


            }
        });

        function promiseA2ei(a2ei_info, id) {

            console.warn(arguments)

            console.warn(this_app.a2ei.ceat_data)
            console.trace();
            return $.ajax({
                //url: "https://hsm.land.copernicus.eu/php/akp_energy/a2ei_info.php",
                url: "https://pere.gis-ninja.eu/akp_energy/a2ei/a2ei_info.php",
                type: "GET",
                data: { id: id },
                dataType: "json",
                async: true,

                success: function (data) {

                    var highcharts_html = '';
                    highcharts_html += '<div class="a2ei row" style="display:none;overflow-y:auto;"> <div class="a2ei_div" class="container"> <div class="popup_info_container_a2ei row">'

                    console.log(a2ei_info, data)
                    if (this_app.a2ei.ceat_data) {
                        var modeled_data = this_app.a2ei.ceat_data.modeled_data;
                        var inCeat = this_app.a2ei.ceat_data.gid;
                    }


                    if (inCeat) {
                        console.log(modeled_data)
                        var CeatData = this_app.a2ei.ceat_data;

                        highcharts_html += '<table class="table"> <thead> <tr> <th></th> <th>CEAT</th> <th>A2EI</th> </tr></thead><tbody>';
                        highcharts_html += `<tr><td></td><td>Category: ${CeatData['category_l']}</td><td>Facilities: ${data.facilities}</td>`

                        highcharts_html += `<tr><td>CAPEX</td><td>${CeatData['capex_soft']}</td><td>${a2ei_info.capex}</td>`

                        highcharts_html += `<tr><td>LCOEH</td><td>${CeatData['lcoeh_soft']}</td><td>${a2ei_info.lcoe}</td>`
                        highcharts_html += `<tr><td>PV</td><td>${CeatData['pv']}</td><td>${a2ei_info.pv_kwp}</td>`
                        highcharts_html += `<tr><td>Total demand</td><td>${CeatData['eldemand']}</td><td>${a2ei_info.total_demand_kwh}</td>`
                        highcharts_html += `<tr><td>Other</td><td></td><td><div>Power reading: ${data.power_reading}</div>
                            <div>Type of network: ${data.type_of_network}</div>
                            <div>Type of smartmeter: ${data.type_of_smartmeter}</div>
                            </td></tr>`
                        highcharts_html += '</body></table>'

                    }
                    else {
                        highcharts_html += '<table> <thead> <tr> <th></th> <th>CEAT</th> <th>A2EI</th> </tr></thead><tbody>';
                        highcharts_html += `<tr><td></td><td>Category: No data</td><td>Facilities: ${data.facilities}</td>`

                        highcharts_html += `<tr><td>CAPEX</td><td>No data</td><td>${a2ei_info.capex}</td>`

                        highcharts_html += `<tr><td>LCOEH</td><td>No data</td><td>${a2ei_info.lcoe}</td>`
                        highcharts_html += `<tr><td>PV</td><td>No data</td><td>${a2ei_info.pv_kwp}</td>`
                        highcharts_html += `<tr><td>Total demand</td><td>No data</td><td>${a2ei_info.total_demand_kwh}</td>`
                        highcharts_html += `<tr><td>Other</td><td></td><td><div>Power reading: ${data.power_reading}</div>
                            <div>Type of network: ${data.type_of_network}</div>
                            <div>Type of smartmeter: ${data.type_of_smartmeter}</div>
                            </td></tr>`
                        highcharts_html += '</body></table>'
                    }
                    highcharts_html += ' <div style="display:inline-flex;justify-content:center;margin-top:10px;"> <img style="width:50%" src="https://hsm.land.copernicus.eu/php/ceat2/media/' + a2ei_info.a2ei_id + '.jpg"  alt="" class="responsive-img"/></div><div class="highcharts_all"> <div class="highcharts"> </div>'
                        + '<div class="a2ei_chart_description"><div class="withChart">Load profiles of monitored health facilities over local daytime (on-grid and off-grid). Solid lines represent the average power; the filled area marks the variance of the power through all the measurements at the same hour of the day</div><div style="text-align:center" class="withoutChart row">No data available at this moment to generate the charts</div> </div>'
                    if (data.appliances.includes('{NULL}')) {

                        highcharts_html += '<div class="appliances_style">No appliances data available</div></div>'
                    }
                    else {
                        const jsonArray = data.appliances.slice(1, -1).split('","');
                        var arr = []
                        var data_appliances = jsonArray.map((d, i) => {
                            if (i == 0) {
                                return JSON.parse(d + '"')
                            } else {

                                if (i < jsonArray.length - 1) {
                                    arr.push(JSON.parse('"' + d + '"'))
                                    return JSON.parse('"' + d + '"')
                                } else {
                                    arr.push(JSON.parse('"' + d))
                                    return JSON.parse('"' + d)
                                }
                                console.info(arr)

                            }
                        }).map(d => JSON.parse(d))

                        console.log(data_appliances);

                        var trs = '';
                        data_appliances.forEach((d) => {
                            trs += '<tr><td>' + d.appliance + '</td>'
                            trs += '<td>' + d.type + '</td>'
                            trs += '<td>' + d.power_w + '</td>'
                            trs += '<td>' + d.quantity + '</td>';
                        })

                        highcharts_html += '<div class="appliances_style">Existing Appliances in Health Centre</div></div>'

                        highcharts_html += '<table> <thead> <tr> <th></th> <th>Type</th> <th>Power (w)</th> <th>Quantity</th> </tr> </thead> <tbody>' + trs + '</tbody> </table>';
                    }

                    highcharts_html += '</div> </div>';
                    console.log(highcharts_html)
                    if (this_app.hc_general_html) {


                        this_app.a2eiPopup = { html: this_app.hc_general_html, highcharts_html: highcharts_html }
                    }
                    else {

                        //<i class="close_popup bi bi-x-circle-fill right"></i>
                        var general_html = '<div class="row"> <div class="col offset-s9 col-3"> <i a2ei_id="' + id + '" class="a2ei_chart material-icons right a2ei">insert_chart</i>';
                        general_html += '</div></div> '
                        general_html += '<div class="popup_info_container_all no_ceat row"> <div class="popup_info_container ' + id + ' col s12"></div><div class="row" style="text-align:center;color:orange">No corresponding CEAT health centre</div></div>';
                        this_app.a2eiPopup = { html: general_html, highcharts_html: highcharts_html }
                    }

                }
            })
        }

        function create_hc_popup_html(properties, a2ei_json) {

            console.warn(arguments)


            this_app.hc_general_html = null;

            this_app.popup_active_counts++;
            var _id = this_app.popup_active_counts;
            var layer_info = app_data.layers_info.filter(function (d) {

                return d.id == 'hospitals'
            })[0];
            var legends = layer_info.params_symbology;
            var active_param = layer_info.active_param;


            if (properties) {
                var gid = properties['gid'];
                if (!a2ei_json && app_data.a2ei_geojson) {

                    var o = app_data.a2ei_geojson.features.filter(d => d.properties.ceat_gid == gid)
                    if (o.length > 0) {
                        var a2ei_info = o[0].properties;
                    } else {
                        var a2ei_info = null;
                    }
                } else {
                    var a2ei_info = a2ei_json;
                }


                if (a2ei_info) {
                    // <i class="close_popup bi bi-x-circle-fill right"></i>
                    var general_html = '<div class="row"> <div class="col offset-s9 col-3"><i a2ei_id="' + a2ei_info.a2ei_id + '" class="a2ei_chart material-icons right a2ei">insert_chart</i>';
                }
                else {
                    // <i class="close_popup bi bi-x-circle-fill right"></i>
                    var general_html = '<div class="row"> <div class="col offset-s9 col-3"> ';
                }
                general_html += '</div></div> ';


                if (properties.facility_n && properties.facility_n !== 'null') {
                    var hc_name = properties.facility_n;
                }
                else {
                    if (a2ei_info && a2ei_info.a2ei_name) {
                        var hc_name = a2ei_info.a2ei_name;
                    }
                }


                var hc_name = hc_name ? hc_name : 'Unknown name'

                general_html += '<div class="popup_info_container_all row"> <div class="f_name center">' + hc_name + ' </div> <div class="popup_info_container ceat ' + _id + ' col-12">';




                var props_in_popup = ['capex_soft', 'lcoeh_soft', 'pv', 'eldemand'];
                console.warn(properties)

                console.warn(properties['lcoeh_soft'])
                var p_in_popup = {
                    category_l: properties['category_l'],

                    capex_soft: parseInt(properties['capex_soft']),
                    lcoeh_soft: properties['lcoeh_soft'].toFixed(2),
                    pv: parseFloat(properties['pv']).toFixed(2),
                    eldemand: parseFloat(properties['eldemand'])
                };

                for (var p in p_in_popup) {
                    console.info(p)
                    var val = properties[p + '_class'];
                    console.log(val)
                    var pos = layer_info.params_arr.indexOf(p);

                    if (p !== 'category_l' && p !== 'gen_type_class' && p !== 'dist_grid') {

                        var leg_prop = legends[pos]['legend'][val];

                        var markup = '<span class="col-6 badge left" style="background-color:' + leg_prop.color + '!important">' + leg_prop.text + '</span>';
                    } else {
                        var markup = '';
                    }


                    if (p == active_param) {

                        general_html += '<div class="row highlighted_row center">';
                    } else {

                        general_html += '<div class="row center">';
                    }


                    if (p == 'category_l' || p == 'gen_type_class') {
                        console.warn(layer_info.params_description)
                        console.warn(pos)
                        general_html += '<div class="col-12 popup_col_f_name">' + layer_info.params_description[pos].short_title + '</div><div class="row info_container"><span>' + p_in_popup[p] + '</span></div></div>';
                    }

                    if (p !== 'category_l' && p !== 'gen_type_class' && p !== 'dist_grid') {
                        var pos = layer_info.params_arr.indexOf(p);

                        general_html += '<div class="col popup_col_f_name s12">' +
                            layer_info.params_description[pos].short_title +
                            '</div><div class="row info_container"><span class="left col-4">' + p_in_popup[p] + '</span>' + markup + '</div></div>';
                    }

                    if (p == 'dist_grid')
                        html += '<div class="col popup_col_f_name s6"> Distance (km) </div><div class="col s6 info_container"><span class="badge">' + properties['dist_grid'] + 'Km</span></div></div>';


                }

                this_app.hc_general_html = general_html;


            }


            if (a2ei_json) {

                console.info(a2ei_json)

                this_app.a2ei = { id: a2ei_json.a2ei_id, series: [], properties: a2ei_json, ceat_data: properties };
                var t = promiseA2ei(this_app.a2ei.properties, this_app.a2ei.id).then(() => {

                    return this_app.a2eiPopup;
                })

                console.info(t)
                return t;




            }
            else {
                this_app.a2eiPopup = this_app.hc_general_html;

                return { html: this_app.hc_general_html }
            }




        }

        function queryHC_gid(data, a2ei_info, latlng) {


            return $.ajax({
                type: "GET",
                dataType: 'json',
                async: true, // this is by default false, so not need to mention
                data: data,

                //url: 'https://hsm.land.copernicus.eu/php/akp_energy/get_hc_gid2.php',
                url: 'https://hsm.land.copernicus.eu/php/akp_energy/get_hc_gid2.php',
                success: function (properties) {
                    console.log(properties)


                    $.when(create_hc_popup_html(properties[0], a2ei_info)).done((resp) => {
                        console.log(this_app.a2eiPopup)

                        console.info(resp)
                        var _id = this_app.popup_active_counts;

                        console.log(JSON.stringify(this_app.a2eiPopup));
                        new mapboxgl.Popup({
                            closeButton: true,
                            closeOnClick: false,
                            className: 'feature_map_popup ' + _id, //+ ' ' + layer_to_query,
                            offset: 15

                        })

                            .setLngLat(latlng)
                            .setHTML(this_app.a2eiPopup.html)
                            .addTo(map);
                        $('.feature_map_popup.' + _id).find('.popup_info_container_all').append(this_app.a2eiPopup.highcharts_html);

                        if ($('.feature_map_popup.' + _id).find('.a2ei_chart').length > 0) {
                            $('.feature_map_popup.' + _id).find('.a2ei_chart').trigger('click');
                            $('.feature_map_popup.' + _id).find('.a2ei_chart').hide();
                        }

                        $('.feature_map_popup.' + _id).attr('popup_id', _id).attr('position', _id);


                    })

                }
            })

        }

        function queryHC(a2ei_info, t, latlng) {
            console.warn(arguments);
            var layer_info = app_data.layers_info.filter(function (d) {

                return d.id == 'hospitals'

            })[0];


            var _id = this_app.popup_active_counts;

            if (!a2ei_info) {
                var gid = t.properties['gid'];
                if (app_data.a2ei_geojson)
                    var a2ei_info = app_data.a2ei_geojson.features.filter(d => d.properties.ceat_gid == gid
                        || d.properties.a2ei_id == t.properties.a2ei_id)[0];

                if (!a2ei_info) {
                    console.log('uqerying normal hc')
                    console.log(t)
                    var legends = layer_info.params_symbology;
                    var active_param = layer_info.active_param;

                    $.when(create_hc_popup_html(t.properties, null)).done((resp) => {


                        console.warn(resp)
                        var _id = this_app.popup_active_counts;


                        new mapboxgl.Popup({
                            closeButton: false,
                            closeOnClick: false,
                            className: 'feature_map_popup ' + _id, //+ ' ' + layer_to_query,
                            offset: 15

                        })

                            .setLngLat(latlng)
                            .setHTML(resp.html)
                            .addTo(map);



                    })
                }
                else {
                    console.log('uqerying normal hc with associated a2ei')

                    if (!a2ei_info.ceat_gid) {


                        console.log('no corresponding ceat_gid')

                        //<i class="close_popup bi bi-x-circle-fill right"></i>'
                        var html = '<div class="row">';

                        this_app.a2ei = { id: a2ei_info.a2ei_id, series: [] }

                        var _id = this_app.popup_active_counts;

                        $.when(create_hc_popup_html(null, a2ei_info[0])).done((resp) => {
                            console.log(this_app.a2eiPopup.highcharts_html)

                            console.info(resp)

                            console.log(this_app.a2eiPopup)
                            new mapboxgl.Popup({
                                closeButton: true,
                                closeOnClick: false,
                                className: 'feature_map_popup ' + _id, //+ ' ' + layer_to_query,
                                offset: 15

                            })

                                .setLngLat(latlng)
                                .setHTML(this_app.a2eiPopup.html)
                                .addTo(map);
                            $('.feature_map_popup.' + _id).find('.popup_info_container_all').append(this_app.a2eiPopup.highcharts_html);

                            if ($('.feature_map_popup.' + _id).find('.no_ceat').length > 0) {
                                $('.feature_map_popup.' + _id).find('.a2ei_chart').trigger('click');
                            }

                            $('.feature_map_popup.' + _id).attr('popup_id', _id).attr('position', _id);


                        })

                        $('.feature_map_popup.' + this_app.a2ei.id).find('.a2ei_chart').trigger('click');
                        return false


                    }
                    else {
                        var data = { gid: a2ei_info.ceat_gid, a2ei_id: a2ei_info.a2ei_id }
                        $.when(queryHC_gid(data, a2ei_info, latlng)).then((properties) => {

                            console.log(properties)
                        })
                    }


                }

            }
            else {
                console.log('querying a a2ei')
                console.log(a2ei_info)

                if (!a2ei_info.ceat_gid) {


                    console.log('no corresponding ceat_gid')

                    //<i class="close_popup bi bi-x-circle-fill right"></i>
                    var html = '<div class="row">';

                    this_app.a2ei = { id: a2ei_info.a2ei_id, series: [] }

                    var _id = this_app.popup_active_counts;

                    $.when(create_hc_popup_html(null, a2ei_info)).done((resp) => {
                        console.log(this_app.a2eiPopup.highcharts_html)

                        console.info(resp)

                        console.log(this_app.a2eiPopup)
                        new mapboxgl.Popup({
                            closeButton: true,
                            closeOnClick: false,
                            className: 'feature_map_popup ' + _id, //+ ' ' + layer_to_query,
                            offset: 15

                        })

                            .setLngLat(latlng)
                            .setHTML(this_app.a2eiPopup.html)
                            .addTo(map);
                        $('.feature_map_popup.' + _id).find('.popup_info_container_all').append(this_app.a2eiPopup.highcharts_html);

                        if ($('.feature_map_popup.' + _id).find('.no_ceat').length > 0) {
                            $('.feature_map_popup.' + _id).find('.a2ei_chart').trigger('click');
                        }

                        $('.feature_map_popup.' + _id).attr('popup_id', _id).attr('position', _id);


                    })

                    $('.feature_map_popup.' + this_app.a2ei.id).find('.a2ei_chart').trigger('click');
                    return false


                }
                else {
                    var data = { gid: a2ei_info.ceat_gid, a2ei_id: a2ei_info.a2ei_id }
                    $.when(queryHC_gid(data, a2ei_info, latlng)).then((properties) => {

                        console.log(properties)

                    })
                }
            }
        }

        map.on('click', function (e) {

            console.warn(this_app.clickType)
            console.trace()

            this_app.clickType = 'simple';
            console.log('clicking somwhere')
            this_app.popup_active_counts++;
            var _id = this_app.popup_active_counts;

            if (this_app.drawing_rect == true)
                return false;

            console.warn(this_app.measuring_line)

            if (this_app.measuring_line == true) {

                var features = map.queryRenderedFeatures(e.point, {
                    layers: ['measure-points']
                });
                console.log(line_geojson)
                console.log(linestring)
                distanceContainer.innerHTML = '';
                console.log(features.length)
                if (features.length) {
                    var id = features[0].properties.id;
                    line_geojson.features = line_geojson.features.filter(function (point) {
                        return point.properties.id !== id;
                    });
                } else {
                    var point = {
                        'type': 'Feature',
                        'geometry': {
                            'type': 'Point',
                            'coordinates': [e.lngLat.lng, e.lngLat.lat]
                        },
                        'properties': {
                            'id': String(new Date().getTime())
                        }
                    };

                    line_geojson.features.push(point);
                }
                console.warn(line_geojson.features.length)
                if (line_geojson.features.length > 1) {
                    if (line_geojson.features.length > 2) {
                        console.warn('already 2 points');
                        this_app.measuring_line = false;


                        $('.distance-container').hide();
                        console.log('hide distance')
                        line_geojson.features = [];
                        linestring.coordinates = [];



                    } else {



                        linestring.geometry.coordinates = line_geojson.features.map(function (
                            point
                        ) {
                            return point.geometry.coordinates;
                        });

                        line_geojson.features.push(linestring);
                        var value = document.createElement('span');
                        value.textContent =
                            'Distance: ' +
                            turf.length(linestring).toLocaleString() +
                            'km';
                        distanceContainer.appendChild(value);
                        $('.distance-container').show();
                        console.info('show distance')
                        map.getCanvas().style.cursor = 'unset';
                        this_app.measuring_line = false;



                    }


                }
                map.getSource('line_geojson_source').setData(line_geojson);
                console.info(line_geojson)
                return false;
            }
            clearTimeout(clickTimeout);

            clickTimeout = setTimeout(() => {

                console.log('single click')

                /* clearTimeout(clickTimeout);
                // Delay single-click action to distinguish from double-click

                clickTimeout = setTimeout(() => { */
                console.log("Single click detected!", e.lngLat);
                var features = map.queryRenderedFeatures(e.point);

                var queryable_layers = app_data.layers_info.filter(d => d.queryable).map(d => d.id);

                var t = features.filter(d => queryable_layers.includes(d.layer.id))
                if (t.length == 0) {

                    if (map.getLayer('a2ei_points') && map.getSource('a2ei_geojson_source') && map.isSourceLoaded('a2ei_geojson_source')) {
                        var json_features = map.queryRenderedFeatures(e.point, {
                            layers: ['a2ei_points']
                        });
                        console.info(json_features)

                        if (json_features.length > 0) {
                            console.log('FIRST querying a2ei without a HC layer below')
                            t = json_features;
                        }
                        else {
                            console.warn('maybe clicking on raster 1??')
                            if (map.getLayer('schools')) {
                                this_app.getFeature(e);
                            }
                            return false;
                        }

                    }
                    else {

                        if (map.getLayer('schools')) {
                            this_app.getFeature(e);
                        }
                        return false;
                    }
                }

                if (t.length > 1) {
                    console.warn('more than 1 queryable layer')
                    t = t[t.length - 1]
                }
                else {
                    t = t[0]
                }



                var layer_to_query = t.layer.id;


                if (t.layer.id == 'refugee_camps') {
                    var layer_info = app_data.layers_info.filter(function (d) {

                        return d.id == 'refugee_camps_geojson'
                    })[0];
                } else {

                    var layer_info = app_data.layers_info.filter(function (d) {

                        return d.id == layer_to_query
                    })[0];
                }
                console.warn(layer_info)
                console.info(t)


                var properties = t.properties;
                var proj_point = [];
                if (t.sourceLayer !== 'clustered_pop') {
                    var c = t.geometry.coordinates;
                } else {
                    var c = [+properties.latitude, +properties.longitude]
                }


                console.info(c)
                proj_point['centroid'] = [c[0], c[1]];
                proj_point.x = point_project([c[0], c[1]]).x;
                proj_point.y = point_project([c[0], c[1]]).y;

                svg.selectAll('rect')
                    .transition()
                    .duration(100)
                    .attr("fill", '#000');


                var centroids = d3.selectAll('.points_container')
                    .selectAll('circle.new')
                    .data([proj_point])
                    .enter()
                    .append('circle')
                    .classed('selected', true)
                    .classed(layer_to_query, true)
                    .attr('r', 20)
                    .attr('cx', function (d) {

                        return d.x
                    })
                    .attr('cy', d => d.y)

                    .attr('fill', d => '#ff9800')
                    .attr('stroke', d => 'white')
                    .transition()
                    .delay(20)
                    .duration(duration)
                    .attr('r', function (d) {
                        return 5
                    })
                    .style('opacity', 0)
                    .call(transition_circle, minRadius, maxRadius);

                setTimeout(function () {
                    svg.selectAll('rect')
                        .transition()
                        .duration(200)

                        .attr("fill", 'rgba(0,0,0,0.01)');
                }, 100)


                if (t._geometry.coordinates) {

                    var latlng = t._geometry.coordinates
                    console.log(latlng)

                }
                else {
                    var latlng = e.lngLat;
                }


                if (layer_to_query == 'hospitals' || layer_to_query == 'a2ei_points') {

                    if (properties.a2ei_id > 0) {

                        var a2ei_info = app_data.a2ei_geojson.features.filter(d => d.properties.a2ei_id == properties.a2ei_id)[0].properties;
                    }
                    else {
                        console.warn('noi a2ei')
                        var a2ei_info = null;
                    }

                    queryHC(a2ei_info, t, latlng)
                }
                else {
                    if (layer_to_query.includes('taf_') || layer_to_query.includes('ref_unhcr_merged_simple')) {


                        if (layer_to_query == 'taf_hydrogen_grouped') {

                            return false;

                        }
                        var properties = t.properties;
                        console.log(properties)

                        var extra_popup_params;

                        if (layer_info.id.includes('pumps')) {
                            var layer_info = app_data.layers_info.filter(function (d) {
                                return d.id == 'taf_pumps_up'

                            })[0]
                            if (layer_info.extra_popup_params)
                                extra_popup_params = layer_info.extra_popup_params;
                        }
                        else {
                            extra_popup_params = layer_info.extra_popup_params;
                        }
                        //<div class="col-3"><i class="close_popup bi bi-x-circle-fill right"></i></div>
                        var html = '<div class="row"><div style="font-size:1.7rem;color:_orange" class="col-12 center">' + layer_info.title + '</div> </div>';

                        if (layer_to_query.includes('ref_unhcr_merged_simple'))
                            html += `<div class="row info_container"><span style="font-size:1.2rem;color:orangered;" class="center col-12">${properties.gis_name}</span></div>`


                        var active_param = layer_info.active_param;

                        if (!layer_info.params_arr) {
                            console.warn('no params_arr??')
                            return false;
                        }

                        layer_info.params_arr.forEach(function (d, i) {
                            console.log(d) //the name of the param to plot



                            if (d == layer_info.active_param)
                                html += '<div class="row d-flex text-align-center highlighted_row">';
                            else
                                html += '<div class="row d-flex text-align-center">';


                            var symbology = layer_info.params_symbology[i];

                            var val = this_app.roundIfNecessary(properties[d], false);
                            console.log(properties[d], val)

                            if (symbology.using_class == false) {
                                //check

                                if (typeof val == 'string') {

                                    if (d === 'loc_subtyp') {
                                        //in fact we should change in db...
                                        var val = symbology.legend.filter(d => d.code == val)[0].val;
                                    }
                                    else {

                                        var val = val;
                                    }
                                }
                                else {

                                    var val = symbology.legend.filter(d => d.code == val)[0].val;
                                }
                            }
                            console.log(properties[d], val, typeof properties[d], typeof val);

                            if (!val) var val = 'No data';
                            if (layer_info.id.includes('wind')) {
                                html += '<div class="col-8 popup_col_f_name">' + symbology.title + '</div><div class="col-4 info_container text-end">' + val + '</div>';
                            }
                            else {
                                if (layer_info.id.includes('pumps'))
                                    html += '<div class="col-8 popup_col_f_name">' + symbology.title + '</div><div class="col-4 info_container text-end">' + val + '</div>';
                                else
                                    html += '<div class="col-7 popup_col_f_name">' + symbology.title + '</div><div class="col-5 info_container text-end">' + val + '</div>';
                            }
                            html += '</div>'


                        });




                        if (extra_popup_params) {

                            if (layer_info.id.includes('pumps')) {
                                layer_info.extra_popup_params.forEach(function (d, i) {

                                    html += '<div class="row valign-wrapper"><div class="col-9 popup_col_f_name">' + d.title + '</div><div class="col-3 info_container text-end">' +
                                        properties[d.name] + '</div></div>';


                                })
                            }
                            else {

                                if (layer_info.id === 'ref_unhcr_merged_simple') {
                                    console.warn(layer_info.extra_popup_params)
                                    var extra_popup_params_filtered = layer_info.extra_popup_params.filter(d => {
                                        console.warn(d.name, properties[d.name])

                                        if (properties[d.name] && +properties[d.name] > 0) {

                                            return d
                                        }

                                    });

                                }
                                else {
                                    console.log(layer_info.extra_popup_params)
                                    var extra_popup_params_filtered = layer_info.extra_popup_params.filter(d => {
                                        console.warn(d.name, properties[d.name])

                                        if (properties[d.name] && +properties[d.name] > 0) {

                                            return d
                                        }

                                    });
                                }
                                console.warn(extra_popup_params_filtered)
                                extra_popup_params_filtered.forEach(function (d, i) {


                                    if (layer_info.id.includes('wind'))

                                        html += '<div class="row d-flex text-align-center"><div class="col-8 popup_col_f_name">' + d.title + '</div><div class="col-4 info_container text-end">';
                                    else
                                        html += '<div class="row d-flex text-align-center"><div class="col-7 popup_col_f_name">' + d.title + '</div><div class="col-5 info_container text-end">';


                                    if (d.name.includes('url')) {
                                        html += '<a href="' + properties[d.name] + '" target="_blank">' + properties[d.name] + '</a</div></div>';
                                    }

                                    else {



                                        html += numberWithCommas(properties[d.name]) + '</div></div>';
                                    }



                                })
                            }

                        }
                        if (layer_info.id == 'taf_bess') {
                            if (properties.lat == 0 || properties.lng == 0) {
                                html += '<div class="row" style="color:#ff6347b0"><div class="col-12 center">No location data available</div></div>';

                            }
                        }

                    }
                    else {
                        var legends = layer_info.params_symbology;
                    }
                    if (layer_to_query == 'ref_settlements') {


                        var properties = t.properties;
                        console.warn(properties)
                        var marker = $('.marker[id=' + parseInt(properties.id_code) + ']');

                        console.warn(layer_info)
                        //<div class="col-3"><i class="close_popup bi bi-x-circle-fill right"></i>
                        var html = '<div class="row"><div style="font-size:1.7rem;color:orange" class="col-12 center">' + layer_info.title + '</div></div></div>' +
                            '<div class="popup_info_container ' + properties.id_code + '"><div class="f_name center">' + properties.site_name + '</div>';



                        var props_in_popup = ['pv', 'bat', 'pop'];
                        var p_in_popup = {
                            dist_grid: numberWithCommas(properties['dist_grid']),
                            dist_border: numberWithCommas(properties['dist_border']),
                            pop: numberWithCommas(properties['pop']),
                            hh: numberWithCommas(parseInt(properties['hh'])),
                            bus: numberWithCommas(parseInt(properties['bus'])),
                            inst: numberWithCommas(parseInt(properties['inst'])),

                        };
                        console.log(p_in_popup)
                        for (var i in layer_info.params_non_analyse_description) {
                            console.info(i)
                            var d = layer_info.params_non_analyse_description[i];
                            console.warn(d)

                            //html += '<div class="row d-flex text-align-center"><div class="col-8 popup_col_f_name">' + d + '</div><div class="col-4 info_container text-end"><span class="bg-secondary badge" data-badge-caption="">' + p_in_popup[i] + '</span></div></div>';
                            html += '<div class="row d-flex text-align-center"><div class="col-8 popup_col_f_name">' + d + '</div><div class="col-4 info_container text-end">' + p_in_popup[i] + '</div></div>';
                        }

                        var active_param = layer_info.active_param;
                        layer_info.params_arr.forEach(function (d, i) {
                            console.log(d) //the name of the param to plot
                            if (d == layer_info.active_param)
                                html += '<div class="row highlighted_row align-items-center">';
                            else
                                html += '<div class="row align-items-center">';

                            html += '<div class="popup_col_f_name col-8">' + layer_info.params_description[i].long_title_txt + '</div>';

                            var symbology = layer_info.params_symbology[i];
                            var _class = parseInt(properties[d + '_class']); //0,1,2...
                            html += `<div class="col-4 info_container text-end">${numberWithCommas(properties[d])}</div>`;

                            /*  if (d == 'tot_cost_usd') {
                                 var markup = '<span class="col-5 badge left" style="background-color:' + symbology.legend[_class].color + '!important" data-badge-caption="' + symbology.legend[_class].text + '"></span>';
                                 html += '<div class="row info_container"><span class="left col-5">' + numberWithCommas(parseInt(properties[d])) + '</span>' + markup + '</div>';
                             } else {
                                 if (_class) {
                                     var markup = '<span class="col-6 badge left" style="background-color:' + symbology.legend[_class].color + '!important" data-badge-caption="' + symbology.legend[_class].text + '"></span>';
                                     html += '<div class="row info_container"><span class="left col-4">' + numberWithCommas(properties[d]) + '</span>' + markup + '</div>';
                                 }
                             } */

                            html += "</div>";

                        })

                    }


                    if (layer_to_query == 'power_plants') {


                        //<div class="col-3"><i class="close_popup bi bi-x-circle-fill right"></i></div>
                        var html = '<div class="row"><div style="font-size:1.7rem;color:orange" class="col-12 center">' + layer_info.title + '</div></div>' +
                            '<div class="popup_info_container ' + _id + '"><div class="f_name center">' + properties.name + '</div>';
                        var props_in_popup = ['mw', 'gen_type'];
                        var p_in_popup = {
                            gen_type: properties['gen_type'],
                            mw: parseFloat(properties['mw']).toFixed(2)
                        };

                        for (var p in p_in_popup) {
                            console.info(p)
                            var pos = layer_info.params_arr.indexOf(p);
                            if (p == 'mw') {

                                var val = properties[p + '_class'];

                                var leg_prop = legends[pos]['legend'][val];

                                var markup = '<span class="col center" style="background-color:' + leg_prop.color + '!important">' + leg_prop.text + '</span>';

                            } else {
                                var markup = '';
                            }

                            if (p == active_param) {

                                html += '<div class="row highlighted_row align-items-center">';
                            } else {
                                html += '<div class="row align-items-center">';
                            }

                            if (p == 'gen_type') {

                                html += '<div class="col-12 center popup_col_f_name">' + layer_info.params_description[pos].short_title + '</div><div class="row info_container center"><span>' + p_in_popup[p] + '</span></div></div>';
                            }

                            if (p !== 'gen_type') {

                                html += '<div class="col-12 center popup_col_f_name">' +
                                    layer_info.params_description[pos].short_title +
                                    '</div><div class="row info_container"><span class="center col-6">' + p_in_popup[p] + '</span>' + markup + '</div></div>';

                            }
                        }
                    }

                    var popup = new mapboxgl.Popup({
                        closeButton: true,
                        closeOnClick: false,
                        className: 'feature_map_popup ' + _id + ' ' + layer_to_query,
                        offset: 1

                    })

                    if (layer_to_query.includes('pumps') || layer_to_query.includes('solar') || layer_to_query == 'taf_geotermal_ungrouped' || layer_to_query.includes('wind')) {

                        // popup.setMaxWidth("35vw");
                        /* popup.setMaxWidth("1000px"); */


                    }
                    popup.setLngLat(latlng)
                        .setHTML(html)
                        .addTo(map);

                    setTimeout(function () {


                        $('.mapboxgl-popup').show();

                    }, 100)
                }



            }, 500)



        });
        var arr = ['windid', 'solarpvid', 'solarirrid', 'agbid', 'pvvsdieselid', 'batteryid', 'lcoeid', 'travelid'];

        var data = africa_platform_layers_obj.filter(function (d) {
            return arr.indexOf(d.layer_id) > -1
        });

        var all_html = '';
        var geo_base_url = 'https://geospatial.jrc.ec.europa.eu/geoserver';
        var data_objs = data.reduce(function (memo, t_data, i) {

            var leg_obj = [];
            var t_legend = $.parseHTML(t_data.legend.safe_value)
            console.log(t_legend)
            var lis = $(t_legend).find('.c-legend-choropleth_stats li');
            console.info(lis)


            var colors_div = $(t_legend).find('.c-legend-choropleth_stats .icon-choropleth_stats');
            console.warn(colors_div)


            if (colors_div.length == 0) {
                console.log('no colors_div')
                console.log(t_legend)
                var html = '<li class="main_layer ' + t_data.layer_id + '"> <div class="card e_black">  <div class="card-content"> <span class="card-title activator"><div class="row">';
                html += '<div class="afr_layer_title col-9">' + t_data.title + '</div><div class="col-3 d-flex justify-content-end"><i class="material-icons highlight layers right">layers</i><i class="bi bi-brightness-high-fill opacity"></i></div></div>';
                html += '<div class="d3_legend">' + t_data.legend.value + '</div><div class="range-wrap"><div class="range-value rangeV"></div><input class="range_z" type="range" min="0" max="100" value="100" step="1"></span></div>';
                html += '<div class="card-reveal"> <span class="card-title grey-text text-darken-4"><i class="material-icons close_custom_reveal right "></i>' + t_data.title + '</span> <p class="more abstract_modal ">Cost of electrcity (USD/kWh) produced by a off-grid<span class="moreellipses">...&nbsp;</span><span class="morecontent"><span>d diesel generator</span>&nbsp;&nbsp;<a href="" class="morelink"><i class="material-icons">keyboard_arrow_down</i></a></span> </p> <hr> </div> </div></div> </li>';
                $('.layers_list').append(html)

            } else {
                colors_div.map(function (i, d) {
                    var color = $(d).attr('style').split('background-color: ')[1].split('!important')[0];
                    leg_obj.push({
                        color: color
                    })
                });
                console.log(leg_obj)
                var val_div = $(t_legend).find('.c-legend-choropleth_stats span.name');
                val_div.map(function (i, d) {
                    leg_obj[i].val = $(d).text();
                });
                t_data.energy_legend = leg_obj;

                $('.layers_list').append(generate_layer_li(t_data));
            }
            switch (t_data.server_type) {

                case 'GeoWebCache':

                    map.addSource(t_data.layer_id + '_source', {
                        "type": "raster",
                        "scheme": "tms",
                        "tiles": [geo_base_url + '/gwc/service/tms/1.0.0/' + t_data.wms_name + '@EPSG:900913@png/{z}/{x}/{y}.png']

                    });

                    app_data.layers_info.push({
                        id: t_data.layer_id,
                        _type: t_data.server_type,
                        'title': t_data.title
                    });

                    break;

                case 'Google':

                    break;

                case 'Geoserver':

                    var url = t_data.wms_url + '/wms';
                    map.addSource(t_data.layer_id + '_source', {
                        "type": "raster",


                        "tiles": [url + '?bbox={bbox-epsg-3857}&format=image/png&service=WMS&version=1.0.0&request=GetMap&srs=EPSG:3857&transparent=true&width=256&height=256&styles=' + t_data.wms_style + '&layers=' + t_data.wms_name]

                    });

                    app_data.layers_info.push({
                        id: t_data.layer_id,
                        _type: t_data.server_type,
                        'title': t_data.title
                    });

                    break;

                case 'Jeodpp':

                    break;
                default:
                    break;
            }


            memo.layers.push(t_data)

            if (i == data.length - 1) {


                $('.list.layers_list li.main_layer').each(function () {
                    var t = $(this);

                    var layer_name = t.attr('class').split(' ')[1];
                    var range = t.find('.range_z');
                    if (range.length > 0) {
                        var rangeV = t.find('.rangeV');

                        document.addEventListener("DOMContentLoaded", setValue(range, rangeV));
                        range.on('input', function (e) {

                            setValue($(e.target), rangeV);
                            console.log(opacity)
                        })
                        range.on('change', function (e) {
                            opacity = null;

                            this_app.throttle(change_opacity(e.target.value, layer_name, true), 500)
                        })
                    }

                })

            }
            return memo
        }, {
            layers: [],
            legends: []
        });

        $('.analysis_layer .activator i').on('mouseover', function (e) {


            $('.material-tooltip.instructions').remove();

            //  var layer_id = $(this).attr('class').split(' ')[1];
            var target = $(e.target);
            console.log(target)
            if (target.is('i') || target.hasClass('activator')) {


                console.log(target)


                if (target.hasClass('opacity')) {
                    var txt = 'Change layer symbology';
                }
                if (target.hasClass('highlight')) {
                    var txt = 'Add/remove layer';
                }
                if (target.hasClass('download')) {
                    var txt = 'Download per country';
                }
                if (target.hasClass('slide_layer')) {
                    var txt = 'Slide up/down information';
                }

                /* if (target.hasClass('col-8'))
                    var txt = 'Add/remove layer'; */

                target
                    .attr('data-tooltip', function () {
                        return `<div class="init_instructions">${txt}</div>`;

                    })
                    .tooltip_materialize({
                        delay: 50,
                        delayOut: 50000,
                        position: 'right',
                        html: true
                    });
                setTimeout(function () {
                    target.tooltip_materialize("open");
                    var container = $(".init_instructions").parents().closest('.material-tooltip').addClass('instructions')


                }, 100)
            }



        });

        function draw_a2ei() {

            var defs = d3.select('rect.rect_container').append('defs');
            var filter = defs.append("filter")
                .attr("id", "glow");

            filter.append("feGaussianBlur")
                .attr("class", "blur")
                .attr("stdDeviation", "1.5")
                .attr("result", "coloredBlur");

            var outer_centroids = d3.selectAll('.points_container')
                .selectAll('.a2ei_outer_centroids')
                .data(app_data.a2ei_geojson.features)
                .enter()
                .append('circle')
                .attr('class', 'a2ei_outer_centroids')

                .attr('r', 15)
                .attr('cx', function (d) {

                    return point_project(d.geometry.coordinates).x;
                })
                .attr('cy', d => point_project(d.geometry.coordinates).y)
                .attr('fill', (d) => {
                    if (d.properties.ceat_gid)
                        return '#ffc107'
                    else

                        return '#ff0067c7'
                })
                .attr('opacity', d => 0.6)
                .style("stroke", '#ff9800')
                .style("filter", "url(#glow)")

            var inner_centroids = d3.selectAll('.points_container')
                .selectAll('.a2ei_inner_centroids')
                .data(app_data.a2ei_geojson.features)
                .enter()
                .append('circle')
                .classed('a2ei_inner_centroids', true)
                .attr('r', 7)
                .attr('cx', function (d) {

                    return point_project(d.geometry.coordinates).x;
                })
                .attr('cy', d => point_project(d.geometry.coordinates).y)
                .attr('fill', (d) => {

                    if (d.properties.ceat_gid)
                        return '#ffc107'
                    else

                        return '#ff0067c7'

                })
                .style("stroke", 'white')
                .style("stroke-width", '1')
                .attr('opacity', d => 1);

            var out_circles = d3.selectAll('.a2ei_outer_centroids')


            function expandCircle() {
                out_circles
                    .transition()
                    .duration(2000)
                    .attr('r', 4)
                    .on('end', contractCircle);
            }

            function contractCircle() {
                out_circles
                    .transition()
                    .duration(2000)
                    .attr('r', 15)
                    .on('end', expandCircle);
            }

            expandCircle();




            console.warn(map.getZoom())
            console.warn(this_app.scale_r(map.getZoom()))

            map.setPaintProperty('a2ei_points', 'circle-opacity', .3);
            map.setPaintProperty('a2ei_points', 'circle-radius', this_app.scale_r(map.getZoom()))

        }

        $('.a2ei_div input').on('change', function (e) {
            console.log($(e.target))
            if ($(this).prop('checked')) {
                console.warn('checked')
                if ($('.a2ei_inner_centroids').length == 0) {
                    draw_a2ei();
                    map.setLayoutProperty('a2ei_points', "visibility", "visible");
                }
                $('.analysis_layer.hospitals .a2ei_div').find('.param_extended').show();



            } else {
                console.warn('not checkedsss')
                $('.points_container circle').remove();
                map.setLayoutProperty('a2ei_points', "visibility", "none");
                $('.analysis_layer.hospitals .a2ei_div').find('.param_extended').hide();
            }
        })



        $('.unhcr_div input[type="checkbox"]').change(function () {

            var layer_id = 'ref_unhcr_merged_simple';
            var $container = $('.unhcr_div.ref_unhcr_merged_simple');
            var t_layer = app_data.layers_info.filter(function (d) {

                return d.id == layer_id;
            })[0];

            if ($(this).attr('id') == 'show_hide_labels_input') {

                var f = map.getFilter(layer_id);



                if ($(this).is(':checked')) {
                    if (!map.getLayer(layer_id + '_labels')) {

                        map.addLayer(app_data.layers_labels_info.filter(d => d.id == layer_id + '_labels')[0]._layer);
                        if (f) {
                            map.setFilter(layer_id + '_labels', f)
                        }

                    }

                }
                else {
                    if (map.getLayer(layer_id + '_labels')) {

                        map.removeLayer(layer_id + '_labels');
                    }
                }
            }

            if ($(this).attr('id') == 'unhcr_div_input') {

                if ($(this).is(':checked')) {

                    if (!map.getLayer('ref_unhcr_polygons')) {

                        if (map.getLayer('ref_settlements')) {

                            map.addLayer(app_data.layers_info.filter(d => d.id == 'ref_unhcr_polygons')[0]._layer, 'ref_settlements');

                        }
                        else {

                            map.addLayer(app_data.layers_info.filter(d => d.id == 'ref_unhcr_polygons')[0]._layer)
                        }

                    }
                    map.addLayer(t_layer._layer);

                    var layer_labels = app_data.layers_labels_info.filter(d => d.id == layer_id + '_labels')[0];



                    app_data.temp_layer = t_layer._layer;
                    $container.find('.content').show();



                    this_app.generate_selects_legend($container, t_layer);
                    $container.find('.params_select').show();
                    if (t_layer.default_param) {

                        console.warn(t_layer)
                        console.warn(t_layer.mapbox_paint_style)

                        var style = t_layer.mapbox_paint_style.filter(d => d.param == t_layer.default_param)[0].style;
                        for (var p in style) {

                            map.setPaintProperty(t_layer.id, p, style[p]);
                        }
                    }
                } else {
                    map.removeLayer(t_layer.id);

                    $container.find('.show_hide_labels_form input[type="checkbox"]').prop('checked', false);


                    if (map.getLayer('ref_unhcr_polygons')) {
                        map.removeLayer('ref_unhcr_polygons');

                    }
                    if (map.getStyle().layers.filter(d => d.id == layer_id + '_labels').length > 0) {
                        if (map.getLayer(layer_id + '_labels'))
                            map.removeLayer(layer_id + '_labels');
                    }

                    // $('.unhcr_div.ref_unhcr_merged_simple').find('.content>div').hide();
                    $container.find('.d3_legend').removeClass('active');
                    $container.find('.content>div').hide();

                }
            }

        })


        $('li.analysis_layer,li.taf_layer').click(function (e) {



            var target = $(e.target);


            if (target.parents().closest('.unhcr_div').length > 0) {
                console.warn('unhcr_div layer!!')
                var _container = target.parents().closest('.unhcr_div');
                var layer_id = _container.attr('class').split(' ')[1];

            }
            else {
                var layer_id = $(this).attr('class').split(' ')[1];
                var _container = $(this);
            }
            var t_layer = app_data.layers_info.filter(function (d) {

                return d.id == layer_id;
            })[0];

            console.warn(target)
            if (!target.is('i') && target.parents().closest('.activator').length > 0
                || target.hasClass('activator')) {

                _container.find('.highlight.layers').trigger('click');

            }
            if ($(e.target).hasClass('slide_layer')) {

                console.warn('slide up or down')

                if ($(e.target).hasClass('on')) {
                    $(this).find('.card-content-content').hide();
                    $(e.target).removeClass('bi-caret-up-fill').addClass('bi-chevron-down-fill');
                    $(e.target).removeClass('on');
                }
                else {
                    $(this).find('.card-content-content').show();

                    $(e.target).removeClass('bi-chevron-down-fill').addClass('bi-caret-up-fill');

                    $(e.target).addClass('on');
                }

            }




            var parent = target.parent();




            if (target.hasClass('list_stats_container_title')) {

                if (_container.find('.list_stats_container').is(':visible')) {
                    debugger;
                    _container.find('.list_stats_country').trigger('click');


                }
            }

            if (target.hasClass('asTable')) {

                var layer_id = _container.attr('class').split(' ')[1];

                var table = $('#taf_modal table')
                if ($.fn.dataTable.isDataTable('#taf_modal table')) {
                    $('#taf_modal').modal_materialize('open');
                }
                else {

                    $.ajax({

                        //url: 'https://hsm.land.copernicus.eu/php/akp_energy/taf_info.php',
                        url: 'https://hsm.land.copernicus.eu/php/akp_energy/taf_info.php',


                        type: 'GET',

                        data: { taf_id: layer_id },

                        dataType: 'json',
                        success: function (resp) {
                            console.warn(resp)
                            var headers = resp.headers;
                            console.warn(headers.map((d) => `<tr>${d}</tr>`).join(''))
                            var data = resp.data;

                            var table = $('#taf_modal table')
                            if ($.fn.dataTable.isDataTable('#taf_modal table')) {
                                table.DataTable().destroy();
                                table.find('tbody tr,thead tr').empty();
                            }


                            $('#taf_modal').modal_materialize('open');
                            /*    table.find('tbody,thead tr').empty();
                               table.find('thead tr').append(headers.map((d) => `<th>${d}</th>`).join('')); */


                            setTimeout(function () {

                                var data_table_obj = table.DataTable({
                                    bSort: true,
                                    scrollY: "800px",
                                    dom: 'Bfrtip',
                                    // scrollX: true,
                                    scrollCollapse: true,
                                    paging: false,
                                    fixedColumns: false,
                                    fixedHeader: true,
                                    data: resp.data,
                                    buttons: [
                                        {
                                            extend: 'pdfHtml5',
                                            text: 'Export to PDF',


                                            customize: function (doc) {
                                                doc.defaultStyle = {
                                                    font: 'OpenSans',
                                                    bold: false, // Disable bold text,
                                                    fontSize: 10
                                                };

                                                // Remove all bold text explicitly
                                                if (doc.styles) {
                                                    for (var key in doc.styles) {
                                                        if (doc.styles[key].bold) {
                                                            doc.styles[key].bold = false;
                                                        }
                                                    }
                                                }
                                            },

                                        },
                                        {
                                            extend: 'excelHtml5',
                                            text: 'Export to Excel'


                                        }



                                    ],
                                    columns: headers.map(header => ({ title: header }))
                                });

                                /*  var data_table_obj = table.DataTable({
                                     bSort: false,
                                     scrollY: "800px",
                                     dom: 'Bfrtip',
                                     // scrollX: true,
                                     scrollCollapse: true,
                                     paging: false,
                                     fixedColumns: false,
                                     fixedHeader: true,
                                     data: resp.data,
                                     columns: headers.map(header => ({ title: header }))
                                 }); */


                                $('#taf_modal .download_excel_table').on('click', function () {
                                    $('#taf_modal').find('.buttons-excel').click();
                                })
                            }, 1000)


                        }
                    })

                }

            }

            if (target.parents().closest('.a2ei_div').length > 0) {



            }

            if (target.hasClass('show_hide_outside')) {

                if (target.hasClass('on') == false) {
                    target.addClass('on');
                    if ($('.show_hide_inside').hasClass('on')) {
                        t_layer.active_filters[0]['basic'] = ["in", "nea", 'yes', 'no'];



                    } else {
                        console.warn('show only inside')
                        t_layer.active_filters[0]['basic'] = ["in", "nea", 'yes'];
                    }
                } else {
                    target.removeClass('on');
                    if ($('.show_hide_inside').hasClass('on')) {
                        console.log('show only inside')
                        t_layer.active_filters[0]['basic'] = ["in", "nea", 'no'];
                    } else {
                        console.log('none active')
                        t_layer.active_filters[0]['basic'] = ["in", "nea", 'nooone'];


                    }
                }
                var all_filters = ["all"]

                for (var p in t_layer.active_filters) {
                    var key = Object.keys(t_layer.active_filters[p]);
                    if (t_layer.active_filters[p][key]) {
                        all_filters.push(t_layer.active_filters[p][key])
                    }

                }
                console.log(all_filters)

                map.setFilter(layer_id, all_filters)
            }
            if (target.hasClass('show_hide_inside')) {

                if (target.hasClass('on') == false) {
                    target.addClass('on');
                    if ($('.show_hide_outside').hasClass('on')) {
                        console.log('show all')
                        t_layer.active_filters[0]['basic'] = ["in", "nea", 'yes', 'no'];
                    } else {
                        console.warn('show only inside')
                        t_layer.active_filters[0]['basic'] = ["in", "nea", 'no'];
                    }
                } else {
                    target.removeClass('on');

                    if ($('.show_hide_outside').hasClass('on')) {
                        console.log('show only outside')
                        t_layer.active_filters[0]['basic'] = ["in", "nea", 'yes'];
                    } else {
                        console.log('none active')
                        t_layer.active_filters[0]['basic'] = ["in", "nea", 'noone'];

                    }
                }


                var all_filters = ["all"]

                for (var p in t_layer.active_filters) {
                    var key = Object.keys(t_layer.active_filters[p]);
                    if (t_layer.active_filters[p][key]) {
                        all_filters.push(t_layer.active_filters[p][key])
                    }

                }
                console.log(all_filters)

                map.setFilter(layer_id, all_filters)
            }

            function setLayerSource(layerId, source, sourceLayer) {
                console.warn(arguments)
                const oldLayers = map.getStyle().layers;
                console.log(oldLayers)
                const layerIndex = oldLayers.findIndex(l => l.id === layerId);
                console.warn(layerIndex)
                const layerDef = oldLayers[layerIndex];
                const before = oldLayers[layerIndex + 1] && oldLayers[layerIndex + 1].id;
                layerDef.source = source;
                if (sourceLayer) {
                    layerDef['source-layer'] = sourceLayer;
                }
                map.removeLayer(layerId);
                map.addLayer(layerDef, before);
            }

            if (target.hasClass('_info')) {
                target.toggleClass('on');
                $('.material-tooltip.instructions').remove();

                if (target.hasClass('on')) {


                    var html = '<div class="init_instructions"><p>The query tool for this layer if asctivated</p><p>Click on a map feature to query it</p></div>';


                    target
                        .attr('data-tooltip', function () {
                            return html;

                        })
                        .tooltip_materialize({
                            delay: 50,
                            delayOut: 50000,
                            position: 'right',
                            html: true
                        });
                    target.tooltip_materialize("open");

                    var pos = this_app.querying_popup_list.arr.indexOf(layer_id);
                    if (pos == -1) {
                        this_app.querying_popup_list.arr.push(layer_id);
                        var o = {
                            active: true,
                            layer_id: layer_id
                        };
                        if (t_layer.marker_join) {
                            var t_layer = app_data.layers_info.filter(function (d) {
                                return d.id == layer_id + '_geojson';
                            })[0];
                            o.active_param = t_layer.active_param;
                        } else {
                            o.active_param = t_layer.active_param;
                        }
                        this_app.querying_popup_list.list.push(o);
                    } else {
                        this_app.querying_popup_list.list[pos].active = true;
                    }
                } else {
                    var pos = this_app.querying_popup_list.arr.indexOf(layer_id);
                    this_app.querying_popup_list.arr.splice(pos, 1);
                    this_app.querying_popup_list.list.splice(pos, 1);
                }

            }
            if (target.hasClass('opacity')) {

                if (target.hasClass('on') == false) {

                    _container.find('.card-content-content').show();

                    target.addClass('on');
                    _container.find('.ranges_container').show();
                    _container.find('.ranges_container > .row').show().children().show();

                    setTimeout(function () {

                        console.info(_container.find('.ranges_container').height())
                        var l = _container.find('.ranges_container').offset().top - _container.find('.ranges_container').height() + 30
                        console.info(l)
                        $('.queryable_li .collapsible-body').animate({
                            scrollTop: l
                        }, 2000);

                    }, 500)
                } else {
                    target.removeClass('on');

                    _container.find('.ranges_container').hide();
                }



            }

            if (target.hasClass('general_layer_info_more')) {

                _container.find('.general_info_layer').toggle();
                target.toggleClass('on');
                target.hasClass('on') ? target.text('less info') : target.text('more info');

            }

            if (target.hasClass('more')) {

                var param = target.attr('class').split(' ')[1];


                if (param) {
                    if (param.includes('unhcr_')) {
                        var container = $('.unhcr_div.' + param);
                    }
                    else {
                        var container = $(this);
                    }
                }
                else {
                    var container = $(this);
                }
                if (target.hasClass('on')) {
                    //we hide the extended description
                    container.find('.extended_description').hide();
                    target.text('more info');

                } else {
                    //we show the extended description
                    var $extended_description_html = container.find('.extended_description_html');
                    container.find('.extended_description').show()


                    if (param && param.includes('unhcr_') == false) {
                        $extended_description_html.addClass('on');
                        setTimeout(function () {

                            $extended_description_html.removeClass('on');
                        }, 1500)

                        var _t = $(this)

                        if (param == 'category_l' || param == 'eldemand') {
                            setTimeout(function () {
                                var l = container.find('.extended_description_html').offset().top - container.find('.extended_description_html').height();
                                console.info(l)
                                $('.queryable_li .collapsible-body').animate({
                                    scrollTop: l
                                }, 1500);

                            }, 500)
                        }
                    }

                    target.text('less info');





                }
                target.toggleClass('on');

            }


            if (target.hasClass('list_stats_country') || target.parent().hasClass('list_stats_country')) {
                if (!target.hasClass('list_stats_country')) {
                    var target = target.parent();
                }

                console.log(layer_id)
                if (layer_id == 'ref_settlements') {

                    var layer_id = 'refugee_camps';
                }



                if (!target.hasClass('active')) {


                    var r = this_app.gaul0_global_info.filter(d => d.data[layer_id] > 0).sort(function (a, b) {
                        return b.data[layer_id] - a.data[layer_id]
                    }).reduce((memo, data) => {

                        memo.push({ name: data.adm0_name, counts: data.data[layer_id] })
                        return memo
                    }, []);
                    console.info(r)
                    _container.find('.list_stats_container').show();
                    _container.find('.list_stats_container ul').append(r.map(function (d) {
                        this_app.roundIfNecessary
                        return `<li class="row list-group-item d-flex"><span class="col-8">${d.name}</span><span class="col-4">${numberWithCommas(d.counts)}</span></li>`
                    }).join(''));

                    $('.sidenav').scrollTop();

                    var l = _container.find('.list_stats_container').offset().top - 90;
                    console.info(_container, l)
                    setTimeout(function () {
                        $('.sidenav').animate({
                            scrollTop: l
                        }, 1000);
                    }, 1500)

                }
                else {
                    _container.find('.list_stats_container').hide();
                }
                target.toggleClass('active');
            }
            if (target.hasClass('download_btn')) {
                if (target.hasClass('shp')) {
                    var req_format = 'shp';
                } else {
                    var req_format = 'xslx';
                }
                var sel_c = _container.find('.downloads_container select option:selected').attr('_id');
                $("#map").busyLoad("show", this_app.generating_file_busy_option);

                /*   var bbox = {
                      "sw": [
                          15.277642706337303,
                          12.387713374981118
                      ],
                      "ne": [
                          15.541259672861202,
                          12.546113994946879
                      ]
                  } */
                $.ajax({
                    //url: 'https://pere.gis-ninja.eu/akp_energy/downloads_new4.php',
                    url: 'https://hsm.land.copernicus.eu/php/akp_energy/downloads_new4.php',
                    data: {
                        userid: this_app.userid,
                        req_format: req_format,
                        layer_id: layer_id,
                        /* adm0_code: sel_c, */
                        adm_code_name: 'adm0_code',
                        adm_code_value: sel_c,
                        /* bbox: bbox, */
                        to_query: 'admin'
                    },
                    type: 'GET',
                    async: true,
                    dataType: 'json',

                    success: function (data) {


                        console.log(data)
                        if (data.success == true) {


                            var a = document.createElement('a');
                            console.log('https://pere.gis-ninja.eu/akp_energy/downloads/' + this_app.userid + '/zipped/' + layer_id + '_' + sel_c + '.zip')
                            a.href = 'https://hsm.land.copernicus.eu/php/akp_energy/downloads/' + this_app.userid + '/zipped/' + layer_id + '_' + sel_c + '.zip';
                            //a.href = 'https://pere.gis-ninja.eu/akp_energy/downloads/' + this_app.userid + '/zipped/' + layer_id + '_' + sel_c + '.zip';
                            document.body.appendChild(a);

                            a.click();
                            document.body.removeChild(a);
                            $("#map").busyLoad("hide", this_app.generating_file_busy_option);
                        }

                    }
                })

            }
            if (target.hasClass('dropdown-trigger')) {
                var $wrapper = _container.find('.downloads_container');



            }

            if (target.hasClass('download')) {

                if (target.hasClass('on') == false) {
                    target.addClass('on');
                    $(this).find('.card-content-content').show();

                    if (target.hasClass('direct_download')) {

                        var layer_name = _container.attr('class').split(' ')[1];

                        var a = document.createElement('a');

                        a.setAttribute("target", "_blank");

                        a.href = 'https://hsm.land.copernicus.eu/php/akp_energy/downloads/' + layer_name + '.zip';
                        a.download = "download";
                        document.body.appendChild(a);

                        a.click();
                        document.body.removeChild(a);



                        return false;
                    }
                    else {

                        var $form = _container.find('.downloads_container .form-select.countries_select');

                        //if ($form.find('option').length < 2)
                        if (!$form.hasClass('filled')) {

                            var options = '';
                            if (layer_id == 'ref_settlements')
                                var name = 'refugee_camps';
                            else
                                var name = layer_id;


                            if (!layer_id.includes('taf_')) {
                                var filtered = this_app.gaul0_global_info.filter(d => d.data[name] > 0)
                            }
                            else {
                                var filtered = this_app.gaul0_global_info.filter(d => d.data[name] > 0)
                            }
                            console.info(filtered)
                            filtered.forEach(function (d) {
                                options += "<option value='" + d.adm0_code + "' _id='" + d.adm0_code + "'>" +
                                    d.adm0_name + "  <p>" + d.data[name] + "</p></option>"
                            })
                            $form.append(options);
                            $form.addClass('filled');
                            _container.find('.downloads_container').show()
                            console.warn(_container)
                            $form.on('change', function () {

                                if ($(this).find('option:selected').attr('_id')) {

                                    _container.find('.downloads_container .download_btn').css('display', 'inline-block')

                                } else {
                                    /* if ($(this).find('option:selected').attr('_id')) { */
                                    _container.find('.download_btn').hide();
                                    //}
                                }
                            })



                        }

                        _container.find('.downloads_container').show();



                        setTimeout(function () {

                            $('.sidenav').scrollTop();

                            var l = _container.find('.downloads_container').offset().top - 50;
                            console.info(_container, l)
                            $('.sidenav').animate({
                                scrollTop: l
                            }, 1000);



                        }, 500)
                    }

                } else {
                    target.removeClass('on');
                    _container.find('.downloads_container').hide()
                }

            }
            if (target.hasClass('show_hide_all')) {

                if (t_layer.marker_join) {
                    var t_layer = app_data.layers_info.filter(function (d) {
                        console.warn(d.id, layer_id + '_geojson')
                        return d.id == layer_id + '_geojson';
                    })[0];
                }


                console.log(t_layer)
                var param = t_layer.active_param;
                if (target.hasClass('on')) {

                    if (t_layer.html_legend) {
                        console.info('html legend')
                    }
                    else {
                        d3.select('.circles_svg.' + layer_id).selectAll('.legendCells g.cell').each(function (d, i) {

                            d3.select(this).classed('off', false)
                        });

                        target.removeClass('on');
                    }


                    if (t_layer._type && t_layer._type == 'marker') {
                        var $ref = $('.marker.refugee_camps');

                        $ref.show();


                    }


                } else {
                    target.addClass('on')
                    if (t_layer.html_legend) {
                        console.warn('html legend')
                    }
                    else {
                        d3.select('.circles_svg.' + layer_id).selectAll('.legendCells g.cell').each(function (d, i) {

                            d3.select(this).classed('off', true)
                        });

                    }

                    if (t_layer._type && t_layer._type == 'marker') {
                        var $ref = $('.marker.refugee_camps');

                        $ref.hide();


                    }

                }
                var params_description = t_layer.params_description.filter(function (d) {

                    return d.param_name == param
                })[0];

                var scale_val = t_layer.params_symbology.filter(function (d) {
                    return d.param_name == param
                })[0].legend;

                var classed = false;

                if (!params_description.subtype_param) {
                    var classed = true;
                    var f = ['in', param + '_class'];

                } else {
                    if (params_description.subtype_param == 'literal') {
                        console.log('NON categorical index but literal')
                        var f = ['in', param];

                    }
                }

                if (t_layer._type !== 'marker') {

                } else {
                    var f = []
                }

                var present_val = [];
                d3.select('.circles_svg.' + layer_id).selectAll('.legendCells g.cell').each(function (d, i) {

                    if (d3.select(this).classed('off') == false) {

                        if (classed) {
                            f.push(i)
                        }
                        else {
                            f.push(scale_val[i].code)
                        }
                        present_val.push(i)

                    }


                });


                if (t_layer._type !== 'marker' && !t_layer.raster) {

                    if (f.length == 2) {
                        f.push('nooone')
                    }

                    if (t_layer.id == 'taf_pumps_up')
                        debugger
                    t_layer.active_filters[1]['param_filter'] = f;
                    if (t_layer.id == 'taf_pumps_up') {
                        app_data.layers_info.filter(function (d) {
                            return d.id == 'taf_pumps_down' || d.id == 'taf_pumps_lines';
                        }).forEach(function (d) {
                            console.log(d);
                            d.active_filters[1]['param_filter'] = f;

                        })
                    }
                }



                if (t_layer._type && t_layer._type !== 'marker') {
                    var all_filters = ["all"];

                    if (t_layer.raster) {

                        console.log(f, t_layer.active_filters, t_layer.active_filters[0]['param_filter']);
                        t_layer.active_filters[0]['param_filter'] = present_val
                        this_app.set_raster_cql(t_layer);

                    }
                    else {

                        for (var p in t_layer.active_filters) {
                            var key = Object.keys(t_layer.active_filters[p]);
                            if (t_layer.active_filters[p][key]) {
                                all_filters.push(t_layer.active_filters[p][key])
                            }

                        }

                        map.setFilter(layer_id, all_filters);

                        if (t_layer.id == 'taf_pumps_up') {
                            map.setFilter('taf_pumps_down', all_filters);
                            map.setFilter('taf_pumps_lines', all_filters);

                        }

                        if (map.getLayer(layer_id + '_labels')) {
                            map.setFilter(layer_id + '_labels', all_filters);

                        }
                    }
                    console.log(all_filters)


                }

            }

            if (target.hasClass('highlight')) {


                if (target.hasClass('on') == false) {

                    target.addClass('on');

                    this_app.add_layer(layer_id, add = true);




                } else {

                    target.removeClass('on');

                    this_app.add_layer(layer_id, add = false);





                }

            }

        });
        //africa knowledge platform
        $('.indicators_li .layers_list li').click(function (e) {
            var layer_id = $(this).attr('class').split(' ')[1];
            console.info(layer_id)
            var t_layer = data_objs.layers.filter(function (d) {
                return d.layer_id == layer_id;
            })[0];
            var _container = $(this);
            var target = $(e.target);

            var parent = target.parent();

            console.info(target)

            if (!target.is('i') && target.parents().closest('.activator').length > 0
                || target.hasClass('activator')) {
                debugger
                _container.find('.highlight.layers').trigger('click');

            }

            if (target.hasClass('opacity')) {

                if (target.hasClass('on') == false) {
                    target.addClass('on');
                    $(this).find('.range-wrap').show();
                } else {
                    target.removeClass('on');
                    $(this).find('.range-wrap').hide();
                }

                $('.distance_range').hide();

            }



            if (target.hasClass('highlight')) {

                if (target.hasClass('on') == false) {

                    $(this).find('.card-content-content').show();
                    if ($(this).find('.opacity').length > 0)
                        $(this).find('.opacity').show();


                    $(this).find('.d3_legend').show();

                    if (t_layer.server_type == 'Geoserver') {
                        var url = 'https://geospatial.jrc.ec.europa.eu/geoserver/africa_platform/wms';
                        var layer = {
                            'id': layer_id,
                            'type': 'raster',
                            'source': layer_id + '_source',
                            'paint': {}
                        }

                        africa_platform_layers.push({
                            _layer: layer,
                            layer_id: layer_id
                        })
                    } else {


                        var layer = {
                            'id': layer_id,
                            'type': 'raster',
                            'source': layer_id + '_source',
                            'paint': {}
                        }


                    }

                    africa_platform_layers.push({
                        _layer: layer,
                        layer_id: layer_id
                    })
                    var b_index;
                    console.info(map.getStyle().layers)
                    var all_l = map.getStyle().layers.map(d => d.id);
                    if (all_l.indexOf('ghs_africa_250m_bigger_1_5') > -1) {
                        debugger
                        var pos = all_l.indexOf('ghs_africa_250m_bigger_1_5');
                        map.addLayer(layer, all_l[pos]);
                    } else {
                        map.addLayer(layer, 'gaul_level_0_line');
                    }
                    console.log(t_layer)
                    this_app.create_d3_legend('africa_platform', t_layer);

                } else {
                    $(this).find('.card-content-content').hide();
                    $(this).find('.d3_legend,.extended_description,.range-wrap').hide();
                    map.removeLayer(layer_id);

                    if ($(this).find('.opacity').length > 0)
                        $(this).find('.material-icons.opacity').hide().removeClass('on');
                }

                target.toggleClass('on')
            }
        })


    }) //end map.load



    function get_analysis_by_adm(e) {

        console.log(arguments);
        var e = this_app.selected_e;

        $("#map").busyLoad("show", this_app.calculation_busy_option);

        $('.sidenav').css('max-height', 'unset');
        $('.analysis_container').empty().hide();

        var level = this_app.get_analysis_by_adm;
        switch (level) {
            case 'adm0':
                var code_name = 'adm0_code';
                var layer = gaul_level_0;
                var highlight_layer = gaul_level_0_highlighted;
                break;

            case 'adm1':
                var code_name = 'adm1_code';
                var layer = gaul_level_1;
                var highlight_layer = gaul_level_1_highlighted;
                break;

            case 'adm2':
                var code_name = 'adm2_code';
                var layer = gaul_level_2;
                var highlight_layer = gaul_level_2_highlighted;
                break;
            default:
                break;
        }
        /* setTimeout(function () { */

        console.log(e)



        var bbox = [
            [e.point.x - 1, e.point.y - 1],
            [e.point.x + 1, e.point.y + 1]
        ];
        console.warn(highlight_layer)
        var features = map.queryRenderedFeatures(bbox, {
            layers: [highlight_layer.id]
        });

        console.log(features)
        if (!features[0]) {
            alert('it seems there is a problem here or simply there is nothing');
            $("#map").busyLoad("hide", this_app.calculation_busy_option);
            return false;
        }

        var this_f_p = features[0].properties;

        if (this_app.sel_f_admin) {
            this_app.prev_sel_f_admin = this_app.sel_f_admin;
        }

        this_app.sel_f_admin = {
            setFilter_data: [{
                layer_name: 'hospitals'
            }, {
                layer_name: 'power_plants'
            }, {
                layer_name: 'refugee_camps'
            }, {
                layer_name: 'electric_grid'
            }]
        };


        for (var p in this_app.sel_f_admin.setFilter_data) {
            this_app.sel_f_admin.setFilter_data[p].adm_filter_activated = true;
            this_app.sel_f_admin.setFilter_data[p].bbox_filter_activated = false;
        }


        if (code_name == 'adm0_code') {
            var adm_full_text = this_f_p.adm0_name;
            this_app.sel_f_admin.selected_code = 'adm0_code';
            this_app.sel_f_admin.adm0_code = this_f_p.adm0_code;
            var code_value = this_f_p.adm0_code;

            this_app.sel_f_admin.iso2_code = this_f_p.iso2_code;
            this_app.sel_f_admin.adm0_name = this_f_p.adm0_name;

            var highlight_layer = 'gaul_level_0_highlighted';
            var highlight_layer_line = 'gaul_level_0_highlighted_line';

        }

        if (code_name == 'adm1_code') {
            var adm_full_text = this_f_p.adm0_name + '/' + this_f_p.adm1_name;
            this_app.sel_f_admin.selected_code = 'adm1_code';
            this_app.sel_f_admin.adm0_name = this_f_p.adm0_name;
            this_app.sel_f_admin.adm0_code = this_f_p.adm0_code;

            this_app.sel_f_admin.adm1_name = this_f_p.adm1_name;
            this_app.sel_f_admin.adm1_code = this_f_p.adm1_code;
            var code_value = this_f_p.adm1_code;

            var highlight_layer = 'gaul_level_1_highlighted';
            var highlight_layer_line = 'gaul_level_1_highlighted_line';

        }

        if (code_name == 'adm2_code') {
            this_app.sel_f_admin.selected_code = 'adm2_code';
            this_app.sel_f_admin.adm0_name = this_f_p.adm0_name;
            this_app.sel_f_admin.adm0_code = this_f_p.adm0_code;

            this_app.sel_f_admin.adm1_name = this_f_p.adm1_name;
            this_app.sel_f_admin.adm1_code = this_f_p.adm1_code;

            this_app.sel_f_admin.adm2_name = this_f_p.adm2_name;
            this_app.sel_f_admin.adm2_code = this_f_p.adm2_code;
            var code_value = this_f_p.adm2_code;

            var highlight_layer = 'gaul_level_2_highlighted';
            var highlight_layer_line = 'gaul_level_2_highlighted_line';

        }

        console.log(code_name)
        console.warn(this_f_p[code_name])


        this_app.after_selecting_adm_unit(this_f_p[code_name], level, e.lngLat);

        if (!map.getLayer(highlight_layer)) {


            map.addLayer(window[highlight_layer]);
            map.addLayer(window[highlight_layer_line]);



        }
        //check value in array
        if (!this_app.sel_highlighted_arr)
            this_app.sel_highlighted_arr = [];

        var higlighted_pos = this_app.sel_highlighted_arr.indexOf(code_value);
        if (higlighted_pos == -1) {
            this_app.sel_highlighted_arr.push(code_value);
        }

        this_app.fx.highlight_adm_unit(code_name, code_value, adding = true);



        /*   map.setPaintProperty(highlight_layer_line, 'line-opacity', [
              'case',
              ['in', ['get', code_name], ['literal', this_app.sel_highlighted_arr]], // If in the array
              2, // Set fill-opacity to 1
              0 // Otherwise, set to 0.5
          ]) */
        //var filter = ['in', code_name].concat();
        // var opacity_expression = ['in', code_name].concat(this_app.sel_highlighted_arr);
        // ["match", ["get", code_name], code_value, '#ffd617', '#0f0f0f'];
        // var opacity_expression = ["match", ["get", code_name], code_value, 1, 0];

        /* map.setFilter(highlight_layer, filter);
        map.setFilter(highlight_layer_line, filter);

        map.setPaintProperty(highlight_layer, "fill-opacity", 1);
        map.setPaintProperty(highlight_layer_line, "line-opacity", 1); */
        //map.setFilter(highlight_layer, 'fill-opacity', opacity_expression);

        /*  map.setLayoutProperty(highlight_layer, 'visibility', 'visible');

         var line_color_expression = ["match", ["get", code_name], code_value, '#ff7c17', '#7d7272'];
         map.setPaintProperty(highlight_layer_line, "line-color", line_color_expression);

         var line_opacity_expression = ["match", ["get", code_name], code_value, 3, 0];
         map.setPaintProperty(highlight_layer_line, "line-opacity", line_opacity_expression);

         map.setLayoutProperty(highlight_layer_line, 'visibility', 'visible'); */

        setTimeout(function () {
            map.setPaintProperty(highlight_layer, 'fill-opacity', 0);
        }, 700)

        $("#map").busyLoad("hide")


    }
    $('.adm_unit_polygon_btn').click(function () {


        $(this).removeClass('active');
        this_app.prev_sel_f_admin = null;
        var sel_admin = $('.materialize_form_radio').find('input:checked').attr('level');
        if (sel_admin) {

            $('.map_results_control_container').hide();
            this_app.selected_e = null;
            this_app.get_analysis_by_adm = null;
            if ($('.map_results_control_container').find('table').length > 0) {
                $('.datatable_legend').find('input:checked').each(function () {
                    if ($(this).parents().closest('.map_table_interaction').length > 0) {
                        $(this).trigger('click');
                    } else {
                        $(this).trigger('click');
                    }
                })
                if ($('.map_it.active', '.datatable_legend').length > 0) {
                    $('.map_it.active', '.datatable_legend').trigger('click')
                }
                $('.mapboxgl-popup.after_analysis_popup').hide()
            }

            switch (sel_admin) {
                case 'adm0':

                    map.setPaintProperty('gaul_0_labels', "text-opacity", 1);
                    if (map.getLayer('gaul_level_0_highlighted')) {

                        map.removeLayer('gaul_level_0_highlighted_line');
                        map.removeLayer('gaul_level_0_highlighted');
                        map.removeLayer('gaul_level_0_analysis');
                    }
                    break;
                case 'adm1':

                    map.setPaintProperty('gaul_1_labels', "text-opacity", 1);
                    if (map.getLayer('gaul_level_1_highlighted')) {
                        map.removeLayer('gaul_level_1_highlighted_line');
                        map.removeLayer('gaul_level_1_highlighted');
                        map.removeLayer('gaul_level_1_analysis');
                    }
                    break;

                case 'adm2':

                    map.setPaintProperty('gaul_2_labels', "text-opacity", 1);
                    if (map.getLayer('gaul_level_2_highlighted')) {
                        map.removeLayer('gaul_level_2_highlighted_line');
                        map.removeLayer('gaul_level_2_highlighted');
                        map.removeLayer('gaul_level_2_analysis');
                    }
                    break;
                default:
                    break;
            }
            $('.materialize_form_radio').find('input:checked').prop('checked', false);

        }


        $('.adm_unit_polygon_instructions').css('display', 'none');
        if ($('.after_analysis_popup').length > 0)
            this_app.after_analysis_popup.remove();

        if ($('.extra_info.row.analysis_container').is(':visible')) {
            $('.extra_info.row.analysis_container').hide();
            $('#map').removeClass('bordered');
        }


    })
    $('.time_travel_form_select').on('change', function () {


        console.log('time_travel_form_select change')
        var layer_id = $(this).find('option:selected').attr('value');
        $('li.analysis_layer.time_travel').attr('active_layer', layer_id);


        this_app.add_layer(layer_id, add = true);



    })

    $('.materialize_form_radio').on('change', function () {


        this_app.drawing_rect = false;
        map.getCanvas().style.cursor = 'pointer';


        if ($('.adm_unit_polygon_btn').hasClass('active') == false)
            $('.adm_unit_polygon_btn').addClass('active')

        if ($('.after_analysis_popup').length > 0)
            this_app.after_analysis_popup.remove();


        var sel_admin = $(this).find('input:checked').attr('value');

        this_app.get_analysis_by_adm = sel_admin;
        switch (sel_admin) {
            case 'adm0':


                var container = $('#overlays_switcher #gaul_level_0').parents().closest('.valign-wrapper');
                if (container.find('input:checkbox').prop('checked') == false) {
                    container.find('input:checkbox').prop('checked', true);
                    container.find('.overlays_switcher_palette').addClass('active')
                }


                if (!map.getLayer('gaul_level_0')) {
                    console.warn(gaul_level_0_line, gaul_level_0)
                    map.addLayer(gaul_level_0_line);
                    map.addLayer(gaul_0_labels);
                }

                if (!map.getLayer('gaul_level_0_highlighted')) {

                    map.addLayer(gaul_level_0_highlighted);
                    map.addLayer(gaul_level_0_highlighted_line);

                }

                if (map.getLayer('gaul_level_2_highlighted')) {

                    map.removeLayer('gaul_level_2_highlighted');
                    map.removeLayer('gaul_level_2_highlighted_line');
                    map.removeLayer('gaul_level_2');
                    map.removeLayer('gaul_2_labels');

                }

                if (map.getLayer('gaul_level_1_highlighted')) {
                    map.removeLayer('gaul_level_1_highlighted');
                    map.removeLayer('gaul_level_1_highlighted_line');
                    map.removeLayer('gaul_level_1');
                    map.removeLayer('gaul_1_labels');

                }


                break;

            case 'adm1':
                var container = $('#overlays_switcher #gaul_level_1').parents().closest('.valign-wrapper');
                if (container.find('input:checkbox').prop('checked') == false) {
                    container.find('input:checkbox').prop('checked', true);

                    container.find('.overlays_switcher_palette').addClass('active').show()
                }
                if (!map.getLayer('gaul_level_1')) {
                    var layerId = 'gaul_level_1';
                    var pos_2 = map.getStyle().layers.map(d => d.id).indexOf('gaul_level_2');
                    if (pos_2 > -1) {

                        //gert ovwer it
                        var over = map.getStyle().layers[pos_2 + 1].id;
                        console.warn(map.getStyle().layers[pos_2 + 1].id)
                        map.addLayer(window['gaul_level_1'], over)
                        map.addLayer(window['gaul_1_labels'], layerId);
                    }
                    else {
                        var base_id = map.getStyle().layers[1].id;
                        var pos = map.getStyle().layers.map(d => d.id).indexOf('gaul_level_0_line');
                        if (pos > -1) {
                            map.addLayer(window['gaul_level_1'], 'gaul_level_0_line')
                            map.addLayer(window['gaul_1_labels'], layerId);
                        }
                        else {
                            var base_id = map.getStyle().layers[1].id;
                            map.addLayer(window[layerId], base_id)
                            map.addLayer(window['gaul_1_labels'], layerId);
                        }
                    }
                }

                if (!map.getLayer('gaul_level_1_highlighted')) {


                    map.addLayer(gaul_level_1_highlighted);
                    map.addLayer(gaul_level_1_highlighted_line);
                }

                if (map.getLayer('gaul_level_2_highlighted')) {
                    map.removeLayer('gaul_level_2');
                    map.removeLayer('gaul_level_2_highlighted');
                    map.removeLayer('gaul_level_2_highlighted_line');
                    map.removeLayer('gaul_2_labels');

                }


                if (map.getLayer('gaul_level_0_highlighted')) {
                    map.removeLayer('gaul_level_0_highlighted');
                    map.removeLayer('gaul_level_0_highlighted_line');

                }




                break;

            case 'adm2':
                var container = $('#overlays_switcher #gaul_level_1').parents().closest('.valign-wrapper');
                if (container.find('input:checkbox').prop('checked') == false) {
                    container.find('input:checkbox').prop('checked', true);
                    container.find('.overlays_switcher_palette').addClass('active')
                }
                if (!map.getLayer('gaul_level_2')) {
                    map.addLayer(gaul_level_2);
                    map.addLayer(gaul_2_labels);
                }

                if (!map.getLayer('gaul_level_2_highlighted')) {

                    map.addLayer(gaul_level_2_highlighted_line);
                    map.addLayer(gaul_level_2_highlighted);

                }

                if (map.getLayer('gaul_level_1_highlighted')) {
                    map.removeLayer('gaul_level_1');
                    map.removeLayer('gaul_1_labels');
                    map.removeLayer('gaul_level_1_highlighted_line');
                    map.removeLayer('gaul_level_1_highlighted');

                }
                break;
        }

        $('.adm_unit_polygon_instructions').css('display', 'block');
    })




    $('#taf_modal').modal_materialize();
    $('#stats_modal').modal_materialize({
        opacity: 1,
        dismissible: false,
        onCloseEnd: function () {
            console.log('onCloseEnd #stats_modal');

            if ($('.mapboxgl-popup').length > 0)
                $('.mapboxgl-popup').show();

            if ($('.analysis_container .adm_name').length > 0) {
                $('.analysis_container').show();
                debugger
                $('body').css('overflow-y', 'auto!important');


            }
        },
        onOpenEnd: function () {


        }
    });

    setTimeout(function () {

        $('.busy-load-container').hide();

        console.log(map.getStyle().layers)



        map.resize();
        $('.busy-load-container').remove();


        var popperButton_countries = $("#popper-button-countries")[0];


        var popperPopup_countries = document.querySelector("#popper-popup-countries");


        var popperArrow_countries = document.querySelector("#popper-arrow-countries");


        function createInstance_countries() {
            popperInstance = Popper.createPopper(popperButton_countries, popperPopup_countries, {
                placement: "bottom", //preferred placement of popper
                modifiers: [{
                    name: "offset", //offsets popper from the reference/button
                    options: {
                        offset: [-55, 15]
                    }
                }, {
                    name: "flip", //flips popper with allowed placements
                    options: {
                        allowedAutoPlacements: ["right", "left", "top", "bottom"],
                        rootBoundary: "viewport"
                    }
                }]
            });
        }
        function destroyInstance_countries() {
            if (popperInstance) {
                popperInstance.destroy();
                popperInstance = null;
            }
        }
        function showPopper_countries() {
            popperPopup_countries.setAttribute("show-popper", "");
            popperArrow_countries.setAttribute("data-popper-arrow", "");
            createInstance_countries();
        }

        function destroyInstance() {
            if (popperInstance) {
                popperInstance.destroy();
                popperInstance = null;
            }
        }
        function hidePopper_countries() {
            popperPopup_countries.removeAttribute("show-popper");
            popperArrow_countries.removeAttribute("data-popper-arrow");
            destroyInstance();
        }
        function togglePopper_countries() {
            if (popperPopup_countries.hasAttribute("show-popper")) {
                hidePopper_countries();
            } else {
                showPopper_countries();
            }
        }

        var popperButton_highlight = $("#popper-button-highlight")[0];


        const popperPopup_highlight = document.querySelector("#popper-popup-highlight");


        const popperArrow_highlight = document.querySelector("#popper-arrow-highlight");


        /*   function createInstance_highlight() {
              popperInstance = Popper.createPopper(popperButton_highlight, popperPopup_highlight, {
                  placement: "right", //preferred placement of popper
                  modifiers: [{
                      name: "offset", //offsets popper from the reference/button
                      options: {
                          offset: [0, 30]
                      }
                  }, {
                      name: "flip", //flips popper with allowed placements
                      options: {
                          allowedAutoPlacements: ["right", "left", "top", "bottom"],
                          rootBoundary: "viewport"
                      }
                  }]
              });
          } */
        function destroyInstance_highlight() {
            if (popperInstance) {
                popperInstance.destroy();
                popperInstance = null;
            }
        }
        function showPopper_highlight() {
            //  popperPopup_highlight.setAttribute("show-popper", "");
            popperArrow_highlight.setAttribute("data-popper-arrow", "");
            createInstance_highlight();
        }
        function hidePopper_highlight() {
            //   popperPopup_highlight.removeAttribute("show-popper");
            popperArrow_highlight.removeAttribute("data-popper-arrow");
            destroyInstance();
        }
        /*  function togglePopper_highlight() {

             if (popperPopup_highlight.hasAttribute("show-popper")) {
                 hidePopper_highlight();
             } else {
                 showPopper_highlight();
             }
         } */
        popperButton_highlight.addEventListener("mouseover", function (e) {
            e.preventDefault();
            console.warn(e)


            //  togglePopper_highlight();
        });




        setTimeout(function () {
            togglePopper_countries();

            // togglePopper_highlight();
            for (var counts = 0; counts < 7; counts++) {
                var s = $("i.highlight").css('font-size');
                console.log(s)
                $("i.highlight").animate({ "color": '#ffeb3b', fontSize: '1.4rem!important' }, { queue: true, duration: 500 })
                    .animate({ "color": 'white', fontSize: s }, { queue: true, duration: 500 });
                $("i.highlight").css('font-size', s);

            }

            setTimeout(function () {
                $('.popper-popup').hide();

            }, 8000)
        }, 1000);

    }, 2000)

    introModal._element.addEventListener('show.bs.modal', function () {
        $introModal.find('div.active').removeClass('active');
        $('.info_panels_modal .info').hide();
        $('.initial_modal').show();

    })

    introModal._element.addEventListener('hidden.bs.modal', function () {




    });

    $('#intro_modalss').modal_materialize({
        opacity: 0.8,
        onOpenEnd: function () {
            $('#intro_modal div.active').removeClass('active');
            $('.info_panels_modal .info,.initial_modal').hide();
            $('.initial_modal').show()

            map.resize();
        },
        onCloseEnd: function () { }
    });


    map.on("viewreset",
        function () {
            console.log('view reseting')
            if (!this_app.d3_stuff.geojson_mask) {
                svg.classed("hidden", true);
                this_app.d3_stuff.update();
            }
            this_app.update_popup_circle()
        });
    map.on("movestart", function () {
        svg.classed("hidden", true);
    });
    map.on("rotate", function () {
        svg.classed("hidden", true);
    });
    map.on('flystart', function () {
        flying = true;
    });

    map.on('flyend', function () {

        if (map.getLayer('hospitals')) {
            map.setLayoutProperty('hospitals', 'visibility', 'visible');
        }
        flying = false;
    });

    map.on("moveend", function () {

        console.log('moveend line 10272')
        if (flying) {
            map.fire(flyend);
        }

        this_app.update_popup_circle();

        svg.classed("hidden", false);
    })


    $('#intro_modal .info_panels_button >div').click(function (e) {
        $('#intro_modal div.active').removeClass('active');
        console.log($(e.target))
        var _class = $(this).attr('class').split(' ')[2];
        if (_class == 'explore') {
            introModal.hide();
            $('.btn.intro_modal_btn').removeClass('selected')
            return false;
        }
        if ($(this).hasClass('active') == false) {
            $('.info_panels_modal .info,.initial_modal').hide();
            $('.info.' + _class).show();

            $(this).addClass('active');
        } else {

        }
    });

    function throttle(func, timeout) {
        console.log('throttling')
        let wait = false;

        return function (...args) {
            if (!wait) {
                func(...args)
                wait = true;

                setTimeout(function () {
                    wait = false;
                }, timeout);
            }
        }
    }

    $('#slide-out').sidenav({

        edge: 'left', // Choose the horizontal origin
        draggable: true, // Choose whether you can drag to open on touch screens,
        onOpenEnd: function (el) {


            $('.sidenav-trigger').addClass('on');

        },

        onCloseEnd: function (el) {
            $('.sidenav-trigger').removeClass('on');
        }, // A function to be called when sideNav is closed
    });

    $('.ceat_navbar').on('click', function (e) {

        var target = $(e.target)
        if ($(e.target).hasClass('sidenav-actions') || $(e.target).parent().hasClass('sidenav-actions')) {
            if ($('.sidenav').is(':visible')) {
                $('.sidenav').hide();
            } else {
                $('.sidenav').show();
            }
        }

        if ($(e.target).hasClass('ceat_home') || $(e.target).parent().hasClass('ceat_home')) {
            introModal.show();
            $('.info_panels_modal .info').hide();
            $('.initial_modal').show();
        }

        if ($(e.target).hasClass('africa_platform_trigger') || $(e.target).parent().hasClass('africa_platform_trigger')) {

            if ($('.sidenav').is(':visible') == false)
                $('.sidenav').show();

            if ($('.indicators_li .collapsible-body').is(':visible')) {
                $('.indicators_li .collapsible-header').trigger('click');
                $('.indicators_li').hide();
                if ($('.queryable_li .collapsible-body').is(':visible') == false) {
                    $('.queryable_li .collapsible-header').trigger('click');
                }
            } else {

                if ($('.queryable_li .collapsible-body').is(':visible')) {
                    $('.queryable_li .collapsible-header').trigger('click');
                }
                $('.indicators_li').show();
                console.warn('slide down africa platform stuff!')
                $('.indicators_li .collapsible-header').trigger('click');
            }
        }
    })

    $('.collapsible').collapsible();

    $('.nav-link i').tooltip();

    $('.sidenav .collapsible-body').on('scroll', function () {
        var c = $('.init_instructions');
    })
    $('.hospitals').find('.highlight').click()
    $('#adm_unit_polygon').show()



})