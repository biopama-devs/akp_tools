var map;

function matrix_ticks(x_ticks, code) {
  x_ticks.style("opacity", 0.2);
  x_ticks.filter(function (d2) {
    return d2 == code;
  }).style("opacity", 1)
}

app.throttle = function (fn, threshhold, scope) {
  threshhold || (threshhold = 250);
  var last,
    deferTimer;
  return function () {
    var context = scope || this;

    var now = +new Date,
      args = arguments;
    if (last && now < last + threshhold) {
      // hold on to it
      clearTimeout(deferTimer);
      deferTimer = setTimeout(function () {
        last = now;
        fn.apply(context, args);
      }, threshhold);
    } else {
      last = now;
      fn.apply(context, args);
    }
  };
}

function get_scale_data(value) {
  var x = app.scale_values.filter(function(d) {
    return d.value == value;
  })[0];
  return x;
}

all_country_data.features.map(function (data) {
  for (var p in data.data_ranks) {
    data.data_ranks[p + '_chart'] = 39 - data.data_ranks[p];
  }
  return data;
})[0]

app_data.all_data = all_country_data.features;;

d3.json("/modules/custom/akp_tools/energy_json/indicators/countries.json").then(mapDraw);

function mapDraw(geojson) {
  function get_index_data(name) {
    return app.indexes_info.filter(function (d) {
      return d.name == name;
    })[0]
  }

  function sort_by_index(param) {
    return function (a, b) {
      //  console.info(a.data[param])  //2,3,1,1...
      return a.data_class[param] < b.data_class[param] ? 1 : -1;
    }
  }

  app.generate_data = function (param) {
    console.warn(param)

    indexes_arrays.env_index_class_arr = [];
    indexes_arrays.main_index_class_arr = [];
    indexes_arrays.social_index_class_arr = [];
    indexes_arrays.political_index_class_arr = [];
    indexes_arrays.financial_index_class_arr = [];
    //   console.groupCollapsed('inside_generate_data')
    var sorted = app_data.all_data.sort(sort_by_index(param));
    console.log(sorted)
    sorted.forEach(function (d, i) {
      //  if (i > 5) return false
      /*
          "data":{
          env_index_class: 1
          â€‹
          financial_index_class: 5
          â€‹
          main_index_class: 5
          â€‹
          main_index_val: 0.389
          â€‹
          political_index_class: 5
          â€‹
          social_index_class: 3
          }
          */

      var popup = '<div class="popup_country_title">' + d.country + '</div>';

      var data_val_arr = d.data_class;
      for (var index in d.data_class) {

        //main_index_val is on d.data_val
        //main_index_class is on d.data_class

        // if (index !== 'main_index_val') {
        var index_info = get_index_data(index);
        var scale_info = get_scale_data(d.data_class[index]);

        if (typeof scale_info == 'undefined') {
          var scale_info = {
            color: '#2f2e2f'
          }
        }
        var _class = index.split('_class')[0] + '_val';
        popup += '<div class="index_country_info ' + _class + '"><i style="background:' + scale_info.color + '"></i>' + index_info.title + '</div>';

        //var y_elements = ["env_index_class", "financial_index_class", "political_index_class", 'social_index_class', 'main_index_class'];
        var arr = indexes_arrays[index + '_arr'];

        arr.push({
          country: d.country,
          _code: d.code,
          index_type: index,
          index_value: d.data_class[index],
          color: scale_info.color
        })
        //  }
        d.popup = popup;

      }

      // $('#countries_popup').append(popup)
    });

    var matrix_params = {};
    var countries_arr =
      sorted.map(function (d) {
        return d.code;
      });
    matrix_params.countries_arr = countries_arr;

    var all_data_plot = indexes_arrays.env_index_class_arr.concat(indexes_arrays.financial_index_class_arr).concat(indexes_arrays.political_index_class_arr)
      .concat(indexes_arrays.social_index_class_arr).concat(indexes_arrays.main_index_class_arr);
    console.log('all_data_plot', all_data_plot)
    //  console.groupEnd();
    app_data.sorted_country_data = sorted;
    app_data.all_data_plot = all_data_plot;

    // matrix_params.all_data_plot = all_data_plot;
    matrix_params.index_to_plot = param;

    $("#legend_dropdown").show();

    $(".icon_info").click(function() {
      $(this).addClass("on");
    });

    $("#caution_msg").css("left", ($(".indicators_li").width() + 20) + "px");
    $("#caution_msg").show();
    $("#caution_msg .close").on("click", function() {
      $("#caution_msg").hide();
    });

    generate_matrix(matrix_params);

    console.log('setting app_state.matrix_indicators = true');
  }

  var rects;

  function generate_matrix(matrix_params) {
    app_state.initial_state = false;
    $('#matrix_wrapper').show();
    app_state.matrix_indicators = true;
    app_state.lollipop_indicators = false;
    $('#lollipop_graph svg').empty();

    app_state.country_facts = false;

    if ($('.profile_checkbox input').is(':checked')) {
      $('.profile_checkbox input').trigger('click');
    }

    app.matrix_map_popup.remove();

    $("#lollipop_wrapper").hide();
    console.group('matrix_info')
    $(".matrix_tooltip").remove();

    console.log(matrix_params);
    $(".lollipop_tooltip").remove();

    var countries_arr = matrix_params.countries_arr;
    var all_data_plot = app_data.all_data_plot;
    console.log(all_data_plot)

    //margins from graph inside wrapper!
    var margin = {
      top: 0,
      right: 20,
      bottom: 0,
      left: 120
    };

    //added extra 20px for the link to table!
    var mx_w_h = ($('#map').height() / 5) + 15 + 20;

    var svg_h = $('.heatmap').height();

    var map_w = $('#map').width();
    var diff_w = map_w - (map_w / 4);

    var x_elements = countries_arr;
    var buckets = x_elements.length + 1;

    if ($('.heatmap svg g').length == 0) {
      //-30 is some extra space
      var diff_h = $('#map').height() - mx_w_h - 60; // + 50; //- $('.nav-wrapper').height();
      var width = diff_w - margin.left - margin.right;
      $('#matrix_wrapper').css('margin-left', (map_w / 8) + 'px');

      var svg_w = width + margin.left + margin.right;
      var buck_h = (svg_w - 120 - 20 - (0.2 * buckets)) / buckets;
      var bck_h = (buck_h * 5) + 106.703;
      $('.heatmap,#matrix_wrapper').height(bck_h).show();
    } else {
      var diff_h = $('#matrix_wrapper').position().top;
      var svg_w = $('.heatmap svg').width();
      var buck_h = (svg_w - 120 - 20 - (0.2 * buckets)) / buckets;
      var bck_h = (buck_h * 5) + 106.703;
      $('.heatmap,#matrix_wrapper').height(bck_h).show();
      // var diff_h = $('#map').height() - svg_h + $('.nav-wrapper').height();
      //$('.heatmap,#matrix_wrapper').height(mx_w_h).show();
    }

    if ($('.heatmap svg g').length == 0)
      $('#matrix_wrapper').css('top', diff_h).css('width', diff_w + 'px')
    else
      $('#matrix_wrapper').css('top', diff_h).css('width', diff_w + 'px')
      //half of half map width!!
      .css('margin-left', (map_w / 8))
    // .height(150)

    $("#heatmap_tooltip").css("left", (($("#map").width() / 2) - ($("#heatmap_tooltip").width() / 2)) + "px");
    $("#heatmap_tooltip").css("top", (diff_h - 70) + "px");
    $("#heatmap_tooltip").show();
    $("#heatmap_tooltip .close").on("click", function() {
      $("#heatmap_tooltip").hide();
    });

    $('.heatmap svg').empty();

    //   console.groupCollapsed()
    var svg = d3.select(".heatmap svg")
      //.append("svg")
      .attr("width", svg_w)
      .attr("height", bck_h)
      .attr("transform", "translate(0,6)")
      .append("g")
      .attr("class", "rects_container_container")
      .attr("transform",
        //  "translate(" + margin.left + "," + margin.top + ")")
        "translate(" + margin.left + ",80)")

    var xScale = d3.scaleBand()
      .domain(x_elements)
      .range([0, svg_w - margin.left - 10])
      .padding(0.2)

    var xAxis = d3.axisBottom()
      .scale(xScale)
      .tickFormat(function (d) {
        return d;
      })

    var yScale = d3.scaleBand()
      //d3.scaleBand()
      .domain(y_elements)
      .range([0, y_elements.length * xScale.bandwidth() + 5])
      //.range([0, h])
      .padding(0.02)

    var yAxis = d3.axisLeft()
      .scale(yScale)
      .tickFormat(function (d) {
        return d;
      })

    // .orient("left");
    setTimeout(function () {
      //    $('.y.axis').attr('transform', 'translate(0,5)')
    }, 800)

    cellSize = xScale.bandwidth();
    //     value: 1,
    //     color: '#e73232',
    //     title: 'Lower performance'
    // }, {
    //     value: 2,
    //     color: '#f5621c',
    //     title: 'Lower-middle'
    // }, {
    //     value: 3,
    //     color: '#f7c717',
    //     title: 'Middle'
    // }, {
    //     value: 4,
    //     color: '#6ee282',
    //     title: 'Upper middle'
    // }, {
    //     value: 5,
    //     color: '#2196f3',
    //     title: 'Higher performance'
    // }
    var colorScale = d3.scaleThreshold()
      .domain([0, 1, 2, 3, 4])
      .range(["#e73232", "#f5621c", "#f7c717", "#6ee282", "#2196f3"]);

    var mouseover_cell = function(d) {
      console.log(d);
      console.info('mousemoving cell');

      matrix_ticks(x_ticks, d._code);

      rects.style('opacity', 1);
      /*
              _code: "MRT"
      â€‹
      color: "#f5621c"
      â€‹
      country: "Mauritania"
      â€‹
      index_type: "financial_index_class"
      â€‹
      index_value: 2
      */
      var out = rects.filter(function (_d) {
        return _d._code !== d._code
      });
      console.log(out)
      out.style('opacity', 0.3);
      if ($(".country_popup.rect_popup[popup_code=" + d._code + "]").length == 0) {
        app.matrix_map_popup.remove();
      }

      var pos = $(this).position();

      //Safari!
      var pos = this.getBoundingClientRect();

      var this_info = app_data.sorted_country_data.filter(function (info) {
        return info.code == d._code;
      })[0];

      var $html = $.parseHTML(this_info.popup);
      console.info('mousemooove cell with no popup ' + d.index_type);

      tooltip.html('<div popup_code="' + d._code + '" class="rect_popup"></div>')
        .style("left", (pos.x + 30) + "px")
        .style("top", (pos.y - 180) + "px");
      $(".matrix_tooltip .rect_popup").empty().append($html).show();

      //yellow hovering index

      //main_index_class
      var _class = d.index_type.split('_class')[0] + '_val';
      console.info("we only change the matrix popup value on mouseovering cell")
      $('.popup_cell_selected').removeClass('popup_cell_selected')
      $('.matrix_tooltip .index_country_info.' + _class).addClass('popup_cell_selected');
    }

    var mouseleave_cell = function(d) {
      x_ticks.style("opacity, 1");
    }

    $('.heatmap').on('mouseenter', function() {
      console.log('mousenter heatmap');
    })
    // var mouseleave_svg = function(d) {
    // d3.select("#map path").attrs(app.sel_country_style)
    // console.log('leave')
    // rects.style("opacity", 1);
    // tooltip.style("opacity", 0)
    // }
    var cells = svg.selectAll('.heatmap rect');
    cells.data(all_data_plot)
      .enter().append('g')
      .attr('transform', "translate(0,5)")
      //.classed('g_classed', true)
      .attr('class', 'rects_container')
      .append('rect')
      .attr('class', 'cell')
      .attr('width', cellSize)
      .attr('height', cellSize)
      .attr('y', function (d) {
        //  return yScale(d.data.political);
        return yScale(d.index_type);
      })
      .attr('x', function (d) {
        return xScale(d._code);
      })
      .attr('opacity', 1)
      .style('fill', function (d) {
        return d.color;
      })

    rects = d3.selectAll('.heatmap rect.cell')
      //excluding now border_cell
      //.on('mouseover', mouseover)
      .on('mouseover', mouseover_cell)
      .on("mouseleave", mouseleave_cell);

    var click_label = function(d) {
      if ($(d3.event.target).hasClass('y_tick_text')) {
        //political_index_class
        app_data.matrix_current_param = d;
        app.update_by_index(d);
      }
    }

    var mouseover_label = function(d) {
      $(".matrix_tooltip .rect_popup").hide();
      console.info($(d3.event.target))
      if ($(d3.event.target).parent().hasClass('x_tick')) {
        $('.mapboxgl-popup').show();

        var target = $(d3.event.target).parent();
        console.log(d);
        console.info(d3.select(this))

        rects.style("opacity", 0.2);
        rects.filter(function (data) {
          return data.code == d;
        }).style("opacity", 1);

        matrix_ticks(x_ticks, d)
      } else {
        //$(d3.event.target).hasClass('y_tick_text') ||
        if ($(d3.event.target).hasClass('foreign_new') || $(d3.event.target).parent().hasClass('foreign_new')) {
          //skyblue
          //$(d3.event.target).find('text').style('fill', '#87ceeb');
          rects.style("opacity", 0.2);
          rects.filter(function (data) {
            return data.index_type == d;
          }).style("opacity", 1);
          return false;
        } else
          var target = $(d3.event.target);
      }
      if (target) {
        var this_info = app_data.sorted_country_data.filter(function(info) {
          return info.code == d;
        })[0];

        $('.x_mouseovered', '.heatmap').removeClass('x_mouseovered');

        target.addClass('x_mouseovered');

        rects.style("opacity", 0.2);
        var these_rects = rects.filter(function(data) {
          return data._code == d;
        });

        d3.selectAll('.border_cell').style('opacity', 0);

        these_rects.style("opacity", 1);

        var t = '#map path[code=' + d + ']';

        var popup_html = '<div popup_code=' + this_info.code + ' class="country_popup rect_popup">' + this_info.popup + '</div>';

        for (var p in markers_obj_arr) {
          if (markers_obj_arr[p].code == d) {
            app.matrix_map_popup.setLngLat(markers_obj_arr[p].centroid);
            app.matrix_map_popup.setHTML(popup_html).addTo(map);
          }
        }

        setTimeout(function() {
          //social_index_val in html
          $('.popup_selected').removeClass('popup_selected');

          var _class = app_data.matrix_current_param.split('_class')[0] + '_val';

          $('.country_popup .index_country_info.' + _class).addClass('popup_cell_selected');

          // $('.index_country_info.' + app_data.matrix_current_param).addClass('popup_selected');
        }, 500)

        d3.selectAll("#map path").attrs(app.unsel_country_style);
        var sel_path = d3.selectAll("#map path[code=" + d + "]");
        mouseovered_path.ori_color = sel_path.attr('fill');
        mouseovered_path.sel_path = sel_path;

        sel_path
          //.transition()
          //.duration(100)
          .attrs(app.sel_country_style)
        //.attr('fill', '#ea24d2');
        // .attr("stroke", "#ea24d2")
        // .attr('stroke-width', 1)
        // .attr('opacity', 1)
        //.attr
        // debugger
      } else {
        rects.style("opacity", 0.3);
        rects.filter(function (data) {
          return data.index_type == d;
        }).style("opacity", 1);
      }
    }

    var mouseout_label = function(d) {
      x_ticks.style("opacity", 1);

      //triggered also when moving on the map...
      //  app.matrix_map_popup.remove();
      $('.mapboxgl-popup').hide();
      console.log('mouseout_label')
      var cs = d3.selectAll("#map path");
      console.log(cs)
      //cs.attrs(app.sel_country_style)
      cs.attrs(app.initial_no_fill_country_style)
      console.warn($(d3.event.target))
      if ($(d3.event.target).parent().hasClass('x_tick')) {
        //country names
        var target = $(d3.event.target).parent();
        // mouseovered_path.sel_path.transition()
        //     .duration(100)
        //     //.attr('fill', mouseovered_path.ori_color);
        //     .attrs(app.unsel_country_style)

        $('.x_mouseovered', '.heatmap').removeClass('x_mouseovered');
      }

      rects.style("opacity", 1);
    }

    var mouseleave_svg = function(d) {
      //d3.select("#map path").attrs(app.sel_country_style);
      cs.attrs(app.initial_no_fill_country_style);
      console.log('leave');
      rects.style("opacity", 1);
      tooltip.style("opacity", 0);
    }
    // svg.selectAll('g.g_classed').on('mouseover', mouseover)
    //     .on('mousemove', mousemove)
    //     .on("mouseleave", mouseleave)

    //.on('mousemove', mousemove_label)
    //.on("mouseleave", mouseleave_label)

    var tooltip = d3.select("#map")
      .append("div")
      .attr("class", "matrix_tooltip");

    // cells.on('mouseover', mouseover)
    //     .on('mousemove', mousemove)
    //     .on("mouseleave", mouseleave)
    var svg = d3.select('.heatmap svg');
    svg.append("g")
      .attr("class", "y axis")
      // .attr("transform", "translate(120,85)")
      .attr("transform", "translate(0,75)")
      .call(yAxis)
      .selectAll('.tick')
      .classed('y_tick', true)
      .html(function(d) {
        if (d !== 'main_index_class')
          var h = '<foreignObject><div class="foreign_new ' + d + '"><i class="material-icons">info_outline</i>'
        else
          var h = '<foreignObject><div class="foreign_new ' + d + '">';

        for (var p in app.indexes_info) {
          if (app.indexes_info[p].name == d)
            h += '<span class="y_tick_text">' + app.indexes_info[p].title + '</span></div></foreignObject>'
        }
        return h;
      });

    $('.foreign_new').on('click', function(e) {
      if ($(e.target).is('i')) {
        app.modal_x_axis_param = $(this).attr('class').split(' ')[1];
        $("#x_axis_modal").modal("show");
        $("#x_axis_modal .x_axis_info").hide();
        $("#x_axis_modal .x_axis_info." + app.modal_x_axis_param).show();
      }
    })

    /*
        .selectAll('text')
        //.text('test')
        .text(function(d) {
            for (var p in app.indexes_info) {
                if (app.indexes_info[p].name == d)
                    return app.indexes_info[p].title
            }
        })
        .attr('font-weight', 'bolder')
        .attr('class', 'y_tick_text')
        .style('fill', function(d) {
            console.info(d)
            if (d == matrix_params.index_to_plot) {
                //blueish
                return '#f7aa17'
            } else {
                //blackish
                return '#ffffff'
            }
        })
        */

    // d3.select('.heatmap svg').

    svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(120,80)")
      .call(xAxis)
      .selectAll('.tick')
      .classed('x_tick', true)
      .selectAll('text')
      .attr('font-weight', 'normal')
      .style('fill', '#ffffff')
      .style("text-anchor", "start")
      // .attr("dx", ".3em")
      .attr("dy", ".5em")
      .attr("y", -5)

      // .attr('code', function(d) {
      //     console.warn(d.code)
      //     return d.code
      // })
      .attr("transform", function (d) {
        return "rotate(-65)";
      })
      .text(function (d) {
        var name = app_data.all_data.filter(function (data) {
          return data.code === d;
        })[0].country
        //console.log(name)
        return name
      })
    var x_ticks = d3.selectAll('.x_tick');

    console.warn(d3.selectAll('.tick'));

    d3.selectAll('.tick')
      .on('mouseover', mouseover_label)
      .on('mouseout', mouseout_label)
      .on('click', click_label);
    // d3.selectAll('.heatmap').on('mouseout', mouseleave_svg);
  }
  //  map.on('load', function() {

  //$('.mapboxgl-map').css("background-color", "rgb(161, 180, 193)");

  $(document).ready(function() {
    $('#subfooter,#main-navigation-inside,#header,.subheader2').hide()
    //Setup mapbox-gl map
    map = new mapboxgl.Map({
      container: 'map', // container id
      //style: 'mapbox://styles/mapbox/streets-v8',
      style: {
        "version": 8,
        "sources": {
          "geoserver": {
            "type": "vector",
            "tiles": [
              "https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/wmts?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&LAYER=hotspots:gaul_0_simplified&STYLE=&TILEMATRIX=EPSG:900913:{z}&TILEMATRIXSET=EPSG:900913&FORMAT=application/vnd.mapbox-vector-tile&TILECOL={x}&TILEROW={y}"
            ],
          }
        },
        "layers": [{
          "id": "gaul_0_simple",
          "source": "geoserver",
          "source-layer": "gaul_0_simplified",
          "type": "fill",
          // "minzoom": 4,
          // "maxzoom": 6,
          'paint': {
            "fill-color": "#a2a7ab",
            "fill-outline-color": "#87989c",
            "fill-opacity": 0.9

          }
        }]
      },
      center: [2, -12],
      maxZoom: 4,

      //center: [141.15448379999998, 39.702053ã€€],
      zoom: 2,

      attributionControl: false
    });
    $(".mapboxgl-map").css('background-color', "#a1b4c1")

    app.just_name_country_popup = new mapboxgl.Popup({
      closeButton: false,
      closeOnClick: true,
      className: 'initial_map_popup',
      //offset: [20, -20]
      offset: {
        'bottom': [0, -20],
        'top': [0, 15],
        'top-left': [0, 0], //[linearOffset, (markerHeight - markerRadius - linearOffset) * -1],
        'top-right': [0, 0], //[-linearOffset, (markerHeight - markerRadius - linearOffset) * -1],

      }
    });
    app.just_name_country_popup.addTo(map);

    app.matrix_map_popup = new mapboxgl.Popup({
      closeButton: true,
      closeOnClick: true,
      className: 'matrix_map_popup',
      //offset: [20, -20]
      offset: {
        'bottom': [0, -20],
        'top': [0, 15],
        'top-left': [0, 0], //[linearOffset, (markerHeight - markerRadius - linearOffset) * -1],
        'top-right': [0, 0], //[-linearOffset, (markerHeight - markerRadius - linearOffset) * -1],

      }
    });
    map.addControl(new mapboxgl.NavigationControl());

    // disable map rotation using right click + drag
    map.dragRotate.disable();

    // disable map rotation using touch rotation gesture
    map.touchZoomRotate.disableRotation();

    //   Add zoom and rotation controls to the map.
    //  map.addControl(new mapboxgl.NavigationControl());

    class map_legend_control {
      onAdd(map) {
        this.map = map;
        this.container = document.createElement('div');
        //this.container.className = 'click_country_control_container';
        this.container.className = 'charts_control_container';
        //this.container.textContent = '<h5>Just a test</h5>';
        return this.container;
      }
      onRemove() {
        this.container.parentNode.removeChild(this.container);
        this.map = undefined;
      }
    }
    var legend_control = new map_legend_control();
    map.addControl(legend_control, 'bottom-right');

    //$('.click_country_control_container').html('<div class="mapboxgl-ctrl mapboxgl-ctrl-group click_country_control"> <div class="center title">Click on a country to get its profile</div></div>');
    $('.charts_control_container').html('<div class="mapboxgl-ctrl mapboxgl-ctrl-group charts_control_container_ctrl"><div class="i_lollipop"><a class="btn-floating waves-effect waves-light"><i class="material-icons">equalizer</i></a></div><div class="i_matrix"><a class="btn-floating btn-small waves-effect waves-light"><i class="material-icons">view_comfy</i></a></div></div>')

    $('.i_matrix').on('click', function() {
      $('.mapboxgl-ctrl-bottom-right').hide();
      $('#matrix_wrapper').fadeIn();
      app_state.matrix_indicators = true;
    })
    $('.i_lollipop').on('click', function() {
      $('.mapboxgl-ctrl-bottom-right').hide();
      $('#lollipop_wrapper').show();
      app_state.lollipop_indicators = true;
    })
    var legendElementWidth = 20;
    var legend_svg = d3.select('.legend_control svg')

    console.groupCollapsed('my Legend description Group');

    //.attr("width", legendElementWidth+170)
    legend_svg.attr("width", 150)
      //.attr("height",legendElementWidth*8)
      .attr("height", 120);

    var legend = legend_svg.selectAll(".legend")
      .data(app.scale_values, function (d) {
        console.log(d)
        return d.value;
      })
      .enter()
      .append("g")
      .attr("class", "legend")
    //.attr("transform", "translate(0,10)");

    console.groupEnd();
    legend.append("rect")
      .attr("x", function (d, i) {
        return 0 * i;
      })
      //.attr("x",0)
      .attr("y", function (d, i) {
        // if (i > 0) {
        //     return (legendElementWidth * i);
        // } else {
        //     return legendElementWidth * (i + 0.5);
        // }

        return (legendElementWidth + 5) * i;
      })
      .attr("width", legendElementWidth)
      .attr("height", legendElementWidth)
      .style("fill", function (d, i) {
        return d.color;
      })
      .attr("stroke", "white")
      .attr("value", function (d) {
        return d.value;
      });

    legend.append("text")
      .html(function (d) {
        // console.warn(d.legend_text)
        return d.title; //+'<b>test</b>';
      })
      .attr("x", function (d, i) {
        return legendElementWidth + 10;
      })
      //.attr("x",0)
      .attr("y", function (d, i) {
        // 7 is half of 13px!!!
        if (i > 0) {
          return ((legendElementWidth + 5) * (i + 1)) - 7;
          //return ((legendElementWidth / 2) + 7) * i;
        } else {
          console.warn(((legendElementWidth) + 7));
          return (legendElementWidth + 5) - 7;
        }
      })
      .attr("font-size", "13px")
      .attr("fill", "white");
    // $('.legend_dropdown_content').height(200);
    var svg = d3.select('.legend_control svg');
    // $('ul#legend_dropdown').css('min-height',200);

    setTimeout(function () {
      $('.legend_control').show();

      $('ul#legend_dropdown').css('min-height', '200px');
      //$('ul#legend_dropdown').css('min-height', $('.legend_control').height() + 'px');
    }, 500);
    // $('ul#legend_dropdown').css('min-height', svg.style("height"));

    $(".show_hide_circles").click(function() {
      if ($(this).hasClass("on")) {
        $(this).text("Show ranking number").removeClass("on");
        $(".my-icon").hide();
      } else {
        $(this).text("Hide ranking number").addClass("on");
        $(".my-icon").show();
      }
    })

    app.update_country_profile = function(path_code) {
      $("#country_modal").modal("show");
      $("#country_modal").on("shown.bs.modal", function() {
        if ($("#bubble_chart svg").length > 0) {
          $("#bubble_chart svg").remove();
          on_bubble_btn();
        }
      })
      $("#country_modal .btn-close").click(function() {
        if (app_state.matrix_indicators == true) {
          if ($(".i_matrix").is(":visible")) {
            $("#matrix_wrapper,.matrix_tooltip").show();
            $(".i_matrix").hide();
          }
        }
        if (app_state.lollipop_indicators == true) {
          if ($(".i_lollipop").is(":visible")) {
            $("#lollipop_wrapper,.lollipop_tooltip").show();
            $(".i_lollipop").hide();
          }
        }
      })
      $('#country_modal .country_profile_container').show();

      $('.ranks_chart svg').remove();
      app_state.country_facts = true;

      // app_state.lollipop_indicators = false;
      // app_state.matrix_indicators = false;
      $(".table_container").hide();

      //  svg.selectAll("path").attrs(app.initial_country_style);

      var _t = app_data.all_data.filter(function(d) {
        return d.code == path_code;
      })[0];
      app_state.country_facts_sel = _t;

      // if ($('.country_li').hasClass('active') == false) {
      //     $('.country_li .collapsible-header').trigger('click');
      // }
      console.warn(_t.data_val)
      var tds = $('#country_modal table:first tbody tr td');
      var i = 0;
      var ranks_text_arr = [];
      for (var p in _t.data_ranks) {
        tds.eq(i).find('div span:first').html(_t.data_ranks[p] + '<small> /38<i class="rank_chart tiny material-icons">insert_chart</i></small>');
        i++;
      }
      var i = 0;
      for (var p in _t.data_val) {
        tds.eq(i).find('div span:last').css('font-size', '0.8em!important');
        tds.eq(i).find('div span:last').text(_t.data_val[p])
        i++;
      }

      //  $('.country_info').show();
      for (var p in _t.country_facts[0]) {
        if (p !== 'country') {
          if (typeof _t.country_facts[0][p] == 'number') {
            if (p.indexOf('pop_') > -1)
            //&& p !== 'pop_ghsl') 
            {
              //????/
              if (_t.country_facts[0][p] > 100000)
                var val = parseInt(_t.country_facts[0][p]);
              // / 1000);
              else
                var val = parseInt(_t.country_facts[0][p]);
              //var val = _t.country_facts[0][p];

            } else {
              var val = _t.country_facts[0][p];
            }
            // $(this).find('.col').eq(i).text(numberWithCommas(format_num(r.x_val)));
            $('.' + p, '#country_modal').next().text(numberWithCommas(format_num(val)));
          } else {
            //$('.' + p, '#country_modal').next().text(numberWithCommas(format_num(_t.country_facts[0][p])))
            $('.' + p, '#country_modal').next().text(_t.country_facts[0][p]);
          }
        } else {
          $('.country_name', '#country_modal').text(_t.country_facts[0][p])
        }
      }

      $('#country_modal .rank_chart').on('click', function(e) {
        var param = $(this).parents().closest('.medium').attr('class').split(' ')[1];
        if ($(this).hasClass('active_chart')) {
          $('svg.' + param).remove();
          $(this).removeClass('active_chart');
        } else {
          update_profile_rank_chart(param, path_code);
          $(this).addClass('active_chart');
        }
      })

      // var sel_path = d3.selectAll("#map path[code=" + path_code + "]");

      // d3.selectAll("#map path.active").transition()
      //   .duration(300)
      //   .attr('fill', function(d) {
      //     return d3.select(this).attr('ori_color');
      //     //sel_path.attr('ori_color'));
      //   })

      // // mouseovered_path.sel_path = sel_path;

      // if (sel_path.classed('active') == false) {
      //   sel_path.attr('ori_color', sel_path.attr('fill'))
      //   sel_path.transition()
      //     .duration(300)
      //     //pinky
      //     .attr('fill', '#ea24d2');
      //   sel_path.classed('active', true);
      // } else {
      //   sel_path.classed('false', true);
      //   sel_path.transition()
      //     .duration(600)
      //     .attr('fill', sel_path.attr('ori_color'));
      // }

      // setTimeout(function() {
      //   $('.btn_bubble').click();
      // }, 800)
      $(".modal-trigger.country_modal_trigger i").click(function(e) {
        e.preventDefault();
      });
      //end update_country_profile
      console.groupEnd();
    }

    function update_profile_rank_chart(param, code) {
      switch (param) {
        case 'rank_env':
          var txt = 'Environmental';
          break;
        case 'rank_political':
          var txt = 'Political';
          break;
        case 'rank_financial':
          var txt = 'Financial';
          break;
        case 'rank_social':
          var txt = 'Social';
          break;
        case 'general_rank':
          var txt = 'General Index';
          break;
        default:
          break;
      }

      var this_rank_arr = param + '_arr';
      $('table .medium').map(function () {
        return $(this).attr('class').split(' ')[1]
      })
      var memo = {
        general_rank_arr: [],
        rank_env_arr: [],
        rank_social_arr: [],
        rank_political_arr: [],
        rank_financial_arr: [],
      };
      console.log(app_data.all_data)
      var reduced = app_data.all_data.reduce(function (memo, d, i) {
        // console.info(i)
        //memo.countries.push(d.country)
        memo.general_rank_arr.push({
          code: d.code,
          rank: d.data_ranks.general_rank

        });
        console.warn(memo)
        console.warn(memo[this_rank_arr])
        console.log(d.data_ranks)
        console.log(this_rank_arr)
        memo[this_rank_arr].push({
          code: d.code,
          rank: d.data_ranks[param]
        });
        if (i == app_data.all_data.length - 1) {
          console.log('last')
          console.info(memo[this_rank_arr])
          memo[this_rank_arr].sort(function (a, b) {
            console.warn(a.code)
            if (a.code < b.code) //sort string ascending
              return -1;
            if (a.code > b.code)
              return 1;
            return 0; //default return value (no sorting)
            // return a.code < b.code
            //return a - b
          });

          // memo.rank_env_arr.sort(function(a, b) {
          //     return b.code - a.code
          //         //return a - b
          // });
        }

        return memo

      }, memo);
      console.log(JSON.stringify(reduced))

      var _d = reduced[this_rank_arr];
      console.log(_d)
      // set the dimensions and margins of the graph
      var margin = {
        top: 5,
        right: 30,
        bottom: 30,
        left: 40
      };
      var c_width = $('.profile-overview').width();
      $('.ranks_chart,table .profile-overview.ranks').css('max-width', c_width + 'px!important')

      width = c_width - margin.left - margin.right,
        height = 100 - margin.top - margin.bottom;

      console.log(width)

      var t_class = 'svg_title ' + param;
      console.log(t_class)
      var title_svg = d3.select(".ranks_chart")
        .append('svg')
        .attr('class', t_class)
        .attr("height", 20)
        .attr("width", width + margin.left + margin.right)
        .append("g")
        .attr('class', 'g_title')
        .attr("transform",
          "translate(" + margin.left + "," + margin.top + ")");

      var t_class = '.svg_title.' + param;
      d3.selectAll(t_class).select('.g_title').append("text")
        .attr("x", (width / 2))
        .attr("y", 10)
        .attr("text-anchor", "middle")
        .style("font-size", "1em")
        .style('fill', '#ebc247')
        .text(txt + ' rank classification');

      // append the svg object to the body of the page
      var svg2 = d3.select(".ranks_chart")
        .append("svg")
        .attr('class', param)
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

      // Parse the Data
      //d3.csv("https://raw.githubusercontent.com/holtzy/data_to_viz/master/Example_dataset/7_OneCatOneNum_header.csv", function(data) {

      // X axis
      var x = d3.scaleBand()
        .range([0, width])
        .domain(_d.map(function (d) {
          return d.code;
        }))
        //_d.map(data.map(function(d) { return d.country; }))
        .padding(0.2);
      svg2.append("g")
        .attr("transform", "translate(0," + height + ")")
        .transition()
        .duration(1800)
        .call(d3.axisBottom(x))
        .selectAll("text")
        .attr("transform", "translate(-12,5)rotate(-90)")
        .style("text-anchor", "end");

      // Add Y axis
      var y = d3.scaleLinear()
        //.domain([0, 38])
        .domain([39, 1])
        .range([height, 0]);
      svg2.append("g")
      // .call(d3.axisLeft(y));

      // Bars
      svg2.selectAll("mybar")
        .data(_d)
        .enter()
        .append("rect")
        .attr("x", function (d) {
          return x(d.code);
        })
        .attr("width", x.bandwidth())
        .attr("fill", "#69b3a2")
        // no bar at the beginning thus:
        .attr("height", function (d) {
          return 0
        }) // always equal to 0
        .attr("y", function (d) {
          return y(39);
        })

      // Animation
      svg2.selectAll("rect")
        .transition()
        .duration(300)
        .attr("y", function (d) {
          return y(d.rank);
        })
        .attr("height", function (d) {
          console.log(d)
          return height - y(d.rank);
        })
        .delay(function (d, i) {
          console.log(i);
          return (i * 50)
        });
      svg2.selectAll("rect").filter(function (d) {
          console.info(d)
          return d.code == code
        })
        .attr("fill", "#ffc107")
    }

    var container = map.getCanvasContainer();
    var svg = d3.select(container).append("svg");
    var gaul_1;

    var transform = d3.geoTransform({
      point: projectPoint
    });
    var path = d3.geoPath().projection(transform);


    var featureElement = svg.selectAll("path")
      .data(geojson.features)
      .enter()
      .append("path")
      .classed('country', true)
      .attrs(app.initial_country_style)
      .attr("code", function (d) {
        return d.properties.code;
      })
      .on('click', function(d) {
        var path_code = d.properties.code;

        if (app_state.country_facts == true)
          app.update_country_profile(path_code);
      })
      // .on('mouseover', // app.throttle(
      //     function(d) {
      //         //}, 500)
      //     })
      //, 200))
      .on('mouseenter', function (d) {
        $('.mapboxgl-popup').show();
        var x_ticks = d3.selectAll('.x_tick');
        var path_code = d.properties.code;
        matrix_ticks(x_ticks, path_code)
        console.info('mouseenter geo path ' + mouseovered_path.code)

        if (app_state.matrix_indicators !== true && app_state.lollipop_indicators !== true) {
          mouseovered_path._data = d.properties;
          //   app.just_name_country_popup
        }

        console.warn('passed, ' + mouseovered_path.code + ' / ' + path_code)

        //var app_state = { indicators: false, country_facts: false };
        // if (app_state.matrix_indicators == true) {
        if ($('.heatmap svg').is(':visible') == true) {
          //https://github.com/d3/d3-selection-multi#d3-selection-multi
          var _sel = rects.filter(function (data) {
            //   console.log(data)
            return data._code == path_code;
          });

          d3.selectAll('.x_tick').filter(function (data) {
            if (data === path_code) {
              d3.select(this).dispatch('mouseover');
            }
          })

          d3.selectAll('.sel_cell_from_map')
            .classed('sel_cell_from_map', false);

          _sel
            .classed('sel_cell_from_map', true);
        }

        //if (app_state.lollipop_indicators == true) 
        if ($('#lollipop_graph svg').is(':visible') == true) {
          d3.selectAll('#lollipop_graph rect').filter(function (data) {
            if (data.code === path_code) {
              d3.select(this).dispatch('mouseover');
            }
          })
        }

        console.warn('mousenter geo path ' + d.properties.code)
        if (app_state.country_facts == true && app_state.initial_state == true) {
          //if ($('#lollipop_graph svg').length
          $('.mapboxgl-popup').show();
          var t = '#map path[code=' + d.properties.code + ']';
          app.mouseover_prev_color = d3.select(t).attr('fill');
          app.mouseover_prev_opacity = d3.select(t).attr('opacity');
          //d3.selectAll('#map path').style('opacity', .2);

          d3.select(t)
            .attr('fill', '#9acd32')
            .transition().delay(50)
            .attr('fill', '#ffeb3b')
            .attr('opacity', .8)
        }

        // setTimeout(function() {
        //     d3.select(t).transition().delay(500)
        //         .style('opacity', prev_opacity)
        //     d3.selectAll('#map path').style('opacity', .2);
        // }, 1000)


        //attrs(app.highlighted_country_style)
        //   var sel_path = d3.selectAll("#map path[code=" + d + "]");

        // var path_code = d.properties.code;
        // mouseovered_path.code = path_code;
        //   console.info(path_code)

      })
      .on('mouseout', function (d) {
        // app.throttle(
        console.warn('mouseout geo path ' + d.properties.code)
        if (app_state.country_facts == true && app_state.initial_state == true) {
          console.warn('country_facts TRUE mouseout geo path color ' + app.mouseover_prev_color)
          var t = '#map path[code=' + d.properties.code + ']';
          d3.select(t)
            .transition().delay(100)
            //.attr('fill', app.mouseover_prev_color)
            //from initial_no_fill_country_style
            .attr('fill', 'black')
            //.attr('opacity', app.mouseover_prev_opacity)
            .attr('opacity', 0.4)
        }
        //d3.selectAll(t).attrs(app.initial_country_style)
        if (app_state.matrix_indicators == false && app_state.lollipop_indicators == false) {
          console.log('out ')
          $('.mapboxgl-popup').hide();
        }
        $('.mapboxgl-popup').hide();
        //$('.click_country_control').hide();

        // mouseovered_path.code = null;
        //  $('.rect_popup[popup_code="' + d.properties.code + '"]').hide();


        if (app_state.matrix_indicators == true) {
          d3.selectAll('.sel_cell_from_map')
            // .attrs(
            //     function(d, i) {
            //         return {

            //             //"stroke": 'white',
            //             'stroke-width': 0
            //                 //  'stroke-opacity': 0.9

            //         }
            //     }
            // )
            .classed('sel_cell_from_map', false);

          d3.selectAll('.x_tick').filter(function (data) {
            if (data === d.properties.code) {
              // console.log(d3.select(this))
              // var parent = this.parentNode;
              // console.info(parent);

              console.info(this)
              d3.select(this).dispatch('mouseout');
            }
          })
        }

        if (app_state.lollipop_indicators == true) {
          // d3.selectAll('.myrect').transition()
          //     .duration(10)
          //     .style('opacity', 0)
        }

      })

    //app.update_country_profile('ETH');

    function update() {
      mouseovered_path.code = null;
      featureElement.attr("d", path);
    }

    //
    map.on("viewreset", update);
    map.on("movestart", function () {
      svg.classed("hidden", true);
    });
    map.on("rotate", function () {
      svg.classed("hidden", true);
    });
    map.on("moveend", function () {
      update();
      svg.classed("hidden", false);
    })

    update();

    /*
     var el = document.createElement('circle');
                el.setAttribute('r', 50);
                el.setAttribute('fill', '#0e44e7');
                */
    console.info(geojson.features);

    for (var p in geojson.features) {
      var d = geojson.features[p];

      var c = d.properties.centroid.split(' , ');
      var lng = round_this(c[0], 2);
      var lat = round_this(c[1], 2);
      markers_obj_arr.push({
        centroid: [lng, lat],
        code: d.properties.code,
        //  random_num: getRandomInRange(5, 70)
      });
    }

    markers_obj_arr.forEach(function (d) {
      // create a HTML element for each feature
      var el = document.createElement('div');
      el.className = 'animated-icon my-icon';
      el.id = 'centroid_' + d.code;
      var t_data = all_country_data.features.filter(function (data) {
        return data.code == d.code;
      })[0]

      if (!t_data) return false;
      new mapboxgl.Marker(el)
        .setLngLat(d.centroid)
        .addTo(map);

    })

    var map_paths = d3.selectAll("#map path.gaul_1");

    function get_color(d, param) {
      //     console.log(arguments)
      //d comes from the geojson
      //param: environmental, etc.
      //if (d.properties.code==)
      var color;
      //   console.log(indexes_arrays)
      indexes_arrays[param + '_arr'].forEach(function (data) {
        if (data._code == d.properties.code) {
          color = data.color;
        }
      })
      return color;
    }

    function update_circles_by_index(param) {
      //financial_index_class
      switch (param) {
        case 'main_index_class':
          var rank_code = 'general_rank';
          break;
        case 'env_index_class':
          var rank_code = 'rank_env';
          break;
        case 'social_index_class':
          var rank_code = 'rank_social';
          break;
        case 'political_index_class':
          var rank_code = 'rank_political';
          break;
        case 'financial_index_class':
          var rank_code = 'rank_financial';
          break;
        default:
          break;
      }

      var params_arr = indexes_arrays[param + '_arr'];

      var circs_values = params_arr.map(function(d) {
        return d.index_value;
      })

      var rscale = d3.scaleLinear()
        .domain([d3.min(circs_values), d3.max(circs_values)])
        .range([25, 45])
      // update_by_index('main_index_class');


      markers_obj_arr.forEach(function (d) {
        //console.log(d)
        var t_data = app_data.all_data.filter(function (data) {
          return data.code == d.code;
        })[0];

        /*
        data_ranks:
        general_rank: 12
        rank_env: 36
        rank_financial: 14
        rank_political: 6
        rank_social: 24
        */
        if (!t_data) {
          return false;
        }

        var sel_data = t_data.data_class;
        var sel_rank = t_data.data_ranks[rank_code];

        var sel_radius = rscale(sel_data[param]);
        var sel_scale_value = app.scale_values.filter(function (data) {
          return data.value == sel_data[param]
        })[0];
        //   console.log(sel_scale_value)
        //{value: 4, color: "#6ee282", title: "4- Good"}

        var sel_scale_title = app.indexes_info.filter(function (data) {
          return data.title == param
        })[0];
        // make a marker for each feature and add to the map

        var $centroid = $('#centroid_' + d.code);

        if ($centroid.find('.inner_circle').length == 0)
          $centroid.append('<span class="inner_circle">' + sel_rank + '</span>');
        else
          $centroid.find('.inner_circle').text(sel_rank)

        $centroid.data(t_data);

        $centroid.animate({
          //   width: '50px',
          width: sel_radius + 'px',
          height: sel_radius + 'px'
          //  height: '50px'
        }).css(
          'background-color',
          sel_scale_value.color
        )

        $centroid.find('.inner_circle').animate({
          'line-height': sel_radius + 'px',
          'font-size': sel_radius / 2.5
        })
      });

      $('.inner_circle').on('mouseenter', function () {
        var parent = $(this).parent();
        console.log(parent, parent.data())

        var code = parent.attr('id').split('centroid_')[1];

        d3.select('path[code=' + code + ']').dispatch('mouseenter');
      })
    }

    app.update_by_index = function (param) {
      map.flyTo({
        center: [5, -21],
        zoom: 2,
        speed: 0.4
      })
      console.warn(param);
      app.generate_data(param);

      for (var p in app.indexes_info) {
        if (app.indexes_info[p].name == param)
          $('.legend_control_container_title').text(app.indexes_info[p].title);
      }

      rects.style("opacity", 0.2);
      rects.filter(function (data) {
        return data.index_type == param;
      }).style("opacity", 1);
      var map_paths = d3.selectAll("#map path.country");

      map_paths
        .transition()
        .duration(1550)

        .attr("delay", function (d, i) {
          return 100 * i
        })
        .attrs(app.initial_no_fill_country_style)

        .attr('fill', function (d) {
          return get_color(d, param)
        })
        .attr('opacity', 1)
      // .on('click', function(d) {
      //     console.warn(d)
      // })
      setTimeout(function () {
        update_circles_by_index(param)

        if ($('.show_hide_circles').hasClass('on'))
          $('.animated-icon').show();
        else
          $('.animated-icon').hide();
      }, 800)

      $('.foreign_new.' + param).find('.y_tick_text').addClass('on');
    }

    function projectPoint(lon, lat) {
      var point = map.project(new mapboxgl.LngLat(lon, lat));
      this.stream.point(point.x, point.y);
    }
    //$('#map').on('mousemove', function(e) {
    map.on("mousemove", function (e) {
      var latlng = e.lngLat;
      var x = e.originalEvent.clientX;
      var y = e.originalEvent.clientY;
      if (!mouseovered_path._data)
        return false;

      if (app_state.country_facts == true && app_state.initial_state == true) {
        app.just_name_country_popup.setLngLat(latlng)
        app.just_name_country_popup.setHTML('<div class="country_popup small_popup">' + mouseovered_path._data.admin + '</div>')
      }

      //debugger
    })

    //app.update_country_profile('TZA');
  }); //end document.ready
}