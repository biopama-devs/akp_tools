<?php

header('Content-Type: application/json');

$link = pg_connect("host=yourhost dbname=db user=userorPwd password=userorPwd");
$query = 'select a.adm0_code,a.adm0_name,a.num_px,a.sum_pop,a.density,a.position,b.centroid_lat,b.centroid_lng,
b.bbox,b.area_sqkm from
energy.ghs_adm0_general_stats a
inner join gaul_0_info_all b
on a.adm0_code::text=b.adm0_code';

$result = pg_query($link, $query);
$json = array();
$i = 0;
while ($data = pg_fetch_array($result)) {
   $adm_0_code = (int)$data['adm0_code'];


   $json[] = array(
      "adm0_name" => $data['adm0_name'],
      "adm0_code" => $data['adm0_code'],
      "centroid_lat" => $data['centroid_lat'],
      "centroid_lat" => $data['centroid_lng'],

      "num_px" => $data['num_px'],
      "sum_pop" => $data['sum_pop'],
      "density" => $data['density'],
      "position" => $data['position']



   );
}

$json = json_encode($json, JSON_NUMERIC_CHECK);
if ($_GET['callback']) {
   echo $_GET['callback'] . "(" . $json . ")";
} else {
   echo 'callback(' . $json . ')';
};
