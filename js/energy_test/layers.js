var app_data = {
    layers_info: [],
    layers_labels_info: []

};

var this_app = {};

var drawn_test = {};

var drawn_pol = {
    'id': 'drawn_pol',
    'type': 'line',
    'source': 'drawn_pol_source',

    'paint': {
        'line-color': "#f75d36",
        "line-width": 2
    }
}

var white_cartodb = {
    active: false,
    'id': 'white_cartodb',
    'type': 'raster',
    'source': 'carto_white'

}
var black_cartodb = {
    active: false,
    'id': 'black_cartodb',
    'type': 'raster',
    'source': 'carto_black'

}
var jawg = {
    active: false,
    'id': 'jawg',
    'type': 'raster',
    'source': 'jawg_source'

}

var natgeo = {
    active: false,
    'id': 'natgeo',
    'type': 'raster',
    'source': 'natgeo_source'

}

var esri_satellite = {
    active: false,
    'id': 'esri_satellite',
    'type': 'raster',
    'source': 'esri_satellite_source'

}

var base_layers = [white_cartodb, jawg, natgeo, black_cartodb, esri_satellite];
app_data.raster_external = base_layers;

var sel_grid_buffer = {
    'id': 'sel_grid_buffer',
    'type': 'line',
    'source': 'sel_lines_buffer_source',
    "paint": {
        "line-color": '#414c52'

    }
}

var sel_hospitals_grid_dist_lines = {
    "id": "sel_hospitals_grid_dist_lines",
    "type": "line",
    "source": "sel_hospitals_grid_dist_lines_source",

    'layout': {
        'visibility': 'visible'
    },
    "paint": {

        "line-color": [
            "interpolate", ["exponential", 0.2],
            ["get", "length"],
            2000,
            "#6b86f9",
            8000,
            "#80acd1",
            15000,
            "#a1cbb2",
            40000,
            "#cbe697",
            60000,
            "#fbff7c",
            150000,
            "#3919a8",
            5000000,
            "#832681"

        ],
        "line-width": [
            "interpolate", ["linear"],
            ["zoom"],
            3,
            0.8,
            5,
            1,
            7,
            1.5,
            9,
            1.8
        ],
        "line-opacity": 1

    }
}



var sel_elect = {
    'id': 'sel_elect',
    'type': 'line',
    'source': 'elec_lines_source',

    "paint": {

        "line-color": [
            "match", ["get", "volt_class"],
            0,
            "hsl(277, 40%, 75%)", [1],
            "hsl(41, 87%, 65%)", [2],
            "hsl(0, 68%, 67%)",
            "#000000"
        ],
        "line-width": [
            "interpolate", ["linear"],
            ["zoom"],
            3,
            1.2,
            5,
            1.3,
            7,
            1.8,
            9,
            2.2
        ],
        "line-dasharray": [2, 0.5]

    }
}

var ghs_250m_bigger_than_5 = {
    'id': 'ghs_250m_bigger_than_5',
    'type': 'raster',
    'source': 'ghs_250m_bigger_than_5_source',
    'paint': {}
};

app_data.layers_info.push({
    _layer: ghs_250m_bigger_than_5,
    id: 'ghs_250m_bigger_than_5',
    raster: true,
    queryable: false,
    activated_query: false,
    title: 'GHS 250m values bigger than 5'
});
var time_travel = {
    'id': 'time_travel',
    'type': 'raster',
    'source': 'time_travel_source',
    'paint': {}
};

app_data.layers_info.push({
    _layer: time_travel,
    id: 'time_travel',
    raster: true,
    queryable: false,
    activated_query: false,
    title: 'Travel time'
});

var travel_hc_access_minimum_all_motorized = {

    'id': 'travel_hc_access_minimum_all_motorized',
    'type': 'raster',
    'source': 'travel_hc_access_minimum_all_motorized_source',
    'paint': {
        'raster-opacity': 1

    }
}

app_data.layers_info.push({
    _layer: travel_hc_access_minimum_all_motorized,
    id: 'travel_hc_access_minimum_all_motorized',
    raster: true,
    queryable: false,
    activated_query: false,
    title: 'Travel motorized time to nearest health centre'
});


var travel_hc_access_minimum_all_walking = {

    'id': 'travel_hc_access_minimum_all_walking',
    'type': 'raster',
    'source': 'travel_hc_access_minimum_all_walking_source',
    'paint': {
        'raster-opacity': 1

    }
}

app_data.layers_info.push({
    _layer: travel_hc_access_minimum_all_walking,
    id: 'travel_hc_access_minimum_all_walking',
    raster: true,
    queryable: false,
    activated_query: false,
    title: 'Walking time to nearest health facility'
});

var travel_time_primary_all_walking = {

    'id': 'travel_time_primary_all_walking',
    'type': 'raster',
    'source': 'travel_time_primary_all_walking_source',
    'paint': {
        'raster-opacity': 1

    }
}

app_data.layers_info.push({
    _layer: travel_time_primary_all_walking,
    id: 'travel_time_primary_all_walking',
    raster: true,
    queryable: false,
    activated_query: false,
    title: 'Walking time to nearest primary school'
});


var travel_time_primary_all_motorized = {

    'id': 'travel_time_primary_all_motorized',
    'type': 'raster',
    'source': 'travel_time_primary_all_motorized_source',
    'paint': {
        'raster-opacity': 1

    }
}

app_data.layers_info.push({
    _layer: travel_time_primary_all_motorized,
    id: 'travel_time_primary_all_motorized',
    raster: true,
    queryable: false,
    activated_query: false,
    title: 'Travel motorized time to nearest primary school'
});






var nightlights = {
    'id': 'nightlights',
    'type': 'raster',
    'source': 'nightlights_source',
    'paint': {
        'raster-opacity': 1

    }
}

app_data.layers_info.push({
    _layer: nightlights,
    id: 'nightlights',
    raster: true,
    queryable: false,
    activated_query: false,

    'title': 'Night lights'
});

var ghs_africa_250m_bigger_2 = {
    id: 'ghs_africa_250m_bigger_2',
    'type': 'raster',
    'source': 'ghs_africa_250m_bigger_2_source',
    'paint': {}
};

var ghs_africa_250m_bigger_1_5 = {
    id: 'ghs_africa_250m_bigger_1_5',
    'type': 'raster',
    'source': 'ghs_africa_250m_bigger_1_5_source',
    'paint': {}
};

var schools = {
    'id': 'schools',
    'type': 'raster',
    'source': 'schools_source',
    'paint': {},
    "default_param": "demand_e",
    active_param: 'demand_e'
}


app_data.layers_info.push({
    id: 'schools',
    raster: true,
    _layer: schools,
    queryable: true,
    activated_query: false,

    active_filters: [
        {
            'param_filter': null
        },
        {
            'admin_filter': null
        }
    ],
    _type: 'circle',
    cql_filter: null,
    'title': 'Schools',
    default_param: "demand_e",
    active_param: 'demand_e',
    params_arr: ['wea', 'category', 'bat_kw_e', 'demand_e', 'opex', 'lcoe', 'pv_kw_e', 'capex'],
    popup_params: [
        { name: 'category', title: 'Type of school' },

        { name: 'demand_e', title: 'Electricity demand per school (kWh/year)' },
        { name: 'pv_kw_e', title: 'Optimised PV Capacity (kWp)' },
        { name: 'bat_kw_e', title: 'Optimised Battery (kWh)' },
        { name: 'capex', title: 'CAPEX (EUR)' },
        { name: 'lcoe', title: 'LCOE (EUR/kWh)' },
        { name: 'opex', title: 'OPEX (EUR)' },
        { name: 'co2_e', title: 'Avoided CO2 emissions (kTonnes/year)' },
        { name: 'students_count', title: 'Number of students per school' },
        { name: 'mean_tt', title: 'Motorized travel time (min) ' },
        { name: 'mean_tt_walk', title: 'Walking time (min)' },
        { name: 'pot_min', title: 'Time saved upon school electrification (minutes)' },
        { name: 'pot_min_walk', title: 'Walking time saved upon school electrification (minutes)' },

        { name: 'wea', title: 'With electricity access' },



    ],
    params_description: [
        {
            param_name: 'wea',
            type_param: 'categorical_index',
            subtype_param: 'range',
            long_title: 'With electricity access <span class="more category">more info...</span>',
            long_title_txt: 'With electricity access',
            short_title: 'With electricity access',
            more_info_html: `<div class="extended_description_html wea">Binary (Yes/No)
            Yes is under the grid or presence of nightlights,
            NO is out of grid and no presence of nightlights </div>`

        },

        {
            param_name: 'category',
            type_param: 'categorical_index',

            long_title: 'Type of school <span class="more category">more info...</span>',
            long_title_txt: 'Type of school',
            short_title: 'Type of school',
            more_info_html: '<div class="extended_description_html category">-primary and primary (N/D) these are the ones did not have category so by default are primary N/D, -secondary - secondary & primary</div> '

        },
        {
            type_param: "categorical_index",
            subtype_param: 'range',
            param_name: 'bat_kw_e',
            long_title: 'Battery size to install [kWh]',
            long_title_txt: 'Battery size to install [kWh]',
            short_title: 'Optimised Battery (kWh)'

        },
        {
            param_name: 'demand_e',
            type_param: "categorical_index",
            subtype_param: 'range',

            long_title: 'Annual electricity demand per facility per year (kwh/year)',
            long_title_txt: 'Annual electricity demand per facility per year (kwh/year).',
            short_title: 'Electricity demand per school (kWh/year)'

        },
        {
            type_param: "categorical_index",
            subtype_param: 'range',
            param_name: 'opex',
            long_title: 'OPEX (EUR)',
            long_title_txt: 'OPEX (EUR)',
            short_title: 'OPEX (EUR)'

        },
        {
            type_param: "categorical_index",
            subtype_param: 'range',
            param_name: 'lcoe',
            long_title: 'Levelised cost of electricity per school [EUR/kWh]',
            long_title_txt: 'Levelised cost of electricity per school [EUR/kWh]',
            short_title: 'LCOE (EUR/kWh)'

        },
        {
            type_param: "categorical_index",
            subtype_param: 'range',
            param_name: 'pv_kw_e',
            long_title: 'Solar PV installed capacity [kWp]',
            long_title_txt: 'Solar PV installed capacity [kWp]',
            short_title: 'Optimised PV Capacity (kWp)'

        },
        {
            type_param: "categorical_index",
            subtype_param: 'range',
            param_name: 'capex',
            long_title: 'Up-front cost per health facility [EUR]',
            long_title_txt: 'Up-front cost per health facility [EUR]',
            short_title: 'CAPEX (EUR)'

        }
    ],
    params_symbology: [


        {
            symbol_type: 'circle',
            title: 'With electricity access',
            using_class: false,
            orienation: 'vertical',
            param_name: 'wea',
            style_type: 'color',
            orienation: 'vertical',
            stops: false,
            data_type: 'string',
            exact_value: true,
            legend: [{
                val: 'No', //0 in dbase
                color: '#a13131'
            }, {
                val: 'Yes',
                color: '#1e7dc9'
            }]
        },
        {
            symbol_type: 'circle',
            using_class: true,
            title: 'Type of school',
            orienation: 'vertical',
            param_name: 'category',
            style_type: 'color',
            orienation: 'vertical',
            stops: false,
            data_type: 'string',
            exact_value: true,
            legend: [{
                val: 'Primary',
                color: '#1e7dc9'
            },
            {
                val: 'Secondary',
                color: '#8bc34a'
            }
                , {
                val: 'Primary & secondary',
                color: '#a13131'
            },]
        },


        {
            symbol_type: 'circle',
            using_class: true,
            title: 'Annual electricity demand per facility per year (kwh/year)',
            orienation: 'vertical',
            param_name: 'demand_e',
            style_type: 'radius',
            stops: true,
            data_type: 'text',
            exact_value: false,
            legend: [{
                val: '<1825',
                color: '#e91e63',
                text: 'Very low'
            }, {
                val: '1825-3450',
                color: '#ff9800',
                text: 'Low'
            }, {
                val: '3450-7300',
                color: '#52c657',
                text: 'Medium'
            },
            {
                val: '>=7300',
                color: '#03a9f4',
                text: 'High'
            },
            {
                val: 'No data',
                color: '#673AB7',
                text: 'No data'
            }
            ]


        },
        {
            symbol_type: 'circle',
            using_class: true,
            title: 'bat_kw_e',
            orienation: 'vertical',
            param_name: 'bat_kw_e',
            style_type: 'radius',
            stops: true,
            data_type: 'text',
            exact_value: false,
            legend: [{
                val: '<3',

                color: '#e91e63',
                text: 'Very low'
            }, {
                val: '3-5',

                color: '#ff9800',
                text: 'Low'
            }, {
                val: '5-25',

                color: '#52c657',
                text: 'Medium'
            },
            {
                val: ' ≥25',

                color: '#03a9f4',
                text: 'High'
            },
            {
                val: 'No data',
                color: '#673AB7',

            }]


        },
        {
            symbol_type: 'circle',
            using_class: true,
            title: 'opex',
            orienation: 'vertical',
            param_name: 'opex',
            style_type: 'radius',
            stops: true,
            data_type: 'text',
            exact_value: false,
            legend: [{
                val: '<200',

                color: '#e91e63',
                text: 'Very low'
            }, {
                val: '200-500',

                color: '#ff9800',
                text: 'Low'
            }, {
                val: '500-1000',

                color: '#52c657',
                text: 'Medium'
            },
            {
                val: ' ≥1000',

                color: '#03a9f4',
                text: 'High'
            },
            {
                val: 'No data',
                color: '#673AB7',

            }
            ]


        },
        {
            symbol_type: 'circle',
            using_class: true,
            title: 'lcoe',
            orienation: 'vertical',
            param_name: 'lcoe',
            style_type: 'radius',
            stops: true,
            data_type: 'text',
            exact_value: false,
            legend: [{
                val: '<0.25',

                color: '#e91e63',
                text: 'Very low'
            }, {
                val: '0.25-0.3',

                color: '#ff9800',
                text: 'Low'
            }, {
                val: '0.3-0.45',

                color: '#52c657',
                text: 'Medium'
            },
            {
                val: ' ≥0.45',

                color: '#03a9f4',
                text: 'High'
            },
            {
                val: 'No data',
                color: '#673AB7',

            }
            ]


        },
        {
            symbol_type: 'circle',
            using_class: true,
            title: 'pv_kw_e',
            orienation: 'vertical',
            param_name: 'pv_kw_e',
            style_type: 'radius',
            stops: true,
            data_type: 'text',
            exact_value: false,
            legend: [{
                val: '<2',

                color: '#e91e63',
                text: 'Very low'
            }, {
                val: '2-53',

                color: '#ff9800',
                text: 'Low'
            }, {
                val: '5-10',

                color: '#52c657',
                text: 'Medium'
            },
            {
                val: '≥10',

                color: '#03a9f4',
                text: 'High'
            },
            {
                val: 'No data',
                color: '#673AB7',

            }
            ]


        },
        {
            symbol_type: 'circle',
            using_class: true,
            title: 'capex',
            orienation: 'vertical',
            param_name: 'capex',
            style_type: 'radius',
            stops: true,
            data_type: 'text',
            exact_value: false,
            legend: [{
                val: '<3000',

                color: '#e91e63',
                text: 'Very low'
            }, {
                val: '3000 - 7500',

                color: '#ff9800',
                text: 'Low'
            }, {
                val: '7500 - 15000',

                color: '#52c657',
                text: 'Medium'
            },
            {
                val: '≥15000',

                color: '#03a9f4',
                text: 'High'
            },
            {
                val: 'No data',
                color: '#673AB7',

            }
            ]


        }

    ]
});

app_data.layers_info.push({
    _layer: ghs_africa_250m_bigger_1_5,
    id: 'ghs_africa_250m_bigger_1_5',
    raster: true,
    queryable: false,
    activated_query: false,
    title: 'Population (inhabitants/250 m2)'

});

var distance_to_grid = {
    'id': 'distance_to_grid',
    'type': 'raster',
    'source': 'distance_to_grid_source',
    'paint': {}
}

app_data.layers_info.push({
    _layer: distance_to_grid,
    id: 'distance_to_grid',
    raster: true,
    queryable: false,
    activated_query: false,

    title: 'Distance to grid'
});

var ghs_1km_bigger_than_5 = {
    'id': 'ghs_1km_bigger_than_5',
    'type': 'raster',
    'source': 'ghs_1km_bigger_than_5_source',
    'paint': {}
}


app_data.layers_info.push({
    _layer: ghs_1km_bigger_than_5,
    id: 'ghs_1km_bigger_than_5',
    raster: true,
    queryable: false,
    activated_query: false,

    'title': 'GHS 1km values bigger than 5'
});





var gaul_0_analysis_labels = {
    'id': 'gaul_0_analysis_labels',
    'type': 'symbol',
    'source': 'gaul_0_analysis_labels_source',

    "paint": {
        "text-color": "#312f2f"

    },

    'layout': {
        "text-field": "{value}\n",
        "text-font": ["Open Sans Regular"],
        "text-size": 15,
        'text-justify': 'auto',
        'symbol-placement': "point"
    }
};

var ref_unhcr_merged_simple = {
    id: 'ref_unhcr_merged_simple',

    source: 'ref_unhcr_merged_simple_source',
    'source-layer': 'ref_unhcr_merged_simple',
    type: 'circle',
    paint: {}

}

app_data.layers_info.push({
    _layer: ref_unhcr_merged_simple,
    title: 'UNHCR settlements',
    id: 'ref_unhcr_merged_simple',
    _type: 'circle',
    /* main_sidenav: false,
    not_styled: true, */

    border_width: {
        base: 1, // default value
        stops: [
            // first # is the zoom level, second # is the style val
            [5, 0],
            [5.1, .8],
            [16, 1.5],
        ],
    },

    circle_opacity: {
        base: 2, // default value
        stops: [
            // first # is the zoom level, second # is the style val
            [3, 1],
            [5, 1]

        ],
    },
    border: '#fff',
    border_width: .5,
    queryable: true,
    activated_query: false,
    active_filters: [{
        'basic': null
    },
    {
        'param_filter': null
    },
    {
        'admin_filter': null
        //['in', 'adm0_code', 29]
    }
    ],
    default_param: 'loc_subtyp',
    active_param: 'loc_subtyp',
    title: 'UNHCR settlements',
    //'npv_consn','npv_h'
    params_arr: ['loc_subtyp', 'total', 'pop_type'],
    extra_popup_params: [
        {
            name: 'ref',
            title: 'Refugees'
        },
        {
            name: 'asy',
            title: 'Asylum-seekers'
        },
        {
            name: 'idp',
            title: 'Internally displaced people'
        },

        {
            name: 'oip',
            title: 'Other people in need of international protection'
        },
        {
            name: 'ret',
            title: 'Returnees (refugees)'
        },
        {
            name: 'sta',
            title: 'Stateless people'
        },
        {
            name: 'ooc',
            title: 'Others of concern to UNHCR'
        },
        {
            name: 'hst',
            title: 'Host community'
        },
    ],
    params_symbology: [
        {
            using_class: false,
            symbol_type: 'circle',
            title: 'Location type',
            orienation: 'vertical',
            param_name: 'loc_subtyp',
            style_type: 'color',
            orienation: 'vertical',
            stops: false,
            data_type: 'number',
            exact_value: true,
            legend: [
                {
                    val: 'Formal settlement',
                    color: '#03f9a6',
                    code: "38"
                },
                {
                    val: 'Informal settlement',
                    color: '#2196f3',
                    code: "39"
                },


            ]
        },
        {
            symbol_type: 'circle',
            using_class: true,
            title: 'Total population',
            orienation: 'vertical',
            param_name: 'total',
            style_type: 'radius',
            stops: true,
            data_type: 'text',
            exact_value: false,

            legend: [

                {
                    val: '<105',

                    color: '#e91e63',
                    text: 'Very low'
                }, {
                    val: '105 - 685',

                    color: '#ff9800',
                    text: 'Low'
                }, {
                    val: '685 - 2,800',

                    color: '#52c657',
                    text: 'Medium'
                },
                {
                    val: '2,800 - 13,000',

                    color: '#03a9f4',
                    text: 'Medium'
                },
                {
                    val: '>13,000',
                    //pruple
                    color: '#673AB7',
                    test: '#673AB7',

                },
                {
                    val: 'No data',
                    color: '#e40ee8',
                    text: 'No data'
                }
            ]


        },

        {
            using_class: false,
            symbol_type: 'circle',
            title: 'Population type',
            orienation: 'vertical',
            param_name: 'pop_type',
            style_type: 'color',
            orienation: 'vertical',
            stops: false,
            data_type: 'number',
            exact_value: true,
            legend: [
                {
                    val: 'Mixed',
                    color: '#009688',
                    code: 20
                },
                {
                    val: 'Refugees',
                    color: '#b01c92',
                    code: 52
                }, {
                    val: 'Returnees',
                    color: '#e69f07',
                    code: 53
                },
                {
                    val: 'IDP',
                    color: '#0e6fc5',
                    code: 54
                },
                {
                    val: 'Asylum-seeker',
                    color: '#ff5722',
                    code: 55
                }

            ]
        }
    ],
    params_description: [{
        type_param: 'categorical_index',
        subtype_param: 'literal',
        param_name: 'loc_subtyp',
        long_title: 'Location type',
        long_title_txt: 'Long title txt loc_subtyp',
        short_title: 'Location type',
        more_info_html: '<div class="extended_description_html loc_subtyp"><ul> <li> <strong>Formal Settlement:</strong> Formal settlements are places where official land is allocated for a group of asylum seekers, refugees, or IDPs. They are accommodated in purpose-built settlements with access to facilities and services. An official management entity is assigned. Camps are a type of formal settlement. </li> <li> <strong>Informal Settlement:</strong> In an informal settlement, a group of asylum-seekers, refugees, or IDPs choose to settle in self-identified spontaneous sites. Self-settled settlements are often located on state-owned, private, or communal land, with or without negotiations with the local population or private landowners. </li></ul></div>',


    },
    {
        //categorical_index, we use numbers (index) instead of string (gen_type_class=>0, gen_type=>Biomass)
        type_param: "range",
        param_name: 'total',
        long_title: 'Total population in the settlement',
        long_title_txt: 'Total population in the settlementss',
        short_title: 'Total population',
        more_info_html: `<div class="extended_description_html total">
    <div>Population in the settlement. It may include several categories:</div>
    <div>
    Refugees, Returnees, IDP, Asylum-seeker, Stateless and Mixed.
    Check <a href="https://www.unhcr.org/refugee-statistics/methodology/definition" target="_blank">UNHCR description</a> for the different categories of refugees
    </div>

    </div>`,
        //The range is the set of resulting values of a function
        _range: [5, 6, 7, 9, 12, 3],

        //The domain is the complete set of values
        //divided by 100
        _domain: [3, 5, 7, 25, 180, 2]


    },
    {
        type_param: 'categorical_index',
        subtype_param: 'literal',
        param_name: 'pop_type',
        long_title: 'Population type',
        long_title_txt: 'Population type',
        short_title: 'Population type',
        more_info_html: '<div style="color:" class="extended_description_html pop_type">Total population counted (October 2024) in each settlement. Check <a href="https://www.unhcr.org/refugee-statistics/methodology/definition" target="_blank">UNHCR description</a> for the different categories of refugees </div>',


    }],
    mapbox_paint_style: [
        {
            param: 'pop_type',
            style: {

                "circle-opacity": 0.9,
                "circle-color": [
                    "match", ["get", "pop_type"],
                    52,

                    '#b01c92',

                    53,
                    '#e69f07',

                    54,

                    '#0e6fc5',

                    55,
                    '#ff5722',

                    //no data, green
                    20, '#009688',

                    "#000000"
                ],
                //orange
                "circle-stroke-color": "#e9e6ed",
                "circle-stroke-width": 1,
                "circle-radius": 5
            }
        },

        {
            param: 'total',
            style: {

                "circle-opacity": 0.7,
                "circle-color": [
                    "match", ["get", "total_class"],
                    0, '#e91e63',
                    1, '#ff9800',
                    2, '#52c657',
                    3, '#03a9f4',
                    4, '#673AB7',
                    5, '#e40ee8',
                    "#000000"
                ],
                //orange
                "circle-stroke-color": "#e9e6ed",
                "circle-stroke-width": 1,
                "circle-radius": [
                    "interpolate", ["linear"],
                    ["get", "total_class"],

                    0, 4,
                    1, 5,
                    2, 6,
                    3, 7,
                    4, 9,
                    5, 3

                ]
            }
        },
        {
            param: 'loc_subtyp',
            style: {

                "circle-opacity": .8,
                "circle-color": ["match", ["get", "loc_subtyp"], ["38"], "#03f9a6", ["39"], "#2196f3", "#ff0000"]
                ,
                //orange
                "circle-stroke-color": "#e9e6ed",
                "circle-stroke-width": .5,
                "circle-radius":
                    [
                        "interpolate", ["linear"],
                        ["zoom"],

                        8, 4,
                        12, 6,
                        15, 8
                    ],
            }
        },]

});

var ref_unhcr_merged_simple_labels = {
    'id': 'ref_unhcr_merged_simple_labels',
    'type': 'symbol',
    //'type': 'line',
    'source': 'ref_unhcr_merged_simple_source',
    "source-layer": "ref_unhcr_merged_simple",

    fontstack: ['Open Sans Regular'],

    //If the map's current zoom level is lower than the new minimum, the map will zoom to the new minimum.
    //smaller zoom, more 'far' we are
    //minzoom: 5,
    layout: {
        "text-field": "{gis_name}\n",
        "text-allow-overlap": false,
        'text-variable-anchor': ["top", "center"],
        "text-transform": "uppercase",
        "text-offset": [0, 1],
        /*
        accepts a scalar value that represents the offset distance of text from its anchor and is applied to the text-variable-anchor.
        Positive values specify right and down, while negative values specify left and up
        */
        //'text-radial-offset': 1,


        "text-font": ["Open Sans Bold"],
        "text-size":

            [
                "interpolate", ["linear"],
                ["zoom"],
                3, 0,
                5.5, 6,
                8, 12,
                9, 14
            ],

        /* 'symbol-placement': "point", */
        'text-letter-spacing': 0.2

    },
    "paint": {
        //"text-halo-style": "rectangle",
        "text-color": "#a76202",
        "text-halo-color": "#bbb7b7",
        "text-halo-width": 1.5
        //"text-color": "#ffd617",
        // "text-color": "#f2d654",

        // "text-halo-color": "#fff",
        // "text-halo-width": 3
    }
};

app_data.layers_labels_info.push({
    _layer: ref_unhcr_merged_simple_labels,
    id: 'ref_unhcr_merged_simple_labels',
    active: false
});




var ref_settlements_labels = {
    'id': 'ref_settlements_labels',
    'type': 'symbol',
    //'type': 'line',
    'source': 'ref_settlements_source',
    "source-layer": "refugee_camps_geoserver",

    fontstack: ['Open Sans Regular'],

    //If the map's current zoom level is lower than the new minimum, the map will zoom to the new minimum.
    //smaller zoom, more 'far' we are
    //minzoom: 5,
    layout: {
        "text-field": "{site_name}\n",
        "text-allow-overlap": false,
        'text-variable-anchor': ["top", "center"],
        /* "text-transform": "uppercase", */
        "text-offset": [0, 1],
        /*
        accepts a scalar value that represents the offset distance of text from its anchor and is applied to the text-variable-anchor.
        Positive values specify right and down, while negative values specify left and up
        */
        //'text-radial-offset': 1,


        "text-font": ["Open Sans Regular"],
        "text-size":

            [
                "interpolate", ["linear"],
                ["zoom"],
                3,
                7,
                5.5,
                9,
                8,
                11,
                9,
                14
            ],

        'symbol-placement': "point",
        'text-letter-spacing': 0.2

    },
    "paint": {
        //"text-halo-style": "rectangle",

        "text-color": "#1389de",
        //"text-color": "#ffd617",
        // "text-color": "#f2d654",

        "text-halo-color": "#d6d4d4",
        "text-halo-width": .5
    }
};
app_data.layers_labels_info.push({
    _layer: ref_settlements_labels,
    id: 'ref_settlements_labels',
    active: false
});



app_data.layers_labels_info.push({
    _layer: gaul_0_analysis_labels,
    id: 'gaul_0_analysis_labels',
    active: false
});
var gaul_1_analysis_labels = {
    'id': 'gaul_1_analysis_labels',
    'type': 'symbol',
    'source': 'gaul_1_analysis_labels_source',
    "paint": {
        "text-color": "#312f2f",
    },
    'layout': {
        'symbol-placement': 'point',
        'text-variable-anchor': ["bottom", "top", "left"],
        "text-field": "{value}\n",
        "text-font": ["Open Sans Regular"],
        "text-size": 15,
        'text-justify': 'auto',
        'symbol-placement': "point"
    }


};

app_data.layers_labels_info.push({
    _layer: gaul_1_analysis_labels,
    id: 'gaul_1_analysis_labels',
    active: false
});


var gaul_2_analysis_labels = {
    'id': 'gaul_2_analysis_labels',
    'type': 'symbol',
    'source': 'gaul_2_analysis_labels_source',
    "paint": {
        "text-color": "#312f2f",
    },
    'layout': {
        "text-field": "{value}\n",
        "text-font": ["Open Sans Regular"],
        "text-size": 12,
        'text-justify': 'auto',
        'symbol-placement': "point"
    }
};

app_data.layers_labels_info.push({
    _layer: gaul_2_analysis_labels,
    id: 'gaul_2_analysis_labels',
    active: false
});



var refugee_camps_geojson_labels = {
    id: "refugee_camps_geojson_labels",
    type: "symbol",
    source: "refugee_camps_geojson_labels_source",
    fontstack: ['Open Sans Regular'],
    layout: {
        "text-field": "{site_name}\n",
        "text-allow-overlap": false,
        'text-variable-anchor': ["top", "center"],
        "text-transform": "uppercase",
        "text-offset": [0, 1.5],


        "text-font": ["Open Sans Light"],
        "text-size":

            [
                "interpolate", ["linear"],
                ["zoom"],
                3,
                7,
                5.5,
                10,
                8,
                12,
                9,
                15
            ],

        'symbol-placement': "point",
        'text-letter-spacing': 0.2

    },
    "paint": {

        "text-color": "#f6d330",

        "text-halo-color": "#fff",
        "text-halo-width": 0.1
    },
}

app_data.layers_info.push({
    _layer: refugee_camps_geojson_labels,
    id: 'refugee_camps_geojson_labels',
    active: false
});


var gaul_0_labels = {
    id: "gaul_0_labels",
    type: "symbol",
    source: "gaul_0_labels_source",
    fontstack: ['Open Sans Light'],
    "source-layer": "gaul_0_labels",
    layout: {
        "text-field": "{adm0_name}\n",
        "text-allow-overlap": true,
        "text-transform": "uppercase",
        "text-font": ["Open Sans Light"],
        "text-size":

            [
                "interpolate", ["linear"],
                ["zoom"],
                0, 5,
                3, 9,
                4.5, 15,
                7, 20,
                9, 22
            ],

        'symbol-placement': "point",

    },
    "paint": {
        "text-color": "#f58f0d",
        "text-opacity": 1,
        "text-halo-color": "#3c3b38",
        "text-halo-width": 1

    },
    filter: ['match', ['get', 'adm1_name'],
        ['None'], false, true
    ]
}

app_data.layers_info.push({
    _layer: gaul_0_labels,
    queryable: false,
    active: false
});




var gaul_level_2 = {
    "id": "gaul_level_2",
    "type": "line",
    "source": "gaul_2_source",
    "source-layer": "gaul_level_2_africa",
    'layout': {
        'visibility': 'visible'
    },
    "paint": {

        'line-color': "#aba8a4",
        "line-opacity": [
            "interpolate", ["linear"],
            ["zoom"],
            3,
            0.2,
            4, 0.4,
            12, .7
        ],
        "line-width": [
            "interpolate", ["linear"],
            ["zoom"],
            3, 0.1,
            5, 0.3,
            7, 0.8,
            9, 1
        ],
    },

};

var gaul_level_2_analysis = {
    "id": "gaul_level_2_analysis",
    "source": "gaul_2_source",
    "source-layer": "gaul_level_2_africa",
    "type": "fill",
    'layout': {
        'visibility': 'visible'

    },

    'paint': {
        "fill-color-transition": {
            "duration": 1500,
            "delay": 100
        },
        "fill-color": "#cbd5d5",
        "fill-outline-color": "#c1bfbf",
        "fill-opacity": 1,


    }
};
var gaul_level_2_highlighted = {
    "id": "gaul_level_2_highlighted",
    "type": "fill",
    "source": "gaul_2_source",
    "source-layer": "gaul_level_2_africa",
    'layout': {
        'visibility': 'visible'

    },
    "paint": {
        "fill-color": '#f7d711',
        'fill-outline-color': 'rgba(255,255,255,0.0)',

        'fill-outline-color': '#c1bfbf',
        "fill-opacity": 0
    }



};

var gaul_level_2_highlighted_line = {
    "id": "gaul_level_2_highlighted_line",
    "source": "gaul_2_source",
    "source-layer": "gaul_level_2_africa",
    "type": "line",
    'layout': {
        'visibility': 'visible'

    },

    "paint": {
        'line-color': "#ffae0d",
        "line-width": [
            "interpolate", ["linear"],
            ["zoom"],
            2,
            1,
            4.5,
            2,

            9,
            4
        ],
        'line-opacity': 0

    }
};
var gaul_2_labels = {
    id: "gaul_2_labels",
    type: "symbol",
    source: "gaul_2_labels_source",
    fontstack: ['Open Sans Regular'],
    "source-layer": "gaul_2_labels",
    layout: {
        "text-field": "{adm2_name}\n",
        "text-font": ["Open Sans Regular"],
        "text-allow-overlap": false,
        //"text-transform": "uppercase",
        'text-letter-spacing': 0.1,
        "text-size":

            [
                "interpolate", ["linear"],
                ["zoom"],

                5.39, 0,
                5.4, 9,
                5.49, 9,
                6, 10,
                9, 11,
                15, 17
            ],

        //  'symbol-placement': "point",

    },
    "paint": {
        "text-opacity": 1,
        "text-color": "#c7c4c2",
        "text-halo-color": "#837d7a",
        "text-halo-width": .5

    },
    filter: ['match', ['get', 'adm2_name'],
        ['None'], false, true
    ]
}

app_data.layers_info.push({
    _layer: gaul_2_labels,
    queryable: false,
    active: false
});



var gaul_level_0_all = {
    "id": "gaul_level_0_all",
    "group": 0,
    "source": "gaul_level_0_all_source",
    "source-layer": "gaul_0_simplified",
    "type": "line",
    'layout': {
        'visibility': 'visible'

    },

    'paint': {

        "line-color": "#443d3d",

        "line-opacity": 1

    }
};


var gaul_level_0 = {
    "id": "gaul_level_0",
    "group": 0,
    "source": "gaul_level_0_source",
    "source-layer": "gaul_level_0_africa3",
    "type": "fill",
    'layout': {
        'visibility': 'visible'

    },

    'paint': {

        "fill-color": "#443d3d",

        "fill-opacity": 1

    }
};

var gaul_level_0_background = {
    "id": "gaul_level_0_background",

    "source": "gaul_level_0_source",
    "source-layer": "gaul_level_0_africa3",
    "type": "fill",
    'layout': {
        'visibility': 'visible'

    },
    "paint": {
        "fill-opacity": 0,
        "fill-color": '#0d0d0c',
        "fill-opacity-transition": {
            "duration": 2500,
            "delay": 100
        }

    }
};

var gaul_level_0_highlighted = {
    "id": "gaul_level_0_highlighted",

    "source": "gaul_level_0_source",
    "source-layer": "gaul_level_0_africa3",
    "type": "fill",
    'layout': {
        'visibility': 'visible'

    },
    "paint": {
        "fill-opacity": 0,
        "fill-color": '#f7d711',

    }
};

var gaul_level_0_highlighted_line = {
    "id": "gaul_level_0_highlighted_line",
    "group": 0,
    "source": "gaul_level_0_source",
    "source-layer": "gaul_level_0_africa3",
    "type": "line",
    'layout': {
        'visibility': 'visible'

    },
    "paint": {
        'line-color': "#f75d36",
        "line-width": [
            "interpolate", ["linear"],
            ["zoom"],
            2,
            1,
            4.5,
            2,

            9,
            4
        ],
        'line-opacity': 0
    }
};


var gaul_level_0_analysis = {
    "id": "gaul_level_0_analysis",
    "source": "gaul_level_0_source",
    "source-layer": "gaul_level_0_africa3",
    "type": "fill",

    'layout': {
        'visibility': 'visible'

    },

    'paint': {
        "fill-color-transition": {
            "duration": 1500,
            "delay": 100
        },
        "fill-color": "#cc3333",

        "fill-outline-color": "#c1bfbf",
        "fill-opacity": 1

    }
};

var gaul_level_1_analysis = {
    "id": "gaul_level_1_analysis",
    "source": "gaul_level_1_source",
    "source-layer": "gaul_level_1_africa",
    "type": "fill",
    'layout': {
        'visibility': 'visible'

    },

    'paint': {
        "fill-color-transition": {
            "duration": 1500,
            "delay": 100
        },
        "fill-color": "#cbd5d5",

        "fill-outline-color": "#c1bfbf",
        "fill-opacity": 1

    }
};



var electric_grid = {
    "id": "electric_grid",
    "type": "line",
    "source": "electric_grid_source",
    "source-layer": "electric_grid_new_gaul",
    'layout': {
        'visibility': 'visible'
    },
    "paint": {

        "line-color": [
            "match", ["get", "vltg_kv_class"],
            [0],
            "#ffeb3b", [1],
            "#4363d8", [2],
            "#f032e6", [3],
            "#e6194B",
            "#000000"
        ],


        "line-width": [
            "interpolate", ["linear"],
            ["zoom"],
            3,
            0.5,
            5,
            0.8,
            7,
            1.2,
            9,
            1.8
        ],
        "line-dasharray": [2, 0.5],
        "line-opacity": 1

    }
};





var gaul_level_0_line = {
    "id": "gaul_level_0_line",
    "type": "line",
    "source": "gaul_level_0_line_source",

    "source-layer": "gaul_level_0_africa3",
    'layout': {
        'visibility': 'visible'

    },
    'paint': {
        'line-color': '#ff9800',
        "line-width": [
            "interpolate", ["linear"],
            ["zoom"],
            2, .5,
            4.5, 1,
            9, 3
        ],
        'line-opacity': .5,
    },

};

var gaul_level_1 = {
    "id": "gaul_level_1",
    "type": "line",
    "source": "gaul_level_1_source",
    "source-layer": "gaul_level_1_africa",
    'layout': {
        'visibility': 'visible',
        'line-join': 'round',
        'line-cap': 'round',


    },
    "paint": {
        'line-color': "#a8590a",
        "line-dasharray": [2, 0.5],
        "line-opacity": [
            "interpolate", ["linear"],
            ["zoom"],
            3,
            0.2,
            4, 0.4,
            12, .7
        ],
        "line-width": [
            "interpolate", ["linear"],
            ["zoom"],
            3, 0.6,
            5, 1,
            7, 1.4,
            9, 2.2
        ]
    },

};

var gaul_1_labels = {
    id: "gaul_1_labels",
    type: "symbol",
    source: "gaul_1_labels_source",
    fontstack: ['Open Sans Regular'],
    "source-layer": "gaul_1_labels",
    layout: {
        "text-field": "{adm1_name}\n",
        "text-font": ["Open Sans Regular"],
        'text-allow-overlap': false,    // Allow text overlap
        //'symbol-placement': 'point',
        "text-transform": "uppercase",
        'text-variable-anchor': ["top", "bottom", "left"],
        "text-size":

            [
                "interpolate", ["linear"],
                ["zoom"],

                4.99, 0,
                5, 8,
                5.4, 10,


                9, 17,
                15, 21
            ]

    },
    "paint": {
        "text-opacity": 1,
        "text-color": "#a69e9e",
        "text-halo-color": "#a8590a",
        "text-halo-width": 0.5
    },
    filter: ['match', ['get', 'adm1_name'],
        ['None'], false, true
    ]
}



app_data.layers_info.push({
    _layer: gaul_1_labels,
    queryable: false,
    active: false
});

var gaul_level_1_highlighted = {
    "id": "gaul_level_1_highlighted",
    "source": "gaul_1_source",
    "source-layer": "gaul_level_1_africa",
    "type": "fill",
    'layout': {
        'visibility': 'visible'

    },
    "paint": {
        "fill-color": '#f7d711',
        'fill-outline-color': '#9E9E9E',
        "fill-opacity": 0
    }
};

var gaul_level_1_highlighted_line = {
    "id": "gaul_level_1_highlighted_line",
    "source": "gaul_1_source",
    "source-layer": "gaul_level_1_africa",
    "type": "line",
    'layout': {
        'visibility': 'visible'
    },
    "paint": {
        'line-color': "#ffae0d",
        'line-width': 1,
        'line-opacity': 0

    }

};

var ref_unhcr_polygons = {
    'id': 'ref_unhcr_polygons',
    'source': 'ref_unhcr_polygons_source',
    'source-layer': 'ref_unhcr_polygons',
    'type': 'fill',
    'paint': {
        'fill-color': '#11e1e0',
        'fill-opacity': 0.7,
        'fill-outline-color': '#fff'

    },

    'layout': {
        'visibility': 'visible'
    }


}

app_data.layers_info.push({
    _layer: ref_unhcr_polygons,
    title: 'UNHCR polygons',
    _type: 'fill',
    id: 'ref_unhcr_polygons',

})
var clustered_pop = {
    'id': 'clustered_pop',
    'source': 'clustered_pop_source',
    'source-layer': 'clustered_pop',
    "default_param": "population_class",
    'type': 'fill',
    'paint': {
        'fill-color': [
            'interpolate', ['linear'],
            ['get', 'population_class'],
            0,
            '#003f5c',
            1,
            '#2f4b7c',
            2,
            '#665191',
            3,
            '#d45087',
            4,
            '#f95d6a',
            5,
            '#ff7c43',

        ]
    }
}
var p_plants_gen_type = [
    { val: 'Biomass', color: '#b30e32' },
    { val: 'Coal', color: '#495053' },
    { val: 'Gas', color: 'hsl(242, 83%, 56%)' },
    { val: 'Geothermal', color: '#795548' },
    { val: 'Hydro', color: 'hsl(226, 60%, 56%)' },

    { val: 'Oil', color: '#aa0dc5' },
    { val: 'Solar', color: '#f3dc16' },
    { val: 'Unknown', color: 'hsl(39, 78%, 89%)' },
    { val: 'Nuclear', color: 'hsl(347, 94%, 36%)' },
    { val: 'Waste-to-energy', color: '#0d5e21' },

    { val: 'Wind', color: '#6232a8' }
]


var power_plants = {
    'id': 'power_plants',
    'source': 'power_plants_source',
    "default_param": "gen_type",
    'source-layer': 'power_plants_2019_class2',
    'type': 'circle',
    'layout': {
        'visibility': 'visible'
    },
    filter: [
        "all", ["has", "gen_type"]


    ],

    'paint': {
        "circle-stroke-color": "hsl(0, 2%, 95%)",
        "circle-stroke-width": 1.2,
        "circle-translate": [0, 0],
        "circle-color": p_plants_gen_type.reduce(function (memo, data, i) {

            if (i !== p_plants_gen_type.length - 1)

                memo.push(i, data.color)
            else
                memo.push('red')

            return memo

        }, ["match", ["get", "gen_type_class"]]),
        "circle-radius": [
            "interpolate", ["exponential", 1],
            ["zoom"],
            3, 3,
            7, 6,
            13, 10
        ]
    }
};


var ref_settlements = {
    id: 'ref_settlements',

    source: 'ref_settlements_source',
    'source-layer': 'refugee_camps_geoserver',
    type: 'circle',


    paint: {

        "circle-opacity": 1,
        "circle-color": [
            "match", ["get", "pv_class"],
            [0],
            '#ff8c54', [1],
            '#F04F00', [2],
            "#f23f70", [3],
            "#b51641",


            "#000000"
        ],
        "circle-stroke-color": "#e9e6ed",
        "circle-stroke-width": 1,
        "circle-radius": [
            "interpolate", ["linear"],
            ["get", "pv_class"],

            0, 3,
            1, 4.5,
            2, 8,
            3, 11,

        ]
    }



}



app_data.layers_info.push({
    _layer: ref_settlements,
    title: 'Refugee settlements',
    _type: 'circle',
    id: 'ref_settlements',
    border: '#e9e6ed',
    scale_val_arr: [],
    active_filters: [{
        'basic': ["has", "bat"]
    },
    {
        'param_filter': null
    },
    {
        'admin_filter': null
    }
    ],

    default_param: "pv",
    activated_query: false,
    active_param: "pv",
    queryable: true,
    params_non_analyse_description: {
        pop: 'Population [# of refugees]',
        hh: 'Num. Households',
        bus: 'Num. Business',
        inst: 'Num. Institutions',
        dist_grid: 'Distance from network (km)',
        dist_border: 'Distance from national borders (km)'
    },
    params_arr: ['year_co2_t', 'lcoe_usdc_kwh_10', 'tot_cost_usd', 'pop', 'bat', 'pv'],
    params_description: [{
        type_param: 'range',
        param_name: 'year_co2_t',
        long_title: 'Yearly avoided carbon emissions [t/yr]<span class="more year_co2_t">more info...</span>',
        long_title_txt: 'Yearly avoided carbon emissions [t/yr]',
        short_title: 'Yearly avoided carbon emissions [t/yr]',
        more_info_html: '<div class="extended_description_html year_co2_t">Tonnes of CO2 avoided yearly by fulfilling the electricity needs of the settlement with a fully renewable PV system instead of a diesel generator<span class="reference">Source: Baldi, D. (2021), Nature journal paper: <a href="https://www.nature.com/articles/s41560-022-01006-9" target="_blank">Planning sustainable electricity solutions for refugee settlements in sub-Saharan Africa</a><br>The RSEA DB generated during this study is available in two file formats (csv and geopackage) following <a href="https://data.jrc.ec.europa.eu/dataset/4261bf3c-7e8e-4b16-925b-68cfd4eade37" target="_blank">this link</a></span></div>',
        _range: [4, 7, 10, 14],
        _domain: [101, 501, 1001, 1200]

    },
    {
        type_param: 'range',
        param_name: 'lcoe_usdc_kwh_10',
        long_title: 'LCOE [USD ¢ /kWh] - 10% discount rate<span class="more lcoe_usdc_kwh_10">more info...</span>',
        long_title_txt: 'LCOE [USD ¢ /kWh] - 10% discount rate',
        short_title: 'LCOE [USD ¢ /kWh] - 10% discount rate',
        _range: [4, 7, 10, 14],
        _domain: [26, 31, 36, 40],
        more_info_html: '<div class="extended_description_html lcoe_usdc_kwh_10">LCOE of the PV mini-grid, including battery storage, O&M costs, replacement  costs for battery and PV invert after 10 years, distribution network and soft costs, 10 % discount rate,  lifetime of the system set at 20 years<span class="reference">Source: Baldi, D. (2021), Nature journal paper: <a href="https://www.nature.com/articles/s41560-022-01006-9" target="_blank">Planning sustainable electricity solutions for refugee settlements in sub-Saharan Africa</a><br>The RSEA DB generated during this study is available in two file formats (csv and geopackage) following <a href="https://data.jrc.ec.europa.eu/dataset/4261bf3c-7e8e-4b16-925b-68cfd4eade37" target="_blank">this link</a></span></div>',

    },
    {
        type_param: 'range',
        param_name: 'tot_cost_usd',
        long_title: 'Total project cost  [USD]<span class="more tot_cost_usd">more info...</span>',
        long_title_txt: 'Total project cost  [USD]',
        short_title: 'Total project cost  [USD]',
        more_info_html: '<div class="extended_description_html tot_cost_usdv">Capital costs [US Dollars] of PV system generation components (including battery storage and BOS), distribution network (customer connections and overhead wiring) and other project management costs. The assumption used in this model is to neglect regional or country variations associated with component costs<span class="reference">Source: Baldi, D. (2021), Nature journal paper: <a href="https://www.nature.com/articles/s41560-022-01006-9" target="_blank">Planning sustainable electricity solutions for refugee settlements in sub-Saharan Africa</a><br>The RSEA DB generated during this study is available in two file formats (csv and geopackage) following <a href="https://data.jrc.ec.europa.eu/dataset/4261bf3c-7e8e-4b16-925b-68cfd4eade37" target="_blank">this link</a></span></div>',
        _range: [4, 6.5, 9, 13],
        _domain: [1000001, 3000001, 5000001, 6000001]

    },
    {
        type_param: 'range',
        param_name: 'pop',
        long_title: 'Population [# of refugees]<span class="more pop">more info...</span>',
        long_title_txt: 'Population [# of refugees]',
        short_title: 'Population [# of refugees]',
        more_info_html: '<div class="extended_description_html pop">Total number of people per settlement<span class="reference">Source: Baldi, D. (2021), Nature journal paper: <a href="https://www.nature.com/articles/s41560-022-01006-9" target="_blank">Planning sustainable electricity solutions for refugee settlements in sub-Saharan Africa</a><br>The RSEA DB generated during this study is available in two file formats (csv and geopackage) following <a href="https://data.jrc.ec.europa.eu/dataset/4261bf3c-7e8e-4b16-925b-68cfd4eade37" target="_blank">this link</a></span></div>',
        _range: [4, 6.5, 9, 11],
        _domain: [26, 31, 36, 40]

    },
    {
        type_param: 'range',
        param_name: 'bat',
        long_title: 'Required battery capacity [kWh]<span class="more bat">more info...</span>',
        long_title_txt: 'Required battery capacity [kWh]',
        short_title: 'Required battery capacity [kWh]',
        more_info_html: '<div class="extended_description_html bat">The size of the battery storage to be installed in each settlement to ensure the correct function of the system is optimized according to the relative PV system capacity and the night load profile<span class="reference">Source: Baldi, D. (2021), Nature journal paper: <a href="https://www.nature.com/articles/s41560-022-01006-9" target="_blank">Planning sustainable electricity solutions for refugee settlements in sub-Saharan Africa</a><br>The RSEA DB generated during this study is available in two file formats (csv and geopackage) following <a href="https://data.jrc.ec.europa.eu/dataset/4261bf3c-7e8e-4b16-925b-68cfd4eade37" target="_blank">this link</a></span></div>',

        _range: [4, 7, 10, 14],
        _domain: [501, 1001, 5001, 6200]

    },

    {
        type_param: 'range',
        param_name: 'pv',
        long_title: 'Required PV capacity [kWp]<span class="more pv">more info...</span>',
        long_title_txt: 'Solar PV installed capacity [kWp]',
        short_title: 'Required PV capacity [kWp]',
        more_info_html: '<div class="extended_description_html pv">The  size of the PV system (kWp) to be installed in each settlement is optimised according to the local solar radiation and the specific electricity load profile of the settlement<span class="reference">Source: Baldi, D. (2021), Nature journal paper: <a href="https://www.nature.com/articles/s41560-022-01006-9" target="_blank">Planning sustainable electricity solutions for refugee settlements in sub-Saharan Africa</a><br>The RSEA DB generated during this study is available in two file formats (csv and geopackage) following <a href="https://data.jrc.ec.europa.eu/dataset/4261bf3c-7e8e-4b16-925b-68cfd4eade37" target="_blank">this link</a></span></div>',
        _range: [4, 7, 10, 13],
        _domain: [51, 501, 1001, 1200]

    }



    ],

    params_symbology: [{
        title: 'Yearly avoided carbon emissions [t/yr]',
        long_title: 'Yearly avoided carbon emissions [t/yr]',
        orienation: 'vertical',
        param_name: 'year_co2_t',
        data_type: 'text',
        exact_value: false,

        legend: [{
            val: '≥5 <100',
            color: '#F2D6D3',
            text: 'Very small'
        }, {
            val: '≥100 <500',
            color: '#ECB193',
            text: 'Small'
        }, {
            val: '≥500 <1000',
            color: '#B14D75',
            text: 'Medium'
        }, {
            val: '≥1000',

            color: '#602F72',
            text: 'Large'
        }]
    },
    {
        title: 'LCOE [USD/kWh] - 10% discount rate',
        long_title: 'LCOE [USD/kWh] - 10% discount rate',
        orienation: 'vertical',
        param_name: 'lcoe_usdc_kwh_10',
        data_type: 'text',
        exact_value: false,

        legend: [{
            val: '≥20 <25',
            color: '#00bcd4',
            text: 'Very small'
        }, {
            val: '≥25 <30',
            color: '#40c06d',
            text: 'Small'
        }, {
            val: '≥30 <35',
            color: '#eee614',
            text: 'Medium'
        }, {
            val: '≥35',

            color: '#f44336',
            text: 'Large'
        }]
    },
    {
        title: 'Total project cost  [USD]',
        long_title: 'Total project cost  [USD]',
        orienation: 'vertical',
        param_name: 'tot_cost_usd',
        data_type: 'text',
        exact_value: false,

        legend: [{
            val: '≥55.000 < 1M',
            color: '#00bcd4',
            text: 'Very small'
        }, {
            val: '≥ 1M < 3M',
            color: '#40c06d',
            text: 'Small'
        }, {
            val: '≥3M < 5M',
            color: '#eee614',
            text: 'Medium'
        }, {
            val: '≥ 5M',

            color: '#f44336',
            text: 'Large'
        }]
    },

    {
        title: 'Population [# of refugees]',
        long_title: 'Population [# of refugees]',
        orienation: 'vertical',
        param_name: 'pop',
        data_type: 'text',
        exact_value: false,

        legend: [{
            val: '≥250 <5000',
            color: '#00bcd4',
            text: 'Very small'
        }, {
            val: '≥5.000 <10.000',
            color: '#40c06d',
            text: 'Small'
        }, {
            val: '≥10.000 <50.000',
            color: '#eee614',
            text: 'Medium'
        }, {
            val: '≥50.000',

            color: '#f44336',
            text: 'Large'
        }]
    }, {
        title: 'Battery energy capacity [kWh]',
        long_title: 'Battery energy capacity [kWh]',
        orienation: 'vertical',
        param_name: 'bat',
        data_type: 'text',
        exact_value: false,

        legend: [{
            val: '≥40 <500',
            color: '#FFA0BA',
            text: 'Very small'
        }, {
            val: '≥500 <1000',
            color: '#FB5A75',
            text: 'Small'
        }, {
            val: '≥1000 <5000',
            color: '#DA2A64',
            text: 'Medium'
        }, {
            val: '≥5000',

            color: '#6E096E',
            text: 'Large'
        }]
    },
    {
        title: 'Optimised size of PV (kWp) per daylight load profile with specific energy demand per facility',
        orienation: 'vertical',
        param_name: 'pv',
        data_type: 'string',
        stops: true,
        exact_value: false,
        legend: [{



            val: '≥10 <50',
            color: '#ff8c54',
            text: 'Very small'
        }, {
            val: '≥50 <500',
            color: '#F04F00',
            text: 'Small'
        }, {
            val: '≥500 <1000',
            color: '#f23f70',
            text: 'Medium'
        }, {
            val: '≥1000',

            color: '#b51641',
            text: 'Large'
        }]

    }
    ],
    mapbox_paint_style: [{
        param: 'year_co2_t',
        style: {

            "circle-opacity": 0.6,
            "circle-color": [
                "match", ["get", "year_co2_t_class"],
                [0],
                '#F2D6D3', [1],
                '#ECB193', [2],
                "#B14D75", [3],
                "#602F72",


                "#000000"
            ],
            "circle-stroke-color": "#e9e6ed",
            "circle-stroke-width": 1,
            "circle-radius": [
                "interpolate", ["linear"],
                ["get", "year_co2_t_class"],

                0,
                5,
                1,
                7,
                2,
                10,
                3,
                15

            ]
        }
    },
    {
        param: 'lcoe_usdc_kwh_10',
        style: {

            "circle-opacity": 0.6,
            "circle-color": [
                "match", ["get", "lcoe_usdc_kwh_10_class"],
                [0],
                '#00bcd4', [1],
                '#40c06d', [2],
                "#eee614", [3],
                "#f44336",


                "#000000"
            ],
            "circle-stroke-color": "#e9e6ed",
            "circle-stroke-width": 1,
            "circle-radius": [
                "interpolate", ["linear"],
                ["get", "lcoe_usdc_kwh_10_class"],

                0,
                5,
                1,
                7,
                2,
                10,
                3,
                15

            ]
        }
    },
    {
        param: 'tot_cost_usd',
        style: {

            "circle-opacity": 1,
            "circle-color": [
                "match", ["get", "tot_cost_usd_class"],
                [0],
                '#00bcd4', [1],
                '#40c06d', [2],
                "#eee614", [3],
                "#f44336",



                "#000000"
            ],
            "circle-stroke-color": "#e9e6ed",
            "circle-stroke-width": 0,
            "circle-radius": [
                "interpolate", ["linear"],
                ["get", "pv_class"],

                0, 2,
                1, 4.5,
                2, 8,
                3, 11,

            ]
        }
    },
    {
        param: 'pop',
        style: {

            "circle-opacity": 1,
            "circle-color": [
                "match", ["get", "pop_class"],
                [0],
                '#00bcd4', [1],
                '#40c06d', [2],
                "#eee614", [3],
                "#f44336",



                "#000000"
            ],
            "circle-stroke-color": "#e9e6ed",
            "circle-stroke-width": 0,
            "circle-radius": [
                "interpolate", ["linear"],
                ["get", "pop_class"],

                0, 4,
                1, 6.5,
                2, 9,
                3, 13

            ]
        }
    },
    {
        param: 'bat',
        style: {

            "circle-opacity": 0.5,
            "circle-color": [
                "match", ["get", "bat_class"],
                [0],
                '#FFA0BA', [1],
                '#FB5A75', [2],
                "#DA2A64", [3],
                "#6E096E",



                "#000000"
            ],
            "circle-stroke-color": "#e9e6ed",
            "circle-stroke-width": 1,
            "circle-radius": [
                "interpolate", ["linear"],
                ["get", "bat_class"],

                0, 4,
                1, 6.5,
                2, 9,
                3, 13

            ]
        }
    },
    {
        param: 'pv',
        style: {

            "circle-opacity": 0.6,
            "circle-color": [
                "match", ["get", "pv_class"],
                [0],
                '#ff8c54', [1],
                '#F04F00', [2],
                "#f23f70", [3],
                "#b51641",


                "#000000"
            ],
            "circle-stroke-color": "#e9e6ed",
            "circle-stroke-width": 1,
            "circle-radius": [
                "interpolate", ["linear"],
                ["get", "pv_class"],

                0, 3,
                1, 4.5,
                2, 8,
                3, 11,

            ]
        }
    }



    ]
});

app_data.layers_info.push({
    _layer: clustered_pop,
    _type: 'polygon',
    id: 'clustered_pop',
    title: 'Clustered population',
    default_param: 'population',
    activated_query: false,
    active_param: 'population',
    queryable: true,

    params_arr: ['population', 'lcoe_min'],

    active_filters: [
        {
            'param_filter': null
        },
        {
            'admin_filter': null
        }
    ],
    params_description: [{
        type_param: "categorical_index",
        param_name: 'population',
        long_title: 'population_class',
        long_title_txt: 'population_class',
        short_title: 'population_class'

    },

    {
        type_param: 'categorical_index',
        param_name: 'lcoe_min',
        long_title: 'lcoe_min_class',
        long_title_txt: 'lcoe_min_class',
        short_title: 'lcoe_min_class',

    }
    ],

    params_symbology: [{
        title: 'population_class',
        orienation: 'vertical',
        param_name: 'population',
        style_type: 'color',
        stops: false,
        symbol_type: 'polygon',
        data_type: 'text',
        exact_value: false,
        legend: [{
            val: 'Very Low//<1000',
            color: '#003f5c',
            text: 'Very low'
        }, {
            val: 'Low//1000-2000)',
            color: '#2f4b7c',
            text: 'Low'
        }, {
            val: 'Medium//2000-5000',
            color: '#665191',
            text: 'Medium'
        },
        {
            val: 'Medium High//5000-15000',
            color: '#d45087',
            text: 'Medium High'
        },
        {
            val: 'High//15000-100000',
            color: '#9d02eb',
            text: 'High'
        },
        {
            val: 'Very high//>100000',
            color: '#f0165e',
            text: 'Very high'
        },
        ]

    },
    {
        title: 'lcoe_min_class',
        orienation: 'vertical',
        param_name: 'lcoe_min',
        style_type: 'color',
        stops: false,
        data_type: 'string',
        exact_value: true,

        exact_value: false,
        legend: [{
            val: '<0.167',
            color: '#488f31',
            text: 'Very low (<0.167)'
        }, {
            val: '0.167-0.2',
            color: '#bad0af',
            text: 'Low (0.167-0.2)'
        }, {
            val: '0.2-0.3',
            color: '#f0b8b8',
            text: 'Medium (0.2-0.3)'
        },

        {
            val: '0.3-0.4',
            color: '#e67f83',
            text: 'High (0.3-0.4)'
        },
        {
            val: '0.4-0.587',
            color: '#de425b',
            text: 'Very high (0.4-0.587)'
        },
        ]
    }
    ],
    mapbox_paint_style: [{
        param: 'population',
        style: {


            "fill-color": [
                "match", ["get", "population_class"],
                [0],
                '#003f5c', [1],
                "#2f4b7c", [2],
                "#665191", [3],

                "#d45087", [4],
                "#9d02eb", [5],
                "#f0165e",
                "#000000"
            ],

        }
    },
    {
        param: 'lcoe_min',
        style: {


            "fill-color": [
                "match", ["get", "lcoe_min_class"],
                [0],
                '#488f31', [1],
                "#bad0af", [2],
                "#f0b8b8", [3],

                "#e67f83", [4],
                "#de425b",
                "#000000"
            ],

        }
    }
    ]
},

    {

        _layer: power_plants,
        _type: 'circle',
        id: 'power_plants',
        border: 'white',
        border_width: 1,
        scale_val_arr: [],

        default_param: "gen_type",
        active_filters: [{
            'basic': ["has", "gen_type"]
        },
        {
            'param_filter': null
        },
        {
            'admin_filter': null
        }
        ],
        title: 'Power plants',

        activated_query: false,
        active_param: 'gen_type',
        queryable: true,
        params_arr: ['gen_type', 'mw'],
        params_description: [{
            type_param: "categorical_index",


            param_name: 'gen_type',

            long_title: 'Generation',
            long_title_txt: 'Generation',
            short_title: 'Generation'

        },

        {
            type_param: 'range',
            param_name: 'mw',
            long_title: 'Installed capacity [MW]',
            long_title_txt: 'Installed capacity [MW]',
            short_title: 'Installed capacity [MW]',
            _range: [4, 4, 6, 8, 10, 13],
            _domain: [1, 11, 101, 1001, 1600]
        }
        ],


        params_symbology: [{
            title: 'gen_type_class',
            orienation: 'vertical',
            param_name: 'gen_type',
            using_class: true,
            style_type: 'color',
            stops: false,
            data_type: 'string',
            exact_value: true,

            legend: p_plants_gen_type
        },
        {
            title: 'Installed capacity (MW)',
            orienation: 'vertical',
            param_name: 'mw',
            style_type: 'color',
            stops: true,
            data_type: 'string',

            exact_value: false,
            legend: [{
                val: 'No data',
                color: '#023612',
                text: 'No data'
            }, {
                val: '≤1',
                color: '#dd1339',
                text: 'Very low'
            }, {
                val: '>1 ≤ 10',
                color: '#0e9bed',
                text: 'Low'
            }, {
                val: '>10 ≤100',
                color: '#fcad03',
                text: 'Medium'
            },
            {
                val: '>100 ≤1000',
                color: '#f207bf',
                text: 'High'
            },
            {
                val: '> 1000',
                color: '#9d02eb',
                text: 'Very high'
            }
            ]
        }
        ],
        mapbox_paint_style: [{
            param: 'mw',
            style: {


                "circle-color": [
                    "match", ["get", "mw_class"],
                    [0],
                    '#023612', [1],
                    "#dd1339", [2],
                    "#0e9bed", [3],

                    "#fcad03", [4],
                    "#f207bf", [5],
                    "#9d02eb",
                    "#000000"
                ],
                "circle-stroke-color": "hsl(0, 8%, 93%)",
                "circle-stroke-width": 1,
                "circle-radius": [
                    "interpolate", ["linear"],
                    ["get", "mw_class"],
                    0,
                    4,
                    6,
                    13,
                    17,
                    32


                ],
            }
        },

        {
            param: 'gen_type',
            style: {
                "circle-stroke-color": "hsl(0, 2%, 95%)",
                "circle-stroke-width": 2,
                "circle-translate": [0, 0],
                "circle-color": p_plants_gen_type.reduce(function (memo, data, i) {

                    if (i !== p_plants_gen_type.length - 1)

                        memo.push(i, data.color)
                    else
                        memo.push('red')

                    return memo

                }, ["match", ["get", "gen_type_class"]]),
                "circle-radius": [
                    "interpolate", ["exponential", 1],
                    ["zoom"],
                    3, 5,
                    7, 7,
                    13, 11
                ]
            }
        }
        ]
    });

let taf_hydrogen_grouped = {
    'id': 'taf_hydrogen_grouped',
    'type': 'circle',
    'source': 'taf_hydrogen_grouped_source',
    'source-layer': 'taf_hydrogen_grouped',
    "maxzoom": 11.5, // Set zoom level to whatever suits your needs
    'paint': {
        'circle-opacity': .7,
        'circle-color': "#4caf50",
        "circle-stroke-color": "orange",
        "circle-stroke-width": 2,
        "circle-translate": [0, 20],
        "circle-radius": [
            "interpolate", ["linear"],
            ["get", "count"],

            3,
            12,
            13,
            20
        ],

    }
}


var taf_hydrogen_grouped_label = {
    'id': 'taf_hydrogen_grouped_label',
    'type': 'symbol',
    'source': 'taf_hydrogen_grouped_source',
    'source-layer': 'taf_hydrogen_grouped',
    "maxzoom": 11.5, // Set zoom level to whatever suits your needs
    'layout': {
        "text-field": '{count}\n',
        "text-allow-overlap": false,
        "text-transform": "uppercase",
        "text-size":

            [
                "interpolate", ["linear"],
                ["zoom"],

                3,
                11,
                13,
                17
            ],

        'symbol-placement': "point",
        'text-letter-spacing': 0.2

    },
    "paint": {
        "text-color": 'white',
        "text-translate": [0, 20]

    }
}


app_data.layers_info.push(
    {
        _layer: taf_hydrogen_grouped,
        title: 'Hydrogen',
        id: 'taf_hydrogen_grouped',
        _type: 'circle',
        not_styled: true,
        queryable: true,
        activated_query: false,
        active_filters: [{
            'basic': null
        },
        {
            'param_filter': null
        },
        {
            'admin_filter': null
        }
        ],
        title: 'Hydrogen',
        mapbox_paint_style: [
            {
                'circle-opacity': .6,
                'circle-color': "gray",
                "circle-stroke-color": "orange",
                "circle-stroke-width": 2,

                "circle-radius": [
                    "interpolate", ["linear"],
                    ["get", "count"],

                    1,
                    20,
                    10,
                    40
                ],

            }
        ]
    });


var taf_geotermal_grouped = {
    'id': 'taf_geotermal_grouped',
    'type': 'circle',
    'source': 'taf_geotermal_grouped_source',
    'source-layer': 'taf_geotermal_grouped',
    "maxzoom": 7, // Set zoom level to whatever suits your needs
    'paint': {
        'circle-opacity': .7,
        'circle-color': '#ff0054',
        "circle-stroke-color": "black",
        "circle-stroke-width": .7,

        "circle-radius": [
            "interpolate", ["linear"],
            ["get", "count"],

            1,
            10,
            20,
            30
        ],

    }
}

app_data.layers_info.push(
    {
        _layer: taf_geotermal_grouped,
        title: 'Geothermal',
        id: 'taf_geotermal_grouped',
        _type: 'circle',
        not_styled: true,
        queryable: true,
        activated_query: false,
        active_filters: [{
            'basic': null
        },
        {
            'param_filter': null
        },
        {
            'admin_filter': null
        }
        ],
        mapbox_paint_style: [

        ]
    });


var taf_geotermal_grouped_label = {
    'id': 'taf_geotermal_grouped_label',
    'type': 'symbol',
    'source': 'taf_geotermal_grouped_source',
    'source-layer': 'taf_geotermal_grouped',
    "maxzoom": 11.2, // Set zoom level to whatever suits your needs
    'layout': {
        "text-field": '{count}\n',
        "text-allow-overlap": false,
        "text-transform": "uppercase",
        "text-size":

            [
                "interpolate", ["linear"],
                ["zoom"],

                1, 12,
                13, 20
            ],

        'symbol-placement': "point",
        'text-letter-spacing': 0.2

    },
    "paint": {
        "text-color": 'white',

    }
}

var taf_bess = {
    "id": "taf_bess",
    "default_param": "category",
    "type": 'circle',
    "source": "taf_bess_source",

    'source-layer': 'taf_bess',
    'layout': {
        'visibility': 'visible'

    }


};
app_data.layers_info.push(
    {
        _layer: taf_bess,
        id: 'taf_bess',
        title: 'Battery Energy Storage Systems',
        _type: 'circle',
        border_width: 1.5,
        border: '#FF006E',
        circle_radius: ["interpolate", ["exponential", 1],
            ["zoom"],
            2, 9,
            7, 13,
            13, 18],

        queryable: true,
        activated_query: false,
        html_legend: true,
        active_filters: [{
            'basic': null
        },
        {
            'param_filter': null
        },
        {
            'admin_filter': null
        }
        ],
        default_param: 'category',
        active_param: 'category',
        title: 'Battery Energy Storage Systems',



        params_arr: ['power_pool', 'category'],
        params_description: [{
            param_name: 'power_pool',
            type_param: 'categorical_index',
            long_title: 'Power pool',
            long_title_txt: 'Power pool',
            short_title: 'Power pool',
            more_info_html: 'Power pool'

        },
        {
            param_name: 'category',
            type_param: 'categorical_index',
            long_title: 'Category',
            long_title_txt: 'Category',
            short_title: 'Category',
            short_title: 'Category',
            more_info_html: 'Category',


        }
        ],
        extra_popup_params: [
            {
                name: 'country',
                title: 'Country'
            },
            {
                name: 'name',
                title: 'Name'
            }],
        params_symbology: [
            {
                using_class: true,
                symbol_type: 'circle',
                title: 'Type of Power pool',
                orienation: 'vertical',
                param_name: 'power_pool',
                style_type: 'color',
                orienation: 'vertical',
                stops: false,
                data_type: 'string',
                exact_value: true,
                legend: [

                    {
                        val: 'OTHER',
                        color: '#FFBE0B'
                    }, {
                        val: "OTHER (Off-Grid, Island Nations, and Behind-the-Meter)",
                        color: '#FB5607'
                    }, {
                        val: "COMELEC",
                        color: '#FF006E'
                    },
                    {
                        val: "EAPP",
                        color: '#8338EC'
                    },
                    {
                        val: "WAPP",
                        color: '#3A86FF'
                    }, {
                        val: "CAPP",
                        color: '#d62828'
                    },
                    {
                        val: "SAPP",
                        color: '#2a9d8f'
                    }
                ]
            },
            {
                using_class: true,
                symbol_type: 'circle',
                title: 'Category',
                orienation: 'vertical',
                param_name: 'category',
                style_type: 'color',
                orienation: 'vertical',
                stops: false,
                data_type: 'string',
                exact_value: true,
                legend: [

                    {
                        val: 'Planned large-scale grid-connected BESS projects in Africa',
                        color: '#0b5de0'
                    }, {
                        val: "OFF-GRID EXISTING & PLANNED BESS PROJECTS OTHER (Off-Grid, Island Nations, and Behind-the-Meter)",
                        color: '#058505'
                    }, {
                        val: "ADDITIONAL EXISTING & PLANNED BESS PROJECTS",
                        color: '#d4c308'
                    }

                ]
            },
        ],
        mapbox_paint_style: [


        ]


    })
var taf_geotermal_ungrouped = {
    "id": "taf_geotermal_ungrouped",
    "default_param": "site_status",
    "type": 'circle',
    "source": "taf_geotermal_ungrouped_source",
    "minzoom": 7,
    'source-layer': 'taf_geotermal_ungrouped',
    'layout': {
        'visibility': 'visible'

    }


};
app_data.layers_info.push(
    {
        _layer: taf_geotermal_ungrouped,
        title: 'Geothermal',
        id: 'taf_geotermal_ungrouped',
        _type: 'circle',
        border_width: .5,
        border: 'white',
        queryable: true,
        activated_query: false,
        active_filters: [{
            'basic': null
        },
        {
            'param_filter': null
        },
        {
            'admin_filter': null
        }
        ],
        default_param: 'site_status',
        active_param: 'site_status',
        title: 'Geothermal',
        extra_popup_params: [
            {
                name: 'country',
                title: 'Country'
            },
            {
                name: 'gpp_status',
                title: 'GPP status'
            },

            {
                name: 'name',
                title: 'Name'
            },

            {
                name: 'comm_date',
                title: 'GPP comissioning date'
            },
            {
                name: 'decomm_date',
                title: 'GPP decomissioning date'
            },

            {
                name: 'gr_br',
                title: 'GREENFIELD/BROWNFIELD'
            },
            {
                name: 'type',
                title: 'GPP type'
            },
            {
                name: 'capex_mw',
                title: 'Estimated CAPEX USD/MW'
            }


        ],

        params_arr: ['site_status', 'cap_mw'],
        params_description: [{
            param_name: 'site_status',
            type_param: 'categorical_index',
            long_title: 'Site Status',
            long_title_txt: 'Site Status',
            short_title: 'Site Status',
            more_info_html: 'Site Status',

        },
        {
            param_name: 'cap_mw',
            type_param: 'range',
            long_title: 'GPP capacity [MW]',
            long_title_txt: 'GPP capacity [MW]',
            short_title: 'GPP capacity [MW]',
            short_title: 'GPP capacity [MW]',
            more_info_html: 'GPP capacity [MW]',
            _range: [4, 7, 10, 16],
            _domain: [20, 50, 100, 200]

        }
        ],
        params_symbology: [{
            using_class: false,
            symbol_type: 'circle',
            title: 'Site Status',
            orienation: 'vertical',
            param_name: 'site_status',
            style_type: 'color',
            orienation: 'vertical',
            stops: false,
            data_type: 'string',
            exact_value: true,
            legend: [
                {
                    val: 'Under development',
                    color: '#ff9800'
                }, {
                    val: "In Production",
                    color: '#26a69a'
                }, {
                    val: "Surface studies",
                    color: '#e617bc'
                },
                {
                    val: "Exploration & Appraisal Drilling",
                    color: '#b00020'
                }
            ]
        },
        {
            using_class: true,
            symbol_type: 'circle',
            title: 'Power capacity [MW]',
            orienation: 'vertical',
            param_name: 'cap_mw',
            style_type: 'color',
            orienation: 'vertical',
            stops: false,
            data_type: 'number',
            exact_value: true,
            legend: [
                {
                    val: '<20 MW',
                    color: '#ffbe0b'
                }, {
                    val: "≥20 - <50 MW",
                    color: '#fb5607'
                }, {
                    val: "≥50 - <100 MW",
                    color: '#ff006e'
                },
                {
                    val: "≥100 - <150 MW",
                    color: '#8338ec'
                },
                {
                    val: "≥150 MW",
                    color: '#3a86ff'
                }
            ]
        }
        ],
        mapbox_paint_style: [
            {
                param: 'cap_mw',
                style: {


                    "circle-color": [
                        "match", ["get", "cap_mw_class"],
                        [0], '#ffbe0b',
                        [1], "#fb5607",
                        [2], "#ff006e",
                        [3], "#8338ec",
                        [4], "#3a86ff",
                        "#b00020"
                    ],
                    "circle-stroke-color": "hsl(0, 8%, 93%)",
                    "circle-stroke-width": 1,
                    "circle-radius": [
                        "interpolate", ["linear"],
                        ["get", "cap_mw_class"],
                        0, 3,
                        1, 5,
                        2, 9,
                        3, 12,
                        4, 17




                    ],
                }
            },

        ]


    })

console.log(app_data.layers_info)
var taf_solar = {
    "id": "taf_solar",
    "default_param": "capacity_m",

    "type": 'circle',
    "source": "taf_solar_source",

    'source-layer': 'taf_solar',
    'layout': {
        'visibility': 'visible'

    }
};

app_data.layers_info.push({
    _layer: taf_solar,
    title: 'Solar',
    id: 'taf_solar',
    _type: 'circle',

    border_width: {
        base: 1, // default value
        stops: [
            [5, 0],
            [5.1, .8],
            [16, 1.5],
        ],
    },

    circle_opacity: {
        base: 2, // default value
        stops: [
            [3, 1],
            [5, 1]

        ],
    },
    border: '#d779c0',
    border_width: 1.5,
    queryable: true,
    activated_query: false,
    active_filters: [{
        'basic': null
    },
    {
        'param_filter': null
    },
    {
        'admin_filter': null
    }
    ],
    default_param: 'capacity',
    active_param: 'capacity',
    title: 'Solar',
    params_arr: ['capacity', 'technology', 'status'],
    extra_popup_params: [
        {
            name: 'country',
            title: 'Country'
        },
        {
            name: 'name',
            title: 'Project name'
        },
        {
            name: 'url',
            title: 'Wiki URL'
        },

    ],
    params_description: [{
        param_name: 'capacity',
        type_param: 'categorical_index',
        long_title: 'Capacity in [MW]',
        long_title_txt: 'Capacity in [MW]',
        short_title: 'Capacity in [MW]',

    },
    {
        param_name: 'technology',
        type_param: 'categorical_index',
        long_title: 'Technology type',
        long_title_txt: 'Technology type',
        short_title: 'Technology type',

    },
    {
        param_name: 'status',
        type_param: 'categorical_index',
        long_title: 'Status',
        long_title_txt: 'Status',
        short_title: 'Status',

    }
    ],
    params_symbology: [{
        using_class: true,
        symbol_type: 'circle',
        title: 'Capacity in [MW]',
        orienation: 'vertical',
        param_name: 'capacity',
        style_type: 'color',
        orienation: 'vertical',
        stops: false,
        data_type: 'number',
        exact_value: true,
        legend: [
            {
                val: '≥1 <20',
                color: '#8aff02'
            }, {
                val: '≥20 <50',
                color: '#3A86FF'
            }, {
                val: '≥50 <100',
                color: '#8338EC'
            },
            {
                val: '≥100 <500',
                color: '#FF006E'
            },
            {
                val: '≥500 <1000',
                color: '#FB5607'
            },
            {
                val: '>1000',
                color: '#FFBE0B'
            },
        ]
    },
    {

        using_class: true,
        symbol_type: 'circle',
        title: 'Technology type',
        orienation: 'vertical',
        param_name: 'technology',
        style_type: 'color',
        orienation: 'vertical',
        stops: false,
        data_type: 'number',
        exact_value: true,
        legend: [

            {
                val: 'Solar Thermal',
                color: '#3a86ff'
            }, {
                val: 'PV',
                color: '#ff5400'
            }, {
                val: 'Assumed PV',
                color: '#9e0059'
            },
        ]
    },
    {

        using_class: true,
        symbol_type: 'circle',
        title: 'Status',
        orienation: 'vertical',
        param_name: 'status',
        style_type: 'color',
        orienation: 'vertical',
        stops: false,
        data_type: 'number',
        exact_value: true,
        legend: [

            {
                val: 'Announced',
                color: '#BAC1B8'
            }, {
                val: 'Pre-construction',
                color: '#58A4B0'
            }, {
                val: 'Construction',
                color: '#0C7C59'
            },
            {
                val: 'Operating',
                color: '#2B303A'
            },
        ]
    }

    ],
    mapbox_paint_style: []


})

var taf_wind = {
    "id": "taf_wind",
    "default_param": "iec_group",

    "type": 'circle',
    "source": "taf_wind_source",

    'source-layer': 'taf_wind',
    'layout': {
        'visibility': 'visible'

    }
};


app_data.layers_info.push({
    _layer: taf_wind,
    title: 'Wind',
    id: 'taf_wind',
    _type: 'circle',
    border_width: {
        base: 1, // default value
        stops: [
            [5, .6],
            [5.1, .8],
            [16, 3.5],
        ],
    },
    circle_opacity: 1,
    circle_radius: [
        "interpolate", ["linear", 1],
        ["zoom"],
        3, 3,
        7, 6,
        13, 10,
        19, 15
    ],
    border: 'white',
    queryable: true,
    activated_query: false,
    active_filters: [{
        'basic': null
    },
    {
        'param_filter': null
    },
    {
        'admin_filter': null
    }
    ],
    default_param: 'iec_group',
    active_param: 'iec_group',
    title: 'Wind',
    params_arr: ['iec_group', 'capacity_m'],
    extra_popup_params: [
        {
            name: 'country_na',
            title: 'Country'
        },
        {
            name: 'msr_id',
            title: 'MSR ID'
        },
        {
            name: 'area_km2',
            title: 'Area (km2)'
        },
        {
            name: 'cf_100m',
            title: 'Annual average capacity factor (corrected by Wind SSS)'
        }
    ],
    params_description: [{
        param_name: 'iec_group',
        type_param: 'categorical_index',
        long_title: 'Wind turbine class',
        long_title_txt: 'Wind turbine class',
        short_title: 'Wind turbine class',

    },
    {
        param_name: 'capacity_m',
        type_param: 'categorical_index',
        long_title: 'Wind site power capacity',
        long_title_txt: 'Wind site power capacity',
        short_title: 'Wind site power capacity',

    }
    ],
    params_symbology: [{
        using_class: true,
        symbol_type: 'circle',
        title: 'Type of iec_group',
        orienation: 'vertical',
        param_name: 'iec_group',
        style_type: 'color',
        orienation: 'vertical',
        stops: false,
        data_type: 'number',
        exact_value: true,
        legend: [
            {
                val: 'Class 1',
                color: '#3a86ff'
            }, {
                val: 'Class 2',
                color: '#ff5400'
            }, {
                val: 'Class 3',
                color: '#9e0059'
            },
        ]
    },
    {

        using_class: true,
        symbol_type: 'circle',
        title: 'Wind site power capacity [MW]',
        orienation: 'vertical',
        param_name: 'capacity_m',
        style_type: 'color',
        orienation: 'vertical',
        stops: false,
        data_type: 'number',
        exact_value: true,
        legend: [

            {
                val: '≥30',
                color: '#ffbe0b',
                text: 'Very low'
            }, {
                val: '30≥100',
                color: '#718205',
                text: 'Low'
            }, {
                val: '100 ≥500',
                color: '#ff006e',
                text: 'Medium',
            },
            {
                val: '500 ≥1000',
                color: '#8338ec',
                text: 'High'
            },
            {
                val: '1000-2700',
                color: '#3a86ff',
                text: 'Very high',
            },
        ]
    }
    ],
    mapbox_paint_style: []


})


var taf_pumps_up = {
    "id": "taf_pumps_up",
    "default_param": "power_storage",

    "type": 'circle',
    "source": "taf_pumps_up_source",

    'source-layer': 'taf_pumps_up',
    'layout': {
        'visibility': 'visible'

    }
};


app_data.layers_info.push({
    _layer: taf_pumps_up,
    title: 'Hydro pumped storage',

    id: 'taf_pumps_up',
    _type: 'circle',
    border: '#ffb703',
    circle_radius:
        ["interpolate", ["exponential", 1],
            ["zoom"],
            2, 7,
            7, 11,
            13, 16],
    queryable: true,
    activated_query: false,
    active_filters: [{
        'basic': null
    },
    {
        'param_filter': null
    },
    {
        'admin_filter': null
    }
    ],
    default_param: 'power_storage',
    active_param: 'power_storage',
    title: 'Hydro pumped storage',
    params_arr: ['power_storage', 'power'],
    params_description: [{
        param_name: 'power_storage',
        type_param: 'categorical_index',
        long_title: 'Storage Energy in [GWh]',
        long_title_txt: 'Storage Energy in [GWh]',
        short_title: 'Storage Energy in [GWh]',
        more_info_html: 'Storage Energy in [GWh]',

    },
    {
        param_name: 'power',
        type_param: 'categorical_index',
        long_title: 'Power in [GW]',
        long_title_txt: 'Power in [GW]',
        short_title: 'Power in [GW]',
        more_info_html: 'Power in [GW]',

    }

    ],
    extra_popup_params: [
        {
            name: 'country',
            title: 'Country'
        },
        {
            name: 'adm_name',
            title: 'Administraton name'
        },
        {
            name: 'site_no',
            title: 'Power pool'
        },
        {
            name: 'head_max',
            title: 'Head (max) in [m]'
        },
        {
            name: 'penstock_l',
            title: 'Length of the penstock in [m]'
        },
        {
            name: 'up_elevati',
            title: 'Upper reservoir elevation in [m]'
        },
        {
            name: 'lower_elev',
            title: 'Lower reservoir elevation in [m]'
        },
        {
            name: 'up_area',
            title: 'Upper reservoir area in [ha]'
        },
        {
            name: 'low_area',
            title: 'Lower reservoir area in [ha]'
        },
        {
            name: 'euros_kw',
            title: 'Estimated CAPEX in [Euro/kW]'
        }


    ],
    params_symbology: [{
        using_class: true,
        symbol_type: 'circle',
        title: 'Storage Energy in [GWh]',
        orienation: 'vertical',
        param_name: 'power_storage',
        style_type: 'color',
        orienation: 'vertical',
        stops: false,
        data_type: 'number',
        exact_value: true,

        legend: [
            {
                val: '≥2 <5',
                color: '#219ebc'
            }, {
                val: '≥5 <15',
                color: '#fcbf49'
            }, {
                val: '≥15 ⋜52',
                color: '#d62828'
            },
        ]
    },
    {
        using_class: true,
        symbol_type: 'circle',
        title: 'Power in [GW]',
        orienation: 'vertical',
        param_name: 'power',
        style_type: 'color',
        orienation: 'vertical',
        stops: false,
        data_type: 'number',
        exact_value: true,

        legend: [
            {
                val: '≥0.3 <2',
                color: '#219ebc'
            }, {
                val: '≥2 <5',
                color: '#fcbf49'
            }, {
                val: '≥5 ⋜9',
                color: '#d62828'
            },
        ]
    }

    ],
    mapbox_paint_style: []
});


var taf_pumps_down = {
    "id": "taf_pumps_down",
    "default_param": "power_storage",

    "type": 'circle',
    "source": "taf_pumps_down_source",

    'source-layer': 'taf_pumps_down',
    'layout': {
        'visibility': 'visible'

    },
    paint: {
        'circle-color': '#0f44bf',
        "circle-radius":

            [
                "interpolate", ["exponential", 1],
                ["zoom"],
                3, 4,
                7, 8,
                13, 12

            ],
        'circle-blur': 0,

        "circle-stroke-color": 'white',

        "circle-stroke-width":
        {
            base: 2, // default value
            stops: [
                [5, 1.5],
                [7, 1.8],
                [16, 3.5],
            ],
        },
        "circle-opacity": 1

    }

};

app_data.layers_info.push({
    _layer: taf_pumps_down,
    id: 'taf_pumps_down',
    title: 'Hydro pumped storage',
    border: '#ffb703',
    _type: 'circle',
    circle_radius:
        ["interpolate", ["exponential", 1],
            ["zoom"],
            2, 4,
            7, 8,
            13, 12],

    queryable: true,
    activated_query: false,
    active_filters: [{
        'basic': null
    },
    {
        'param_filter': null
    },
    {
        'admin_filter': null
    }
    ],
    default_param: 'power_storage',
    active_param: 'power_storage',
    'title': 'Pumps down test',
    params_arr: ['power_storage', 'power'],
    params_description: [{
        param_name: 'power_storage',
        type_param: 'categorical_index',
        long_title: 'power_storage',
        long_title_txt: 'power_storage longtitle',
        short_title: 'power_storage short title',
        more_info_html: 'power_storage more info',

    }

    ],
    mapbox_paint_style: []
});
console.info(app_data.layers_info);


var taf_pumps_lines = {
    "id": "taf_pumps_lines",
    "type": "line",
    "source": "taf_pumps_lines_source",
    "source-layer": "taf_pumps_lines",
    'layout': {
        'visibility': 'visible'
    },
    "paint": {
        "line-color": "#219ebc",
        "line-width": [
            "interpolate", ["linear"],
            ["zoom"],
            3,
            0.5,
            5,
            0.8,
            7,
            1.2,
            9,
            1.8
        ],
        "line-dasharray": [2, 0.5],
        "line-opacity": 1
    }

}

app_data.layers_info.push({
    _layer: taf_pumps_lines,
    title: 'Hydro pumped storage',
    id: 'taf_pumps_lines',
    _type: 'line',
    queryable: false,
    activated_query: false,
    default_param: 'none',
    active_param: 'none',
    active_filters: [{
        'basic': null
    },
    {
        'param_filter': null
    },
    {
        'admin_filter': null
    }
    ],
})

app_data.layers_info.filter((d) => {
    console.log(d)
    return d.id && d.id.indexOf('taf_') > -1 && d._type == 'circle' && !d.not_styled
}).forEach((t_layer, i) => {


    console.warn(t_layer)
    if (t_layer.params_symbology) {
        t_layer.params_symbology.forEach((symbology, i) => {


            if (t_layer.id !== 'taf_pumps_down') {
                if (symbology.using_class === false) {
                    var expr = ["match", ["get", symbology.param_name]];
                    symbology.legend.forEach((d, i) => {
                        expr.push(d.val);
                        expr.push(d.color);
                    });
                }
                else {
                    var expr = ["match", ["get", symbology.param_name + '_class']];
                    symbology.legend.forEach((d, i) => {
                        expr.push(i);
                        expr.push(d.color);
                    });
                }

                expr.push("hsl(0, 85%, 50%)");
            }


            var style = {
                param: symbology.param_name,
                style: {
                    'circle-color': t_layer.id !== 'taf_pumps_down' ? expr : '#0f44bf',
                    "circle-radius": t_layer.circle_radius ? t_layer.circle_radius :

                        [
                            "interpolate", ["exponential", 1],
                            ["zoom"],
                            3, 6,
                            7, 10,
                            13, 13

                        ],
                    'circle-blur': t_layer.blur ? t_layer.blur : 0,
                    "circle-stroke-color": t_layer.border ? t_layer.border : 'white',
                    "circle-stroke-width": t_layer.border_width ? t_layer.border_width :
                        {
                            base: 2, // default value
                            stops: [
                                [5, 1.5],
                                [7, 1.8],
                                [16, 3.5],
                            ],
                        },
                    "circle-opacity": t_layer.circle_opacity ? t_layer.circle_opacity : 1,

                }
            }
            t_layer.mapbox_paint_style.push(style);





        })
    }
    else {


    }

});


console.warn(app_data.layers_info.filter(d => d.id && d.id.indexOf('taf_') > -1))

var hospitals_sel = {
    "id": "hospitals_sel",
    "default_param": "category_l",

    "type": 'circle',
    "source": "hospitals_source",

    'source-layer': 'health_centres_updated',
    'layout': {
        'visibility': 'none'

    },


    "paint": {
        "circle-radius": 10,
        "circle-color": 'white',
        "circle-stroke-color": "black"
    },


}

app_data.layers_info.push({
    _layer: hospitals_sel,
    id: 'hospitals_sel',
    _type: 'sel_circle',

})

var hospitals = {
    "id": "hospitals",
    "default_param": "category_l",
    "type": 'circle',
    "source": "hospitals_source",
    'source-layer': 'health_centres_updated',
    'layout': {
        'visibility': 'visible'

    },


    "paint": {

        "circle-radius": [
            "interpolate", ["linear", 1],
            ["zoom"],
            3, 1,
            7, 4,
            13, 10,
            19, 15
        ],
        "circle-stroke-color": "#fff",

        "circle-stroke-width": [
            "interpolate", ["exponential", 1],
            ["zoom"],
            5,
            0,
            6, 0.3,
            10, .6,

            16, 1.5
        ],
        "circle-opacity": [
            "match", ["get", "nea"],
            ["yes"], 1, ["no"], 0.4,
            0

        ],
        "circle-stroke-opacity": [
            "match", ["get", "nea"],
            ["yes"], 1, ["no"], 0.6,
            0

        ],


        "circle-color": [
            "match", ["get", "category_l_class"],
            0, "#1e7dc9",
            1, "#a13131",
            2, "#8bc34a",
            3, "#cbb70f",
            "#eee"
        ]


    },

    filter: [
        "all", ["in", "nea", 'yes']
    ]
};




app_data.layers_info.push({
    _layer: hospitals,
    title: 'Health facilities',
    id: 'hospitals',
    _type: 'circle',
    border: 'black',
    border_width: .6,
    queryable: true,
    activated_query: false,
    active_filters: [{
        'basic': ["in", "nea", 'yes']
    },
    {
        'param_filter': null
    },
    {
        'admin_filter': null
    }
    ],

    default_param: 'category_l',
    active_param: 'category_l',
    'title': 'Health facilities',
    params_arr: ['category_l', 'eldemand', 'lcoeh_soft', 'pv',
        'bat', 'capex_soft'
    ],
    params_description: [{
        param_name: 'category_l',
        type_param: 'categorical_index',
        long_title: 'Type of health facilities according to WHO (primary, first, secondary, tertiary)<span class="more category_l">more info...</span>',
        long_title_txt: 'Type of health facilities according to WHO (primary, first, secondary, tertiary)',
        short_title: 'Type of health facility',
        more_info_html: '<div class="extended_description_html category_l"><table class="striped responsive-tablexxx"><thead><tr><th>Facility level</th><th>Coverage</th><th>Description</th></tr></thead><tbody><tr><td>Tertiary referral hospital</td><td>>120 beds</td><td>High-energy requirements. May contain sophisticated diagnostic devices requiring additional power and perform surgical procedures</td></tr><tr><td>Secondary hospital</td><td>60-120 beds</td><td>Moderate energy requirements. May accommodate sophisticated diagnostic medical equipment</td></tr><tr><td>First hospital</td><td>30-60 beds</td><td>Low/moderate energy requirements.Lighting during evening hours and maintaining the cold chain for vaccines, blood, and other medical supplies</td></tr><tr><td>Primary health post</td><td>No beds other than for emergencies/ maternity care</td><td>Low energy requirements. Typically located in a remote setting with limited services and a small staff. Typically operates weekdays</td></tr></tbody></table><div class="table_references"><span style="font-weight:700;font-size:.9rem">Sources:</span> <span>USAID. Powering Health, Electrification Options for Rural Health Centers. Washington, DC: 2006</span> <span>Africa Renewable Energy Access Program (AFREA). Photovoltaics for Community Service Facilities. Washington DC, US: 2010. doi:10.1596/27575</span>' +
            '<span style="display:block;margin-top:8px">For further description of the methodology used on the electrification of health centres, see <a target="_blank" href="https://www.cell.com/joule/pdf/S2542-4351(21)00438-4.pdf">the following publication</a></span>' +
            '<span style="display:block;margin-top:8px">Check also the <a target="_blank" href="https://data.jrc.ec.europa.eu/dataset/7c9e054b-bdbe-46a6-b8d1-b6eda2414a0a">complete dataset</a></span></div></div>'

    }, {

        param_name: 'eldemand',
        type_param: 'categorical_index',
        long_title: 'Annual electricity demand per facility per year [kWh/year]<span class="more eldemand">more info...</span>',
        long_title_txt: 'Annual electricity demand per facility per year [kWh/year]',
        short_title: 'Electricity demand [kWh/year]',
        _range: [3, 5, 7, 11],
        _domain: [1825, 7300, 14600, 91250],
        more_info_html: '<div class="extended_description_html eldemand"> <table class="striped responsive-tablexxx"> <thead> <tr> <th>Facility level</th> <th>Electricity requirements</th> </tr> </thead> <tbody> <tr> <td>Tertiary</td> <td>High energy demand, 30 kWh/day</td> </tr> <tr> <td>Secondary</td> <td>Moderate energy demand, 20 kWh/day</td> </tr> <tr> <td>First</td> <td>Low energy demand, 10 kWh/day </td> </tr> <tr> <td>Primary</td> <td>Low energy demand, 5 kWh/day </td> </tr> </tbody> </table> <div class="table_references"> <span style="font-weight: bold;font-size:0.9rem">Sources:</span> <span>USAID. Powering Health, Electrification Options for Rural Health Centers. Washington, DC: 2006</span> <span>Africa Renewable Energy Access Program (AFREA). Photovoltaics for Community Service Facilities. Washington DC, US: 2010. doi:10.1596/27575</span> </div> </div>'
    },
    {
        param_name: 'lcoeh_soft',
        type_param: 'categorical_index',
        long_title: 'Levelised cost of electricity per hospital [EUR/kWh]<span class="more lcoeh_soft">more info...</span>',
        long_title_txt: 'Levelised cost of electricity per hospital [EUR/kWh]',
        short_title: 'LCOE [EUR/kWh]',
        _range: [4, 5, 7, 8, 9, 12],
        _domain: [0, 0.25, 0.35, 0.45, 0.67],
        more_info_html: '<div class="extended_description_html lcoeh_soft">LCOE of electricity generated by a PV system including storage. The LCOE is calculated per location, giving 24-hour electricity service and including replacement costs, lifetime 20 years, storage by Li battery and energy demand load curve on daylight consumption<span style="display:block" class="table_references">Source: M. Moner-Girona, G. Kakoulaki, N. Taylor. “Quick respond to electricity needs for health facilities in Sub-Saharan Africa”  (in process of publication)</span></div>'




    },
    {
        param_name: 'pv',
        type_param: 'categorical_index',
        long_title: 'PV capacity to install [kWp]<span class="more pv">more info...</span>',
        long_title_txt: 'PV capacity to install [kWp]',
        short_title: 'Required PV capacity [kWp]',
        _range: [2, 4, 5, 7, 10],
        _domain: [0, 2, 5, 25, 180],
        more_info_html: '<div class="extended_description_html pv">The size of the PV system (kWp) to install in each health centre is optimised according to the local solar radiation and specific energy demand load profile<span style="display:block" class="table_references"><span>Sources</span><span>T.Huld, M. Moner Girona, A. Kriston, “Geospatial Analysis of Photovoltaic Mini Grid System Performance ”, Energies, Feb 2017 </span><span>M. Moner-Girona, G. Kakoulaki, N. Taylor. <a style="color:bold" href="https://www.cell.com/joule/pdf/S2542-4351(21)00438-4.pdf" target="_blank">“Achieving universal electrification of rural healthcare facilities in sub-Saharan Africa with decentralized renewable energy technologies”</a> </span></span></div>'

    },
    {
        param_name: 'bat',
        type_param: 'categorical_index',
        long_title: 'Battery size to install [kWh]<span class="more bat">more info...</span>',
        long_title_txt: 'Battery size to install [kWh]',
        short_title: 'Required battery capacity [kWh]',
        _range: [3, 4, 5, 7, 11],
        _domain: [0, 3, 5, 25, 40],
        more_info_html: '<div class="extended_description_html bat">The PV/battery size has been optimised for each health facility. The battery size [kWh] to install in each health centre is optimised according to the local solar radiation and specific energy demand load profile<span style="display:block" class="table_references">Source: T.Huld, M. Moner Girona, A. Kriston, “Geospatial Analysis of Photovoltaic Mini Grid System Performance ”,Energies, Feb 2017</span></div>'

    },
    {
        param_name: 'capex_soft',
        type_param: 'categorical_index',
        long_title: 'Up-front cost per health facility [EUR]<span class="more capex_soft">more info...</span>',
        long_title_txt: 'Capital costs [EUR] of PV installation (including storage and BOS). This is modelled for a host-owned business model and does not represent regional or country variations associated with component costs.',
        short_title: 'CAPEX [EUR]',
        _range: [4, 6, 8, 10, 12, 15],
        _domain: [0, 4500, 7500, 15000, 50000, 100000], //?? 30000
        more_info_html: '<div class="extended_description_html capex_soft">Capital costs [EUR] of PV installation (including storage and BOS). This is modelled for a host-owned business model and does not represent regional or country variations associated with component costs.</div>'

    }
    ],
    mapbox_paint_style: [

        {
            param: 'bat',
            style: {
                "circle-radius": [
                    "interpolate", ["linear"],
                    ["get", "bat_class"],
                    0, 2,
                    1, 2.5,
                    3, 5,
                    4, 7,

                ],
                "circle-color": [
                    "match", ["get", "bat_class"],
                    0, "#a354b3",
                    1, "#273899",
                    [2], "#2196f3",
                    [3], "#1f7222",
                    [4], "#d913c3",

                    '#e3141b'
                ],
                "circle-stroke-color": "black",
                "circle-stroke-width": [
                    "interpolate", ["exponential", 1],
                    ["zoom"],
                    5, .1,
                    6, .5,
                    10, .8,
                    16, 1
                ],
                "circle-stroke-opacity": .6
            }
        },

        {
            param: 'capex_soft',
            style: {



                "circle-color": [
                    "match", ["get", "capex_soft_class"],
                    0, "#412d27",
                    1, "#273899",
                    2, "#2196f3",
                    3, "#1f7222",
                    4, "#ff9800",
                    5, '#d913c3',
                    "hsl(0, 85%, 50%)"
                ],

                "circle-radius": [
                    "interpolate", ["linear"],
                    ["get", "capex_soft_class"],

                    0, 3,
                    1, 3.5,
                    3, 6,
                    4, 8
                ],
                "circle-stroke-color": "black",
                "circle-stroke-width": [
                    "interpolate", ["exponential", 1],
                    ["zoom"],
                    5, .1,
                    6, .5,
                    10, .8,
                    16, 1
                ],
                "circle-stroke-opacity": .6
            }
        },

        {
            param: 'pv',
            style: {
                "circle-radius": [
                    "interpolate", ["linear"],
                    ["get", "pv_class"],

                    0, 3,
                    1, 3.5,
                    3, 6,
                    4, 8

                ],
                "circle-color": [
                    "match", ["get", "pv_class"],
                    [0], "#412d27",
                    [1], "#273899",
                    [2], "#2196f3",
                    [3],
                    "hsl(167, 73%, 43%)", [4],
                    "#ff9800",
                    "hsl(0, 0%, 0%)"
                ],
                "circle-stroke-color": "black",
                "circle-stroke-width": [
                    "interpolate", ["exponential", 1],
                    ["zoom"],
                    5, .1,
                    6, .5,
                    10, .8,
                    16, 1
                ],
                "circle-stroke-opacity": .6

            }
        },
        {
            param: 'eldemand',
            style: {
                "circle-radius": [


                    "interpolate", ["linear"],
                    ["get", "eldemand_class"],
                    0, 3.5,
                    1, 5,
                    3, 8,
                    4, 12

                ],
                "circle-color": [
                    "match", ["get", "eldemand_class"],

                    [0],
                    "#273899", [1],
                    "#2196f3", [2],
                    "#1f7222", [3],
                    "#DE2173",
                    '#aae3d7'


                ],
                "circle-stroke-color": "black",
                "circle-stroke-width": [
                    "interpolate", ["exponential", 1],
                    ["zoom"],
                    5, .1,
                    6, .5,
                    10, .8,
                    16, 1
                ],
                "circle-stroke-opacity": .6

            }
        },

        {
            param: 'category_l',
            "style": {
                "circle-radius": [
                    "interpolate", ["linear", 1],
                    ["zoom"],
                    3, 2,
                    7, 5,
                    13, 7,
                    19, 9
                ],
                "circle-stroke-color": "#fff",
                "circle-stroke-width": [
                    "interpolate", ["exponential", 1],
                    ["zoom"],
                    5,
                    0,
                    6,
                    0.6,
                    10,
                    1,
                    16,
                    2
                ],
                "circle-color": [
                    "match", ["get", "category_l_class"],
                    0, "#1e7dc9",
                    1, "#a13131",
                    2, "#8bc34a",
                    3, "#cbb70f",
                    "#eee"
                ]
            }
        },
        {
            param: 'lcoeh_soft',
            style: {

                "circle-color": [
                    "match", ["get", "lcoeh_soft_class"],
                    0, "#412d27",
                    1, '#273899',

                    2, "#2196f3",
                    3, "#1f7222",
                    4, "#f7ac2c",
                    5, "#a8329d",
                    "red"


                ],
                "circle-radius": [
                    "interpolate", ["linear"],
                    ["get", "lcoeh_soft_class"],

                    0, .8,
                    1, 1.5,
                    2, 2,
                    3, 2.5,
                    4, 3.5,
                    5, 5

                    /*  "circle-radius": [
                         "interpolate", ["linear"],
                         ["get", "lcoeh_soft_class"],

                         0, .8,
                         1, 1.5,
                         2, 2,
                         3, 2.5,
                         4, 3.5,
                         5, 5
      */
                ],
                "circle-stroke-width": [
                    "interpolate", ["linear", 1],
                    ["zoom"],
                    5, 0.3,
                    7.7, 0.8,
                    10, 1.3,
                    16, 2
                ],
                "circle-stroke-color": "#fff",

                "circle-stroke-opacity": .6
            }
        }
    ],




    params_symbology: [{
        symbol_type: 'circle',
        using_class: true,
        title: 'Type of hospital',
        orienation: 'vertical',
        param_name: 'category_l',
        style_type: 'color',
        orienation: 'vertical',
        stops: false,
        data_type: 'string',
        exact_value: true,
        legend: [{
            val: 'First',
            color: '#1e7dc9'
        }, {
            val: 'Primary',
            color: '#a13131'
        }, {
            val: 'Secondary',
            color: '#8bc34a'
        }, {
            val: 'Tertiary',
            color: '#cbb70f'
        }]
    },
    {
        symbol_type: 'circle',
        using_class: true,
        title: 'Annual electricity demand per facility per year (kwh/year).',
        orienation: 'vertical',
        param_name: 'eldemand',
        style_type: 'radius',
        stops: true,
        data_type: 'text',
        exact_value: false,
        legend: [{
            val: '1825',
            color: '#273899',
            text: 'Very low'
        }, {
            val: '7300',
            color: '#2196f3',
            text: 'Low'
        }, {
            val: '14600',
            color: '#1f7222',
            text: 'Medium'
        },
        {
            val: '91250',
            color: '#DE2173',
            text: 'High'
        }
        ]
    },
    {
        symbol_type: 'circle',
        using_class: true,
        title: 'LCOE',
        orienation: 'vertical',
        param_name: 'lcoeh_soft',
        style_type: 'radius',
        stops: true,
        data_type: 'text',
        exact_value: false,
        legend: [{
            val: 'No data',
            color: '#412d27',
            text: 'No data'
        }, {
            val: '>0 ≤0.25',
            color: '#273899',
            text: 'Very low'
        }, {
            val: '≥0.25  <0.35',
            color: '#2196f3',
            text: 'Low'
        }, {
            val: '≥0.35  <0.45',
            color: '#1f7222',
            text: 'Medium'
        }, {
            val: '≥0.45  <0.55',

            color: '#f7ac2c',
            text: 'High'
        }, {
            val: '≥0.55 <0.67',
            color: "#a8329d",
            text: 'Very High'
        }]
    },
    {
        symbol_type: 'circle',
        using_class: true,
        title: 'Optimised size of PV (kWp)per daylight load profile with specific energy demand per facility',
        orienation: 'vertical',
        param_name: 'pv',
        style_type: 'radius',
        stops: true,
        data_type: 'text',
        exact_value: false,
        legend: [{
            val: 'No data',
            color: '#412d27',
            text: 'No data'
        },
        {
            val: '>0 <2',
            color: '#273899',
            text: 'Very small'
        }, {
            val: '≥2 <5',
            color: '#2196f3',
            text: 'Small'
        }, {
            val: '≥5 <25',
            color: '#1f7222',
            text: 'Medium'
        },
        {
            val: '≥25',
            color: '#ff9800',
            text: 'Large'
        }
        ]
    },
    {
        symbol_type: 'circle',
        using_class: true,
        title: 'Optimised size of battery',
        orienation: 'vertical',
        param_name: 'bat',
        style_type: 'radius',
        stops: true,
        data_type: 'text',
        exact_value: false,

        legend: [{
            val: 'No data',
            color: '#a354b3',
            text: 'No data'
        },
        {
            val: '≤ 3',
            color: '#273899',
            text: 'Very small'
        }, {
            val: '>3 ≤5',
            color: '#2196f3',
            text: 'Small'
        }, {
            val: '>5 ≤25',
            color: '#1f7222',
            text: 'Medium'
        }, {
            val: '>25',
            color: '#d913c3',
            text: 'Large'
        }
        ]
    },


    {
        symbol_type: 'circle',
        using_class: true,
        title: 'Up-front cost per health facility',
        orienation: 'vertical',
        param_name: 'capex_soft',
        style_type: 'radius',
        stops: true,
        data_type: 'text',
        exact_value: false,
        legend: [{
            val: 'No data',
            color: '#412d27',
            text: 'No data'
        }, {
            val: '< 3000',
            color: '#273899',
            text: 'Very low'
        }, {
            val: '3K - 5K',
            color: '#2196f3',
            text: 'Low'
        }, {
            val: '5K - 10K',
            color: '#1f7222',
            text: 'Medium'
        },
        {
            val: '10K - 20K',
            color: '#ff9800',
            text: 'High'
        },
        {
            val: '> 20000',
            color: '#d913c3',
            text: 'Very high'
        }
        ]
    },
    ]

});

app_data.layers_info.push({
    _layer: electric_grid,
    id: 'electric_grid',
    _type: 'line',
    queryable: true,
    activated_query: false,
    default_param: 'vltg_kv',

    active_param: 'vltg_kv',
    active_filters: [{
        'basic': ["has", "status_class"]
    },
    {
        'param_filter': null
    },
    {
        'admin_filter': null
    }
    ],
    params_arr: ['vltg_kv', 'status'],
    title: 'Electric grid',
    params_description: [{
        param_name: 'vltg_kv',
        type_param: 'categorical_index',
        long_title: 'Voltage (kV)',
        long_title_txt: 'Voltage (kV)',
        short_title: 'Voltage (kV)'
    },
    {
        param_name: 'status',
        type_param: 'categorical_index',
        long_title: 'Status',
        long_title_txt: 'Status',
        short_title: 'Status'
    }
    ],


    params_symbology: [{
        symbol_type: 'line',
        title: 'Voltage classification',
        orienation: 'vertical',
        param_name: 'vltg_kv',
        using_class: true,
        style_type: 'color',
        stops: false,
        data_type: 'string',
        exact_value: true,
        legend: [{
            val: 'Unknown',
            color: "#f6df0f"
        }, {
            val: 'Medium ≥33 kV',
            color: "#4363d8"
        }, {
            val: 'High <220 kV',
            color: "#f032e6"
        },
        {
            val: 'Extra High >220 kV',
            color: "#e6194B"
        }
        ]
    },
    {
        symbol_type: 'line',
        title: 'Status',
        orienation: 'vertical',
        param_name: 'status',
        using_class: true,
        style_type: 'color',
        stops: false,
        data_type: 'string',
        exact_value: true,
        legend: [{
            val: 'Unknown',
            color: "#469990"
        }, {
            val: 'Under construction',
            color: "#a9a9a9"
        }, {
            val: 'Missing',
            color: "#ffe119"
        },
        {
            val: 'Planned',
            color: "#f032e6"
        },
        {
            val: 'Existing',
            color: "#e6194B"
        },
        {
            val: 'Decommissioned',
            color: "#bfef45"
        }
        ]
    }
    ],
    mapbox_paint_style: [{
        param: 'vltg_kv',
        style: {


            "line-color": [
                "match", ["get", "vltg_kv_class"],
                [0],
                "#ffeb3b", [1],
                "#4363d8", [2],
                "#f032e6", [3],
                "#e6194B",
                "#000000"
            ],


            "line-width": [
                "interpolate", ["linear"],
                ["zoom"],
                3,
                0.5,
                5,
                0.8,
                7,
                1.2,
                9,
                1.8
            ],
            "line-dasharray": [2, 0.5],
            "line-opacity": 1

        }
    },
    {
        param: 'status',
        style: {
            "line-color": [
                "match", ["get", "status_class"],
                [0],
                "#469990", [1],
                "#a9a9a9", [2],
                "#ffe119", [3],
                "#f032e6", [4],
                "#e6194B", [5],
                "#bfef45",

                "#000000"
            ],


            "line-width": [
                "interpolate", ["linear"],
                ["zoom"],
                3,
                0.5,
                5,
                0.8,
                7,
                1.2,
                9,
                1.8
            ],
            "line-dasharray": [2, 0.5],
            "line-opacity": 1

        }
    }
    ]

})



