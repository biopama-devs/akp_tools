function attach_analysis_events() {

    $('.analysis_container').on('click', '.to_stats_modal', function () {

        var level_code = this_app.get_analysis_by_adm + '_code';

        var adm_code_value = this_app.queried_adm_params[level_code];

        $('#stats_modal').modal_materialize('open');

        $('#stats_modal .tabs').tabs('select', 'main_dataTables_wrapper');

        var t = $('#main_dataTables_wrapper table.dataTable');
        var t2 = $('#hc_main_dataTables_wrapper table.dataTable');




        //we know app_data.datatables_info already is setup
        /* adm0_code: 68
 ​
adm0_name: "Democratic Republic of the Congo"
 ​
adm1_code: 1068
 ​
        adm1_name: "Equateur" */

        this_app.get_ajax_calls.getAdm_tables(
            {
                to_query: this_app.queried_adm_params.to_query,
                adm_code_value: this_app.queried_adm_params.adm_code_value

            }).then(function (resp) {
                console.log(resp)
                this_app.plot_modal_datatable(resp);
                this_app.plot_hc_modal_datatable(resp);
                $('#stats_modal .nav-link.active').removeClass('active');
                var profileTab = new bootstrap.Tab(document.getElementById('nav-main_dataTables_wrapper-tab'));
                profileTab.show();
            })




    })

    $('.analysis_container').on('click', '.print_pdf', function () {

        var level_code = this_app.get_analysis_by_adm + '_code';

        var adm_code_value = this_app.queried_adm_params[level_code];

        if (this_app.this_pdf.unique_id == false || this_app.this_pdf.unique_id !== this_app.userid + '_' + adm_code_value) {
            console.warn('no prev adm pdf created or different, we put the uniqueid onpdf and continue process')
            this_app.this_pdf.unique_id = this_app.userid + '_' + adm_code_value;

        } else {
            if (this_app.this_pdf.unique_id == this_app.userid + '_' + adm_code_value) {
                console.log(this_app.this_pdf);

                console.warn('same adm unit analysis!, just add image')
                $('.mapboxgl-popup.after_analysis_popup,.mapboxgl-control-container').hide();
                $("body").busyLoad("hide", this_app.generating_file_busy_option);
                this_app.print_on_pdf = true;

                $('.print_map').trigger('click');

                $('.mapboxgl-popup.after_analysis_popup,.mapboxgl-control-container').show();
                return false
            }
        }

        $("body").busyLoad("show", this_app.generating_file_busy_option);
        var t_layer = app_data.layers_info.filter(function (d) {
            return d.id == 'hospitals';
        })[0];

        if (!t_layer.active_param) {
            var param = t_layer.default_param;
        } else {
            var param = t_layer.active_param;
        }
        console.info(param)

        $('.mapboxgl-popup.after_analysis_popup,.mapboxgl-control-container').hide();

        $('html,body,document').animate({
            scrollTop: $("#map").offset().top
        }, 10);

        this_app.queried_adm_params['adm0_code'] = this_app.sel_f_admin_extended.adm0_code;
        setTimeout(function () {

            this_app.get_ajax_calls.getAdm_tables(this_app.queried_adm_params).then(function (resp) {


                this_app.fx.printPdf(resp)


            })

        }, 200)
    })

    $('.analysis_container').on('click', '.close_analysis_container', function () {

        $('.analysis_container').empty().hide();

        if (map.getLayer("drawn_pol")) {
            map.removeLayer("drawn_pol")
        }

        var mapCanvas = document.getElementsByClassName('mapboxgl-canvas')[0];
        var mapDiv = document.getElementById('map');
        mapDiv.style.height = '100vh';
        mapCanvas.style.height = '100vh';
        $('body').css('overflow-y', 'hidden');

		//back to initial
        $('.sidenav').css('max-height', '90vh');
        $('.sidenav').css('position', 'fixed');
        map.resize();

        var l = ['hospitals', 'power_plants'];
        var c = [];
        for (var p in l) {

            if (map.getLayer(l[p])) {
                c.push(app_data.layers_info.filter(function (d) {
                    return d.id == l[p]

                })[0]);
            }
        }

        console.log(c)
        for (var p in c) {
            var t_layer = c[p];
            var all_filters = ["all"]
            t_layer.active_filters[2]['admin_filter'] = null;

            for (var p in t_layer.active_filters) {
                var key = Object.keys(t_layer.active_filters[p]);
                if (t_layer.active_filters[p][key]) {
                    all_filters.push(t_layer.active_filters[p][key])
                }

            }

            console.log(all_filters)
            map.setFilter(t_layer.id, all_filters)
        }

        $("body,html,document").animate({
            scrollTop: $('.nav-wrapper').height() - 50
        });

    });

    $('.analysis_container').on('click', '.expand_analysis_container', function (e) {

        if ($(this).hasClass('on')) {
            screenfull.exit();

        } else {

            screenfull.request($('.analysis_container')[0]);

        }

        $(this).toggleClass('on');

    })

    $('.analysis_container').on('click', '.param_to_map', function (e) {

        var layer_name = $(this).attr('_layer');

        var param = $(this).attr('_param');
        var layer_info = app_data.layers_info.filter(function (d) {
            return d.id == layer_name

        })[0];

        var select = $('.analysis_layer.' + layer_name + ' select');

        select.find('option').prop('selected', false);

        select.find('option[value=' + param + ']').prop('selected', true);

        if (!map.getLayer(layer_name)) {
            $('.analysis_layer.' + layer_name).find('.highlight').click();

        }

    })

    $('.analysis_container').on('mouseleave', function () {
        map.scrollZoom.disable();
        setTimeout(function () {
            map.scrollZoom.enable();
        }, 3000)
        console.log('leving')
    });
}