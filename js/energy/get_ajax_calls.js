
this_app.get_ajax_calls = {

    get_download_info: function () {
        console.trace();
        $.ajax({
            type: "GET",
            async: true, // this is by default false, so not need to mention
            //url: 'https://pere.gis-ninja.eu/akp_energy/get_download_info_new.php',
            url: 'https://hsm.land.copernicus.eu/php/akp_energy/get_download_info.php',

            dataType: 'json',
            success: function (d) {
                console.log(d)
                this_app.gaul0_global_info = d;

            }
        });
    },
    getF: function (e) {
        var zoom = map.getZoom();


        if (zoom >= 12.5) {
            var r = 100;
        }
        if (zoom <= 12.5 && zoom > 9) {
            var r = 500; //meters buffer
        }

        if (zoom <= 9) {
            var r = 700; //meters buffer
        }
        console.warn(zoom);
        console.warn(r)
        return $.ajax({
            url: 'https://hsm.land.copernicus.eu/php/akp_energy/get_school.php?lat=' + e.lngLat.lat + '&lng=' + e.lngLat.lng + '&buffer=' + r,
            dataType: 'json',
            async: true, // this is by default false, so not need to mention
            type: "GET",


        })
    },
    get_a2ei_points: function () {
        return $.ajax({
            type: "GET",
            dataType: 'json',

            url: 'https://hsm.land.copernicus.eu/php/akp_energy/get_a2eid_geojson.php',
            async: true, // this is by default false, so not need to mention
            success: function (a2ei_geojson) {


                console.warn(a2ei_geojson)
                app_data.a2ei_geojson = a2ei_geojson;


                map.getSource('a2ei_geojson_source').setData(app_data.a2ei_geojson);
                if (!map.getLayer('a2ei_points'))
                    map.addLayer({
                        id: 'a2ei_points',
                        type: 'circle',
                        source: 'a2ei_geojson_source',
                        paint: {
                            'circle-radius': 10,
                            'circle-color': '#2869d1',
                            'circle-opacity': 0
                        }
                    })

            }

        })
    },
    getCountries_select: function () {
        console.trace();
        return $.ajax({
            type: "GET",
            dataType: 'json',
            async: true, // this is by default false, so not need to mention
            url: 'https://hsm.land.copernicus.eu/php/akp_energy/get_subsah_info.php?requested_level=gaul_level_0',
            //url: 'https://pere.gis-ninja.eu/akp_energy/get_subsah_info2.php?requested_level=gaul_level_0',


        })
    },

    getAdm_tables: function (obj) {

        console.trace();
        console.log(obj)

        if (!obj.adm0_code) {
            if (this_app.sel_f_admin && this_app.sel_f_admin.adm0_code)
                obj.adm0_code = this_app.sel_f_admin.adm0_code;
        }
        //  obj.adm0_code = this_app.sel_f_admin.adm0_code;

        return $.ajax({
            type: "GET",
            async: true, // this is by default false, so not need to mention
            url: 'https://hsm.land.copernicus.eu/php/akp_energy/adm_tables_pdf_info_dev_3_taf.php',
            data: obj,
            dataType: 'json'
        })



    }

}
console.log(this_app)