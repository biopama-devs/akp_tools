<?php

header('Content-Type: application/json');

// requested_level: 'level_0_gaul',
// param: 'hospitals',
// adm0_code: this_f_p.adm0_code

$adm_code_value = $_GET['adm_code_value'];
$adm0_code = $_GET['adm0_code'];

$layer = 'hospitals';

$requested_level = $_GET['to_query'];
switch ($requested_level) {
    case 'adm0':
        $code = 'adm0_code';
        # code...
        $table = 'gaul_0_info_hosp_cat';
        $adm_name = 'adm0_name';
        $query = "select * from $table order by adm0_name";

        break;
    case 'adm1':
        $code = 'adm1_code';
        # code...
        $table = 'gaul_1_info_hosp_cat';
        $adm_name = 'adm1_name';
        // $query="select * from $table where adm0_code=$adm_code_value order by adm1_name";
        $query = "select * from $table where adm0_code=$adm0_code order by adm1_name";
        break;
    case 'adm2':
        $code = 'adm2_code';
        # code...
        $table = 'gaul_2_info_hosp_cat';
        $adm_name = 'adm2_name';
        $query = "select * from $table where adm0_code=$adm0_code order by adm2_name";
        break;


    default:
        # code...
        break;
}


$link = pg_connect("host=yourhost dbname=userorPwd user=userorPwd password=userorPwd");
// $query="select count($param) as count,$param,$code from hospitals_no_grid_gaul2 group by $code,$param order by $code desc";


//where adm0_code=$adm_code o

//echo $query;

$result = pg_query($link, $query);

$json = array('data_arr' => array()); //,'adm1_codes2'=>array(),'adm2_codes'=>array());

$i = 0;

$count = pg_num_rows($result);

$adm_code_arr = array();

if ($count > 0) {
    while ($data = pg_fetch_array($result)) {

        // var_dump($data);
        //$adm0=(int)$data['adm0_code'];
        $adm_code = (int)$data[$code];
        $adm_name_value = $data[$adm_name];

        $pos = array_search($adm_code, $adm_code_arr);
        //echo $pos.$i;

        if ($pos === false) {

            array_push($adm_code_arr, $adm_code);
            array_push($json['data_arr'], array(
                $code => $adm_code,
                $adm_name => $adm_name_value,
                'num_features' => 0
            ));
            // $name=>$data[$name]));


        }
        if ($data['is_max'] === 'true') {
            $pos = array_search($adm_code, $adm_code_arr);
            if ($data['param'] == 'lcoeh') {
                $lcoeh_max = $data['counts'];
                $json['data_arr'][$pos]['lcoeh'] = array('cat' => $data['category'], 'max' => $lcoeh_max);
            }
            if ($data['param'] == 'capex') {
                $capex_max = $data['counts'];
                $json['data_arr'][$pos]['capex'] = array('cat' => $data['category'], 'max' => $capex_max);
            }
            if ($data['param'] === 'elctdemand') {
                $elctdemand_max = $data['counts'];
                $json['data_arr'][$pos]['elctdemand'] = array('cat' => $data['category'], 'max' => $elctdemand_max);
            }
        }

        if ($data['param'] == 'lcoeh') {
            //otherwise, duplicated (or more), one for each param...
            $json['data_arr'][$pos]['num_features'] += (int)$data['counts'];
        }


        $i++;
    }
}

$all_val = array();

//var_dump($json);

$i = 0;
// foreach ($json['data_arr'] as $data)
// {

//     array_push($all_val,(int)$data['total']);
// }
// $json['min']=min($all_val);
// $json['max']=max($all_val);
// $json['total_f']=sizeof($all_val);
// $json['avg']=round((max($all_val)-min($all_val))/sizeof($all_val));
// $json['total_hospitals']=$total_hospitals;
// sort($all_val,SORT_NUMERIC);
// $pos=(sizeof($all_val)+1)/2;
// $json['median']=$all_val[$pos];



$main_json = json_encode($json);
if ($_GET['callback']) {
    echo $_GET['callback'] . "(" . $main_json . ")";
} else {
    echo $main_json;
};
