<?php
header('Content-Type: application/json');

$json = array(
  'adm_code_name' => null,
  'hospitals_categories' => array(
    'counts' => 0,
    'ids' => array(),
    'categories_data' => array()
  ),
  'power_categories' => array(
    'counts' => 0,
    'ids' => array(),
    'categories_data' => array()
  )
);

$link = pg_connect("host=yourhost dbname=yourdb user=usr password=pwd");

$userid = $_GET['userid'];
$to_query = $_GET['to_query'];

if ($to_query !== 'bbox') {
  $adm_code_value = $_GET['adm_code_value'];
  $adm_name;

  switch ($to_query) {
    case 'adm0':
      $adm_code_name = 'adm0_code';
      //  $adm_sql="select ST_extent(geom),adm0_code_int,adm0_name,area_sqkm from gaul_0_info_all where adm0_code_int=$adm_code_value";
      $adm_sql = "select adm0_code,adm0_name,area_sqkm,iso2_code from gaul_0_info_all where adm0_code=$adm_code_value";
      break;
    case 'adm1':
      $adm_code_name = 'adm1_code';
      $adm_sql = "select adm0_code,adm0_name,adm1_code,adm1_code,adm1_name,area_sqkm from gaul_1_info_all where adm1_code=$adm_code_value";
      break;
    case 'adm2':
      $adm_code_name = 'adm2_code';
      $adm_sql = "select adm0_code,adm0_name,adm1_code,adm1_name,adm2_code,adm2_name,area_sqkm from gaul_2_info_all where adm2_code=$adm_code_value";
      break;

    default:
      # code...
      break;
  }
} else {
  $sw = $_GET['bbox']['sw'];
  $xmin = $sw[0];
  $ymin = $sw[1];

  $ne = $_GET['bbox']['ne'];

  $xmax = $ne[0];
  $ymax = $ne[1];

  //bbox = min Longitude , min Latitude , max Longitude , max Latitude
  //(float xmin, float ymin, float xmax, float ymax, integer srid=unknown);
  $pol = 'POLYGON((' . $xmin . ' ' . $ymin . ',' . $xmin . ' ' . $ymax . ',' . $xmax . ' ' . $ymax . ',' . $xmax . ' ' . $ymin . ',' . $xmin . ' ' . $ymin . '))';
}
if ($to_query == 'bbox') {
  $power_sql = "select id,mw,mw_class,gen_type,gen_type_class from power_plants_2019_class where ST_Within(power_plants_2019_class.geom, ST_GeomFromText('" . $pol . "',4326))";
} else {
  $power_sql = "select id,mw,mw_class,gen_type,gen_type_class from power_plants_2019_class where $adm_code_name='$adm_code_value'";
}

//echo $adm_sql;
array_push($json['power_categories']['categories_data'], array(
  'param' => 'mw',
  'cat_arr' => array(),
  'data' => array()
), array(
  'param' => 'gen_type',
  'cat_arr' => array(),
  'data' => array()
));

$mw_arr = array();
$gen_type_arr = array();

$result = pg_query($link, $power_sql);

$counter_power = 0;
$count_power = pg_num_rows($result);
if ($count_power > 0) {
  $json['power_categories']['counts'] = $count_power;
  // echo 'initial counts are '.$json['power_categories']['counts'];
  while ($data = pg_fetch_array($result)) {
    array_push($json['power_categories']['ids'], $data['id']);

    $all_p_list = array(
      'mw',
      'gen_type'
    );

    for ($i = 0; $i < sizeof($all_p_list); $i++) {
      //'lcoe_h',
      $s = $all_p_list[$i];

      if ($s == 'mw') {
        $s_class = 'mw_class';
        $this_class_value = $data['mw_class'];
        $this_data = $data['mw'];
        //position into external array
        $param_pos = array_search($data['mw_class'], $mw_arr);
        $this_class_pos = array_search($this_class_value, $mw_arr);
        if ($this_class_pos === false) {
          array_push($mw_arr, $this_class_value);
          array_push($json['power_categories']['categories_data'][0]['cat_arr'], $this_class_value);
          array_push($json['power_categories']['categories_data'][0]['data'], array(
            'cat' => $this_class_value,
            'sum' => $data['mw'],
            'counts' => 1,
            'all_values' => array(
              $this_data
            )
          ));
        } else {
          $param_pos = array_search($data['mw_class'], $mw_arr);
          $json['power_categories']['categories_data'][0]['data'][$this_class_pos]['counts'] += 1;
          array_push($json['power_categories']['categories_data'][0]['data'][$this_class_pos]['all_values'], $this_data);

          if ($counter_power == $count_power - 1) {
            $this_class_pos = array_search($this_class_value, $json['hospitals_categories']['categories_data'][0]['cat_arr']);
          }
        }
      }

      if ($s == 'gen_type') {
        //  var_dump($json['power_categories']['categories_data'][1]);
        $s_class = 'gen_type_class';
        $this_class_value = $data['gen_type_class'];
        $this_data = $data['gen_type'];
        //position into external array
        $param_pos = array_search($data[$s_class], $gen_type_arr);
        $this_class_pos = array_search($this_class_value, $gen_type_arr);
        if ($this_class_pos === false) {
          array_push($gen_type_arr, $this_class_value);
          array_push($json['power_categories']['categories_data'][1]['cat_arr'], $this_class_value);
          array_push($json['power_categories']['categories_data'][1]['data'], array(
            'cat' => $this_class_value,
            'gen_type_name' => $data['gen_type'],
            'counts' => 1
          ));
        } else {
          $param_pos = array_search($data['gen_type_class'], $gen_type_arr);

          $json['power_categories']['categories_data'][1]['data'][$this_class_pos]['counts'] += 1;

          if ($counter_power == $count_power - 1) {
            $this_class_pos = array_search($this_class_value, $json['hospitals_categories']['categories_data'][1]['cat_arr']);
          }
        }
      }
    }
    $counter_power++;
  }
}

$c = 0;

$all_p = array(
  'lcoe_h',
  'elctdemand',
  'pv_kw_h',
  'bat_kwh_h',
  'capex'
);
//,'npv_h');
//$all_p=array('bat_kwh_h');
//'bat_kwh_m'
$all_p_class = array();

$health_sql = "WITH
     clipped AS (
        SELECT row_id,";
$geo_health_sql = $health_sql . ' geom,';
$health_sql .= 'facility_t,facility_n,adm0_code,adm0_name,adm1_code,adm1_name,adm2_code,adm2_name,';
$geo_health_sql .= 'facility_t,facility_n,adm0_code,adm0_name,adm1_code,adm1_name,adm2_code,adm2_name,';

for ($i = 0; $i < sizeof($all_p); $i++) {
  if ($i < sizeof($all_p) - 1) {
    $health_sql .= $all_p[$i] . ",$all_p[$i]_class,";
    $geo_health_sql .= $all_p[$i] . ",$all_p[$i]_class,";
  } else {
    $health_sql .= $all_p[$i] . ",$all_p[$i]_class";
    $geo_health_sql .= $all_p[$i] . ",$all_p[$i]_class";
  }
}

if ($to_query == 'bbox') {
  $health_sql .= " from hospitals_no_grid_gaul2 where ST_Within(hospitals_no_grid_gaul2.geom, ST_GeomFromText('" . $pol . "',4326)))
            SELECT row_id,";
  $health_sql .= 'facility_t,facility_n,adm0_code,adm0_name,adm1_code,adm1_name,adm2_code,adm2_name,';

  $geo_health_sql .= " from hospitals_no_grid_gaul2 where ST_Within(hospitals_no_grid_gaul2.geom, ST_GeomFromText('" . $pol . "',4326)))
            SELECT geom,facility_t,facility_n,adm0_code,adm0_name,adm1_code,adm1_name,adm2_code,adm2_name,";
} else {
  $health_sql .= " from hospitals_no_grid_gaul2 where $adm_code_name=$adm_code_value)
            SELECT row_id,adm0_code,adm0_name,adm1_code,adm1_name,adm2_code,adm2_name,facility_t,facility_n,";

  $geo_health_sql .= " from hospitals_no_grid_gaul2 where $adm_code_name=$adm_code_value)
            SELECT geom,adm0_code,adm0_name,adm1_code,adm1_name,adm2_code,adm2_name,facility_t,facility_n,";
}
// switch ($to_query) {
//   case 'adm0': $health_sql.='adm0_code,adm0_name,';
//   break;
//   case 'adm1': $health_sql.='adm0_code,adm0_name,adm1_code,adm1_name,';
//   break;
//   case 'adm2': $health_sql.='adm0_code,adm0_name,adm1_code,adm1_name,adm2_code,adm2_name,';
//   break;
//   default:break;
// }
//    $health_sql.=" from hospitals_no_grid_gaul2 where $adm_code_name=$adm_code_value)
//       SELECT ".$adm_code_name.','.$to_query.'_name,';
for ($i = 0; $i < sizeof($all_p); $i++) {
  if ($i < sizeof($all_p) - 1) {
    $health_sql .= $all_p[$i] . ",$all_p[$i]_class,";
    $geo_health_sql .= $all_p[$i] . ",$all_p[$i]_class,";
  } else {
    $health_sql .= $all_p[$i] . ",$all_p[$i]_class";
    $geo_health_sql .= $all_p[$i] . ",$all_p[$i]_class";
  }
  $v = $all_p[$i];
  //  echo $v;
  array_push($json['hospitals_categories']['categories_data'], array(
    'param' => $v,
    'cat_arr' => array(),
    'data' => array()
  ));

  $all_p[$i] = array();
  array_push($all_p_class, $all_p[$i]);
}
$health_sql .= ' from clipped';
$geo_health_sql .= ' from clipped';

$hospitals_categories_param = array();

//  echo $health_sql;
$result = pg_query($link, $health_sql);

//echo $health_sql;
$counter = 0;

$count = pg_num_rows($result);

if ($count > 0) {
  $json['hospitals_categories']['counts'] = $count;

  while ($data = pg_fetch_array($result)) {
    array_push($json['hospitals_categories']['ids'], $data['row_id']);

    $all_p_list = array(
      'lcoe_h',
      'elctdemand',
      'pv_kw_h',
      'bat_kwh_h',
      'capex'
    );
    //,'npv_h');
    //$all_p_list=array('bat_kwh_h');
    for ($i = 0; $i < sizeof($all_p_list); $i++) {
      //'lcoe_h',
      $s = $all_p_list[$i];
      $param_pos = array_search($all_p_list[$i], $all_p_list);

      //array_push($json['hospitals_categories']['categories_data'],array('param'=>$v,'cat_arr'=>array(),'data'=>array()));
      //'lcoe_h_class',
      $s_class = $all_p_list[$i] . '_class';

      $this_class_value = $data[$s_class];
      //$t_json=$json['hospitals_categories']['categories_data'][$param_pos];
      $this_class_pos = array_search($this_class_value, $json['hospitals_categories']['categories_data'][$param_pos]['cat_arr']);

      $this_data = $data[$all_p_list[$i]];

      if ($this_class_pos === false) {
        //  echo 'pos  '.$this_class_pos;
        array_push($json['hospitals_categories']['categories_data'][$param_pos]['cat_arr'], $this_class_value);
        array_push($json['hospitals_categories']['categories_data'][$param_pos]['data'], array(
          'cat' => $this_class_value,
          'counts' => 1,
          'all_values' => array(
            $this_data
          ),
          'sum' => $this_data
        ));
      } else {
        $cat_pos = array_search($this_class_value, $json['hospitals_categories']['categories_data'][$param_pos]['cat_arr']);

        $json['hospitals_categories']['categories_data'][$param_pos]['data'][$cat_pos]['counts'] += 1;

        $json['hospitals_categories']['categories_data'][$param_pos]['data'][$cat_pos]['sum'] += $this_data;
        array_push($json['hospitals_categories']['categories_data'][$param_pos]['data'][$cat_pos]['all_values'], $this_data);

        if ($counter == $count - 1) {
          $all_v = $json['hospitals_categories']['categories_data'][$param_pos]['data'][$cat_pos]['all_values'];

          $json['hospitals_categories']['categories_data'][$param_pos]['data'][$cat_pos]['min'] = min($all_v);
          $json['hospitals_categories']['categories_data'][$param_pos]['data'][$cat_pos]['max'] = max($all_v);

          $counts = $json['hospitals_categories']['categories_data'][$param_pos]['data'][$cat_pos]['counts'];
          $sum = $json['hospitals_categories']['categories_data'][$param_pos]['data'][$cat_pos]['sum'];
          $avg = $sum / $counts;
          if ($avg < 0) {
            $avg2 = round($avg, 3);
          } else {
            $avg2 = round($avg, 2);
          }

          $json['hospitals_categories']['categories_data'][$param_pos]['data'][$cat_pos]['avg'] = $avg2;
          // $json['hospitals_categories']['categories_data'][$param_pos]['data'][$cat_pos]['avg']=$sum/$json['hospitals_categories']['categories_data'][$param_pos]['data'][$cat_pos]['counts'];
          // }
        }
      }
    }

    $counter++;
  }
}

//  var_dump($json);

$c = 0;
$json2 = array(
  'adm_code_name' => $adm_code_name,
  'adm_code_value' => $adm_code_value,
  'hospitals_categories' => array(
    'counts' => 0,
    'categories_data' => array()
  ),
  'power_categories' => array(
    'counts' => 0,
    'categories_data' => array()
  ),
  'count_px' => 0,
  'sum_total' => 0,
  'ghs_data' => array()
);

foreach ($json['hospitals_categories']['categories_data'] as $key => $value) {
  foreach ($value['data'] as $key2 => $value2) {
    unset($value2['all_values']);
  }
  array_push($json2['hospitals_categories']['categories_data'], $value);
  $c++;
}
$json2['hospitals_categories']['counts'] = $json['hospitals_categories']['counts'];
$json2['hospitals_categories']['ids'] = $json['hospitals_categories']['ids'];

foreach ($json['power_categories']['categories_data'] as $key => $value) {
  foreach ($value['data'] as $key2 => $value2) {
    unset($value2['all_values']);
  }

  array_push($json2['power_categories']['categories_data'], $value);
  $c++;
}

$json2['power_categories']['counts'] = $json['power_categories']['counts'];
$json2['power_categories']['ids'] = $json['power_categories']['ids'];

if ($to_query !== 'bbox') {
  $sql = "select sum(px_val),count(px_class_250m),px_class_250m _class from ghs_all where px_class_250m is not NULL and
                    $adm_code_name=$adm_code_value group by px_class_250m";
} else {
  $sql = "select sum(px_val),count(px_class_250m),px_class_250m _class from ghs_all where px_class_250m is not NULL and ST_Within(ghs_all.geom, ST_GeomFromText('" . $pol . "',4326)) group by px_class_250m";
}

$result = pg_query($link, $sql);

$count = pg_num_rows($result);

if ($count > 0) {
  while ($data = pg_fetch_array($result)) {
    $json2['count_px'] += $data['count'];
    $json2['sum_total'] += round($data['sum']);
    array_push($json2['ghs_data'], array(
      'sum' => round(($data['sum'])),
      'num_px' => $data['count'],
      'px_class' => $data['_class']
    ));
  }
}

if ($to_query !== 'bbox') {
  $adm_code_value = $_GET['adm_code_value'];
  $result = pg_query($link, $adm_sql);

  $count_adm = pg_num_rows($result);
  if ($count_adm > 0) {
    while ($data = pg_fetch_array($result)) {
      $json2['adm_area'] = $data['area_sqkm'];
      switch ($to_query) {
        case 'adm0':
          $json2['adm0_name'] = $data['adm0_name'];
          $json2['adm0_code'] = $data['adm0_code'];
          $json2['iso2_code'] = $data['iso2_code'];
          break;
        case 'adm1':
          $json2['adm0_name'] = $data['adm0_name'];
          $json2['adm0_code'] = $data['adm0_code'];
          $json2['adm1_name'] = $data['adm1_name'];
          $json2['adm1_code'] = $data['adm1_code'];
          break;
        case 'adm2':
          $json2['adm0_name'] = $data['adm0_name'];
          $json2['adm0_code'] = $data['adm0_code'];

          $json2['adm1_name'] = $data['adm1_name'];
          $json2['adm1_code'] = $data['adm1_code'];

          $json2['adm2_name'] = $data['adm2_name'];
          $json2['adm2_code'] = $data['adm2_code'];
          break;
        default:

          break;
      }
    }
  }
}

function search_info($layer_id, $userid, $json2)
{
  //echo 'asdfsadfasf';
  exec('cd /var/www/html/energy_phps/downloads/' . $userid . '/; zip -r "./zipped/' . $layer_id . '.zip" "./unzipped"');

  if (file_exists('/var/www/html/energy_phps/downloads/' . $userid . '/zipped' . '/' . $layer_id . '.zip')) {
    // $json=array('success'=>true);
    // echo json_encode($json,false);
    // $json2[$layer_id.'_zip']=true;
    $json2['created_files'] = true;
  } else {
    $json2['created_files'] = false;

    // $json=array('success'=>false);
    // echo json_encode($json,false);
    //return false;

  }
  //exec('zip -r "./zipped/archive.zip" "/srv/www/htdocs/copernicus_downloads/unzipped"');
  $main_json = json_encode($json2, JSON_NUMERIC_CHECK);
  if ($_GET['callback']) {
    echo $_GET['callback'] . "(" . $main_json . ")";
  } else {
    echo $main_json;
  };
}
function generate_shp($layer_id, $userid, $sql, $json2)
{
  //download/1234987  /unzipped...
  $dir = '/var/www/html/energy_phps/downloads';
  if (!file_exists($dir . '/' . $userid)) {
    mkdir($dir . '/' . $userid, 0777, true);
    mkdir($dir . '/' . $userid . '/unzipped', 0777, true);
    mkdir($dir . '/' . $userid . '/zipped', 0777, true);
    mkdir($dir . '/' . $userid . '/xlsx', 0777, true);
  } else {
    $user_dir = $dir . '/' . $userid;

    $unzipped_dir = $user_dir . '/unzipped';
    $files = scandir($unzipped_dir);
    foreach ($files as $file) { // iterate files
      //	var_dump($dir.'/'.$file);
      //if(is_file($file))
      if (is_file($unzipped_dir . '/' . $file)) {
        unlink($unzipped_dir . '/' . $file);
      }
    }

    $excel_dir = $user_dir . '/xlsx';
    $files = scandir($excel_dir);
    foreach ($files as $file) { // iterate files
      //	var_dump($dir.'/'.$file);
      //if(is_file($file))
      if (is_file($excel_dir . '/' . $file)) {
        unlink($excel_dir . '/' . $file);
      }
    }

    $zipped_dir = $user_dir . '/zipped';
    $files = scandir($zipped_dir);
    foreach ($files as $file) { // iterate files
      //	var_dump($dir.'/'.$file);
      //if(is_file($file))
      if (is_file($zipped_dir . '/' . $file)) {
        unlink($zipped_dir . '/' . $file);
      }
    }
  }
  $link = pg_connect("host=yourhost dbname=yourdb user=userorPwd password=userorPwd");
  // if ($req_format==='shp')
  // {
  $dir = '/var/www/html/energy_phps/downloads';
  $user_dir = $dir . '/' . $userid;

  $c = "cd $user_dir/; ogr2ogr -f 'ESRI Shapefile' unzipped/$layer_id" . _ . "shapefile.shp PG:'host=yourhost dbname=yourdb user=userorPwd password=userorPwd' -sql '$sql';";

  //  $c.="cd $user_dir/; ogr2ogr -f 'XLSX' xlsx/$layer_id"._.".xlsx PG:'host=yourhost dbname=yourdb user=userorPwd password=userorPwd' -sql $sql";
  // $c='cd '.$user_dir.'\/; ogr2ogr -f "XLSX" unzipped/'.$layer_id.'.xlsx PG:"host=yourhost dbname=yourdb user=userorPwd password=userorPwd" -sql "'.$sql.'"';
  // }
  //echo $c;
  exec($c, $out, $ret);
  search_info($layer_id, $userid, $json2);
}

// if ($layer_id=='hospitals')
// {
//   $sql=$health_sql;
// }
// else
// {
//   $sql=$power_sql;
// }
//echo $geo_health_sql;
//generate_shp('hospitals',$userid,$geo_health_sql,$json2);
$main_json = json_encode($json2, JSON_NUMERIC_CHECK);
if ($_GET['callback']) {
  echo $_GET['callback'] . "(" . $main_json . ")";
} else {
  echo $main_json;
};
//if ($_GET['callback']) { echo $_GET['callback']."(".$main_json.")"; } else { echo $main_json; };
