

var this_app = {


    clickTimeout: null,
    clickType: null,
    active_admin_filter: {
        code_name: null,
        code_val: null
    },
    drawing_rect: false,
    active_query_layer: null,
    active_param: null,
    geojsons: [],
    querying_popup: false,
    querying_popup_list: { arr: [], list: [] },
    get_analysis_by_adm: null,
    print_on_pdf: false,
    sel_highlighted_arr: [],


    set_raster_cql: function (t_layer) {
        console.info('set_raster_cql')
        console.log(t_layer)

        var param_name = t_layer.active_param;
        console.warn('raster symbology, we have to do a cql query')
        var symb = t_layer.params_symbology.filter(d => d.param_name == param_name)[0];
        var source = map.getSource('schools_source');

        if (source.tiles[0].includes('&cql_filter'))
            var tiles = source.tiles[0].split('&cql_filter')[0];
        else
            var tiles = source.tiles[0];

        if (this_app.active_admin_filter.code_name) {
            var cql_filter = "&cql_filter=" + this_app.active_admin_filter.code_name + ' in (' + this_app.active_admin_filter.code_val + ') ';
        }
        else {
            var cql_filter = "&cql_filter="
        }
        if (t_layer.active_filters[0]['param_filter']) {
            if (t_layer.active_filters[0]['param_filter'].length > 0) {

                if (this_app.active_admin_filter.code_name)
                    cql_filter += ' and ';


                if (symb.using_class === true)
                    cql_filter += param_name + '_cat';

                else
                    cql_filter += param_name;

                cql_filter += ' in (' + t_layer.active_filters[0]['param_filter'].join(', ') + ') ';
            }
            else {
                if (this_app.active_admin_filter.code_name)
                    cql_filter += 'and ' + param_name + ' in (200) ';
                else
                    cql_filter += param_name + ' in (200) ';
            }



        }
        else {
            if (this_app.active_admin_filter.code_name)
                cql_filter += 'and ' + param_name + ' not in (200) ';
            else
                cql_filter += param_name + ' not in (200) ';
        }
        if (cql_filter)
            tiles += cql_filter;

        console.log(cql_filter)
        console.warn(tiles)
        map.getSource('schools_source').tiles = [tiles];

        if (map.getLayer('schools')) {
            map.style.sourceCaches['schools_source'].clearTiles();
            map.style.sourceCaches['schools_source'].update(map.transform);
        }


        t_layer.cql_filter = cql_filter;
    },
    get_radius_by_param: function (layer, param, exp) {


        console.warn(arguments)
        if (layer.id == 'refugee_camps') {
            var t_layer = app_data.layers_info.filter(function (d) {
                return d.id == 'refugee_camps_geojson';
            })[0];
            console.warn(t_layer)
            console.warn(t_layer.mapbox_paint_style)
            var o_style = t_layer.mapbox_paint_style[0];
        } else {

            var t_layer = app_data.layers_info.filter(function (d) {
                return d.id == layer.id;
            })[0];

            if (!t_layer.not_styled)
                var o_style = layer.mapbox_paint_style.filter(function (d) {
                    return d.param == param
                })[0];
            else
                var o_style = layer.mapbox_paint_style[0];

        }
        console.warn(o_style)


        if (o_style.style)
            var o = o_style.style['circle-radius'];
        else
            var o = o_style['circle-radius'];

        console.log(o)


        if (typeof o === 'object') {
            var new_o = o.map(function (d, i) {
                console.info(d)
                if (isNaN(d) === false && !(i % 2)) {

                    console.log(d, exp, d * exp)

                    return +(d * exp).toFixed(2)
                } else {

                    return d
                }
            });
            return new_o
        } else {
            console.info(o, exp)
            console.log(+(o * exp).toFixed(2))
            if (layer.id == 'refugee_camps') {

                return { marker_scale: +exp, circle_scale: +(o * exp).toFixed(2) }
            } else {
                return +(o * exp).toFixed(2)
            }

        }



    },
    reset_palette: function (t_layer) {



        var layer_id = t_layer.id;
        var container = $('.analysis_layer.' + layer_id);

        var t_layer = app_data.layers_info.filter(function (d) {

            return d.id == layer_id
        })[0];

        if (!t_layer.raster) {

            container.find('.range-wrap-container.opacity b').text('100%');
            container.find('.range_z_opacity').val(100);



            if (t_layer._type == 'circle') {


                map.setPaintProperty(
                    layer_id,
                    'circle-opacity',
                    1
                );

                map.setPaintProperty(
                    layer_id,
                    "circle-stroke-width",
                    .6
                );


                container.find('input.range_z_radius input').val(33);

                container.find('.fcolorpicker-curbox').css('background', t_layer.border);

                container.find('input.range_z_stroke input').val(66);
            }
        }
    },
    update_popup_circle: function () {

        var sel = d3.selectAll('circle.selected');

        sel
            .attr("cx", function (d) {
                return point_project(d.centroid).x;
            })
            .attr("cy", function (d) {
                return point_project(d.centroid).y;
            });

        var sel = d3.selectAll('circle.a2ei_inner_centroids,circle.a2ei_outer_centroids');

        sel
            .attr('cx', function (d) {

                return point_project(d.geometry.coordinates).x;
            })
            .attr('cy', d => point_project(d.geometry.coordinates).y)
    },
    getFeature: function (e) {

        console.warn(this_app.drawing);

        if (this_app.drawing == true)
            return false;

        console.log(e, e.lngLat)

        $.when(this_app.get_ajax_calls.getF(e)).then(function (data) {
            var properties = data[0];

            if (properties.data && properties.data === 'empty') {
                console.warn('No schools here, try on a closer zoom level')
            }
            else {
                var t_layer = app_data.layers_info.filter(d => d.id == 'schools')[0];



                var active_param = t_layer.active_param;

                var params_description = t_layer.params_description.filter(function (d) {

                    return d.param_name == active_param
                })[0];

                if (!params_description.subtype_param || params_description.subtype_param == 'range') {

                    if (active_param == 'wea')
                        var to_search = properties[active_param];
                    else
                        var to_search = properties[active_param + '_cat'];
                }
                else {
                    var to_search = properties[active_param];
                }


                var param_filter = t_layer.active_filters[0]['param_filter'];
                if (param_filter) {
                    console.log(param_filter)
                    if (param_filter.indexOf(to_search) === -1) {

                        return false;
                    }
                }

                var name = properties['name'] ? properties['name'] : 'No name';
                //<div class="col-3"><i class="close_popup bi bi-x-circle-fill"></i></div>
                var html = '<div class="row"><div style="font-size:1.7rem;color:orange" class="col-12 center">Schools</div></div>' +
                    '<div class="popup_info_container"><div class="f_name center">' + name + '</div>';
                t_layer.popup_params.forEach(function (d, i) {
                    if (d.name == active_param)
                        console.info(d, active_param)

                    var val = properties[d.name];

                    if (d.name == active_param)
                        html += '<div class="row valign-wrapper highlighted_row">';
                    else
                        html += '<div class="row valign-wrapper">';


                    if (d.title) {
                        html += '<div class="col popup_col_f_name col-9">' + d.title + '</div><div class="col-3 info_container">';
                        if (d.name === 'wea') {
                            html += val === 0 ? 'No' : 'Yes' + '</div></div>';
                        }
                        else {
                            if (this_app.roundIfNecessary(val))
                                html += this_app.roundIfNecessary(properties[d.name]) + '</div></div>';
                            else
                                html += 'No data</div></div>';
                        }

                    }


                })

                console.warn(html)
                html += '</div>';

                var popup = new mapboxgl.Popup({
                    closeButton: true,
                    closeOnClick: false,
                    className: 'feature_map_popup schools',
                    offset: 1

                })

                var latlng = e.lngLat;

                popup.setLngLat({
                    lng: properties.lng,
                    lat: properties.lat
                })
                    .setHTML(html)
                    .addTo(map);

                popup.setMaxWidth("350px");

                setTimeout(function () {


                    $('.mapboxgl-popup.schools').show();

                }, 100)
            }
        })



    },

    add_layer: function (layer_id, add) {

        if (layer_id.includes('travel_')) {
            var container = $('.analysis_layer.time_travel');
            var t_layer = app_data.layers_info.filter(function (d) {

                return d.id == layer_id;
            })[0];
        }
        else {

            var container = $('.analysis_layer.' + layer_id);
            var t_layer = app_data.layers_info.filter(function (d) {

                return d.id == layer_id;
            })[0];

        }

        if (add) {


            console.warn(layer_id)
            console.warn(container)
            if (layer_id.includes('travel_') == false || layer_id == 'time_travel') {

                container.find('.card-content-content').show();

                if (layer_id !== 'time_travel') {
                    t_layer.activated_query = true;


                    if (container.find('.download').length > 0)
                        container.find('.download').show();
                    if (container.find('.opacity').length > 0)
                        container.find('.opacity').show();

                }
                container.find('.slide_layer.bi-chevron-down-fill').removeClass('bi-arrow-bar-dpwn').addClass('bi-caret-up-fill');

                container.find('.slide_layer').addClass('on').show();

                if (container.find('._info').length > 0) {
                    container.find('._info').show();
                }
            }

            if (layer_id.includes('travel_')) {

                if (container.find('.download').length > 0)
                    container.find('.download').show();
                if (container.find('.opacity').length > 0)
                    container.find('.opacity').show();
            }

            if (layer_id == 'time_travel') {
                console.warn('JUST lets show a list of time related layers');


                return false;
            }

            console.warn('highlighting, add layer', t_layer)

            $("#map").busyLoad("show", {
                spinner: "accordion",
                background: '#ff980000',
                textPosition: "bottom",
                fontSize: "1rem",
                animation: "fade",
                animationDuration: "slow",
                text: "Loading layer"
            });


            app_data.temp_layer = t_layer._layer;

            if (!t_layer.raster) {
                if (layer_id == 'hospitals') {

                    if (!map.getLayer('hospitals_sel')) {
                        map.addLayer(app_data.layers_info.filter(d => d.id == 'hospitals_sel')[0]._layer)

                    }

                    $('.hc_btn_description').css('display', 'block').children().show();

                    if (!map.getLayer('a2ei_points')) {

                        this_app.get_ajax_calls.get_a2ei_points();
                    }

                }


            }

            if (t_layer.raster == true) {

                if (t_layer.id == 'schools') {


                    $('.schools_main_description .extra').show();
                    $('.schools_main_description').addClass('active');


                    this_app.generate_selects_legend(container, t_layer);

                    console.log(t_layer);
                    var param_name = t_layer.active_param;
                    console.warn(param_name)

                    var symb = t_layer.params_symbology.filter(d => d.param_name == param_name)[0];
                    var source = map.getSource('schools_source');


                    console.warn(source.tiles[0])
                    console.warn(source.tiles[0].includes('&styles=energy'))

                    if (source.tiles[0].includes('&styles=energy:'))
                        var tiles = source.tiles[0].split('&styles=energy:')[0];
                    else
                        var tiles = source.tiles[0];

                    var new_tiles = tiles + '&styles=energy:schools_' + t_layer.active_param;
                    map.getSource('schools_source').tiles = [new_tiles];

                    this_app.set_raster_cql(t_layer);

                    this_app.generate_selects_legend(container, t_layer);
                    container.find('.params_select').show();

                }

                var b = base_layers.filter(function (d) {
                    return d.active == true;
                })[0].id




                if (t_layer.id !== 'schools') {

                    if (t_layer.id.includes('travel_')) {



                        var time_layers = ['travel_hc_access_minimum_all_motorized', 'travel_hc_access_minimum_all_walking',
                            'travel_time_primary_all_walking', 'travel_time_primary_all_motorized'
                        ];
                        time_layers.forEach(function (item) {
                            if (item !== layer_id) {
                                if (map.getLayer(item)) {
                                    map.removeLayer(item);
                                }
                            }
                        });

                        $('.travel_legends_container>div').hide();
                        $('.travel_legends_container').removeClass('off');
                        if (layer_id.includes('_hc_')) {
                            $('.travel_hc_legend').show();
                        }
                        else {
                            $('.travel_schools_legend').show();
                        }

                    }

                    var layers = map.getStyle().layers;





                    let firstIndex = -1;
                    for (let i = 0; i <= layers.length - 1; i++) {
                        if (layers[i].type !== 'raster' && layers[i].id !== 'gaul_level_0') {
                            firstIndex = i;
                            break;
                        }
                    }
                    console.warn(firstIndex + 1)
                    map.addLayer(t_layer._layer, layers[firstIndex].id);

                    console.log('adding a ceat raster under ' + layers[firstIndex].id)

                }
                else {
                    map.addLayer(t_layer._layer);
                }

                if (t_layer.id == 'ghs_africa_250m_bigger_1_5') {

                    container.find('.raster_scale_container,.raster_legend_classif,.row.extra').show();

                    if (container.find('svg g').length == 0)
                        this_app.create_raster_legend('ghs_africa_250m_bigger_1_5', 'orange_purple');
                    else
                        container.find('svg').show();

                    t_layer.active_param = 'px_val';
                } else {
                    if (t_layer._layer.id == 'distance_to_grid') {
                        $('.distance_raster_legend_classif').show();
                    }

                    if (t_layer._layer.id == 'nightlights') {
                        $('.nightlights_classif').show();
                    }
                }

            }

            if (!t_layer.raster) {

                console.log(layer_id)

                if (layer_id == 'taf_hydrogen_grouped') {

                    setTimeout(function () {
                        map.addLayer(taf_hydrogen_grouped_label);


                    }, 1000);


                }

                if (layer_id == 'taf_geotermal_ungrouped') {
                    map.addLayer(taf_geotermal_grouped);
                    setTimeout(function () {

                        map.addLayer(taf_geotermal_grouped_label);
                    }, 1000);


                }

                if (layer_id == 'taf_pumps_up') {
                    var taf_pumps_down = app_data.layers_info.filter(d => d.id == 'taf_pumps_down')[0];
                    map.addLayer(taf_pumps_down._layer);

                    var taf_pumps_lines = app_data.layers_info.filter(d => d.id == 'taf_pumps_lines')[0];
                    console.log(taf_pumps_lines._layer)
                    map.addLayer(taf_pumps_lines._layer);
                }

                map.addLayer(t_layer._layer);

                if (t_layer.id == 'electric_grid') {
                    map.addLayer(
                        {
                            'id': 'gridfinder',
                            'type': 'raster',
                            'source': 'gridfinder_source',
                            'paint': {}
                        }
                    )
                }

                if (layer_id.indexOf('taf_') > -1) {
                    console.info(layer_id)

                    if (layer_id !== 'taf_pumps_down') {
                        if (t_layer.default_param) {


                            var style = t_layer.mapbox_paint_style.filter(d => d.param == t_layer.default_param)[0].style;
                            for (var p in style) {
                                console.warn(p)
                                map.setPaintProperty(layer_id, p, style[p]);
                            }


                            if (layer_id == 'taf_pumps_up') {
                                var style = t_layer.mapbox_paint_style.filter(d => d.param == t_layer.default_param)[0].style;
                                for (var p in style) {
                                    console.warn(p)
                                    map.setPaintProperty(layer_id, p, style[p]);
                                }
                            }
                        }
                        else {
                            console.warn('no default param for ' + layer_id)
                        }
                    }
                    else {
                        debugger;
                    }


                }


                var layer_labels = app_data.layers_labels_info.filter(d => d.id == layer_id + '_labels')[0];

                if (layer_labels) {


                    map.addLayer(layer_labels._layer);
                }

                console.warn(t_layer.active_filters)


                if ((layer_id.includes('taf') && !t_layer.not_styled)
                    || layer_id == 'ref_settlements' || layer_id == 'hospitals'
                    || layer_id == 'power_plants' || layer_id == 'electric_grid') {


                    var all_filters = ["all"]
                    for (var p in t_layer.active_filters) {

                        var key = Object.keys(t_layer.active_filters[p]);
                        if (t_layer.active_filters[p][key]) {
                            all_filters.push(t_layer.active_filters[p][key])
                        }

                    }
                    console.log(all_filters)
                    map.setFilter(layer_id, all_filters);

                    if (layer_labels)
                        map.setFilter(layer_id + '_labels', all_filters);

                }


                this_app.generate_selects_legend(container, t_layer);
            }
        }

        if (!add) {
            var travel_layers_arr = ['travel_hc_access_minimum_all_motorized', 'travel_hc_access_minimum_all_walking',
                'travel_time_primary_all_walking', 'travel_time_primary_all_motorized'
            ];
            if (travel_layers_arr.includes(layer_id) == false) {

                if (container.find('.download').length > 0)
                    container.find('.download').hide();

                container.find('.slide_layer.bi-caret-up-fill').removeClass('bi-caret-up-fill').addClass('bi-chevron-down-fill');
                container.find('.slide_layer').removeClass('on').hide();
                container.find('.card-content-content').hide();
                container.find('.ranges_container,.extra,.extended_description').hide();
                if (container.find('._info').length > 0) {
                    container.find('._info').hide().removeClass('on');
                    this_app.querying_popup = null;
                }
                container.find('.params_select,.d3_legend,svg').hide();

                if (container.find('.opacity').length > 0) {

                    container.find('.opacity').hide().removeClass('on');
                }

                if (layer_id == 'time_travel') {

                    $('li.analysis_layer.time_travel').attr('active_layer', 'false');
                    $('.time_travel_form_select option:first').prop('selected', true);



                    $('.travel_legends_container>div').hide();
                    $('.travel_legends_container').addClass('off');



                    var time_layers = ['travel_hc_access_minimum_all_motorized', 'travel_hc_access_minimum_all_walking',
                        'travel_time_primary_all_walking', 'travel_time_primary_all_motorized'
                    ];

                    $('li.analysis_layer.time_travel').find('.range_z_opacity').val(100);
                    time_layers.forEach(function (item) {

                        var l = app_data.layers_info.filter(d => d.id == item)[0]._layer;
                        l.paint['raster-opacity'] = 1;



                        if (map.getLayer(item)) {
                            map.removeLayer(item);
                        }
                    });

                    return false;

                }


            }




            if (!t_layer.raster)
                this_app.reset_palette(t_layer);

            if (layer_id == 'schools') {


                $('.schools_main_description').addClass('active');
                t_layer.active_filters[0]['param_filter'] = null;
                t_layer.cql_filter = null;



            }

            if (layer_id == 'hospitals') {
                container.find('.hc_btn_description,.ranges_container').hide();
                if (map.getLayer('a2ei_points') && map.getLayoutProperty('a2ei_points', 'visibility') == 'visible') {

                    $('.a2ei_div input').trigger('click');
                }
            } else {
                container.find('.range-wrap-container').hide();
            }



            $('.feature_map_popup.' + layer_id).remove();
            d3.selectAll('circle.selected.' + layer_id).remove();

            if (layer_id == 'taf_pumps_up') {


                map.removeLayer('taf_pumps_lines');
                map.removeLayer('taf_pumps_down');

            }

            if (map.getLayer(layer_id + '_label')) {
                map.removeLayer(layer_id + '_label');
            }

            if (layer_id == 'taf_geotermal_ungrouped') {
                map.removeLayer('taf_geotermal_grouped_label');
                map.removeLayer('taf_geotermal_grouped');
            }
            map.removeLayer(layer_id);
            if (layer_id == 'ref_settlements') {


                var container = $('.unhcr_div.ref_unhcr_merged_simple');

                if (map.getLayer('ref_unhcr_merged_simple')) {
                    map.removeLayer('ref_unhcr_merged_simple');
                    $('.ref_unhcr_merged_simple input').prop('checked', false)
                    /* container.find('.general_info_layer').hide(); */
                    container.find('.content>div').hide();
                    container.find('.d3_legend').removeClass('active');

                    if (map.getLayer('ref_unhcr_merged_simple_labels')) {

                        map.removeLayer('ref_unhcr_merged_simple_labels');
                        //  $('form.show_hide_labels_form input').prop('checked', false)

                    }


                    if (map.getLayer('ref_unhcr_polygons'))
                        map.removeLayer('ref_unhcr_polygons');
                }
            }




            if (map.getStyle().layers.filter(d => d.id == layer_id + '_labels').length > 0)
                map.removeLayer(layer_id + '_labels');




            t_layer.activated_query = false;
            t_layer.active_param = null;



            if (t_layer.raster == true) {
                if (t_layer._layer.id == 'ghs_africa_250m_bigger_1_5') {
                    $('.analysis_layer.' + layer_id).find('.raster_scale_container,.raster_legend_classif,.row.extra').hide();

                } else {
                    if (t_layer._layer.id == 'distance_to_grid') {
                        $('.distance_raster_legend_classif').hide();
                    }
                }
            }
        }
    },
    create_d3_legend: function (type, t_layer) {
        console.warn(arguments)

        var param_name = t_layer.active_param;

        if (type == 'africa_platform') {
            var layer_id = t_layer.layer_id;
        } else {
            var layer_id = t_layer.id;
            if (layer_id.includes("_geojson"))
                var layer_id = layer_id.slice(0, -8);


        }

        switch (t_layer.id) {

            case 'ref_unhcr_merged_simple': var $container = $('div.unhcr_div.ref_unhcr_merged_simple');
                break;
            case 'ref_unhcr_merged': var $container = $('div.unhcr_div.ref_unhcr_merged');
                break;
            default: var $container = $('.analysis_layer.' + layer_id);

        }






        $container.find('.d3_legend').eq(0).addClass('active');
        if (t_layer.id == 'taf_bess') {

            map.setFilter('taf_bess', null);

            var params = t_layer.params_description;
            console.warn(params, param_name)

            var params_description = params.filter(function (d) {
                console.log(d.param_name + ' ' + param_name)
                return d.param_name == param_name
            })[0];

            $('.analysis_layer.' + layer_id).find('.param_description').empty().append(params_description.long_title)
            if (params_description.more_info_html) {
                var $extended_description = $('.analysis_layer.' + layer_id).find('.extended_description');
                console.log(params_description.more_info_html)
                $extended_description.html(params_description.more_info_html).hide();

            }

            var t_symbol = t_layer.params_symbology.filter(function (d) {
                return d.param_name == param_name
            });

            var border_color = t_layer.border ? t_layer.border : 'white';
            var html = ''
            t_symbol[0].legend.forEach((d, i) => {

                html += `<div class="row" code=${i}><div class="col-2"><div class="symbol" style="outline:1px solid ${border_color};background:${d.color};"> </div></div><div class="col-8">${d.val}</div></div>`
            });
            console.log(html);
            $container.find('.legend').html(html);

            function filter() {
                var present_val = [];
                var f = ['in', param_name + '_class'];

                $container.find('.legend .row').each(function () {
                    console.log($(this))
                    if (!$(this).hasClass('off')) {
                        present_val.push(+$(this).attr('code'));
                        f.push(+$(this).attr('code'));

                    }
                });



                console.log(f, present_val);
                map.setFilter('taf_bess', f);
            }

            $container.find('.legend .row').on('click', function () {
                $(this).toggleClass('off');
                filter();


            });

            return false
        }


        console.log(arguments);

        $container.find('.d3_legend').show();

        if ($container.find('.d3_legend svg g').length > 0)
            $container.find('.d3_legend svg').empty();


        if (type == 'analysis') {


            var params = t_layer.params_description;
            console.warn(t_layer)
            console.warn(params, param_name)

            var params_description = params.filter(function (d) {
                console.log(d.param_name + ' ' + param_name)
                return d.param_name == param_name
            })[0];
            console.log(params_description)
            var el = $container.find('.param_description').eq(0);

            el.empty().append(params_description.long_title)
            if (params_description.more_info_html) {
                $container.find('.extended_description').eq(0).html(params_description.more_info_html)

                if (layer_id.includes('unhcr')) {
                    $container.find('.extended_description').eq(0).show();
                }



            }
            else {
                $container.find('.extended_description').eq(0).hide();
            }




            switch (t_layer.id) {

                case 'ref_unhcr_merged_simple': var svg = d3.select('svg.circles_svg.ref_unhcr_merged_simple'); break;
                case 'ref_unhcr_merged': var svg = d3.select('svg.circles_svg.ref_unhcr_merged'); break;
                default: var svg = d3.select('.analysis_layer.' + layer_id + ' svg.circles_svg');
            }


            console.log(svg)
            svg.style('display', 'block');

            var t_symbol = t_layer.params_symbology.filter(function (d) {
                return d.param_name == param_name
            });
            console.log(t_symbol)


            var scale_val = t_symbol[0].legend;

            console.log(scale_val)
            if (params_description && params_description.type_param == 'range') {

                var width = $('.collapsible-header').eq(0).width();
                var buckets_l = scale_val.length;
                var unit_w = parseInt(width / buckets_l);
                var _domain = params_description._domain.map(d => d / 100);



                console.log(_domain)
                console.warn(t_symbol)

                var foreignObject_height = 35;


                if (t_symbol[0].orienation == 'vertical') {
                    var d3_legend_height = buckets_l * foreignObject_height;

                    svg.attr("height", (buckets_l * foreignObject_height));

                    var unit_w = parseInt(width / buckets_l);
                    var shape_height = 15;
                } else {

                    svg.attr("height", 70);
                    var unit_w = 70;
                    var shape_height = 15;
                }
                var linearSize = d3.scaleQuantize().domain(_domain)
                    .range(params_description._range);
                console.warn(params_description._range)
                svg.append("g")
                    .attr("class", "circles_g")

                    .attr("transform", "translate(30, 10)");


                var legendSize = d3.legendSize()
                    .scale(linearSize);

                legendSize.shape('circle')
                    .shapePadding(8)
                    .labelOffset(20)
                    .orient(t_symbol[0].orienation);



                legendSize
                    .labelFormat('.03f')
                    .labels(scale_val.map(function (d) {
                        return d.val
                    }))
                    .on("cellclick", function (d, i) {

                        if (d3.select(this).classed('off') == true) {
                            var action = 'adding';
                            d3.select(this).classed('off', false);
                        } else {
                            var action = 'removing';
                            d3.select(this).classed('off', true);
                        }


                        var present_val = [];
                        var arr_class = [];
                        console.log(scale_val)


                        var filter = map.getFilter(layer_id);
                        var f = ['in', param_name + '_class'];



                        $container.find('svg.' + t_layer.id + ' g.cell').each(function (i, d) {
                            console.warn(d, i)

                            if (!$(this).hasClass('off')) {

                                if (!scale_val[i].val)
                                    debugger

                                console.log(scale_val[i].val)

                                console.log('we are dealing with range values??')
                                f.push(i)


                                present_val.push({
                                    index: i,
                                    val: scale_val[i].val
                                });
                            } else {
                                present_val.push({
                                    index: i,
                                    val: null
                                });
                            }
                        });
                        console.log(t_layer.active_filters)
                        console.log(t_layer.active_filters[1])
                        t_layer.active_filters[1]['param_filter'] = f;
                        var all_filters = ["all"]

                        for (var p in t_layer.active_filters) {
                            var key = Object.keys(t_layer.active_filters[p]);
                            if (t_layer.active_filters[p][key]) {
                                all_filters.push(t_layer.active_filters[p][key])
                            }

                        }
                        console.log(all_filters)

                        map.setFilter(layer_id, all_filters);
                        if (map.getLayer(layer_id + '_labels')) {
                            map.setFilter(layer_id + '_labels', all_filters);

                        }

                        console.log(t_layer.active_filters)

                    })

                console.log(svg)
                svg.select(".circles_g")
                    .call(legendSize);

                svg.selectAll('.legendCells circle')
                    .attr('fill', function (d, i) {
                        return scale_val[i].color;
                    })
                svg.selectAll('.legendCells text.label')
                    .attr('fill', function (d, i) {
                        return scale_val[i].color;
                    })

                if (t_layer.border) {


                    d3.selectAll('.' + t_layer.id + ' circle')
                        .style('stroke', t_layer.border)
                        .style('stroke-width', 1)
                }
                console.warn(scale_val)
                d3.selectAll('.circles_svg.clustered_pop .cell').each(function () {
                    console.warn(d3.select(this).node().getBBox())
                })

                console.log(svg.select('.circles_g').node().getBoundingClientRect().height + 10 + '  as circles_g height ')
                svg.attr('height', svg.select('.circles_g').node().getBoundingClientRect().height + 20);

            }
            else {


                console.warn('we are on a CATEGORICAL parameter (EX: category_l , capex_class,etc) No different circle sizes!!')

                if (t_layer._type == 'line')
                    var _symbol = d3.symbolSquare;

                if (t_layer._type == 'polygon')
                    var _symbol = d3.symbolSquare;

                if (t_layer._type == 'circle') {

                    if (t_layer.id == 'schools')
                        var _symbol = d3.symbolStar;
                    else
                        var _symbol = d3.symbolCircle;
                }


                if (t_layer._type == 'marker') {
                    console.log(t_symbol)
                    if (t_symbol[0].symbol_type == 'square')
                        var _symbol = d3.symbolSquare;

                    if (t_symbol[0].symbol_type == 'diamond')
                        var _symbol = d3.symbolDiamond;
                }


                console.log(scale_val, _symbol)
                var _colors = d3.scaleOrdinal(scale_val.map(function (d) {
                    return d.color
                })).domain(scale_val.map(function (d) {
                    return d.val
                }));

                svg.style('display', 'block');

                var width = $('.collapsible-header').eq(0).width();
                var buckets_l = scale_val.length;

                if (t_symbol[0].orienation == 'vertical') {
                    var unit_w = parseInt(width / buckets_l);
                    var shape_height = 15;
                } else {

                    svg.attr("height", 70);
                    var unit_w = 70;
                    var shape_height = 15;
                }



                svg.append("g")
                    .attr("class", "circles_g")
                    .attr("transform", "translate(10, 20)");

                var legendOrdinal = d3.legendColor();

                if (t_layer._type !== 'line')
                    legendOrdinal.shape("path", d3.symbol().type(_symbol).size(150)())

                if (layer_id == 'schools') {

                    t_layer.active_filters[0]['param_filter'] = null;
                    t_layer.cql_filter = null;

                    legendOrdinal.shapePadding(15)
                        .cellFilter(function (d) {
                            return d.label !== "e"
                        })
                        .orient(t_symbol[0].orienation)
                        .scale(_colors)
                }
                else {
                    legendOrdinal.shapePadding(20)
                        .cellFilter(function (d) {
                            return d.label !== "e"
                        })
                        .orient(t_symbol[0].orienation)
                        .scale(_colors)
                }

                if (t_layer._type == 'line')
                    legendOrdinal.shapeWidth(25)
                        .shapeHeight(6)
                else
                    legendOrdinal.shapeWidth(unit_w)
                        .shapeHeight(shape_height)

                var t_layer = app_data.layers_info.filter(function (d) {
                    return d.id == layer_id;
                })[0];
                console.warn(t_layer)




                switch (t_layer.id) {
                    case 'ref_unhcr_merged_simple': var $container = $('div.unhcr_div.ref_unhcr_merged_simple');
                        break;
                    case 'ref_unhcr_merged': var $container = $('div.unhcr_div.ref_unhcr_merged');
                        break;
                    default: var $container = $('.analysis_layer.' + layer_id);

                }

                legendOrdinal.labelOffset(10)
                    .on("cellclick", function (d, i) {

                        if (d3.select(this).classed('off') == true) {
                            var action = 'adding';
                            d3.select(this).classed('off', false);
                        } else {
                            var action = 'removing';
                            d3.select(this).classed('off', true);
                        }

                        var present_val = [];


                        console.log(params_description.subtype_param)

                        $container.find('svg g.cell').each(function (i, d) {




                            if ($(this).hasClass('off') == false) {


                                if (!params_description.subtype_param || params_description.subtype_param == 'range') {

                                    present_val.push(i)
                                } else {
                                    if (params_description.subtype_param == 'literal') {
                                        console.log('NON categorical index' + scale_val[i].code)
                                        present_val.push(scale_val[i].code);

                                    }
                                }
                            }
                        })
                        console.log(present_val)

                        t_layer.active_filters[0]['param_filter'] = present_val;

                        if (!t_layer.raster) {

                            console.info(param_name)
                            var c = param_name.split('_class');
                            var fs = [];
                            var f = [];
                            console.warn(c)
                            if (t_layer._type == 'refugee_camps') {

                                if (c.length == 1) {
                                    console.warn("there is no '_class' on param_name")
                                    var f = ['in', param_name + '_class'];
                                } else {
                                    var f = ['in', param_name];
                                }

                                for (var p in present_val) {
                                    fs.push(present_val[p]);
                                    f.push(present_val[p])
                                }

                                var $ref = $('.marker.refugee_camps');
                                $ref.hide();
                                var pos = null;
                                console.log(f)
                                $ref.each(function (i, d) {
                                    if (i == 0) {
                                        var prop = $(this).data();

                                        pos = prop.data_arr.indexOf(param_name);
                                        console.log(param_name, pos, prop.data_arr)
                                        console.warn(prop.data[pos], fs.indexOf(prop.data[pos].cat))
                                    }
                                    var prop = $(this).data();
                                    if (i < 2) {
                                    }


                                    if (fs.indexOf(prop.data[pos].cat) > -1) {
                                        $(this).show()
                                    }

                                });
                            } else {


                                var symb = t_layer.params_symbology.filter(d => d.param_name == param_name)[0];

                                if (symb.hasOwnProperty('using_class')) {

                                    console.warn('using TAF layer???')
                                    console.warn(symb)
                                    if (symb.using_class === true) {

                                        var f = ['in', param_name + '_class'];
                                        present_val.forEach(function (d, i) {
                                            f.push(+d);
                                            fs.push(+d);
                                        })
                                    }
                                    else {
                                        var f = ['in', param_name];
                                        present_val.forEach(function (d, i) {
                                            f.push(d);
                                            fs.push(d);
                                        })
                                    }




                                    console.log(f)
                                }
                                else {
                                    alert('missing using_class in symbology')
                                }



                            } //end not marker


                            if (t_layer._type !== 'line') {
                                console.log(t_layer)

                                console.warn(t_layer)
                                if (t_layer.id == 'taf_pumps_up') {
                                    var layers = ['taf_pumps_up', 'taf_pumps_down', 'taf_pumps_lines'];
                                    layers.forEach(function (this_id) {
                                        var t_layer = app_data.layers_info.filter(function (d) {
                                            return d.id == this_id;
                                        })[0];
                                        console.log(t_layer)

                                        var all_filters = ["all"];
                                        t_layer.active_filters[1]['param_filter'] = f;
                                        for (var p in t_layer.active_filters) {
                                            var key = Object.keys(t_layer.active_filters[p]);
                                            if (t_layer.active_filters[p][key]) {
                                                all_filters.push(t_layer.active_filters[p][key])
                                            }


                                        }

                                        console.log(all_filters)

                                        map.setFilter(this_id, all_filters);
                                    })
                                }
                                else {
                                    var all_filters = ["all"];
                                    t_layer.active_filters[1]['param_filter'] = f;
                                    for (var p in t_layer.active_filters) {
                                        var key = Object.keys(t_layer.active_filters[p]);
                                        if (t_layer.active_filters[p][key]) {
                                            all_filters.push(t_layer.active_filters[p][key])
                                        }

                                    }
                                    console.log(all_filters)

                                    if (map.getLayer(layer_id + '_labels')) {
                                        map.setFilter(layer_id + '_labels', all_filters);

                                    }

                                    map.setFilter(layer_id, all_filters);
                                }

                                console.info(f, fs)

                                if (t_layer.id == 'refugee_camps') {

                                    var $ref = $('.marker.refugee_camps');
                                    $ref.hide();
                                    var pos = null;
                                    console.log(fs)
                                    $ref.each(function (i, d) {
                                        if (i == 0) {
                                            var prop = $(this).data();

                                            pos = prop.data_arr.indexOf(param_name);
                                            console.log(param_name, pos, prop.data_arr)
                                            console.warn(prop.data[pos], fs.indexOf(prop.data[pos].cat))
                                        }

                                        var prop = $(this).data();
                                        if (fs.indexOf(prop.data[pos].cat) > -1) {
                                            $(this).show()

                                        }

                                    });

                                }
                            } else {
                                console.warn(f)

                                if (map.getLayer(layer_id + '_labels')) {
                                    map.setFilter(layer_id + '_labels', all_filters);

                                }
                                map.setFilter(t_layer.id, f);
                            }
                        } //end no RASTER
                        else {

                            console.warn(present_val)

                            this_app.set_raster_cql(t_layer);


                        }
                    }) //end cellclick


                setTimeout(function () {


                    svg.selectAll(".cell").attr('code', function (d, i) {
                        console.log(scale_val[i])
                        return scale_val[i].val
                    });


                }, 900)


                if (t_symbol[0].orienation == 'vertical') {


                    svg.select(".circles_g")
                        .attr("width", width)
                        .call(legendOrdinal);
                }






                if (t_layer.border) {
                    console.log(d3.selectAll('.' + t_layer.id + ' path'))
                    d3.selectAll('.' + t_layer.id + ' path')
                        .style('stroke', t_layer.border)
                        .style('stroke-width', t_layer.border_width)
                }

                console.info(svg.select('.circles_g').node().getBoundingClientRect().height + ' as circles_g height')



                if (layer_id == 'refugee_camps')
                    svg.attr('height', svg.select('.circles_g').node().getBoundingClientRect().height + 35);
                else
                    svg.attr('height', svg.select('.circles_g').node().getBoundingClientRect().height + 30);


            } //end not lcoe_h ???



        } else {
            console.log(t_layer)
            var scale_val = t_layer.energy_legend;
            console.info(scale_val)

            var svg = d3.select('.' + layer_id + ' .d3_legend svg');
            console.warn('.' + layer_id + ' .d3_legend')
            $('.' + layer_id + ' .d3_legend').show();
            svg.style('display', 'block');

            if (!scale_val) {
                $('.' + layer_id + ' .d3_legend').append(t_layer.legend.safe_value)
                return false;
            }

            var shape_h = 15;
            var label_offset = 10;
            var width = $('.' + layer_id + ' .card-title').width();
            var height = (shape_h + label_offset) * 2;

            console.info(width)
            var padding = 3;
            var buckets_l = scale_val.length;


            svg.attr("width", width).attr('height', height);

            var unit_w = (width - (padding * buckets_l)) / buckets_l;

            var colorLegendG__3 = svg
                .append("g")
                .attr("width", width)
                .attr("class", "color-legend")
                .attr("transform", "translate(0, 10)")

            var colorScale__3 = d3.scaleOrdinal(scale_val.map(function (d) {
                return d.color
            })).domain(scale_val.map(function (d) {
                return d.val
            }));

            console.info(unit_w)
            var colorLegend__3 = d3
                .legendColor()
                .scale(colorScale__3)
                .shapePadding(padding)
                .shapeWidth(unit_w)
                .shapeHeight(shape_h)
                .labelOffset(label_offset)
                .orient('horizontal')
                .useClass(false)
                .on("cellclick", function (d, e) {
                    console.log(d, e)
                    var active = d.active ? false : true,
                        newOpacity = active ? 0 : 1
                    d3.select(this)
                        .transition()
                        .duration(100)
                        .style("opacity", newOpacity)
                    d.active = active
                })
            colorLegendG__3.call(colorLegend__3);

        }

    },
    generate_selects_legend: function (container, t_layer) {

        console.trace()
        if (t_layer.not_styled)
            return;
        var layer_id = t_layer.id;
        console.log(t_layer)
        if (container.find('.params_select').length > 0) {

            if (container.find('.params_select').length > 1)
                var $selContainer = container.find('.params_select').eq(0);
            else
                var $selContainer = container.find('.params_select');

            if ($selContainer.find('select').length == 0) {
                var sel = '<select data-placeholder="Parameter to symbolize" class="form-select" onchange="handleSelectChange(this)">';

                t_layer.params_arr.forEach(function (d, i) {

                    var t = t_layer.params_description.filter(function (info) {
                        console.log(d, info.param_name)
                        return info.param_name == d
                    });
                    console.warn(t)



                    if (d == t_layer.default_param) {
                        sel += '<option selected value=' + d + '>' + t[0].short_title + '</option>'
                    } else {
                        sel += '<option value=' + d + '>' + t[0].short_title + '</option>'
                    }

                });
                sel += '</select>';



                console.warn(sel)
                $selContainer.html(sel);
                t_layer.activated_query = true;
                t_layer.active_param = t_layer.default_param;


                if ($('.analysis_description .list span.' + layer_id).length > 0) {
                    $('.analysis_description .list span.' + layer_id).remove();
                }
                $('.analysis_description .list').append('<span class="' + layer_id + '">' + layer_id + ':' + t_layer.active_param + '</span>')
                console.log(' marker or not, create legend')
                this_app.create_d3_legend('analysis', t_layer);



                $selContainer.on('mousedown', function () {
                    if (container.find('.circles_svg').is(':hidden')) {
                        $(this).addClass('active_chosen');
                    } else {
                        $(this).removeClass('active_chosen');
                    }



                })

            } else {

                console.info('form is already done, just crating the legend after highlighting on again');

                var $chosen = container.find('.params_select select');
                var param_name = $chosen.find('option:selected').attr('value');
                console.log(param_name)
                console.warn(t_layer)

                if (t_layer.id !== 'refugee_camps_geojson') {

                    if (t_layer.mapbox_paint_style) {
                        var mapbox_style = t_layer.mapbox_paint_style.filter(function (d) {
                            return d.param == param_name
                        })[0];


                        console.info(mapbox_style);

                        for (var p in mapbox_style.style) {

                            map.setPaintProperty(layer_id, p, mapbox_style.style[p]);
                        }
                    } else {
                        console.warn(t_layer)
                        console.log('no style associated to ' + layer_id)

                    }
                }

                if (!param_name) {
                    var param_name = t_layer.default_param;
                }
                console.warn(param_name)
                t_layer.active_param = param_name;

                this_app.create_d3_legend('analysis', t_layer);
            }




        }
    },
    fx: {
        /*   })
               $.ajax({

                   url: 'https://pere.gis-ninja.eu/akp_energy/adm_tables_pdf_info_dev_3_taf_new.php',

                   type: 'GET',

                   data: this_app.queried_adm_params,

                   dataType: 'jsonp', */
        //  success: function (resp) {
        highlight_adm_unit: function (code_name, code_value, adding) {


            switch (code_name) {
                case 'adm0_code':
                    var highlight_layer = 'gaul_level_0_highlighted';
                    var highlight_layer_line = 'gaul_level_0_highlighted_line';
                    break;
                case 'adm1_code':
                    var highlight_layer = 'gaul_level_1_highlighted';
                    var highlight_layer_line = 'gaul_level_1_highlighted_line';
                    break;
                case 'adm2_code':
                    var highlight_layer = 'gaul_level_2_highlighted';
                    var highlight_layer_line = 'gaul_level_2_highlighted_line';
                    break;
                default: break;

            }

            if (!map.getLayer(highlight_layer)) {


                map.addLayer(window[highlight_layer]);
                map.addLayer(window[highlight_layer_line]);



            }




            $("#map").busyLoad("hide")

            if (adding) {
                map.setPaintProperty(highlight_layer, 'fill-opacity', [
                    'match',
                    ['get', code_name],
                    code_value
                    , // If in the array
                    1, // Set fill-opacity to 1
                    0 // Otherwise, set to 0.5
                ]);

                setTimeout(function () {
                    map.setPaintProperty(highlight_layer, 'fill-opacity', 0);
                }, 700)
            }
            if (this_app.sel_highlighted_arr.length > 0) {
                map.setPaintProperty(highlight_layer_line, 'line-opacity', [
                    'match',
                    ['get', code_name],
                    this_app.sel_highlighted_arr
                    , // If in the array
                    1, // Set fill-opacity to 1
                    0 // Otherwise, set to 0.5
                ]);
            }
            else {
                map.setPaintProperty(highlight_layer_line, 'line-opacity', 0);
            }
        },

        printPdf: function (resp) {
            console.log(resp)
            console.log(this_app.this_pdf)
            this_app.this_pdf.content.push({
                width: '*',
                text: this_app.this_pdf.stats_pdf_txt,
                style: this_app.this_pdf.stats_pdf_style,
                alignment: 'center'

            })
            var table_info = {

                layout: 'lightHorizontalLines',
                table: {

                    widths: ['*', '*', '*', '*', '*', '*', '*'],
                    layout: {
                        fillColor: function (rowIndex, node, columnIndex) {
                            return (rowIndex % 2 === 0) ? '#cec9c9' : null;
                        }
                    },
                    body: [

                        ['Admin. name', 'Off-grid health centres', 'LCOEH', 'CAPEX soft', 'Electric demand', 'PV', 'BAT']
                    ]
                }
            };

            var params_symbology = app_data.layers_info.filter(function (d) {
                if (d.id == 'hospitals')
                    return d
            })[0].params_symbology;
            console.warn(params_symbology)
            var lcoe_h_symbology = params_symbology.filter(function (d) {
                return d.param_name == 'lcoeh_soft'
            })[0].legend;

            var capex_symbology = params_symbology.filter(function (d) {
                return d.param_name == 'capex_soft'
            })[0].legend;

            var eldemand_symbology = params_symbology.filter(function (d) {
                return d.param_name == 'eldemand'
            })[0].legend;

            var pv_symbology = params_symbology.filter(function (d) {
                return d.param_name == 'pv'
            })[0].legend;

            var bat_symbology = params_symbology.filter(function (d) {
                return d.param_name == 'bat'
            })[0].legend;

            for (var p in resp.data_arr) {
                var info = resp.data_arr[p];

                var arr = [];

                if (info[this_app.queried_adm_params.to_query + '_name']) {

                    if (this_app.sel_f_admin[this_app.queried_adm_params.to_query + '_code'] == info[this_app.queried_adm_params.to_query + '_code']) {

                        if (info.lcoeh_soft || info.capex_soft || info.eldemand || info.pv || info.bat) {
                            var sel = {
                                text: info[this_app.queried_adm_params.to_query + '_name'],
                                fillColor: '#86d9ff'
                            }
                            console.info(sel);

                            table_info.table.body.push([sel,
                                numberWithCommas(info.hc_out_grid), lcoe_h_symbology[info.lcoeh_soft.cat].text + ' (' + info.lcoeh_soft.max + ')',
                                capex_symbology[info.capex_soft.cat].text + ' (' + info.capex_soft.max + ')',
                                eldemand_symbology[info.eldemand.cat].text + ' (' + info.eldemand.max + ')',
                                pv_symbology[info.pv.cat].text + ' (' + info.pv.max + ')',
                                bat_symbology[info.bat.cat].text + ' (' + info.bat.max + ')',
                            ]);
                        }
                    } else {
                        if (info.lcoeh_soft || info.capex_soft || info.eldemand || info.pv || info.bat) {

                            table_info.table.body.push([info[this_app.queried_adm_params.to_query + '_name'],
                            numberWithCommas(info.hc_out_grid), lcoe_h_symbology[info.lcoeh_soft.cat].text + ' (' + info.lcoeh_soft.max + ')',
                            capex_symbology[info.capex_soft.cat].text + ' (' + info.capex_soft.max + ')',
                            eldemand_symbology[info.eldemand.cat].text + ' (' + info.eldemand.max + ')',
                            pv_symbology[info.pv.cat].text + ' (' + info.pv.max + ')',
                            bat_symbology[info.bat.cat].text + ' (' + info.bat.max + ')',
                            ]);
                        }
                    }

                }

            }
            console.info(table_info)

            this_app.this_pdf.content.push(table_info);

            this_app.this_pdf.content.unshift({
                width: '*',
                text: this_app.this_pdf.adm_full_text,
                style: this_app.this_pdf.stats_pdf_style,
                alignment: 'center'
            });

            $("body").busyLoad("hide", this_app.generating_file_busy_option);
            this_app.print_on_pdf = true;

            $('.print_map').trigger('click');

        },
        sort_by_param: function (param, order) {


            return function (a, b) {
                console.info(order)
                if (order === 'desc') {

                    return a[param] > b[param] ? -1 : 1;
                }
                else {

                    return a[param] < b[param] ? -1 : 1;
                }


            }

        },

        fillCountry_stats_lateralBar: function (id) {

            console.info(this_app.all_adm_tables)
            var arr = this_app.all_adm_tables.data_arr;


            /* this_app.d3_stuff.sel_f = arr.filter(d => d.adm0_code == parseInt(id))[0];
            console.log(this_app.d3_stuff.sel_f) */

            var t_country = arr.filter(d => d.adm0_code == id)[0];
            console.info(t_country);
            $('.explore_countries_menu .country_title').html(t_country.adm0_name);
            var ps = [
                'hc_in_grid', 'hc_out_grid', 'schools', 'refugee_camps', 'power_plants', 'electric_grid', 'ghs_density', 'ghs_sum_pop',
                'taf_bess', 'taf_geotermal_ungrouped', 'taf_hydrogen_grouped', 'taf_pumps_up', 'taf_solar', 'taf_wind'
            ];
            var html = '';

            ps.forEach((d) => {

                var l = app_data.layers_info.filter((l) => l.id && l.id == d);
                if (l && l.length > 0) {
                    var txt = l[0].title;
                }
                else {
                    switch (d) {
                        case 'hc_in_grid':

                            var txt = 'In-grid health centres';

                            break;

                        case 'hc_out_grid':
                            var txt = 'Off-grid health centres';

                            break;

                        case 'schools':

                            var txt = 'Schools';

                            break;
                        case 'refugee_camps':
                            var txt = 'Refugee settlements';

                            break;
                        case 'power_plants':
                            var txt = 'Power plants';

                            break;
                        case 'electric_grid':
                            var txt = 'Electric grid (Km)';

                            break;
                        case 'ghs_density':
                            var txt = 'Off-grid population density';

                            break;
                        case 'ghs_sum_pop':
                            var txt = 'Off-grid population';

                            break;
                        default:
                            break;
                    }
                }
                arr.sort(this_app.fx.sort_by_param(d, 'desc')).forEach((element, i) => {


                    element[d + '_position'] = i;

                });


                html += '<div class="row valign-wrapper">';

                html += '<span class="title col s7">' + txt + '</span>';
                if (t_country[d]) {
                    console.log(d, t_country[d])
                    html += '<span class="counts col s5">' + numberWithCommas(t_country[d]) + '</span>';
                }
                else {

                    html += '<span class="counts col s5">0</span></span>';
                }
                html += '</div>';
            })



            console.log(html)


            $('.explore_countries_menu .country_data').html(html);

        }
    }

};

this_app.datatables_headers = [{
    code: 'country',
    text: 'Adm. Name',
    short: 'Adm. Name',
    repeated: true,
    out_hc: true,
    i: 0
},
{
    code: 'country',
    text: 'Adm. Name',
    short: 'Adm. Name',
    out_hc: true,
    i: 1

},
{
    code: 'schools',
    text: 'Schools',
    short: 'Schools',


    i: 2

},
{
    code: 'power_plants',
    text: 'Power plants',
    short: 'Power plants',
    i: 2
},
{
    code: 'hc_in_grid',
    text: 'In-grid health centres',
    short: 'In-grid health centres',
    i: 3
},
{
    code: 'hc_out_grid',
    text: 'Out-grid health centres',
    short: 'Out-grid health centres',

    i: 4
},
{
    code: 'hc_out_grid',
    text: 'Out-grid health centres',
    short: 'Out-grid health centres',
    i: 5,
    out_hc: true,
    repeated: true
},
{
    code: 'refugee_camps',
    text: 'Refugee settlements',
    short: 'Refugee settlements',
    i: 6
},
{
    code: 'electric_grid',
    text: 'Electric grid (Km)',
    short: 'Electric grid (km)',
    i: 7
},
{
    code: 'ghs_density',
    text: 'Off-grid density (Hab/250m)',
    short: 'Off-grid density (Hab/250m)',
    i: 8
},
{
    code: 'ghs_density',
    text: 'Off-grid density',
    short: 'Off-grid density',

    repeated: true
},
{
    code: 'ghs_sum_pop',
    text: 'Off-grid population',
    short: 'Off-grid population',


},
{
    code: 'ghs_sum_pop',
    text: 'Off-grid population',
    short: 'Off-grid population',

    repeated: true
}

].map((d, i) => {
    d.i = i;
    return d;
})

function point_project(d) {

    if (d.x) {
        return map.project(new mapboxgl.LngLat(+d.x, +d.y));
    } else {

        return map.project(new mapboxgl.LngLat(+d[0], +d[1]));
    }
}


var base_url = window.location.origin;
switch (window.location.origin) {
    case 'https://pere.gis-ninja.eu':

        var my_web = true;
        var json_path = './json';

        break;

    default:
        var my_web = false;
        var json_path = './energy_json';
}


Object.defineProperty(this_app, 'raise', {
    get: function () {
        return this._raise;
    },
    set: function (value) {
        if (value !== this._raise) {
            debugger; // Trigger debugger when raise changes
            this._raise = value;
        }
    }
});

Object.defineProperty(this_app.active_admin_filter, 'code_val', {
    get: function () {
        return this._code_val;
    },
    set: function (value) {

        if (value !== this._code_val) {
            console.warn('this_app.active_admin_filter.code_val changed to ' + value)
            this._code_val = value;
        }
    }
});


console.warn(this_app.gaul0_global_info)











function toDataURL(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.onload = function () {
        var reader = new FileReader();
        reader.onloadend = function () {
            callback(reader.result);
        }
        reader.readAsDataURL(xhr.response);
    };
    xhr.open('GET', url);
    xhr.responseType = 'blob';
    xhr.send();
}
pdfMake.fonts = {
    OpenSans: {
        normal: 'https://fonts.gstatic.com/s/opensans/v27/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgsiH0B4gaVQUwaEQbjA.woff',
        OpenSansBolded: 'https://fonts.gstatic.com/s/opensans/v27/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgsiH0B4gaVQUwaEQbjA.woff',
        OpenSansItalic: 'https://fonts.gstatic.com/s/opensans/v27/memSYaGs126MiZpBA-UvWbX2vVnXBbObj2OVZyOOSr4dVJWUgsjZ0B4iaVQUwaEQbjB_mQ.woff'
    },
};


this_app.this_pdf = {
    with_map: false,
    pageSize: 'LETTER',

    styles: {

        adm0_style: {
            fontSize: 25,
            "font": "OpenSans",
            OpenSansBolded: true,
            margin: [0, 0, 0, 20],
        },
        adm1_style: {
            fontSize: 20,
            OpenSansBolded: true,
            margin: [0, 0, 0, 20],
            "font": "OpenSans",
        },
        adm2_style: {
            fontSize: 18,
            OpenSansBolded: true,
            margin: [0, 0, 0, 20],
            "font": "OpenSans"
        },
        "general_class": {
            "fontSize": 9,
            "color": "white",
            "font": "OpenSans",

            "margin": [
                10,
                5,
                15,
                5
            ]
        },
        tableHeader: {
            fontSize: 9,
            OpenSansBolded: true,
            "margin": [0, 2, 0, 2],
            "font": "OpenSans",
            OpenSansBolded: true,
            "color": "#f35f16"
        },

        alert: {
            fontSize: 17,
            margin: [0, 7, 0, 0],
            "font": "OpenSans",
            color: '#f44336'
        },
        "counts": {
            "fontSize": 12,
            "alignment": "left",
            "font": "OpenSans",
            color: '#f26830',
            margin: [0, 2]
        },
        "filled_cell": {
            "fontSize": 14,
            "color": "white",
            "font": "OpenSans",
            "fillColor": '#d17506',


            "margin": [
                7,
                4,
                6,
                4
            ]
        },
        "summary_counts": {
            "alignment": "center",
            "font": "OpenSans",
            color: '#a8328d',
            "margin": [
                7,
                4,
                6,
                4
            ]
        },
        "cat": {
            "fontSize": 8,
            "color": "white",
            "font": "OpenSans",
            "margin": [8, 2, 0, 3]
        },
        short_title: {
            "font": "OpenSans",
            fontSize: 9,
            OpenSansItalic: true,
            alignment: 'left',
            OpenSansBolded: true,
            "margin": [0, 10, 0, 0]

        },
        long_title: {
            fontSize: 6,
            color: '#52504b',
            "font": "OpenSans",
            alignment: 'left',
            margin: [0, 5, 0, 5]

        },
        feature_name: {
            fontSize: 15,
            "font": "OpenSans",
            margin: [0, 5, 0, 10]
        },
        stats_pdf_style_0: {
            fontSize: 15,
            color: '#eb9834',
            "font": "OpenSans",
            margin: [0, 5, 0, 5]
        },
        stats_pdf_style_1: {
            fontSize: 12,
            color: '#eb9834',
            "font": "OpenSans",
            margin: [0, 5, 0, 5]
        },
        stats_pdf_style_2: {
            fontSize: 12,
            color: '#eb9834',
            "font": "OpenSans",
            margin: [0, 5, 0, 5]
        },
        feature_highlight: {
            fontSize: 18,
            color: '#f79c2c',
            "font": "OpenSans",
            margin: [0, 12, 0, 0],
        },
        feature_val_highlight: {
            "fontSize": 25,
            "font": "OpenSans",
            OpenSansBolded: true,
            "color": "#a6141e",
            "margin": [
                0,
                12,
                0,
                0
            ]
        },
        bigger_value: {
            fillColor: '#f7a33b',
            color: 'black',
            "font": "OpenSans",
            OpenSansBolded: true
        },

        table_style: {
            fontSize: 7,
            color: '#140b00',
            "font": "OpenSans",
            margin: [0, 5, 0, 5]
        }
    },
    defaultStyle: {
        font: "OpenSans",
        fontSize: 7,
        margin: [0, 2, 0, 5],
        color: '#444'

    },
    content: [

    ],
    counts: [],
    extra: [{
        columns: [

        ]
    }]
};

function getUniqueFeatures(array, comparatorProperty) {
    var existingFeatureKeys = {};
    var uniqueFeatures = array.filter(function (el) {
        if (existingFeatureKeys[el.properties[comparatorProperty]]) {
            return false;
        } else {
            existingFeatureKeys[el.properties[comparatorProperty]] = true;
            return true;
        }
    });
    return uniqueFeatures;
}

function generate_layer_li(layer) {
    var html = `<li class="main_layer ${layer.layer_id}"> <div class="card e_black">
    <div class="card-content">
    <span class="card-title activator">
        <div class="row">
            <div class="afr_layer_title col-9">${layer.title}</div>
            <div class="col-3 d-flex justify-content-end"><i class="bi highlight layers bi-layers-half right"></i><i class="bi bi-brightness-high-fill opacity"></i></div>
        </div>
    </span>
    <div class="card-content-content"> `


    if (layer.layer_id !== 'elnid') {

        html += '<div class="d3_legend"><svg></svg></div><div class="range-wrap"><div class="range-value rangeV"></div><input class="range_z" type="range" min="0" max="100" value="100" step="1"></span></div>';
    } else {
        html += '<div class="afr_layer_title col-9">' + layer.title + '</div><div class="col-3 d-flex justify-content-end"><i class="bi highlight layers bi-layers-half right"></i><i class="bi bi-brightness-high-fill opacity"></i></div></div>';
        html += '<div class="d3_legend">' + layer.legend.value + '</div><div class="range-wrap"><div class="range-value rangeV"></div><input class="range_z" type="range" min="0" max="100" value="100" step="1"></span></div>';
    }


    html += `</div>
    </div>

    </li>`



    console.log(html)
    return html
}

function construct_marker_scale_val(geojson, param, t_layer, data) {

    var index = 0;
    var data = geojson.features.reduce(function (memo, d, i) {
        var prop = d.properties;

        var _p = prop[param];

        if (memo.type_arr.indexOf(_p) == -1) {
            memo.type_arr.push(_p);
            memo.type_objs.push({
                val: _p,
            })
            index++;

        }
        return memo;
    }, {
        type_arr: [],
        type_objs: []
    });

    console.warn(data)

    var pos = t_layer.params_arr.indexOf(param);

    if (pos == 0)
        var _scale = d3.scaleOrdinal(d3.schemeTableau10)
            .domain(d3.range(0, data.type_objs.length))

    if (pos == 1)
        var _scale = d3.scaleOrdinal(d3.schemeDark2)
            .domain(d3.range(0, data.type_objs.length))

    if (pos == 2)
        var _scale = d3.scaleOrdinal(d3.schemePaired)
            .domain(d3.range(0, data.type_objs.length))

    data.type_objs.forEach(function (d, i) {
        d.color = d3.rgb(_scale(i)).formatHex()
    })

    data._scale = _scale;

    return data;

}

function repaint_geojson(t_layer) {
    console.log(arguments)

    var layer_id = t_layer.id;
    var param = t_layer.active_param;

    if (!param) {
        var param = t_layer.default_param
    }


    var geojson = t_layer.geo_data;

    var scale_obj = t_layer.params_symbology.filter(function (d) {

        return d.param_name == param;
    })[0];

    console.warn(scale_obj)

    var legend = scale_obj.legend;


    $('.marker.refugee_camps').each(function (i, d) {
        var data = $(this).data();
        var pos = data.data_arr.indexOf(param);
        if (i < 3) {
            console.log(data.data[pos]);
        }
        $(this).find('path').attr('style', 'fill:' + data.data[pos].color)
    })



}

function draw_geojson2(t_layer, already_present) {
    d3.json(t_layer.geo_data).then(function (geojson) {

        if (!t_layer.active_param) {
            t_layer.active_param = t_layer.default_param;
        }
        console.warn(t_layer.params_symbology);
        var t_symbol = t_layer.params_symbology.filter(function (d) {
            return d.param_name == t_layer.active_param
        })
        var _scale = t_symbol[0].legend;
        var buckets = geojson.features.length;
        console.warn(buckets)

        geojson.features.forEach(function (marker, index) {
            var properties = marker.properties;
            var el = document.createElement('div');
            el.className = 'marker refugee_camps';
            var m = document.querySelector(".diamond").cloneNode(true);

            m.style.display = 'block';
            el.setAttribute("id", properties.id);

            var f_prop = {
                id: properties.id,
                data_arr: [],
                data: [],
                adm_codes: [{ adm0_code: properties.adm0_code, adm1_code: properties.adm1_code, adm2_code: properties.adm2_code }]
            };
            for (var p in t_layer.params_symbology) {
                var s = t_layer.params_symbology[p];
                console.log(s)
                var param = s['param_name'];
                f_prop.data_arr.push(param);
                var cat = properties[param + '_class'];

                var o = {
                    param: param,
                    value: properties[param],
                    cat: cat,
                    color: s['legend'][cat].color,
                    class_name: s['legend'][cat].text

                }
                f_prop.data.push(o);
            }
            console.log(f_prop)
            var cat = properties[t_layer.active_param + '_class'];
            var color = _scale[cat].color;


            el.appendChild(m);

            var children = el.childNodes;
            for (var i = 0; i < children.length; i++) {

                var path = children[i].childNodes[1].children[0];
                path.style.fill = color;

            }

            el.style.width = '70px'; // marker.properties.iconSize[0] + 'px';
            el.style.height = '70px'; //marker.properties.iconSize[1] + 'px';
            el.addEventListener('click', function () {
                var data = $(el).data();
            });
            new mapboxgl.Marker(el)
                .setLngLat(marker.geometry.coordinates)
                .addTo(map);
            $(el).data(f_prop);


            if (index == buckets - 1) {
                $('#map svg.diamond').show();
            }
        })
    })
}
function draw_geojson(info, t_layer, already_present) {
    console.log(arguments)

    var layer_id = t_layer.id;
    var param = t_layer.active_param;

    if (!param) {
        var param = t_layer.default_param

    }


    var geojson = t_layer.geo_data;
    console.log(geojson)
    var buckets = geojson.features.length;
    var scale_obj = t_layer.scale_val_arr.filter(function (d) {

        return d.scale_id == param;
    })[0];

    console.warn(scale_obj)
    if (scale_obj) {
        console.log('yes THIS scales for ' + param)
        var scale_val = scale_obj.scale_val;
    } else {
        console.log('NO THIS scale for ' + param)
        var _leg = construct_marker_scale_val(geojson, param, t_layer);
        console.log(_leg)
        t_layer.scale_val_arr.push({ scale_id: param, scale_val: _leg });

        var scale_val = _leg;

        console.log(t_layer)
    }

    if (already_present === true) {

        var t = d3.selectAll('.marker.' + layer_id);
        console.info(scale_val)
        t.each(function (d) {
            var _this = d3.select(this);
            var cat = d3.select(this).attr(param);

            var _i = scale_val.type_arr.indexOf(cat);
            var color = d3.rgb(scale_val._scale(_i)).formatHex();

            _this.select('path')
                .style('fill', color)
        })
    }

    if (already_present === false) {

        var x = t_layer.scale_val_arr.filter(function (d) {
            console.info(d.scale_id + '  ' + param)
            return d.scale_id == param;
        });
        console.log(x)
        var scale_val = x[0].scale_val;
        console.log(geojson)
        var marker_type = t_layer.marker_type;
        var params_to_plot = t_layer.params_arr;


        geojson.features.forEach(function (marker, index) {
            var properties = marker.properties;
            var el = document.createElement('div');
            el.className = 'marker ' + layer_id;
            var m = document.querySelector("." + marker_type).cloneNode(true);


            m.style.display = 'block';
            var cat = properties[param];
            var _i = scale_val.type_arr.indexOf(cat);
            var color = d3.rgb(scale_val._scale(_i)).formatHex();

            for (var p in params_to_plot) {
                el.setAttribute(params_to_plot[p], properties[params_to_plot[p]]);
            }
            el.setAttribute('cat_data', cat);
            el.setAttribute('index', index);
            el.setAttribute('_visible', true);

            el.appendChild(m);


            var children = el.childNodes;

            for (var i = 0; i < children.length; i++) {

                var path = children[i].childNodes[1].children[0];
                path.style.fill = color;

            }

            el.addEventListener('click', function () {
                console.log(marker.properties);
                console.log(color)
            });

            if (index < 2) {
                console.info($('.marker'))
                console.info($('.marker path'))
            }
            var markers = new mapboxgl.Marker(el)
                .setLngLat(marker.geometry.coordinates)
            markers.addTo(map);



        });
    }

}





console.warn(this_app)


function handleSelectChange(selectElement) {


    console.log($(selectElement))
    if ($(selectElement).parents().closest('.unhcr_div').length == 0) {
        var layer_id = $(selectElement).parents().closest('.analysis_layer').attr('class').split(' ')[1];
    }
    else {
        var layer_id = $(selectElement).parents().closest('.unhcr_div').attr('class').split(' ')[1];
    }

    var t_layer = app_data.layers_info.filter(d => d.id == layer_id)[0];

    this_app.reset_palette(t_layer);
    var param_name = selectElement.value;

    var pos = this_app.querying_popup_list.arr.indexOf(layer_id);
    if (pos == -1) {
        this_app.querying_popup_list.arr.push(layer_id);
        this_app.querying_popup_list.list.push({
            active_param: param_name,
            layer_id: layer_id,
            active: null
        });
    } else {
        this_app.querying_popup_list.list[pos].active_param = param_name;
    }

    var pos = this_app.querying_popup_list.arr.indexOf(layer_id);
    var container = $('.analysis_layer.' + layer_id);

    if (container.find('.layers._info').hasClass('on')) {
        this_app.querying_popup_list.list[pos].active = true;
    } else {

        this_app.querying_popup_list.list[pos].active = false;
    }

    container.find('.params_select').removeClass('active_chosen');
    var t_layer = app_data.layers_info.filter(function (d) {
        return d.id == layer_id;
    })[0];
    console.log(t_layer)
    t_layer.active_param = param_name;
    if (t_layer._type !== 'marker') {

        if (!t_layer.raster) {
            if (t_layer.active_filters && t_layer.active_filters[1].param_filter) {
                t_layer.active_filters[1].param_filter = null;
                var all_filters = ["all"];

                for (var p in t_layer.active_filters) {
                    var key = Object.keys(t_layer.active_filters[p]);
                    if (t_layer.active_filters[p][key]) {
                        all_filters.push(t_layer.active_filters[p][key])
                    }

                }
                console.log(all_filters)

                map.setFilter(layer_id, all_filters)
            }

            var layer = map.getLayer(layer_id)
            console.warn(layer)



            if (t_layer.mapbox_paint_style) {

                var mapbox_style = t_layer.mapbox_paint_style.filter(function (d) {
                    return d.param == param_name
                })[0];
                console.info(mapbox_style);


                for (var p in mapbox_style.style) {
                    console.warn(p)
                    map.setPaintProperty(layer_id, p, mapbox_style.style[p]);
                }
            }
        }
        else {
            console.info('dealing with RASTER school')
            var source = map.getSource('schools_source');


            console.warn(source.tiles[0])
            console.warn(source.tiles[0].includes('&styles=energy'))

            if (source.tiles[0].includes('&styles=energy:'))
                var tiles = source.tiles[0].split('&styles=energy:')[0];
            else
                var tiles = source.tiles[0];
            var new_tiles = tiles + '&styles=energy:schools_' + t_layer.active_param;
            map.getSource('schools_source').tiles = [new_tiles];
            map.style.sourceCaches['schools_source'].clearTiles();
            map.style.sourceCaches['schools_source'].update(map.transform);
        }
    } else {

        console.warn(t_layer)
        console.warn('MARKER, no mapbox_style associated to ' + layer_id)
        if (t_layer._type == 'marker') {
            repaint_geojson(t_layer)
        }
    }
    console.log(t_layer)
    this_app.create_d3_legend('analysis', t_layer);
}


this_app.show_hide_mapbox_layer = function (adm0_code, t_layer) {

    var layer_id = t_layer.id;

    var container = $('.analysis_layer.' + layer_id);

    this_app.generate_selects_legend(container, t_layer)

}


function generate_level_0(color, opacity) {
    base_layer = {
        "id": "gaul_level_0",
        "type": "fill",
        "source": "gaul_level_0_source",
        "source-layer": "gaul_level_0",

        'layout': {
            'visibility': 'visible'

        },
        'paint': {
            "fill-color": color,
            "fill-outline-color": "#072637",
            "fill-opacity": opacity
        }

    }
    this_app.base_layer = base_layer;
    return base_layer
}

this_app.create_raster_legend = function (layer_id, scale_type) {
    console.log(arguments)
    var all_container = d3.select('.' + layer_id + ' .raster_legend_svg_container');

    var $all_container = $('.' + layer_id + ' .' + scale_type + '_container');
    console.log('.' + layer_id + ' .' + scale_type + '_container', $all_container)

    if (layer_id == 'ghs_africa_250m_bigger_1_5') {
        var v_domain = [2, 50, 300, 1500, 445000].map(d => parseInt(d / 16));
    } else {
        var v_domain = [0, 5, 50, 100, 500, 1500, 3000, 4500,
            10000, 20000, 30000, 40000, 100000, 200000,
            300000, 400000, 445000
        ];

    }

    var svg_width = $('.analysis_layer.' + layer_id).width() - 10;
    $('.raster_legend_classif').show();
    $('svg.raster_legend_svg').width(svg_width)

    if (scale_type == 'another_scale') {
        $('.' + layer_id).find('.turbo_scale_container').show();
        $('.' + layer_id).find('.sinebow_scale_container').hide();


        var $stats_container = $('.' + layer_id + ' .turbo_stats_container');
        var svg = all_container.select('svg.turbo_svg');
        var $stats_c = $('.turbo_stats_container');
        $stats_container.empty();

        $all_container.show();
        var v_domain = [2, 50, 300, 1500, 445000];

        var c_scale = ['#f07f7f', '#eb9c0c', '#116fd4', '#9e00b3'];
        var c_scale2 = ['#f07f7f', '#ffeb3b', '#eb9c0c', '#eb9c0c', '#116fd4', '#116fd4', '#9e00b3', '#9e00b3'];

        var sorted_color_arr = c_scale.map(function (d, i) {

            var color = d3.rgb(c_scale[i]).formatHex();

            if (i == c_scale.length - 1) {
                var label = '> ' + v_domain[i]
            } else {
                var label = v_domain[i] + '-' + v_domain[i + 1];
            }
            return {
                rank: v_domain[i],
                code: v_domain[i],
                rank_color: color
            }
        })
        console.log(sorted_color_arr)
    }

    if (scale_type == 'orange_purple') {

        var svg = all_container.select('svg.raster_legend_svg');
        svg.attr('height', '80px')


        $('.' + layer_id).find('.raster_legend_svg,.raster_legend_svg_container').show();


        var colors = [];

        if (layer_id == 'ghs_africa_250m_bigger_1_5') {
            var v_domain = [50, 300, 1500]

            var c_scale = ['#f07f7f', '#eb9c0c', '#116fd4', '#9e00b3'];


            var c_scale2 = ['#f07f7f', '#ffeb3b', '#f0c981', '#eb9c0c', '#90c1f5', '#116fd4', '#ff4040', '#9e00b3'];
            var c_scale2 = ['#eef78d', '#ff9800', '#b2f705', '#4caf50', '#31f7f7', '#6716f2', '#FF1764', '#57266D'];

            $('.gradient').each(function (i, d) {
                console.log(i)
                $(this).css('background', function () {
                    if (i == 0) {
                        return 'linear-gradient(90deg, ' + c_scale2[0] + ' 0%, ' + c_scale2[1] + ' 100%)'
                    } else {
                        return 'linear-gradient(90deg, ' + c_scale2[i * 2] + ' 0%, ' + c_scale2[(i * 2) + 1] + ' 100%)'
                    }

                })
            });
            var v_domain = [2, 50, 300, 1500, 300000];
            var percents = [1, 4, 4.1, 24.99, 25, 59.99, 60, 94.99, 95] //.map(d=>d)


            var x_pos = [];
            for (var p in percents) {
                if (p % 2 === 0) {

                    x_pos.push((svg_width * percents[p]) / 100)
                }
            }

            var colorScale = d3.scaleLinear()
                .domain(v_domain)
                .range(c_scale)

            var w_step = svg_width / 3;
            console.warn(svg_width)
            var xScale = d3.scaleLinear()
                .domain(v_domain)
                .range(x_pos)

        }


        var defs = svg.append("defs");
        var gradient = defs.append("linearGradient")
            .attr("id", "svgGradient")
            .attr("x1", "0%")
            .attr("y1", "0%")
            .attr("x2", "100%")
            .attr("y2", "0%");
        gradient.selectAll("stop")
            .data(c_scale2)
            .enter().append("stop")
            .attr("offset", function (d, i) {
                return percents[i] + '%';
            })
            .attr("stop-color", function (d, i) {
                return c_scale2[i]
            });
        console.log(gradient)

        var rect = svg.append("rect")
            .attr('class', 'rect_legend')
            .attr('width', svg_width)
            .attr('height', 20)
            .attr("fill", "url(#svgGradient)")
        xValues = d3.set(v_domain).values();
        console.log(xValues)


        var axisLeg = d3.axisBottom(xScale)
            .tickValues(xValues)
        var legendHeight = 10;
        var legendsvg = svg.append("g")
            .attr("class", "legendWrapper")


        legendsvg.append("g")
            .attr("width", svg_width)
            .attr("height", 95)
            .attr("class", "axis") //Assign "axis" class
            .attr("transform", "translate(-15, " + (legendHeight + 32) + ")")
            .call(axisLeg)
            .selectAll("text")
            .style("text-anchor", "end")
            .attr("dx", "1.5em")
            .attr("transform", "rotate(-65)");

        legendsvg.selectAll(".tick")
            .style('opacity', function (d, i) {



                return 1
            })
    }
}

this_app.create_raster_legend_calc = function (layer_id, scale_type, data) {
    console.info(arguments)

    var v_domain = [2, 50, 300, 1500, 445000].map(d => parseInt(d / 16));

    var c_scale = ['#f07f7f', '#eb9c0c', '#116fd4', '#9e00b3'];
    var c_scale2 = ['#f07f7f', '#ffeb3b', '#eb9c0c', '#eb9c0c', '#116fd4', '#116fd4', '#9e00b3', '#9e00b3'];

    var sorted_color_arr = c_scale.map(function (d, i) {

        var color = d3.rgb(c_scale[i]).formatHex();

        if (i == c_scale.length - 1) {
            var label = '> ' + v_domain[i]
        } else {
            var label = v_domain[i] + '-' + v_domain[i + 1];
        }

        switch (i) {
            case 0:
                label += ' Dispersed density';
                break;
            case 1:
                label += ' Low density';
                break;
            case 2:
                label += ' Moderate density';
                break;
            case 3:
                label += ' High density';
                break;
        }
        return {
            rank: v_domain[i],
            code: v_domain[i],
            label: label,
            label_html: '<div class="row"><div class="col s6"><span style="background-color: ' + color + '"></span>' + label + '</div>',
            rank_color: color
        }
    })
    console.log(sorted_color_arr)
    var $stats_c = $('.analysis_layer_result li.' + layer_id);
    $stats_c.show();
    if (scale_type == 'turbo_scale') {
        var svg = all_container.select('svg.turbo_svg');
        var $stats_c = $('.' + layer_id).find('.turbo_stats_container');

        $('.' + layer_id).find('.sinebow_stats_container').empty().hide();
        $stats_c.show();

        var _scale = d3.scaleSequential(d3.interpolateTurbo)
            .domain([0, 16])
        var _scale_val = d3.scaleSequential(d3.interpolateTurbo)
            .domain([5, 445000])

        var colors = [];
        var sorted_code_color_arr = d3.range(0, 16).map(function (d, i) {

            var color = d3.rgb(_scale(i)).formatHex()
            colors.push(color);
            return {
                rank: v_domain[i],
                code: v_domain[i],
                rank_color: d3.rgb(_scale(i)).formatHex()
            }

        })
        console.warn(sorted_code_color_arr.length)
        console.log(JSON.stringify(sorted_code_color_arr))
    }

    if (scale_type == 'orange_purple') {

    }

    $stats_c.find('.rows_container').empty();
    var $rows = $stats_c.find('.rows_container');
    if (data.length == 0) {
        $rows.append('<h5>No data here</h5>');
        return false
    } else {
        var total_sum = 0;
        var sum_density = 0;
        sorted_color_arr.forEach(function (d, i) {

            for (var p in data) {
                if (data[p].px_class == i) {
                    var info = data[p];
                    total_sum += info.sum;
                    var density = (info.sum / info.num_px);
                    sum_density += (density);

                }

            }

            if (!info) {
                var info = {
                    num_px: 'None',
                    sum: '0'
                }
                var density = 0;
            }

            var t_html = d.label_html;
            t_html += '<div class="col s3">' + info.sum + '</div>'
            t_html += '<div class="col s3">' + parseFloat(density).toFixed(2) + '</div>'

            t_html += '</div>';

            $rows.append(t_html)

        });


        console.warn(this_app.drawn_area)
        $("li.ghs_africa_250m_bigger_1_5 > div.collapsible-header > span.badge").html(parseFloat(total_sum / this_app.drawn_area).toFixed(2) + ' people/km<sup>2</sup>');


    }


}
var africa_platform_layers = [];

this_app.throttle = function (fn, threshhold, scope) {
    threshhold || (threshhold = 250);
    var last,
        deferTimer;
    return function () {
        var context = scope || this;

        var now = +new Date,
            args = arguments;
        if (last && now < last + threshhold) {
            clearTimeout(deferTimer);
            deferTimer = setTimeout(function () {
                last = now;
                fn.apply(context, args);
            }, threshhold);
        } else {
            last = now;
            fn.apply(context, args);
        }
    };
}

function update_query_layers_list() {
    $('.drawn_polygon_description ul').empty();
    app_data.query_layers = [];

    var html = '';

    app_data.layers_info.filter(function (d) {
        if (d.queryable == true && d.activated_query && d.activated_query == true) {
            console.warn(d)
            console.log(d.active_param)

            if (d.id == 'ghs_africa_250m_bigger_2') {
                html += '<li>' + d.title + '</li>';
            } else {
                for (var p in d.params_description) {
                    console.info(d.params_description[p].param_name);
                    console.warn(d.active_param)
                    if (d.params_description[p].param_name == d.active_param) {
                        html += '<li>' + d.title + ' (' + d.params_description[p].short_title + ')</li>';
                    }
                }
            }


            app_data.query_layers.push({
                layer_id: d.id,
                param: d.active_param
            })
        }
    });

    if (app_data.query_layers.length == 0) {
        $('.drawn_polygon_description div.no_layers_analysis').show();
        $('.drawn_polygon_description ul,#drawn_polygon .btn_maps').hide();
        $('.drawn_polygon_description span').hide();
    } else {
        $('.drawn_polygon_description div.no_layers_analysis').hide();
        $('#drawn_polygon .btn_maps').show()
        $('.drawn_polygon_description ul').append(html).show();
        $('.drawn_polygon_description span').show();
    }


    console.log(app_data.query_layers)

}