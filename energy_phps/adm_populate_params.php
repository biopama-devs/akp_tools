
<?php
$link = pg_connect("host=myhost dbname=db user=user password=pwd");
$param = 'elctdemand';
$param_class = 'elctdemand_class';

$adm_code = 'adm2_code';

switch ($adm_code) {
    case 'adm0_code':
        $codes = 'adm0_code,adm0_name';

        $table = 'gaul_0_info_hosp_cat';
        break;
    case 'adm1_code':
        $codes = 'adm1_code,adm0_code,adm0_name,adm1_name';
        $table = 'gaul_1_info_hosp_cat';
        break;
    case 'adm2_code':
        $codes = 'adm2_code,adm2_name,adm1_code,adm0_code,adm0_name,adm1_name';
        $table = 'gaul_2_info_hosp_cat';
        break;
    default:
        break;
}

$sql = "with q as
(select $param_class,$codes,count($param_class)
from hospitals_no_grid_gaul2
group by $param_class,$codes
order by $param_class
 )
 select $codes,'$param' as param,$param_class,q.count from q
 group by q.count,$param_class,$codes
 order by q.$adm_code;";


$result = pg_query($link, $sql);
$count = pg_num_rows($result);
$all_sqls = '';
if ($count > 0) {
    while ($data = pg_fetch_array($result)) {

        $class = $data[$param_class];

        $counts = $data['count'];
        $adm0_code = $data['adm0_code'];
        $adm1_code = $data['adm1_code'];
        $adm2_code = $data['adm2_code'];

        $adm0_name = $data['adm0_name'];
        $adm1_name = $data['adm1_name'];
        $adm2_name = $data['adm2_name'];

        $sql2 = "insert into $table ($codes,param,category,counts) values (";
        switch ($adm_code) {
                //adm0_name
            case 'adm0_code':
                $sql2 .= "$adm0_code,$adm0_name,'$param',$class,$counts);";
                $all_sqls .= $sql2;
                break;
                //case 'adm1_code': $codes='adm1_code,adm0_code,adm0_name,adm1_name';
            case 'adm1_code':
                $sql2 .= "$adm1_code,$adm0_code,$adm0_name,$adm1_name,'$param',$class,$counts);";
                $all_sqls .= $sql2;

                break;
                //case 'adm2_code': $codes='adm2_code,adm2_name,adm1_code,adm0_code,adm0_name,adm1_name';
            case 'adm2_code':
                $sql2 .= "$adm2_code,$adm2_name,$adm1_code,$adm0_code,$adm0_name,$adm1_name,'$param',$class,$counts);";
                $all_sqls .= $sql2;
                break;

            default:
                break;
        }
    }
}
//echo $all_sqls;



$sql = "with q as
(
	select $adm_code,param,counts
from $table
where param='$param'
group by $adm_code,param,counts
order by $adm_code
 )
 select q.$adm_code,param,max(q.counts) from q
 group by q.$adm_code,q.param
 order by max";
//echo $sql;
$result = pg_query($link, $sql);
$count = pg_num_rows($result);
if ($count > 0) {
    while ($data2 = pg_fetch_array($result)) {
        switch ($adm_code) {
            case 'adm0_code':
                $code = $data2['adm0_code'];
                break;
            case 'adm1_code':
                $code = $data2['adm1_code'];
                break;
            case 'adm2_code':
                $code = $data2['adm2_code'];
                break;
        }

        echo "update $table set is_max='true' where $adm_code=$code and param='$param' and counts=" . $data2['max'] . ";";
    }
}


?>