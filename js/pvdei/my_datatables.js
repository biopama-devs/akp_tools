$(document).ready(function () {
  var buttonCommon = {
    exportOptions: {
      format: {
        body: function (data, row, column, node) {
          // Strip $ from salary column to make it numeric
          // data==>html

          if (column === 4) {
            var html = $(data);
            var val = html.find('.cat').text();
            return 'test';
          } else {
            return data;
          }
        }
      }
    }
  }
  var headers = [{
      code: 'country',
      text: 'Country',
      short: 'Country',
    },
    {
      code: 'rank_index',
      text: 'Main index rank',
      short: 'Main rank'
    },
    {
      code: 'main_index_val',
      text: 'Main index',
      short: 'Main index'
    },
    {
      code: 'env_index_val',
      text: 'Environmental index value',
      short: 'Environmental'
    },
    {
      code: 'social_index_val',
      text: 'Social index value',
      short: 'Social'
    },
    {
      code: 'political_index_val',
      text: 'Political index value',
      short: 'Political'
    },
    {
      code: 'financial_index_val',
      text: 'Financial index value',
      short: 'Financial'
    }
    // {
    //     code:'er_2019',
    //     text:'Electrification rate',
    //     short:'Electrif. rate',
    // },

  ];

  // {
  //     code:'er_2019',
  //     text:'Electrification rate'
  // },
  // {
  //     code:'rer_2019',
  //     text:'Rural electrification rate'
  // }              

  $('.refine_country_filter').on('click', function() {
    if ($('.countries_dropdown').is(":hidden")) {
      $('.countries_dropdown,.select-wrapper input.select-dropdown').show();
    } else {
      $('.countries_dropdown').hide();
    }
  })
  $('.to_table_download').on('click', function () {
    var a = document.createElement('a');

    a.href = 'https://africa.gis-ninja.eu/energy_json/indicators/downloads/PV_DEI_SI.xlsx';
    //a.download = "copernicus download";
    document.body.appendChild(a);

    a.click();
    document.body.removeChild(a);
  });

  $('.to_table_link').on('click', function() {
    $('.matrix_tooltip').hide();

    $('#country_modal .country_profile_container').hide();
    $('#country_modal .table_container').show();
    $('#country_modal').modal('show');

    $('.dataTables_wrapper').css('opacity', 1).show();
    $('.dataTable,.dataTables_info_btn').show();
    if ($('.dataTable tbody tr').length == 0) {
      init_datatable();
    } else {
      //$('.heatmap,.to_table_link').hide();
    }
    $('table.dataTable').show();
  })

  function sort_by_index(param) {
    return function (a, b) {
      return b[param] > a[param] ? -1 : 1;
    }
  }

  function init_datatable() {
    $('.mapboxgl-popup').remove();
    $("#country_modal").css('max-height', 'unset!important')
    $("#country_modal").animate({
      // opacity: 0.25,
      width: "95%",
      height: "95vh",

      //height: "toggle"
    }, 1000, function () {
      $('.dataTables_wrapper').css('opacity', 1);
      var sorted = app_data.all_data.sort(sort_by_index('country'));
      // $('.heatmap').hide();

      var header_html = '';
      for (var p in headers) {
        var t = headers[p];
        if (t.code.indexOf('rank') >= 0) {
          //  var path=
          header_html += '<th>' + t.short + '<span class="th_icon"><i class="fa sort fa-sort-amount-asc" aria-hidden="true"></i></span></th>';
        } else {
          if (t.code.indexOf('index_val') >= 0) {
            header_html += '<th>' + t.short + '<span class="th_icon"><i class="fa expand fa-arrow-up" aria-hidden="true" style="margin-left:4px;"></i></span><span class="th_icon"><i class="fa sort fa-sort-amount-asc" aria-hidden="true"></i></span></th>';
          } else {
            //nothing particula
            header_html += '<th>' + t.short + '<span class="th_icon"><i class="fa sort fa-sort-amount-asc" aria-hidden="true"></i></span></th>';
          }
        }
      }
      $('#example thead tr').append(header_html);
      $('.dataTables_wrapper').show();
      var $form = $('.countries_dropdown select');
      var options = '<optgroup label="Close"></optgroup>'
      sorted.forEach(function(d) {
        options += "<option selected _code='" + d.code + "'>" +
          d.country + "</option>"
      })
      $form.append(options);
      //$form.prop('selectedIndex', 0);

      $form.formSelect();
      $ul = $('.countries_dropdown .select-wrapper').find('ul');
      $ul.find('li').eq(2).hide();
      $('.optgroup:first').addClass('ul_actions').html('<div><span class="btn btn-small select_opt unselect">Un-select all</span><span class="btn btn-small close_select">Close</span></div>');

      $('.countries_dropdown .select-wrapper').on('click', function (e) {
        $(this).find('ul').hide();
        if ($(this).find('ul').css('top') !== '40px') {
          $(this).find('ul').css('top', '40px');
        }

        $('.countries_dropdown .caret').hide();

        $(this).find('ul').show();
        $('.dataTables_info_btn .select-dropdown').css('left', '0px');
      })
      $('.countries_dropdown').on('click', function (e) {
        $('.matrix_tooltip').remove();
        var t = $(e.target);
        $ul = $('.countries_dropdown .select-wrapper').find('ul');
        $ul.removeClass('unset');
        if (t.hasClass('select_opt')) {
          //$form.prop('selectedIndex', 2);
          if (t.hasClass('unselect')) {
            $form.find('option').prop('selected', false);
            $form.formSelect();
            //  $ul.find('li').eq(1).addClass('ul_actions').html('<div><span class="btn btn-small select_opt select">Select all</span><span class="btn btn-small close_select">Close</span></div>');
            $('.countries_dropdown .select-wrapper').find('.optgroup:first').addClass('ul_actions').html('<div><span class="btn btn-small select_opt select">Select all</span><span class="btn btn-small close_select">Close</span></div>');
            $('.select-dropdown.dropdown-trigger').show()
          } else {
            $form.find('option').prop('selected', true);
            $form.formSelect();

            $('.countries_dropdown .select-wrapper').find('.optgroup:first').addClass('ul_actions').html('<div><span class="btn btn-small select_opt unselect">Un-select all</span><span class="btn btn-small close_select">Close</span></div>');
            //$('.countries_dropdown .select-wrapper').find('.optgroup:first').addClass('ul_actions').html('<div><span class="btn btn-small select_opt select">Select all</span><span class="btn btn-small close_select">Close</span></div>');
            $('.select-dropdown.dropdown-trigger').show()
          }

          change_form();
          //   $('.dropdown-trigger').click();
        }
        if (t.hasClass('close_select')) {
          $ul.addClass('unset');
          $('.countries_dropdown .caret').show();
        }
        $('.dataTables_info_btn .select-dropdown').css('left', '0px');
      })

      function change_form() {
        var $form = $('.countries_dropdown select');
        $('.dataTable tbody tr').hide();
        var t = 0;
        $form.find('option').each(function (info, i) {
          if ($(this).prop('selected') === true) {
            $('.dataTable tbody tr[_code=' + $(this).attr('_code') + ']').show()
            t++;
          }
        });
        $('.dataTables_info_btn .num').text(t);
        $('.dataTables_info_btn .select-dropdown').css('left', '0px');

        // $('.countries_dropdown .select-wrapper').find('ul').show();

        // $form.find('option[selected=true]').each(function(info, i) {
        //                                 $('.dataTable tbody tr[_code=' + $(this).attr('_code') + ']').show()
        //                                 })    
      }
      $form.bind('change', function (e) {
        change_form();
      })

      // DataTable initialisation
      var data_table_obj = $('#example').DataTable({
        dom: 'Bfrtip',
        scrollY: "auto",
        fixedHeader: true,
        scrollX: true,
        scrollCollapse: true,
        paging: false,
        //7 columns, first two with just 5% width
        // columnDefs: [{ width: 200, targets: 0 }],
        // columnDefs: [{
        //         width: '5%',
        //         targets: [0,
        //             1
        //         ]
        //     },
        //     {
        //         width: '20%',
        //         targets: [2, 3, 4, 5, 6]
        //     }
        // ],

        buttons: [
          // $.extend( true, {}, buttonCommon, {
          //     extend: 'copyHtml5'
          // } ),
          $.extend(true, {}, buttonCommon, {
            extend: 'excelHtml5'
          })
          // $.extend( true, {}, buttonCommon, {
          //     extend: 'pdfHtml5'
          // } )
        ],
        fnDrawCallback: function(settings, json) {
          $('.download_excel').on('click', function () {
            var a = document.createElement('a');

            a.href = 'https://africa.gis-ninja.eu/energy_json/indicators/downloads/PV_DEI_SI.xlsx';
            //a.download = "copernicus download";
            document.body.appendChild(a);

            a.click();
            document.body.removeChild(a);
          })

          setTimeout(function() {
            $('#country_modal .dataTable th').on('click', function(e) {
              var t = $(e.target);
              if (t.hasClass('sort')) {
                var pos = $(this).prop('cellIndex');
                if (!t.hasClass('active')) {
                  $('.sort.active').removeClass('active').removeClass('fa-sort-amount-desc').addClass('fa-sort-amount-asc');
                  t.removeClass('active fa-sort-amount-asc');
                  t.addClass('active fa-sort-amount-desc');
                  data_table_obj
                    .order([pos, 'desc'])
                    .draw();
                } else {
                  if (t.hasClass('fa-sort-amount-asc')) {
                    var new_c = 'desc';
                    t.removeClass('fa-sort-amount-asc');
                  } else {
                    var new_c = 'asc';
                    t.removeClass('fa-sort-amount-desc');
                  }
                  t.addClass('active fa-sort-amount-' + new_c);
                  data_table_obj
                    .order([pos, new_c])
                    .draw();
                }
              }

              if (t.hasClass('expand')) {
                var _class = $(this).attr('class').split(' ')[0];
                if (t.hasClass('fa-arrow-up')) {
                  t.addClass('active');
                  t.removeClass('fa-arrow-up').addClass('fa-arrow-down');
                  $('td.' + _class).find('.val_div_container').show();
                } else {
                  t.removeClass('active');
                  t.removeClass('fa-arrow-down').addClass('fa-arrow-up');
                  $('td.' + _class).find('.val_div_container').hide();
                }
              }
            })
          }, 800)
        },
        // fixedColumns: false,
        data: app_data.all_data,
        columns: [{
          data: 'country',
          className: 'adm_td'
        }, {
          data: 'rank_index',
          className: 'rank_index'
        }, {
          //  data: 'main_index_val',
          className: 'main_index_val data_td',
          orderable: false,
          'render': function (data, type, row) {
            /*
            "data_class": {
                "main_index_class": 2,
                "env_index_class": 4,
            */

            /*
            "data_val": {
            "main_index_val": 0.404,
            "env_index_val": 0.049,
            */

            /*
              var app.scale_values = [{
                value: 1,
                // color: '#e73232',
                color: 'hsl(0,79%,55.1%,70%)',
                title: 'Lower performance'
            }, {
                value: 2,
                */

            var cat = row.data_class.main_index_class - 1;

            return '<div class="cat_div"><span style="display:none;">' + row.data_val.main_index_val + '</span>' +
              '<div class="cat_container"><span class="cat" style="border-bottom:4px solid ' + app.scale_values[cat].color + '">' +
              app.scale_values[cat].title + '</span></div></div><div class="val_div_container"><span>Value: <span class="val">' + row.data_val.main_index_val + '</span></span> <span>Rank: <span class="val">' + row.data_ranks.general_rank + '</span> </span></div>'
            //return row.data_val.main_index_val
          }
        }, {
          // data: 'env_index_val',
          orderable: false,
          className: 'env_index_val data_td',

          'render': function (data, type, row) {
            var cat = row.data_class.env_index_class - 1;

            return '<div class="cat_div"><span style="display:none;">' + row.data_val.env_index_val + '</span>' +
              '<div class="cat_container"><span class="cat" style="border-bottom:4px solid ' + app.scale_values[cat].color + '">' +
              app.scale_values[cat].title + '</span></div></div><div class="val_div_container"><span>Value: <span class="val">' + row.data_val.env_index_val + '</span></span> <span>Rank: <span class="val">' + row.data_ranks.rank_env + '</span> </span></div>';
            // return row.data_val.env_index_val
          }

        }, {
          // data: 'social_index_val',
          orderable: false,
          className: 'social_index_val data_td',

          'render': function (data, type, row) {
            var cat = row.data_class.social_index_class - 1;

            return '<div class="cat_div"><span style="display:none;">' + row.data_val.social_index_val + '</span>' +
              '<div class="cat_container"><span class="cat" style="border-bottom:4px solid ' + app.scale_values[cat].color + '">' +
              app.scale_values[cat].title + '</span></div></div><div class="val_div_container"><span>Value: <span class="val">' + row.data_val.social_index_val + '</span></span> <span>Rank: <span class="val">' + row.data_ranks.rank_social + '</span> </span></div>';
            //return row.data_val.social_index_val;
          }
        }, {
          //data: 'political_index_val data_td',
          orderable: false,
          className: 'political_index_val',
          'render': function (data, type, row) {
            var cat = row.data_class.political_index_class - 1;

            return '<div class="cat_div"><span style="display:none;">' + row.data_val.political_index_val + '</span>' +
              '<div class="cat_container"><span class="cat" style="border-bottom:4px solid ' + app.scale_values[cat].color + '">' +
              app.scale_values[cat].title + '</span></div></div><div class="val_div_container"><span>Value: <span class="val">' + row.data_val.political_index_val + '</span></span> <span>Rank: <span  class="val">' + row.data_ranks.rank_political + '</span> </span></div>';
            // return row.data_val.political_index_val;
          }
        }, {
          data: 'financial_index_val data_td',
          className: 'financial_index_val',
          'render': function (data, type, row) {
            var cat = row.data_class.financial_index_class - 1;

            return '<div class="cat_div"><span style="display:none;">' + row.data_val.financial_index_val + '</span>' +
              '<div class="cat_container"><span class="cat" style="border-bottom:4px solid ' + app.scale_values[cat].color + '">' +
              app.scale_values[cat].title + '</span></div></div><div class="val_div_container"><span>Value: <span class="val">' + row.data_val.financial_index_val + '</span></span> <span>Rank: <span  class="val">' + row.data_ranks.rank_financial + '</span> </span></div>';
            //   return row.data_val.financial_index_val;
          }
        }],
      });
      setTimeout(() => {
        $('.dataTables_scrollBody').css('max-height', $('#country_modal').height() + 'px');
        $('.dataTables_wrapper').show();
        $('.matrix_tooltip').hide();

        $('.dataTable tbody tr').each(function (i, d) {
          $(this).attr('_code', sorted[i].code);
        })
      }, 100);
    }) //end callback animation
  }
})