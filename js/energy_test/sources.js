function addSources(map) {
    map.addSource("schools_source", {

        "type": "raster",
        "tiles": ['https://pere.gis-ninja.eu/geoserver/energy/wms?service=WMS&request=GetMap&format=image%2Fpng&transparent=true&tiled=true&layers=energy:schools&VERSION=1.1.0&transparent=true&bbox={bbox-epsg-3857}&width=512&height=512&srs=EPSG:3857&format=image/png']

    });


    map.addSource("ref_unhcr_polygons_source", {
        "type": "vector",
        // "tiles": ["https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/wmts?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&LAYER=hotspots:electric_grid_classif2&STYLE=&TILEMATRIX=EPSG:900913:{z}&TILEMATRIXSET=EPSG:900913&FORMAT=application/vnd.mapbox-vector-tile&TILECOL={x}&TILEROW={y}"]
        "tiles": ["https://pere.gis-ninja.eu/geoserver/gwc/service/wmts?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&LAYER=energy:ref_unhcr_polygons&STYLE=&TILEMATRIX=EPSG:900913:{z}&TILEMATRIXSET=EPSG:900913&FORMAT=application/vnd.mapbox-vector-tile&TILECOL={x}&TILEROW={y}"]
    });



    map.addSource("electric_grid_source", {
        "type": "vector",
        // "tiles": ["https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/wmts?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&LAYER=hotspots:electric_grid_classif2&STYLE=&TILEMATRIX=EPSG:900913:{z}&TILEMATRIXSET=EPSG:900913&FORMAT=application/vnd.mapbox-vector-tile&TILECOL={x}&TILEROW={y}"]
        "tiles": ["https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/wmts?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&LAYER=hotspots:electric_grid_new_gaul&STYLE=&TILEMATRIX=EPSG:900913:{z}&TILEMATRIXSET=EPSG:900913&FORMAT=application/vnd.mapbox-vector-tile&TILECOL={x}&TILEROW={y}"]
    });



    map.addSource("nightlights_source", {
        "type": "raster",
        // "tiles": ["https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/wmts?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&LAYER=hotspots:electric_grid_classif2&STYLE=&TILEMATRIX=EPSG:900913:{z}&TILEMATRIXSET=EPSG:900913&FORMAT=application/vnd.mapbox-vector-tile&TILECOL={x}&TILEROW={y}"]
        "tiles": ['https://geospatial.jrc.ec.europa.eu/geoserver/wms?service=WMS&version=1.1.0&request=GetMap&layers=hotspots:ceat_nightlights&transparent=true&bbox={bbox-epsg-3857}&width=768&height=585&srs=EPSG:3857&format=image/png']
    });

    map.addSource("clustered_pop_source", {
        "type": "vector",
        "tiles": ["https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/wmts?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&LAYER=hotspots:clustered_pop&STYLE=&TILEMATRIX=EPSG:900913:{z}&TILEMATRIXSET=EPSG:900913&FORMAT=application/vnd.mapbox-vector-tile&TILECOL={x}&TILEROW={y}"]

    });

    // map.addSource("power_plants_source", {
    //     "type": "vector",
    //     "tiles": ["https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/wmts?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&LAYER=hotspots:power_plants&STYLE=&TILEMATRIX=EPSG:900913:{z}&TILEMATRIXSET=EPSG:900913&FORMAT=application/vnd.mapbox-vector-tile&TILECOL={x}&TILEROW={y}"]
    // })

    map.addSource("gaul_level_0_all_source", {
        "type": "vector",
        "tiles": ["https://pere.gis-ninja.eu/geoserver/gwc/service/wmts?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&LAYER=topp:gaul_0_simplified&STYLE=&TILEMATRIX=EPSG:900913:{z}&TILEMATRIXSET=EPSG:900913&FORMAT=application/vnd.mapbox-vector-tile&TILECOL={x}&TILEROW={y}"]
    })

    map.addSource("gaul_level_0_source", {
        "type": "vector",
        "tiles": ["https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/wmts?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&LAYER=hotspots:gaul_level_0_africa3&STYLE=&TILEMATRIX=EPSG:900913:{z}&TILEMATRIXSET=EPSG:900913&FORMAT=application/vnd.mapbox-vector-tile&TILECOL={x}&TILEROW={y}"]
    })

    map.addSource("gaul_level_1_source", {
        "type": "vector",
        "tiles": ["https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/wmts?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&LAYER=hotspots:gaul_level_1_africa&STYLE=&TILEMATRIX=EPSG:900913:{z}&TILEMATRIXSET=EPSG:900913&FORMAT=application/vnd.mapbox-vector-tile&TILECOL={x}&TILEROW={y}"]
    })
    map.addSource('sel_lines_buffer_source', {
        'type': 'geojson',
        'data': null

    });

    map.addSource('drawn_pol_source', {
        type: 'geojson',
        data: null
    });


    map.addSource('gaul_0_labels_source', {
        'type': 'vector',

        "tiles": ["https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/wmts?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&LAYER=hotspots:gaul_0_labels&STYLE=&TILEMATRIX=EPSG:900913:{z}&TILEMATRIXSET=EPSG:900913&FORMAT=application/vnd.mapbox-vector-tile&TILECOL={x}&TILEROW={y}"]
    });


    map.addSource("power_plants_source", {
        "type": "vector",
        // "tiles": ["https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/wmts?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&LAYER=hotspots:power_plants_2019&STYLE=&TILEMATRIX=EPSG:900913:{z}&TILEMATRIXSET=EPSG:900913&FORMAT=application/vnd.mapbox-vector-tile&TILECOL={x}&TILEROW={y}"]
        "tiles": ["https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/wmts?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&LAYER=hotspots:power_plants_2019_class2&STYLE=&TILEMATRIX=EPSG:900913:{z}&TILEMATRIXSET=EPSG:900913&FORMAT=application/vnd.mapbox-vector-tile&TILECOL={x}&TILEROW={y}"]

    });




    map.addSource('refugee_camps_source', {
        'type': 'vector',

        "tiles": ["https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/wmts?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&LAYER=hotspots:refugee_camps_geoserver&STYLE=&TILEMATRIX=EPSG:900913:{z}&TILEMATRIXSET=EPSG:900913&FORMAT=application/vnd.mapbox-vector-tile&TILECOL={x}&TILEROW={y}"]
    });


    map.addSource('ref_unhcr_merged_simple_source', {
        'type': 'vector',
        "tiles": ["https://pere.gis-ninja.eu/geoserver/gwc/service/wmts?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&LAYER=energy:ref_unhcr_merged_simple&STYLE=&TILEMATRIX=EPSG:900913:{z}&TILEMATRIXSET=EPSG:900913&FORMAT=application/vnd.mapbox-vector-tile&TILECOL={x}&TILEROW={y}"]

        //"tiles": ["https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/wmts?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&LAYER=energy:ref_unhcr_merged_simple&STYLE=&TILEMATRIX=EPSG:900913:{z}&TILEMATRIXSET=EPSG:900913&FORMAT=application/vnd.mapbox-vector-tile&TILECOL={x}&TILEROW={y}"]
    });


    map.addSource('ref_settlements_source', {
        'type': 'vector',

        "tiles": ["https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/wmts?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&LAYER=hotspots:refugee_camps_geoserver&STYLE=&TILEMATRIX=EPSG:900913:{z}&TILEMATRIXSET=EPSG:900913&FORMAT=application/vnd.mapbox-vector-tile&TILECOL={x}&TILEROW={y}"]
    });



    map.addSource('ref_settlements_labels_source', {
        'type': 'vector',

        "tiles": ["https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/wmts?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&LAYER=hotspots:refugee_camps_geoserver&STYLE=&TILEMATRIX=EPSG:900913:{z}&TILEMATRIXSET=EPSG:900913&FORMAT=application/vnd.mapbox-vector-tile&TILECOL={x}&TILEROW={y}"]
    });




    map.addSource('gaul_1_labels_source', {
        'type': 'vector',

        "tiles": ["https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/wmts?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&LAYER=hotspots:gaul_1_labels&STYLE=&TILEMATRIX=EPSG:900913:{z}&TILEMATRIXSET=EPSG:900913&FORMAT=application/vnd.mapbox-vector-tile&TILECOL={x}&TILEROW={y}"]
    });

    map.addSource('gaul_2_labels_source', {
        'type': 'vector',

        "tiles": ["https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/wmts?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&LAYER=hotspots:gaul_2_labels&STYLE=&TILEMATRIX=EPSG:900913:{z}&TILEMATRIXSET=EPSG:900913&FORMAT=application/vnd.mapbox-vector-tile&TILECOL={x}&TILEROW={y}"]
    });

    map.addSource('elec_lines_source', {
        type: 'geojson',
        data: null
    });

    map.addSource('sel_hospitals_grid_dist_lines_source', {
        type: 'geojson',
        data: null
    });

    map.addSource('gaul_0_analysis_labels_source', {
        type: 'geojson',
        data: null
    });
    map.addSource('gaul_1_analysis_labels_source', {
        type: 'geojson',
        data: null
    });

    map.addSource('gaul_2_analysis_labels_source', {
        type: 'geojson',
        data: null
    });
    map.addSource("gaul_2_source", {
        "type": "vector",
        "tiles": ["https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/wmts?layer=hotspots:gaul_level_2_africa&tilematrixset=EPSG:900913&Service=WMTS&Request=GetTile&Version=1.0.0&Format=application/vnd.mapbox-vector-tile&TileMatrix=EPSG:900913:{z}&TileCol={x}&TileRow={y}"]
    });
    map.addSource('ghs_1km_bigger_than_5_source', {
        'type': 'raster',
        'tiles': [

            'https://geospatial.jrc.ec.europa.eu/geoserver/wms?service=WMS&STYLES=hotspots:ghs_1km_bigger_than_5_interpolateSinebox&version=1.1.0&request=GetMap&layers=hotspots:ghs_1km_bigger_than_5&transparent=true&bbox={bbox-epsg-3857}&width=768&height=585&srs=EPSG:3857&format=image/png'
            //'https://geospatial.jrc.ec.europa.eu/geoserver/wms?service=WMS&STYLE=hotspots:raster_ghs&version=1.1.0&request=GetMap&layers=hotspots:ghs_1km_bigger_than_5&transparent=true&bbox={bbox-epsg-3857}&width=768&height=585&srs=EPSG:3857&format=image/png'

        ]
        //'tileSize': 256
    });

    map.addSource('ghs_africa_250m_bigger_1_5_source', {
        'type': 'raster',
        'tiles': [

            //                            'https://geospatial.jrc.ec.europa.eu/geoserver/wms?service=WMS&version=1.1.0&request=GetMap&layers=hotspots:ghs_250m_bigger_than_2&STYLES=hotspots:ghs_250m_new_legend&transparent=true&bbox={bbox-epsg-3857}&width=768&height=585&srs=EPSG:3857&format=image/png'
            'https://geospatial.jrc.ec.europa.eu/geoserver/wms?service=WMS&version=1.1.0&request=GetMap&layers=hotspots:pop_ghs_bigger_1_5_4326&STYLES=hotspots:ghs_250m_new_legend&transparent=true&bbox={bbox-epsg-3857}&width=768&height=585&srs=EPSG:3857&format=image/png'

        ]
        //'tileSize': 256
    });

    map.addSource('distance_to_grid_source', {
        'type': 'raster',
        'tiles': [

            'https://geospatial.jrc.ec.europa.eu/geoserver/wms?service=WMS&version=1.1.0&request=GetMap&layers=hotspots:distance_africa_05092022&transparent=true&bbox={bbox-epsg-3857}&width=768&height=585&srs=EPSG:3857&format=image/png'
            //'https://geospatial.jrc.ec.europa.eu/geoserver/wms?service=WMS&STYLE=hotspots:raster_ghs&version=1.1.0&request=GetMap&layers=hotspots:ghs_1km_bigger_than_5&transparent=true&bbox={bbox-epsg-3857}&width=768&height=585&srs=EPSG:3857&format=image/png'

        ]
        //'tileSize': 256
    });

    map.addSource('ghs_1km_bigger_than_5_source2', {
        'type': 'raster',
        'tiles': [

            'https://geospatial.jrc.ec.europa.eu/geoserver/wms?service=WMS&STYLES=hotspots:ghs_1km_bigger_than_5_interpolateSinebox&version=1.1.0&request=GetMap&layers=hotspots:ghs_1km_bigger_than_5&transparent=true&bbox={bbox-epsg-3857}&width=768&height=585&srs=EPSG:3857&format=image/png'
            //'https://geospatial.jrc.ec.europa.eu/geoserver/wms?service=WMS&STYLE=hotspots:raster_ghs&version=1.1.0&request=GetMap&layers=hotspots:ghs_1km_bigger_than_5&transparent=true&bbox={bbox-epsg-3857}&width=768&height=585&srs=EPSG:3857&format=image/png'

        ]
        //'tileSize': 256
    });

    map.addSource("carto_black", {
        "type": "raster",
        "tiles": ["https://basemaps.cartocdn.com/dark_all/{z}/{x}/{y}.png"],
        //"tiles": ["https://basemaps.cartocdn.com/dark_only_labels/{z}/{x}/{y}.png"],

        'tileSize': 240
    });


    map.addSource("jawg_source", {
        "type": "raster",
        //     "tiles": ["https://api.maptiler.com/maps/bright/style.json?key=PfeqCqeOXLcGceolGsUb"],
        "tiles": ["https://server.arcgisonline.com/ArcGIS/rest/services/Ocean_Basemap/MapServer/tile/{z}/{y}/{x}"],

        'tileSize': 240
    });

    map.addSource("natgeo_source", {
        "type": "raster",
        //     "tiles": ["https://api.maptiler.com/maps/bright/style.json?key=PfeqCqeOXLcGceolGsUb"],
        "tiles": ["https://server.arcgisonline.com/ArcGIS/rest/services/NatGeo_World_Map/MapServer/tile/{z}/{y}/{x}"],

        'tileSize': 240
    });



    map.addSource("esri_satellite_source", {
        "type": "raster",
        "tiles": ['https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}'],

        'tileSize': 240
    });




    map.addSource("carto_white", {
        "type": "raster",

        "tiles": ["https://cartodb-basemaps-a.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png"],

        'tileSize': 240
    });
    map.addSource("gridfinder_source", {
        "type": "raster",

        "tiles": ['https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/wms?service=WMS&version=1.0.0&request=GetMap&layers=energy:gridfinder_lines&transparent=true&bbox={bbox-epsg-3857}&width=256&height=256&srs=EPSG:3857&format=image/png']
    });

    //replaced by new ones???
    map.addSource("time_travel_source", {
        "type": "raster",

        "tiles": ['https://geospatial.jrc.ec.europa.eu/geoserver/wms?service=WMS&version=1.1.0&request=GetMap&layers=hotspots:access_minimum_all_motorized_clipped&transparent=true&bbox={bbox-epsg-3857}&width=768&height=585&srs=EPSG:3857&format=image/png']
    });

    map.addSource("travel_hc_access_minimum_all_motorized_source", {
        "type": "raster",

        "tiles": ['https://pere.gis-ninja.eu/geoserver/wms?service=WMS&version=1.1.0&request=GetMap&layers=energy:travel_hc_access_minimum_all_motorized&transparent=true&bbox={bbox-epsg-3857}&width=768&height=585&srs=EPSG:3857&format=image/png']
    });

    map.addSource("travel_hc_access_minimum_all_walking_source", {
        "type": "raster",

        "tiles": ['https://pere.gis-ninja.eu/geoserver/wms?service=WMS&version=1.1.0&request=GetMap&layers=energy:travel_hc_access_minimum_all_walking&transparent=true&bbox={bbox-epsg-3857}&width=768&height=585&srs=EPSG:3857&format=image/png']
    });

    map.addSource("travel_time_primary_all_walking_source", {
        "type": "raster",

        "tiles": ['https://pere.gis-ninja.eu/geoserver/wms?service=WMS&version=1.1.0&request=GetMap&layers=energy:travel_time_primary_all_walking&transparent=true&bbox={bbox-epsg-3857}&width=768&height=585&srs=EPSG:3857&format=image/png']
    });

    map.addSource("travel_time_primary_all_motorized_source", {
        "type": "raster",

        "tiles": ['https://pere.gis-ninja.eu/geoserver/wms?service=WMS&version=1.1.0&request=GetMap&layers=energy:travel_time_primary_all_motorized&transparent=true&bbox={bbox-epsg-3857}&width=768&height=585&srs=EPSG:3857&format=image/png']
    });








    map.addSource('ghs_250m_bigger_than_5_source', {
        'type': 'raster',
        'tiles': [
            //https://geospatial.jrc.ec.europa.eu/geoserver/hotspots/wms?service=WMS&version=1.1.0&request=GetMap&layers=hotspots%3Aghs_4326_clipped&bbox=-32.18923705835353%2C-46.9898138080226%2C65.25572599444514%2C27.298076275955083&width=768&height=585&srs=EPSG%3A4326&format=application/openlayers
            'https://geospatial.jrc.ec.europa.eu/geoserver/wms?service=WMS&STYLES=hotspots:ghs_250m_bigger_than_5_interpolateSinebox&version=1.1.0&request=GetMap&layers=hotspots:ghs_250m_bigger_than_5&transparent=true&bbox={bbox-epsg-3857}&width=768&height=585&srs=EPSG:3857&format=image/png'
            //'https://img.nj.gov/imagerywms/Natural2015?bbox={bbox-epsg-3857}&format=image/png&service=WMS&version=1.0.0&request=
            //GetMap&srs=EPSG:3857&transparent=true&width=256&height=256&layers=Natural2015'
        ]
        //'tileSize': 256
    });
    //  map.addSource('ghs_250m_bigger_than_2_source', {
    map.addSource('ghs_africa_250m_bigger_2_source', {
        'type': 'raster',
        'tiles': [

            //                            'https://geospatial.jrc.ec.europa.eu/geoserver/wms?service=WMS&version=1.1.0&request=GetMap&layers=hotspots:ghs_250m_bigger_than_2&STYLES=hotspots:ghs_250m_new_legend&transparent=true&bbox={bbox-epsg-3857}&width=768&height=585&srs=EPSG:3857&format=image/png'
            'https://geospatial.jrc.ec.europa.eu/geoserver/wms?service=WMS&version=1.1.0&request=GetMap&layers=hotspots:ghs_africa_250m_bigger_than_2&STYLES=hotspots:ghs_250m_new_legend&transparent=true&bbox={bbox-epsg-3857}&width=768&height=585&srs=EPSG:3857&format=image/png'
        ]
        //'tileSize': 256
    });

    console.warn(gaul_level_1)
    console.warn(gaul_level_2)

    map.addSource("gaul_1_source", {
        "type": "vector",
        "tiles": ["https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/wmts?layer=hotspots:gaul_level_1_africa&tilematrixset=EPSG:900913&Service=WMTS&Request=GetTile&Version=1.0.0&Format=application/vnd.mapbox-vector-tile&TileMatrix=EPSG:900913:{z}&TileCol={x}&TileRow={y}"]
    });
    //alert('http://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/wmts?layer=topp:health_centres_updated&tilematrixset=EPSG:900913&Service=WMTS&Request=GetTile&Version=1.0.0&Format=application/vnd.mapbox-vector-tile&TileMatrix=EPSG:900913:{z}&TileCol={x}&TileRow={y}')

    map.addSource('taf_geotermal_ungrouped_source', {
        /* 'type': 'geojson',
        'data': geotermal.single_geojson, */
        "type": "vector",
        "tiles": ["https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/wmts?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&LAYER=energy:taf_geotermal_ungrouped&STYLE=&TILEMATRIX=EPSG:900913:{z}&TILEMATRIXSET=EPSG:900913&FORMAT=application/vnd.mapbox-vector-tile&TILECOL={x}&TILEROW={y}"]
    });

    map.addSource("hospitals_source", {
        "type": "vector",


        "tiles": ["https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/wmts?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&LAYER=hotspots:health_centres_updated&STYLE=&TILEMATRIX=EPSG:900913:{z}&TILEMATRIXSET=EPSG:900913&FORMAT=application/vnd.mapbox-vector-tile&TILECOL={x}&TILEROW={y}"]
        //["https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/wmts?layer=hotspots:health_centres_updated&tilematrixset=EPSG:900913&Service=WMTS&Request=GetTile&Version=1.0.0&Format=application/vnd.mapbox-vector-tile&TileMatrix=EPSG:900913:{z}&TileCol={x}&TileRow={y}"]

    });

    map.addSource('taf_geotermal_grouped_source', {
        /* 'type': 'geojson',
        'data': geotermal.grouped_geojson, */
        "type": "vector",
        "tiles": ["https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/wmts?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&LAYER=energy:taf_geotermal_grouped&STYLE=&TILEMATRIX=EPSG:900913:{z}&TILEMATRIXSET=EPSG:900913&FORMAT=application/vnd.mapbox-vector-tile&TILECOL={x}&TILEROW={y}"]
        // "tiles": ["https://pere.gis-ninja.eu/geoserver/gwc/service/wmts?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&LAYER=energy:taf_geotermal_grouped&STYLE=&TILEMATRIX=EPSG:900913:{z}&TILEMATRIXSET=EPSG:900913&FORMAT=application/vnd.mapbox-vector-tile&TILECOL={x}&TILEROW={y}"]

    });

    map.addSource('taf_hydrogen_grouped_source', {
        /* 'type': 'geojson',
        'data': geotermal.grouped_geojson, */
        "type": "vector",
        "tiles": ["https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/wmts?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&LAYER=energy:taf_hydrogen_grouped&STYLE=&TILEMATRIX=EPSG:900913:{z}&TILEMATRIXSET=EPSG:900913&FORMAT=application/vnd.mapbox-vector-tile&TILECOL={x}&TILEROW={y}"]

    });

    map.addSource("taf_bess_source", {
        "type": "vector",

        "tiles": ["https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/wmts?layer=energy:taf_bess&tilematrixset=EPSG:900913&Service=WMTS&Request=GetTile&Version=1.0.0&Format=application/vnd.mapbox-vector-tile&TileMatrix=EPSG:900913:{z}&TileCol={x}&TileRow={y}"]
    });

    map.addSource("taf_wind_source", {
        "type": "vector",
        //hc_benin
        "tiles": ["https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/wmts?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&LAYER=energy:taf_wind&STYLE=&TILEMATRIX=EPSG:900913:{z}&TILEMATRIXSET=EPSG:900913&FORMAT=application/vnd.mapbox-vector-tile&TILECOL={x}&TILEROW={y}"]
        //"tiles": ["https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/wmts?layer=energy:taf_wind&tilematrixset=EPSG:900913&Service=WMTS&Request=GetTile&Version=1.0.0&Format=application/vnd.mapbox-vector-tile&TileMatrix=EPSG:900913:{z}&TileCol={x}&TileRow={y}"]

        // "tiles": ["http://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/wmts?layer=topp:health_centres_updated&tilematrixset=EPSG:900913&Service=WMTS&Request=GetTile&Version=1.0.0&Format=application/vnd.mapbox-vector-tile&TileMatrix=EPSG:900913:{z}&TileCol={x}&TileRow={y}"]
        //"tiles": ["https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/wmts?layer=hotspots:hospitals_no_grid_gaul2&tilematrixset=EPSG:900913&Service=WMTS&Request=GetTile&Version=1.0.0&Format=application/vnd.mapbox-vector-tile&TileMatrix=EPSG:900913:{z}&TileCol={x}&TileRow={y}"]
    });

    map.addSource("taf_solar_source", {
        "type": "vector",

        "tiles": ["https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/wmts?layer=energy:taf_solar&tilematrixset=EPSG:900913&Service=WMTS&Request=GetTile&Version=1.0.0&Format=application/vnd.mapbox-vector-tile&TileMatrix=EPSG:900913:{z}&TileCol={x}&TileRow={y}"]

    });

    map.addSource("taf_pumps_up_source", {
        "type": "vector",

        "tiles": ["https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/wmts?layer=energy:taf_pumps_up&tilematrixset=EPSG:900913&Service=WMTS&Request=GetTile&Version=1.0.0&Format=application/vnd.mapbox-vector-tile&TileMatrix=EPSG:900913:{z}&TileCol={x}&TileRow={y}"]

    });

    map.addSource("taf_pumps_down_source", {
        "type": "vector",

        "tiles": ["https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/wmts?layer=energy:taf_pumps_down&tilematrixset=EPSG:900913&Service=WMTS&Request=GetTile&Version=1.0.0&Format=application/vnd.mapbox-vector-tile&TileMatrix=EPSG:900913:{z}&TileCol={x}&TileRow={y}"]

    });

    map.addSource("taf_pumps_lines_source", {
        "type": "vector",
        // "tiles": ["https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/wmts?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&LAYER=hotspots:electric_grid_classif2&STYLE=&TILEMATRIX=EPSG:900913:{z}&TILEMATRIXSET=EPSG:900913&FORMAT=application/vnd.mapbox-vector-tile&TILECOL={x}&TILEROW={y}"]
        "tiles": ["https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/wmts?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&LAYER=energy:taf_pumps_lines&STYLE=&TILEMATRIX=EPSG:900913:{z}&TILEMATRIXSET=EPSG:900913&FORMAT=application/vnd.mapbox-vector-tile&TILECOL={x}&TILEROW={y}"]
    });
}