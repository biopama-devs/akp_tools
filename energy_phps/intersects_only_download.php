<?php
header('Content-Type: application/json');

$json = array();
$link = pg_connect("host=yourhost dbname=db user=user password=pwd");

$userid = $_GET['userid'];
$to_query = $_GET['to_query'];
$req_format = $_GET['req_format'];
$layer_id = $_GET['layer_id'];

if ($to_query != 'bbox') {
  $adm_code_value = $_GET['adm_code_value'];
  $adm_name;

  switch ($to_query) {
    case 'adm0':
      $adm_code_name = 'adm0_code';
      //  $adm_sql="select ST_extent(geom),adm0_code_int,adm0_name,area_sqkm from gaul_0_info_all where adm0_code_int=$adm_code_value";
      $adm_sql = "select adm0_code_int,adm0_name,area_sqkm from gaul_0_info_all where adm0_code_int=$adm_code_value";
      # code...
      break;
    case 'adm1':
      $adm_code_name = 'adm1_code';
      $adm_sql = "select adm0_code_int,adm0_name,adm1_code_int,adm1_name,area_sqkm from gaul_1_info_all where adm1_code_int=$adm_code_value";
      # code...
      break;
    case 'adm2':
      $adm_code_name = 'adm2_code';
      $adm_sql = "select adm0_code,adm0_name,adm1_code,adm1_name,adm2_code,adm2_name,area_sqkm from gaul_2_info_all where adm2_code=$adm_code_value";
      # code...
      break;

    default:
      # code...
      break;
  }
} else {
  $sw = $_GET['bbox']['sw'];
  $xmin = $sw[0];
  $ymin = $sw[1];

  $ne = $_GET['bbox']['ne'];

  $xmax = $ne[0];
  $ymax = $ne[1];

  $pol = 'POLYGON((' . $xmin . ' ' . $ymin . ',' . $xmin . ' ' . $ymax . ',' . $xmax . ' ' . $ymax . ',' . $xmax . ' ' . $ymin . ',' . $xmin . ' ' . $ymin . '))';
}
if ($layer_id == 'power_plants') {


  if ($to_query == 'bbox') {

    $sql = "select geom,name,adm0_name,mw,mw_class,gen_type from power_plants_2019_class where ST_Within(power_plants_2019_class.geom, ST_GeomFromText('" . $pol . "',4326))";
  } else {
    $sql = "select geom,name.adm0_name,mw,mw_class,gen_type from power_plants_2019_class where $adm_code_name='$adm_code_value'";
  }
}

if ($layer_id == 'hospitals') {
  $all_p = array('lcoe_h', 'elctdemand', 'pv_kw_h', 'bat_kwh_h', 'capex', 'npv_h');

  $sql = "WITH
     clipped AS (
        SELECT geom,facility_t,facility_n,adm0_code,adm0_name,adm1_code,adm1_name,adm2_code,adm2_name,";

  for ($i = 0; $i < sizeof($all_p); $i++) {
    if ($i < sizeof($all_p) - 1) {

      $sql .= $all_p[$i] . ",$all_p[$i]_class,";
    } else {
      $sql .= $all_p[$i] . ",$all_p[$i]_class";
    }
  }

  if ($to_query == 'bbox') {

    $sql .= " from hospitals_no_grid_gaul2 where ST_Within(hospitals_no_grid_gaul2.geom, ST_GeomFromText('" . $pol . "',4326)))
            SELECT geom,facility_t,facility_n,adm0_code,adm0_name,adm1_code,adm1_name,adm2_code,adm2_name,";
  } else {

    $sql .= " from hospitals_no_grid_gaul2 where $adm_code_name=$adm_code_value)
            SELECT geom,facility_t,facility_n,adm0_code,adm0_name,adm1_code,adm1_name,adm2_code,adm2_name,";
  }

  for ($i = 0; $i < sizeof($all_p); $i++) {
    if ($i < sizeof($all_p) - 1) {

      $sql .= $all_p[$i] . ",$all_p[$i]_class,";
    } else {

      $sql .= $all_p[$i] . ",$all_p[$i]_class";
    }
  }

  $sql .= ' from clipped';
}


function search_info($layer_id, $userid, $json, $req_format)
{

  //echo 'asdfsadfasf';
  if ($req_format == 'shp') {
    //download/$dir.'/'.$userid.'/'.$layer_id.'/unzipped'
    exec('cd /var/www/html/energy_phps/downloads/' . $userid . '/' . $layer_id . '; zip -r "./zipped/' . $layer_id . '.zip" "./unzipped"');

    if (file_exists('/var/www/html/energy_phps/downloads/' . $userid . '/' . $layer_id . '/zipped' . '/' . $layer_id . '.zip')) {

      $json['created_files'] = true;
    } else {
      $json['created_files'] = false;
    }
  } else {

    if (file_exists('/var/www/html/energy_phps/downloads/' . $userid . '/' . $layer_id . '/xlsx' . '/' . $layer_id . '.xlsx')) {
      $json['created_files'] = true;
    } else {
      $json['created_files'] = false;
    }
  }


  $main_json = json_encode($json, JSON_NUMERIC_CHECK);
  if ($_GET['callback']) {
    echo $_GET['callback'] . "(" . $main_json . ")";
  } else {
    echo $main_json;
  };
}
function generate_shp($layer_id, $userid, $sql, $json, $req_format)
{
  //download/1234987  /unzipped...
  $dir = '/var/www/html/energy_phps/downloads';
  //echo $dir.'/'.$userid.'/'.$layer_id;
  if (!file_exists($dir . '/' . $userid)) {
    mkdir($dir . '/' . $userid, 0777, true);
    mkdir($dir . '/' . $userid . '/' . $layer_id, 0777, true);
  } else {
    if ($req_format == 'shp' && !file_exists($dir . '/' . $userid . '/' . $layer_id . '/unzipped')) {
      mkdir($dir . '/' . $userid . '/' . $layer_id . '/unzipped', 0777, true);
      mkdir($dir . '/' . $userid . '/' . $layer_id . '/zipped', 0777, true);
    }
    if ($req_format == 'xlsx' && !file_exists($dir . '/' . $userid . '/' . $layer_id . '/xslx')) {
      mkdir($dir . '/' . $userid . '/' . $layer_id . '/xlsx', 0777, true);
    }
  }


  $user_dir = $dir . '/' . $userid;
  if ($req_format == 'shp') {
    $unzipped_dir = $user_dir . '/' . $layer_id . '/unzipped';
    $files = scandir($unzipped_dir);
    foreach ($files as $file) {
      if (is_file($unzipped_dir . '/' . $file)) {
        unlink($unzipped_dir . '/' . $file);
      }
    }
    $zipped_dir = $user_dir . '/' . $layer_id . '/zipped';
    $files = scandir($zipped_dir);
    foreach ($files as $file) {
      if (is_file($zipped_dir . '/' . $file)) {
        unlink($zipped_dir . '/' . $file);
      }
    }
  }



  if ($req_format == 'xlsx') {


    $excel_dir = $user_dir . '/' . $layer_id . '/xlsx';
    $files = scandir($excel_dir);
    foreach ($files as $file) {
      if (is_file($excel_dir . '/' . $file)) {
        unlink($excel_dir . '/' . $file);
      }
    }
  }



  $link = pg_connect("host=yourhost dbname=userorPwd user=userorPwd password=userorPwd");

  $dir = '/var/www/html/energy_phps/downloads';
  $user_dir = $dir . '/' . $userid;


  if ($req_format == 'xlsx') {
    $c = "cd $user_dir/$layer_id; ogr2ogr -f 'XLSX' xlsx/$layer_id.xlsx PG:'host=yourhost dbname=userorPwd user=userorPwd password=userorPwd' -sql '$sql';";
  } else {
    $c = "cd $user_dir/$layer_id; ogr2ogr -f 'ESRI Shapefile' unzipped/$layer_id.shp PG:'host=yourhost dbname=userorPwd user=userorPwd password=userorPwd' -sql '$sql';";
  }
  //    echo $c;
  exec($c, $out, $ret);
  search_info($layer_id, $userid, $json, $req_format);
}

//generate_shp($layer_name,$userid,$geo_health_sql,$json,$req_format);
generate_shp($layer_id, $userid, $sql, $json, $req_format);
