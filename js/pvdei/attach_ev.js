$(document).ready(function () {
  app_data.countries = {}
  app_data.all_data.forEach(function (d) {
    app_data.countries[d.country] = null;
  })

  $('input.autocomplete').autocomplete({
    data: app_data.countries,
    onAutocomplete: function (country) {
      //var adm_name = txt;
      var path_code = app_data.all_data.filter(function(d) {
        return d.country == country
      })[0].code;

      $('.country_li').removeClass('expanded');
      $('.country_li .input-field').removeClass('expanded');

      app.update_country_profile(path_code);
    }
  }).bind('input', function () {
    //$('.country_li').css('min-height', '300px').find('.input-field').css('min-height', '300px');
    $('.country_li').addClass('expanded');
    $('.country_li .input-field').addClass('expanded');
  });
  //because of Drupal...
  $('.container').removeClass('container');
  $('.tabs').eq(0).remove();
  $('#main-content').css('padding', 0);
  setTimeout(function () {
    $('#header').css('padding', '0px 0px!important');
  }, 500)

  $("#intro_modal").modal("show");
  $("#intro_modal .btn-close").click(function() {
    $(".icon_info").removeClass("on");
    if ($(".matrix_indicator_select").is(':visible') == false) {
      $(".collapsible-header.indicators").click();
      $(".matrix_indicator_select").trigger("click");
    }
  })

  // Callback for Modal close});
  // $('.country_popup').on('mouseover mouseenter mouseleave mouseup mousedown', function() {
  //     return false
  // });
  $('.btn_bubble').on('click', function() {
    on_bubble_btn();
  })

  $('.profile_checkbox input').on('click', function(e) {
    if ($(this).is(':checked')) {
      app_state.country_facts = true;
      // app_state.matrix_indicators = false;
      // app_state.lollipop_indicators = false;

      if ($('#lollipop_graph svg g').length > 0) {
        //$('#lollipop_wrapper').hide();
        //$('.lollipop_tooltip').hide();
      }
    } else {
      app_state.country_facts = false;
      // if ($('.heatmap svg g').length > 0) {
      //     // $('.heatmap svg').hide();
      //     app_state.matrix_indicators = true;
      //     $('#matrix_wrapper').show();
      // }

      // if ($('#lollipop_graph svg g').length > 0) {
      //     app_state.lollipop_indicators = true;
      //     $('#lollipop_wrapper').show();
      // }
    }
  })

  $("#lollipop_wrapper .close").click(function() {
    $("#lollipop_wrapper").fadeOut();
    $(".lollipop_tooltip").hide();
    app_state.lollipop_indicators = false;
    $(".mapboxgl-ctrl-bottom-right").show();
    $(".mapboxgl-ctrl-bottom-right div.i_matrix").hide();
    $(".mapboxgl-ctrl-bottom-right div.i_lollipop").show();
  })
  $("#matrix_wrapper .close").click(function() {
    $("#matrix_wrapper").fadeOut();
    $(".matrix_tooltip").hide();
    $(".mapboxgl-ctrl-bottom-right").show();
    $(".mapboxgl-ctrl-bottom-right div.i_lollipop").hide();
    $(".mapboxgl-ctrl-bottom-right div.i_matrix").show();
  })

  $("#slide-out").sidenav({
    edge: "left", // Choose the horizontal origin
    draggable: true // Choose whether you can drag to open on touch screens,
  })
  $("#slide-out").sidenav("open");

  $(".legend_dropdown_button").click(function(e) {
    e.preventDefault();
    if ($(this).find("i").hasClass("on")) {
      $(this).find("i").removeClass("on");
      $("#legend_dropdown").hide(300);
    } else {
      $(this).find("i").addClass("on");
      $("#legend_dropdown").show(300);
    }
  })

  $(".rank_legend_dropdown_button").click(function(e) {
    e.preventDefault();
    if ($(this).find("i").hasClass("on")) {
      $(this).find("i").removeClass("on");
      $("#tooltip_rank_instructions").hide(225);
      $("#rank_legend_dropdown").hide(225);
    } else {
      $(this).find("i").addClass("on");
      $("#rank_legend_dropdown").show(225);
      $("#tooltip_rank_instructions").show(225);
    }
  })

  $(".sidenav-trigger").on('click', function(e) {
    e.preventDefault();
    if ($(".sidenav-trigger i").hasClass("on")) {
      $("#slide-out").sidenav("close");
      $(this).find("i").removeClass("on");
      return false;
    } else {
      $(this).find("i").addClass("on");
    }
  });

  $('.collapsible').collapsible({
    onCloseEnd: function () {
      if ($('.country_info').is(':visible') == false) {
        $('.country_li').css('min-height', '0px')
      }
    }
  })

  $('.light_mode').on('click', function (e) {

    if ($(this).hasClass('nightlight')) {
      $(this).removeClass('nightlight').addClass('daylight');
      $('.mapboxgl-map').css("background-color", "rgb(161, 180, 193)");
      // map.setPaintProperty('gaul_0_simple', 'fill-color', '#dae0e4');
      // map.setPaintProperty('gaul_0_simple', 'fill-outline-color', '#a1b4c1');
      // map.setPaintProperty('gaul_0_simple', 'fill-opacity', 0.8);
    } else {
      $(this).removeClass('daylight').addClass('nightlight');
      $('.mapboxgl-map').css("background-color", "rgb(4, 22, 32)");
    }
  })

  $(".indicators_list .matrix_indicator_select").click(function() {
    $(this).addClass("on");
    $(".other_indicator_select").removeClass("on");
    $("#heatmap_tooltip").show();
    $("#matrix_wrapper").show();
    $(".dataTables_wrapper,.i_lollipop").hide();

    app.update_by_index("main_index_class");

    $("#tooltip_rank_instructions").hide();
    $("#rank_legend_dropdown").hide(225);
    $(".rank_legend_dropdown_button").hide();
    $(".legend_dropdown_button").show();
    $("#legend_dropdown").show(300);
  }) 
  
  $(".indicators_list .other_indicator_select").click(function() {
    $(this).addClass("on");
    $(".matrix_indicator_select").removeClass("on");
    $("#heatmap_tooltip").hide();
    $("#matrix_wrapper").hide();
    $(".dataTables_wrapper,.i_matrix").hide();
    $(".mapboxgl-ctrl-bottom-right").hide();

    app.setup_lollipop_graph();
    app.lollipop_graph("financial_index_val");

    $(".legend_dropdown_button").hide();
    $("#legend_dropdown").hide(300);
    $(".rank_legend_dropdown_button").show();
    $("#rank_legend_dropdown").show(225);

    $("#tooltip_rank_instructions").css("top", ($("#rank_legend_dropdown").position().top + 80) + "px");
    $("#tooltip_rank_instructions").show(225);
    $("#tooltip_rank_instructions .close").on("click", function() {
      $("#tooltip_rank_instructions").hide();
    });
  })
})