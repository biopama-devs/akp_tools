<?php

header('Content-Type: application/json');

// requested_level: 'level_0_gaul',
// param: 'hospitals',
// adm0_code: this_f_p.adm0_code

$param = $_GET['param'];
$layer = 'hospitals';
//$param='category_l';
//GET
//$query="select sum(px_val),adm2_code,adm1_code from ghs_wef where adm0_code=182 group by adm1_code,adm2_code
//order by sum desc";

$requested_level = $_GET['requested_level'];
switch ($requested_level) {
    case 'gaul_level_0_analysis':
        $code = 'adm0_code';
        # code...
        $join_table = 'gaul_0_info';
        $name = 'adm0_name';

        break;
    case 'gaul_level_1_analysis':
        $code = 'adm1_code';
        # code...
        $join_table = 'gaul_1_info_all';
        $name = 'adm1_name';
        break;
    case 'gaul_level_2_analysis':
        $code = 'adm2_code';
        # code...
        $join_table = 'gaul_2_info_all';
        $name = 'adm2_name';
        break;


    default:
        # code...
        break;
}

if ($layer == 'hospitals') {
    $link = pg_connect("host=myhost dbname=db user=usr password=pwd");
    // $query="select count($param) as count,$param,$code from hospitals_no_grid_gaul2 group by $code,$param order by $code desc";

    $query = "select h.adm0_code,count(pv_kw_h) as count,h.$code,gaul.$name as $name from hospitals_no_grid_gaul2 h
   inner join $join_table gaul
on h.adm0_code=" . (int)$_GET['adm0_code'] . "
and h.$code=gaul.$code::integer";
    if ($_GET['adm0_code']) {
        //$query.="where adm0_code=".$_GET['adm0_code'];

    }
    $query .= " group by h.adm0_code,h.$code,gaul.$name order by count desc";
}


$total_hospitals = 0;
$result = pg_query($link, $query);
//echo $query;
$json = array('data_arr' => array()); //,'adm1_codes2'=>array(),'adm2_codes'=>array());

$i = 0;

$count = pg_num_rows($result);

$adm_code_arr = array();

// EATE TABLE public.hospitals_no_grid_gaul2
// (
//   id bigint NOT NULL,
//   geom geometry(MultiPoint,4326),
//   facility_n character varying(254),
//   category_l character varying(254),
//   elctdemand numeric,
//   npv_consn numeric,
//   lcoe_h numeric,
//   pv_kw_h numeric,
//   bat_kwh_h numeric,
//   capex numeric,
//   npv_h numeric,
//   adm2_code numeric,
//   adm1_code numeric,
//   adm0_code numeric,
//   region character varying(254),
//   row_id numeric,
if ($count > 0) {
    while ($data = pg_fetch_array($result)) {

        $adm0 = (int)$data['adm0_code'];
        $adm_code = (int)$data[$code];

        $pos = array_search($adm_code, $adm_code_arr);
        //echo $pos.$i;

        if ($pos === false) {
            array_push($adm_code_arr, $adm_code);
            array_push($json['data_arr'], array(
                $code => $adm_code,
                $name => $data[$name],
                'adm0_code' => $adm0,
                'counts' => $data['count']
            ));
        }


        $total_hospitals += $data['count'];
        // else
        // {
        //   //  $pos=in_array($adm1, $adm1_code_arr);
        //   $pos=array_search($adm_code,$adm_code_arr);
        //   $json['data_arr'][$pos]['total']+=$data['count'];
        //    // echo 'pos'.$pos.' for '.$adm1;
        // //    var_dump($adm1_code_arr);
        // $p_pos=array_search($param,$json[$pos]['params_arr']);
        // if ($p_pos==false)
        // {
        //     array_push($json['data_arr'][$pos]['params_arr'],$data[$param]);
        //     array_push($json['data_arr'][$pos]['params_values'],array('param_name'=>$data[$param],'param_sum'=>$data['count']));
        // }

        // else
        // {

        //     array_push($json['data_arr'][$pos]['params_values'],array('param_name'=>$data[$param],'param_sum'=>$data['count']));
        // }





        // }

        if ($i == $count - 1) {
        }

        $i++;
    }
}

$all_val = array();


// $level_1_totals=array();
// $num_level_2=0;
// $num_level_1=0;
$i = 0;
foreach ($json['data_arr'] as $data) {

    array_push($all_val, (int)$data['total']);
}
$json['min'] = min($all_val);
$json['max'] = max($all_val);
$json['total_f'] = sizeof($all_val);
$json['avg'] = round((max($all_val) - min($all_val)) / sizeof($all_val));
$json['total_hospitals'] = $total_hospitals;
sort($all_val, SORT_NUMERIC);
$pos = (sizeof($all_val) + 1) / 2;
$json['median'] = $all_val[$pos];





//var_dump($all_val);

// $arrlength = count($all_val);
// for($x = 0; $x < $arrlength; $x++) {
//     echo $all_val[$x];
//     echo "<br>";
// }

// $level_1_min=min($level_1_totals);
// $level_1_max=max($level_1_totals);

// $level_2_min=min($mins);
// $level_2_max=max($maxs);

// // var_dump($json);

// //array_push($json['level_0_min'],$level_0_min);
// $json['level_1_max']=$level_1_max;
// $json['level_1_min']=$level_1_min;
// $json['num_level_1']=$num_level_1;
// $json['avg_level_1']=(int)(($level_1_max-$level_1_min)/$num_level_1);

// $json['level_2_max']=$level_2_max;
// $json['level_2_min']=$level_2_min;
// $json['num_level_2']=$num_level_2;
// $json['avg_level_2']=(int)(($level_2_max-$level_2_min)/$num_level_2);

$main_json = json_encode($json, JSON_NUMERIC_CHECK);
if ($_GET['callback']) {
    echo $_GET['callback'] . "(" . $main_json . ")";
} else {
    echo $main_json;
};
