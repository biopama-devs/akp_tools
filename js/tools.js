/*!
  Javascript - Tools
*/
(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.myModal = {
    attach: function(context) {
      $(context).find('body')
        .once('my-modal')
        .each(function () {
          var modalText = $('#toolModal .modal-body').text().trim();
          if (modalText){
            $( ".triggerClick" ).click();
          }
        });
    }
  };

})(jQuery, Drupal);