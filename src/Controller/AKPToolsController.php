<?php
/**
 * @file
 * Contains \Drupal\akp_tools\Controller\AKPToolsController.
 */

namespace Drupal\akp_tools\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Provides route responses for the akp_tools module.
 */
class AKPToolsController extends ControllerBase {
  public function pvdei() {
    $element = array(
      '#theme' => 'pvdei',
    );
    return $element;
  }

  public function energy() {
    $element = array(
      '#theme' => 'energy',
    );
    return $element;
  }
	
	public function energy_test() {
    $element = array(
      '#theme' => 'energy_test',
    );
    return $element;
  }

}