this_app.layers_styling = {
    symbol_layer_params: {},
    get_dasharray: function (layer_name, val) {
        var o = [{
            id: 'line-gapped-small',
            type: 'line',
            paint: {

                'line-dasharray': [1, 5]
            },

        }, {
            id: 'line-gapped-large',
            type: 'line',
            paint: {

                'line-dasharray': [1, 9]
            },

        }, {
            id: 'line-dashed-xlarge',
            type: 'line',
            paint: {

                'line-dasharray': [9, 2]
            },

        }, {
            id: 'normal',
            type: 'line',
            paint: {

                'line-dasharray': [1, 0]
            },

        },];

        if (layer_name == 'gaul_level_0')
            var layer_name = 'gaul_level_0_line';

        for (var p in o) {
            if (o[p].id == val) {
                console.log(o[p])

                map.setPaintProperty(layer_name, 'line-dasharray', o[p].paint['line-dasharray']);
            }
        }
    },
    get_width_by_param: function (layer, exp) {
        console.warn(arguments)
        var o_style = window[layer.id].paint;
        console.info(o_style)

        console.warn(o_style)


        var o = o_style['line-width']
        var new_o = o.map(function (d, i) {
            console.info(d)
            if (isNaN(d) === false && !(i % 2)) {

                console.log(d, exp, d * exp)
                if (exp == 0) {
                    return +(d - 1).toFixed(2);
                } else {
                    if (exp == 1) {
                        return d;
                    }
                    if (exp > 0) {
                        return +(d + (exp * 1.3)).toFixed(2);

                    }
                    if (exp < 0) {

                        if (+(d + (exp * 1.3)).toFixed(2) < 0) {
                            return 1
                        }
                        else {
                            return +(d + (exp * 1.3)).toFixed(2);
                        }



                    }
                }
            } else {


                return d
            }
        });
        return new_o


    },
    get_fontSize: function (layer, exp) {
        console.warn(arguments)
        var o_style = window[layer.id].layout;
        console.info(o_style)

        console.warn(o_style)


        var o = o_style['text-size']
        var new_o = o.map(function (d, i) {
            console.info(d)
            if (isNaN(d) === false && !(i % 2)) {

                console.log(d, exp, d + exp)
                if (exp == 0) {
                    return +(d - 1).toFixed(2);
                } else {
                    if (exp == 1) {
                        return d;
                    }
                    if (exp > 0) {
                        return +(d + (exp * 2)).toFixed(2);

                    }
                    if (exp < 0) {
                        console.log(exp, d, +(d + (exp * 2)).toFixed(2))


                        return +(d + (exp * 2)).toFixed(2);
                    }
                }


            } else {


                return d
            }
        });
        console.warn(new_o)
        return new_o


    }

}


function change_fontSize(layer_name, val) {
    console.log(arguments)

    var layer = window[layer_name];

    console.info(this_app.layers_styling.get_fontSize(layer, val))
    map.setLayoutProperty(layer_name, 'text-size', this_app.layers_styling.get_fontSize(layer, val))


}

function change_width(layer_name, val) {
    console.log(arguments)
    if (layer_name == 'gaul_level_0')
        var layer_name = 'gaul_level_0_line'

    var layer = window[layer_name];
    map.setPaintProperty(layer_name, 'line-width', this_app.layers_styling.get_width_by_param(layer, val));
}

function setup_layer_styling() {

    $('.stroke_dash .collapsible').collapsible();

    $('.stroke_dash .collection-item').on('click', function (e) {
        console.warn($(e.target), $(this))

        $(this).parent().find('.active').removeClass('active');
        $(this).addClass('active');



        this_app.layers_styling.get_dasharray(this_app.layers_styling.symbol_layer_params.layer_name, $(this).find('span').attr('val'));


    })

    $('.overlays_switcher_symbol .number.font_size')
        .niceNumber({
            autoSize: true,
            autoSizeBuffer: 1,
            buttonDecrement: '-',
            buttonIncrement: "+",
            buttonPosition: 'around',
            onDecrement: function ($currentInput, amount, settings) {
                console.log(arguments)

                if (this_app.layers_styling.symbol_layer_params.layer_name == 'gaul_level_0') {
                    label_layer_name = 'gaul_0_labels';
                }
                if (this_app.layers_styling.symbol_layer_params.layer_name == 'gaul_level_1') {
                    label_layer_name = 'gaul_1_labels';
                }
                if (this_app.layers_styling.symbol_layer_params.layer_name == 'gaul_level_2') {
                    label_layer_name = 'gaul_2_labels';
                }

                change_fontSize(label_layer_name, parseInt(amount))

            },
            onIncrement: function ($currentInput, amount, settings) {
                if (this_app.layers_styling.symbol_layer_params.layer_name == 'gaul_level_0') {
                    label_layer_name = 'gaul_0_labels';
                }
                if (this_app.layers_styling.symbol_layer_params.layer_name == 'gaul_level_1') {
                    label_layer_name = 'gaul_1_labels';
                }
                if (this_app.layers_styling.symbol_layer_params.layer_name == 'gaul_level_2') {
                    label_layer_name = 'gaul_2_labels';
                }

                change_fontSize(label_layer_name, parseInt(amount))

            }

        });
    $('.overlays_switcher_symbol .number.stroke_width')
        .niceNumber({
            autoSize: true,
            autoSizeBuffer: 1,
            buttonDecrement: '-',
            buttonIncrement: "+",
            buttonPosition: 'around',
            onDecrement: function ($currentInput, amount, settings) {
                console.log(arguments)


                change_width(this_app.layers_styling.symbol_layer_params.layer_name, parseInt(amount))
            },
            onIncrement: function ($currentInput, amount, settings) {
                console.log(arguments)
                change_width(this_app.layers_styling.symbol_layer_params.layer_name, parseInt(amount))
            }

        });
        /*
    var notype = new XNColorPicker({
        color: "#b9bacc",
        selector: ".notype.gaul_level_0.fill_color_tool",
        showprecolor: true, //显示预制颜色
        prevcolors: ['#EF534F', '#BA69C8', '#FFD54F', '#81C784', '#7FDEEA', '#90CAF9', '#F44436', '#AB47BC', '#FFC106', '#66BB6A', '#25C6DA', '#4EC3F7', '#E53934', '#9D27B0', '#FFA726', '#4CAF50', '#00ACC1', '#29B6F6', '#D32E30', '#8F24AA', '#FB8C01', '#378E3C', '#0097A7', '#02AAF4', '#C62928', '#7B1FA2', '#F57C02', '#2F7D31', '#00838F', '#029BE5', '#B71B1C', '#6A1B9A', '#EF6C00', '#34691D', '#006164', '#0388D1', '#980A0B', '#4A148C', '#E65100', '#1A5E20', '#004D41', '#01579B', 'rgba(0,0,0,0)', '#FFFFFF', '#DBDBDB', '#979797', '#606060', '#000000'],
        showhistorycolor: false, //显示历史
        historycolornum: 8, //历史条数
        format: 'hex', //rgba hex hsla,初始颜色类型
        showPalette: true, //显示色盘
        show: false, //初始化显示
        lang: 'en', // cn 、en
        color: '#1a1a1a',
        colorTypeOption: 'single,linear-gradient,radial-gradient', //
        autoConfirm: false,
        hideInputer: false,
        hideCancelButton: false,
        hideConfirmButton: false,
        onError: function (e) {

        },
        onCancel: function (color) {
            console.log("cancel", color)
        },
        onChange: function (color) {
            console.info(color)



            map.setPaintProperty(
                'gaul_level_0',

                "fill-color", color.color.hex)


        },
        onConfirm: function (color) {
            console.info(color)
            map.setPaintProperty(
                'gaul_level_0',

                "fill-color", color.color.hex)


        }
    })
    var notype = new XNColorPicker({
        color: "#b9bacc",
        selector: ".notype.gaul_level_0.font_color_tool",
        showprecolor: true, //显示预制颜色
        prevcolors: ['#EF534F', '#BA69C8', '#FFD54F', '#81C784', '#7FDEEA', '#90CAF9', '#F44436', '#AB47BC', '#FFC106', '#66BB6A', '#25C6DA', '#4EC3F7', '#E53934', '#9D27B0', '#FFA726', '#4CAF50', '#00ACC1', '#29B6F6', '#D32E30', '#8F24AA', '#FB8C01', '#378E3C', '#0097A7', '#02AAF4', '#C62928', '#7B1FA2', '#F57C02', '#2F7D31', '#00838F', '#029BE5', '#B71B1C', '#6A1B9A', '#EF6C00', '#34691D', '#006164', '#0388D1', '#980A0B', '#4A148C', '#E65100', '#1A5E20', '#004D41', '#01579B', 'rgba(0,0,0,0)', '#FFFFFF', '#DBDBDB', '#979797', '#606060', '#000000'],
        showhistorycolor: false, //显示历史
        historycolornum: 8, //历史条数
        format: 'hex', //rgba hex hsla,初始颜色类型
        showPalette: true, //显示色盘
        show: false, //初始化显示
        lang: 'en', // cn 、en
        color: '#ff9800',
        colorTypeOption: 'single,linear-gradient,radial-gradient', //
        autoConfirm: true,
        hideInputer: false,
        hideCancelButton: false,
        hideConfirmButton: false,
        onError: function (e) {

        },
        onCancel: function (color) {
            console.log("cancel", color)
        },
        onChange: function (color) {

            map.setPaintProperty('gaul_0_labels', 'text-color', color.color.hex)


        },
        onConfirm: function (color) {

            map.setPaintProperty('gaul_0_labels', 'text-color', color.color.hex)


        }
    })

    var notype = new XNColorPicker({
        color: "#b9bacc",
        selector: ".notype.gaul_level_1.font_color_tool",
        showprecolor: true, //显示预制颜色
        prevcolors: ['#EF534F', '#BA69C8', '#FFD54F', '#81C784', '#7FDEEA', '#90CAF9', '#F44436', '#AB47BC', '#FFC106', '#66BB6A', '#25C6DA', '#4EC3F7', '#E53934', '#9D27B0', '#FFA726', '#4CAF50', '#00ACC1', '#29B6F6', '#D32E30', '#8F24AA', '#FB8C01', '#378E3C', '#0097A7', '#02AAF4', '#C62928', '#7B1FA2', '#F57C02', '#2F7D31', '#00838F', '#029BE5', '#B71B1C', '#6A1B9A', '#EF6C00', '#34691D', '#006164', '#0388D1', '#980A0B', '#4A148C', '#E65100', '#1A5E20', '#004D41', '#01579B', 'rgba(0,0,0,0)', '#FFFFFF', '#DBDBDB', '#979797', '#606060', '#000000'],
        showhistorycolor: false, //显示历史
        historycolornum: 8, //历史条数
        format: 'hex', //rgba hex hsla,初始颜色类型
        showPalette: true, //显示色盘
        show: false, //初始化显示
        lang: 'en', // cn 、en
        colorTypeOption: 'single,linear-gradient,radial-gradient', //
        autoConfirm: true,
        hideInputer: false,
        hideCancelButton: false,
        hideConfirmButton: false,
        onError: function (e) {

        },
        onCancel: function (color) {
            console.log("cancel", color)
        },
        onChange: function (color) {
            console.info(color)
            map.setPaintProperty('gaul_1_labels', 'text-color', color.color.hex)


        },
        onConfirm: function (color) {
            console.info(color)
            map.setPaintProperty('gaul_1_labels', 'text-color', color.color.hex)


        }
    })

    var notype = new XNColorPicker({
        color: "#b9bacc",
        selector: ".notype.gaul_level_2.font_color_tool",
        showprecolor: true, //显示预制颜色
        prevcolors: ['#EF534F', '#BA69C8', '#FFD54F', '#81C784', '#7FDEEA', '#90CAF9', '#F44436', '#AB47BC', '#FFC106', '#66BB6A', '#25C6DA', '#4EC3F7', '#E53934', '#9D27B0', '#FFA726', '#4CAF50', '#00ACC1', '#29B6F6', '#D32E30', '#8F24AA', '#FB8C01', '#378E3C', '#0097A7', '#02AAF4', '#C62928', '#7B1FA2', '#F57C02', '#2F7D31', '#00838F', '#029BE5', '#B71B1C', '#6A1B9A', '#EF6C00', '#34691D', '#006164', '#0388D1', '#980A0B', '#4A148C', '#E65100', '#1A5E20', '#004D41', '#01579B', 'rgba(0,0,0,0)', '#FFFFFF', '#DBDBDB', '#979797', '#606060', '#000000'],
        showhistorycolor: true, //显示历史
        historycolornum: 8, //历史条数
        format: 'hsla', //rgba hex hsla,初始颜色类型
        showPalette: true, //显示色盘
        show: false, //初始化显示
        lang: 'en', // cn 、en
        colorTypeOption: 'single', //
        autoConfirm: true,
        onError: function (e) {

        },
        onCancel: function (color) {
            console.log("cancel", color)
        },
        onChange: function (color) {
            console.log(color)
            map.setPaintProperty('gaul_2_labels', 'text-color', color.color.hex)


        },
        onConfirm: function (color) {

            console.log(color)
            map.setPaintProperty('gaul_2_labels', 'text-color', color.color.hex)


        }
    })
    var notype = new XNColorPicker({
        color: "#b9bacc",
        selector: ".notype.gaul_level_0.stroke_color_tool",
        showprecolor: true, //显示预制颜色
        prevcolors: ['#EF534F', '#BA69C8', '#FFD54F', '#81C784', '#7FDEEA', '#90CAF9', '#F44436', '#AB47BC', '#FFC106', '#66BB6A', '#25C6DA', '#4EC3F7', '#E53934', '#9D27B0', '#FFA726', '#4CAF50', '#00ACC1', '#29B6F6', '#D32E30', '#8F24AA', '#FB8C01', '#378E3C', '#0097A7', '#02AAF4', '#C62928', '#7B1FA2', '#F57C02', '#2F7D31', '#00838F', '#029BE5', '#B71B1C', '#6A1B9A', '#EF6C00', '#34691D', '#006164', '#0388D1', '#980A0B', '#4A148C', '#E65100', '#1A5E20', '#004D41', '#01579B', 'rgba(0,0,0,0)', '#FFFFFF', '#DBDBDB', '#979797', '#606060', '#000000'],
        showhistorycolor: true, //显示历史
        historycolornum: 8, //历史条数
        format: 'hsla', //rgba hex hsla,初始颜色类型
        showPalette: true, //显示色盘
        show: false, //初始化显示
        lang: 'en', // cn 、en
        colorTypeOption: 'single', //
        autoConfirm: true,
        onError: function (e) {

        },
        onCancel: function (color) {
            console.log("cancel", color)
        },
        onChange: function (color) {

            map.setPaintProperty(
                'gaul_level_0_line',
                "line-color", color.color.hex)


        },
        onConfirm: function (color) {


            map.setPaintProperty(
                'gaul_level_0_line',
                "line-color", color.color.hex)


        }
    })
    var notype = new XNColorPicker({
        color: "#b9bacc",
        selector: ".notype.gaul_level_1.stroke_color_tool",
        showprecolor: true, //显示预制颜色
        prevcolors: ['#EF534F', '#BA69C8', '#FFD54F', '#81C784', '#7FDEEA', '#90CAF9', '#F44436', '#AB47BC', '#FFC106', '#66BB6A', '#25C6DA', '#4EC3F7', '#E53934', '#9D27B0', '#FFA726', '#4CAF50', '#00ACC1', '#29B6F6', '#D32E30', '#8F24AA', '#FB8C01', '#378E3C', '#0097A7', '#02AAF4', '#C62928', '#7B1FA2', '#F57C02', '#2F7D31', '#00838F', '#029BE5', '#B71B1C', '#6A1B9A', '#EF6C00', '#34691D', '#006164', '#0388D1', '#980A0B', '#4A148C', '#E65100', '#1A5E20', '#004D41', '#01579B', 'rgba(0,0,0,0)', '#FFFFFF', '#DBDBDB', '#979797', '#606060', '#000000'],
        showhistorycolor: true, //显示历史
        historycolornum: 8, //历史条数
        format: 'hsla', //rgba hex hsla,初始颜色类型
        showPalette: true, //显示色盘
        show: false, //初始化显示
        lang: 'en', // cn 、en
        colorTypeOption: 'single', //
        autoConfirm: true,
        onError: function (e) {

        },
        onCancel: function (color) {
            console.log("cancel", color)
        },
        onChange: function (color) {

            map.setPaintProperty(
                'gaul_level_1',
                "line-color", color.color.hex)


        },
        onConfirm: function (color) {


            map.setPaintProperty(
                'gaul_level_1',
                "line-color", color.color.hex)


        }
    })

    var notype = new XNColorPicker({
        color: "#b9bacc",
        selector: ".notype.gaul_level_2.stroke_color_tool",
        showprecolor: true, //显示预制颜色
        prevcolors: ['#EF534F', '#BA69C8', '#FFD54F', '#81C784', '#7FDEEA', '#90CAF9', '#F44436', '#AB47BC', '#FFC106', '#66BB6A', '#25C6DA', '#4EC3F7', '#E53934', '#9D27B0', '#FFA726', '#4CAF50', '#00ACC1', '#29B6F6', '#D32E30', '#8F24AA', '#FB8C01', '#378E3C', '#0097A7', '#02AAF4', '#C62928', '#7B1FA2', '#F57C02', '#2F7D31', '#00838F', '#029BE5', '#B71B1C', '#6A1B9A', '#EF6C00', '#34691D', '#006164', '#0388D1', '#980A0B', '#4A148C', '#E65100', '#1A5E20', '#004D41', '#01579B', 'rgba(0,0,0,0)', '#FFFFFF', '#DBDBDB', '#979797', '#606060', '#000000'],
        showhistorycolor: true, //显示历史
        historycolornum: 8, //历史条数
        format: 'hsla', //rgba hex hsla,初始颜色类型
        showPalette: true, //显示色盘
        show: false, //初始化显示
        lang: 'en', // cn 、en
        colorTypeOption: 'single', //
        autoConfirm: true,
        onError: function (e) {

        },
        onCancel: function (color) {
            console.log("cancel", color)
        },
        onChange: function (color) {
            console.log(color)
            map.setPaintProperty(
                'gaul_level_2',
                "line-color", color.color.hex)


        },
        onConfirm: function (color) {


            map.setPaintProperty(
                'gaul_level_2',
                "line-color", color.color.hex)


        }
    });
*/

    $('#menusss input.no_fill').on('click', function () {
        console.log($(this).prop('checked'));
        if ($(this).prop('checked')) {
            map.setPaintProperty('gaul_level_0', 'fill-opacity', 0);
            $('.fill_color_tool').fadeOut();


        } else {
            map.setPaintProperty('gaul_level_0', 'fill-opacity', 1);
            $('.fill_color_tool').fadeIn();

        }
    })
    $('.menu_close').on('click', function () {

        $('.mapboxgl-ctrl-icon.change_basemap').trigger('click');
    })

    /*
    $('#menu .notype')
        .attr('data-tooltip', function () {
            if ($(this).hasClass('stroke_color_tool')) {
                return '<span style="font-size:0.7rem;color:white">Change stroke color</span>';
            }
            if ($(this).hasClass('font_color_tool')) {
                return '<span style="font-size:0.7rem;color:white">Change labels color</span>';
            }
            if ($(this).hasClass('fill_color_tool')) {

                return '<span style="font-size:0.7rem;color:white">Change background color</span>';
            }
        })

        .tooltip_materialize({
            delay: 50,
            delayOut: 50000,
            position: 'left',
            html: true
        })
        */

    
    $('.overlays_switcher_labels').eq(0).addClass('active').show();

    $('.menu_styling_back').on('click', function () {
        $('.overlays_switcher_palette').find('.active').removeClass('active');
        $('#menu').addClass('active');
        $('.overlays_switcher_symbol_container').hide();
        $('.menu_switcher_container').show();
    });

    $('.overlays_switcher_labels,.overlays_switcher_palette').tooltip_materialize();
    $('.overlays_switcher_row .overlays_switcher_labels').on('click', function () {

        $(this).find('i').toggleClass('active')

        var parent = $(this).parents().closest('.overlays_switcher_row');
        console.log(parent)
        var layer_name = parent.find('input:checkbox').attr('id');


        switch (layer_name) {
            case 'gaul_level_0': var label_layer = 'gaul_0_labels'; break;
            case 'gaul_level_1': var label_layer = 'gaul_1_labels'; break;
            case 'gaul_level_2': var label_layer = 'gaul_2_labels'; break;
            default: break;
        }
        console.log(layer_name, label_layer)
        if ($(this).find('i').hasClass('active') == false) {
            map.removeLayer(label_layer);
        }
        else {
            if (layer_name == 'gaul_level_0')
                var layer_name = 'gaul_level_0_line';

            if (!map.getLayer(label_layer))
                map.addLayer(window[label_layer], layer_name);
            /*  var pos = map.getStyle().layers.map(d => d.id).indexOf(layer_name);
             if (pos > -1) {
                 map.addLayer(window[label_layer], layer_name)

             }
             else {
             } */
        }

    })
    $('.overlays_switcher_palette').on('click', function () {




        var parent = $(this).parents().closest('.overlays_switcher_row');
        console.log(parent)
        debugger
        $(this).find('i').toggleClass('active')
        var layer_name = parent.find('input:checkbox').attr('id');

        this_app.layers_styling.symbol_layer_params.layer_name = layer_name;

        var baseLayer_symbol_container = $('.overlays_switcher_symbol_container[_layer="' + layer_name + '"]');
        console.log(baseLayer_symbol_container)

        if ($(this).find('i').hasClass('active')) {
            // $('#overlays_switcher').find('.active').removeClass('active');


            $('.overlays_switcher_symbol').show().find('>div').hide();
            console.log(baseLayer_symbol_container)
            baseLayer_symbol_container.show()
        }
        else {
            baseLayer_symbol_container.hide();

        }


    })

    $('#overlays_switcher input').on('click', function (e) {
        console.log($(e.target))
        if ($(e.target).is('i') == false) {
            var parent = $(this).parents().closest('.overlays_switcher_row');
            console.warn(parent)
            console.log('yes inputing???')

            var palette = parent.find('.overlays_switcher_palette');
            console.warn(palette)
            var labels = parent.find('.overlays_switcher_labels');
            console.warn(palette)
            if (!palette.is(':visible')) {

                $('.overlays_switcher_palette.active').removeClass('active')
                console.warn('show palette')
                palette.css('display', 'inline-block');
                palette.addClass('active');
                labels.show();
                labels.addClass('active').find('i').addClass('active');




            } else {
                console.warn('hide palette')
                palette.css('display', 'none');
                palette.removeClass('active');
                labels.hide().find('i').removeClass('active');;
            }

        } else {
            console.log('no inputing')
        }
    })


    var first_click = true;
    $('#menu #baselayers_switcher').on('click', function (e) {

        console.trace()

        if ($(e.target).hasClass('close_menu')) {
            $('.change_basemap').trigger('click');
        }

        if ($(e.target).is('input')) {
            $('#menu #baselayers_switcher').find('input').each(function (i, elem) {

                var _this = $(elem);

                _this.prop('checked', false)

            });
            $(e.target).prop('checked', true)
            var layerId = $(e.target).attr('id');
            console.log(layerId)
            if (!map.getLayer(layerId)) {

                $('.baselayers_switcher_container').removeClass('s12').addClass('s4');
                $('.baselayers_switcher_symbol').show()

                var t = base_layers.filter(function (d) {

                    return d.id == layerId;
                });
                console.log(t)
                t[0].active = true;



                console.warn(map.getStyle().layers)

                if (map.getStyle().layers[0]) {


                    if (first_click == false) {
                        if ($('.no_fill').prop('checked') == false)
                            $('.no_fill').trigger('click');

                    }
                    else {
                        first_click = false;
                    }

                    if (layerId !== 'gaul_0_simple') {

                        if (map.getStyle().layers[0] && map.getStyle().layers[0].id !== 'gaul_level_0') {
                            map.removeLayer(map.getStyle().layers[0].id);

                            console.warn(map.getStyle().layers[0])
                            map.addLayer(t[0], map.getStyle().layers[0].id);
                        } else {
                            setTimeout(function () {
                                if (map.getStyle().layers[0] && map.getStyle().layers[0].id) {
                                    map.removeLayer(map.getStyle().layers[0].id);

                                    map.addLayer(t[0], map.getStyle().layers[0].id);
                                }
                                else {
                                    setTimeout(function () {
                                        if (map.getStyle().layers[0] && map.getStyle().layers[0].id) {
                                            map.removeLayer(map.getStyle().layers[0].id);

                                            map.addLayer(t[0], map.getStyle().layers[0].id);
                                        }
                                        else {
                                            alert('Looks like there is a problem loading the layer...')
                                            console.info(map.getStyle().layers)
                                        }

                                    }, 3000)
                                }

                            }, 1000)
                        }
                    }
                }





            }

        }
    })


    $('#menu .overlays_switcher_symbol input').on('click', function (e) {
        if ($(e.target).hasClass('no_fill')) {

            if (!map.getLayer('gaul_level_0'))
                map.addLayer(window['gaul_level_0'], 'gaul_level_0_line');

            if ($(e.target).prop('checked')) {
                map.setPaintProperty('gaul_level_0', 'fill-opacity', 0);
                $('.fill_color_tool').fadeOut();


            } else {
                map.setPaintProperty('gaul_level_0', 'fill-opacity', 1);
                $('.fill_color_tool').fadeIn();

            }


        }
    })

    $('#menu #overlays_switcher input').on('click', function (e) {

        console.info($(e.target))
        if ($(e.target).is('input')) {


            var layerId = $(e.target).attr('id');
            console.log($(e.target).is('input'), $(e.target).is(':checked'), $(e.target).prop('checked'))
            if ($(e.target).prop('checked')) {


                if (layerId == 'gaul_level_0') {

                    var base_id = map.getStyle().layers[1].id;



                    if (!map.getLayer('gaul_level_0')) {
                        if (!map.getLayer('gaul_level_0_line'))
                            map.addLayer(window['gaul_level_0_line'])


                        if (!map.getLayer('gaul_0_labels'))
                            map.addLayer(window['gaul_0_labels'], 'gaul_level_0_line')

                        map.addLayer(window[layerId], base_id);

                    }
                    else {

                        map.moveLayer('gaul_level_0', base_id)
                    }
                }

                if (layerId == 'gaul_level_2') {
                    var pos = map.getStyle().layers.map(d => d.id).indexOf('gaul_level_1');
                    if (pos > -1) {

                        map.addLayer(window[layerId], 'gaul_level_1')
                        map.addLayer(window['gaul_2_labels'], layerId);
                    }
                    else {
                        var pos = map.getStyle().layers.map(d => d.id).indexOf('gaul_level_0_line');
                        if (pos > -1) {
                            if (!map.getLayer(layerId))
                                map.addLayer(window[layerId], 'gaul_level_0_line')
                            else
                                map.moveLayer(layerId, 'gaul_level_0_line')

                            if (!map.getLayer('gaul_2_labels'))
                                map.addLayer(window['gaul_2_labels'], layerId);
                            else
                                map.moveLayer('gaul_2_labels', layerId)
                        }
                        else {
                            var base_id = map.getStyle().layers[1].id;
                            if (!map.getLayer(layerId)) {
                                map.addLayer(window[layerId], base_id)

                            }
                            else {
                                map.moveLayer(layerId, base_id)
                            }
                            if (!map.getLayer('gaul_2_labels'))
                                map.addLayer(window['gaul_2_labels'], layerId);
                        }
                    }


                }
                if (layerId == 'gaul_level_1') {

                    //make sure always over gaul_level_0 and under gaul_level_2
                    var pos_2 = map.getStyle().layers.map(d => d.id).indexOf('gaul_level_2');
                    if (pos_2 > -1) {

                        //gert ovwer it
                        var over = map.getStyle().layers[pos_2 + 1].id;
                        console.warn(map.getStyle().layers[pos_2 + 1].id)
                        map.addLayer(window[layerId], over)
                        map.addLayer(window['gaul_1_labels'], layerId);
                    }
                    else {
                        var base_id = map.getStyle().layers[1].id;
                        var pos = map.getStyle().layers.map(d => d.id).indexOf('gaul_level_0_line');
                        if (pos > -1) {
                            map.addLayer(window[layerId], 'gaul_level_0_line')
                            map.addLayer(window['gaul_1_labels'], layerId);
                        }
                        else {
                            var base_id = map.getStyle().layers[1].id;
                            map.addLayer(window[layerId], base_id)
                            map.addLayer(window['gaul_1_labels'], layerId);
                        }
                    }


                }



            } else {

                if (map.getLayer(layerId)) {

                    map.removeLayer(layerId);

                    if (layerId == 'gaul_level_0') {
                        if (map.getLayer('gaul_level_0_line'))
                            map.removeLayer('gaul_level_0_line');

                        if (map.getLayer('gaul_0_labels'))
                            map.removeLayer('gaul_0_labels');

                    }

                    if (layerId == 'gaul_level_1') {
                        if (map.getLayer('gaul_1_labels'))
                            map.removeLayer('gaul_1_labels')
                    }
                    if (layerId == 'gaul_level_2') {
                        if (map.getLayer('gaul_2_labels'))
                            map.removeLayer('gaul_2_labels')
                    }

                }
                else {
                    console.log('we should be removing...?')
                }
            }

        }
    })
}