var adm0_json = [{ "adm0_name": "Algeria", "adm0_code": 4, "bbox": { "type": "Polygon", "coordinates": [
            [
                [-8.67, 18.96],
                [-8.67, 37.09],
                [12, 37.09],
                [12, 18.96],
                [-8.67, 18.96]
            ]
        ] }, "centroid": [2.68, 28.16] }, { "adm0_name": "Angola", "adm0_code": 8, "bbox": { "type": "Polygon", "coordinates": [
            [
                [11.67, -18.04],
                [11.67, -4.39],
                [24.09, -4.39],
                [24.09, -18.04],
                [11.67, -18.04]
            ]
        ] }, "centroid": [17.55, -12.3] }, { "adm0_name": "Benin", "adm0_code": 29, "bbox": { "type": "Polygon", "coordinates": [
            [
                [0.77, 6.23],
                [0.77, 12.41],
                [3.85, 12.41],
                [3.85, 6.23],
                [0.77, 6.23]
            ]
        ] }, "centroid": [2.34, 9.66] }, { "adm0_name": "Botswana", "adm0_code": 35, "bbox": { "type": "Polygon", "coordinates": [
            [
                [20, -26.91],
                [20, -17.78],
                [29.36, -17.78],
                [29.36, -26.91],
                [20, -26.91]
            ]
        ] }, "centroid": [23.81, -22.19] }, { "adm0_name": "Burkina Faso", "adm0_code": 42, "bbox": { "type": "Polygon", "coordinates": [
            [
                [-5.52, 9.4],
                [-5.52, 15.08],
                [2.41, 15.08],
                [2.41, 9.4],
                [-5.52, 9.4]
            ]
        ] }, "centroid": [-1.75, 12.27] }, { "adm0_name": "Burundi", "adm0_code": 43, "bbox": { "type": "Polygon", "coordinates": [
            [
                [29, -4.47],
                [29, -2.31],
                [30.85, -2.31],
                [30.85, -4.47],
                [29, -4.47]
            ]
        ] }, "centroid": [29.89, -3.37] }, { "adm0_name": "Cameroon", "adm0_code": 45, "bbox": { "type": "Polygon", "coordinates": [
            [
                [8.5, 1.65],
                [8.5, 13.08],
                [16.19, 13.08],
                [16.19, 1.65],
                [8.5, 1.65]
            ]
        ] }, "centroid": [12.74, 5.69] }, { "adm0_name": "Cape Verde", "adm0_code": 47, "bbox": { "type": "Polygon", "coordinates": [
            [
                [-25.36, 14.8],
                [-25.36, 17.2],
                [-22.67, 17.2],
                [-22.67, 14.8],
                [-25.36, 14.8]
            ]
        ] }, "centroid": [-23.98, 15.94] }, { "adm0_name": "Central African Republic", "adm0_code": 49, "bbox": { "type": "Polygon", "coordinates": [
            [
                [14.42, 2.22],
                [14.42, 11.01],
                [27.46, 11.01],
                [27.46, 2.22],
                [14.42, 2.22]
            ]
        ] }, "centroid": [20.49, 6.58] }, { "adm0_name": "Chad", "adm0_code": 50, "bbox": { "type": "Polygon", "coordinates": [
            [
                [13.47, 7.44],
                [13.47, 23.44],
                [24, 23.44],
                [24, 7.44],
                [13.47, 7.44]
            ]
        ] }, "centroid": [18.66, 15.34] }, { "adm0_name": "Comoros", "adm0_code": 58, "bbox": { "type": "Polygon", "coordinates": [
            [
                [43.23, -12.42],
                [43.23, -11.36],
                [44.54, -11.36],
                [44.54, -12.42],
                [43.23, -12.42]
            ]
        ] }, "centroid": [43.68, -11.89] }, { "adm0_name": "Congo", "adm0_code": 59, "bbox": { "type": "Polygon", "coordinates": [
            [
                [11.2, -5],
                [11.2, 3.7],
                [18.65, 3.7],
                [18.65, -5],
                [11.2, -5]
            ]
        ] }, "centroid": [15.24, -0.83] }, { "adm0_name": "C\ufffdte d'Ivoire", "adm0_code": 66, "bbox": { "type": "Polygon", "coordinates": [
            [
                [-8.6, 4.36],
                [-8.6, 10.74],
                [-2.49, 10.74],
                [-2.49, 4.36],
                [-8.6, 4.36]
            ]
        ] }, "centroid": [-5.55, 7.63] }, { "adm0_name": "Democratic Republic of the Congo", "adm0_code": 68, "bbox": { "type": "Polygon", "coordinates": [
            [
                [12.2, -13.46],
                [12.2, 5.39],
                [31.31, 5.39],
                [31.31, -13.46],
                [12.2, -13.46]
            ]
        ] }, "centroid": [23.66, -2.88] }, { "adm0_name": "Djibouti", "adm0_code": 70, "bbox": { "type": "Polygon", "coordinates": [
            [
                [41.77, 10.91],
                [41.77, 12.71],
                [43.45, 12.71],
                [43.45, 10.91],
                [41.77, 10.91]
            ]
        ] }, "centroid": [42.58, 11.73] }, { "adm0_name": "Equatorial Guinea", "adm0_code": 76, "bbox": { "type": "Polygon", "coordinates": [
            [
                [5.62, -1.47],
                [5.62, 3.79],
                [11.34, 3.79],
                [11.34, -1.47],
                [5.62, -1.47]
            ]
        ] }, "centroid": [10.33, 1.7] }, { "adm0_name": "Eritrea", "adm0_code": 77, "bbox": { "type": "Polygon", "coordinates": [
            [
                [36.44, 12.35],
                [36.44, 18.01],
                [43.16, 18.01],
                [43.16, 12.35],
                [36.44, 12.35]
            ]
        ] }, "centroid": [38.84, 15.38] }, { "adm0_name": "Ethiopia", "adm0_code": 79, "bbox": { "type": "Polygon", "coordinates": [
            [
                [33, 3.4],
                [33, 14.89],
                [47.99, 14.89],
                [47.99, 3.4],
                [33, 3.4]
            ]
        ] }, "centroid": [39.64, 8.63] }, { "adm0_name": "Gabon", "adm0_code": 89, "bbox": { "type": "Polygon", "coordinates": [
            [
                [8.7, -3.99],
                [8.7, 2.32],
                [14.5, 2.32],
                [14.5, -3.99],
                [8.7, -3.99]
            ]
        ] }, "centroid": [11.79, -0.62] }, { "adm0_name": "Gambia", "adm0_code": 90, "bbox": { "type": "Polygon", "coordinates": [
            [
                [-16.82, 13.06],
                [-16.82, 13.82],
                [-13.8, 13.82],
                [-13.8, 13.06],
                [-16.82, 13.06]
            ]
        ] }, "centroid": [-15.45, 13.45] }, { "adm0_name": "Ghana", "adm0_code": 94, "bbox": { "type": "Polygon", "coordinates": [
            [
                [-3.26, 4.74],
                [-3.26, 11.17],
                [1.2, 11.17],
                [1.2, 4.74],
                [-3.26, 4.74]
            ]
        ] }, "centroid": [-1.21, 7.97] }, { "adm0_name": "Guinea", "adm0_code": 106, "bbox": { "type": "Polygon", "coordinates": [
            [
                [-15.08, 7.19],
                [-15.08, 12.68],
                [-7.64, 12.68],
                [-7.64, 7.19],
                [-15.08, 7.19]
            ]
        ] }, "centroid": [-10.91, 10.44] }, { "adm0_name": "Guinea-Bissau", "adm0_code": 105, "bbox": { "type": "Polygon", "coordinates": [
            [
                [-16.71, 10.86],
                [-16.71, 12.68],
                [-13.63, 12.68],
                [-13.63, 10.86],
                [-16.71, 10.86]
            ]
        ] }, "centroid": [-14.91, 12.04] }, { "adm0_name": "Ilemi triangle", "adm0_code": 61013, "bbox": { "type": "Polygon", "coordinates": [
            [
                [34.38, 4.62],
                [34.38, 5.02],
                [35.51, 5.02],
                [35.51, 4.62],
                [34.38, 4.62]
            ]
        ] }, "centroid": [35.04, 4.76] }, { "adm0_name": "Kenya", "adm0_code": 133, "bbox": { "type": "Polygon", "coordinates": [
            [
                [33.91, -4.72],
                [33.91, 4.62],
                [41.91, 4.62],
                [41.91, -4.72],
                [33.91, -4.72]
            ]
        ] }, "centroid": [37.86, 0.53] }, { "adm0_name": "Lesotho", "adm0_code": 142, "bbox": { "type": "Polygon", "coordinates": [
            [
                [27.03, -30.67],
                [27.03, -28.57],
                [29.47, -28.57],
                [29.47, -30.67],
                [27.03, -30.67]
            ]
        ] }, "centroid": [28.25, -29.58] }, { "adm0_name": "Liberia", "adm0_code": 144, "bbox": { "type": "Polygon", "coordinates": [
            [
                [-11.5, 4.36],
                [-11.5, 8.55],
                [-7.37, 8.55],
                [-7.37, 4.36],
                [-11.5, 4.36]
            ]
        ] }, "centroid": [-9.31, 6.45] }, { "adm0_name": "Madagascar", "adm0_code": 150, "bbox": { "type": "Polygon", "coordinates": [
            [
                [43.19, -25.61],
                [43.19, -11.95],
                [50.49, -11.95],
                [50.49, -25.61],
                [43.19, -25.61]
            ]
        ] }, "centroid": [46.7, -19.39] }, { "adm0_name": "Malawi", "adm0_code": 152, "bbox": { "type": "Polygon", "coordinates": [
            [
                [32.67, -17.13],
                [32.67, -9.37],
                [35.93, -9.37],
                [35.93, -17.13],
                [32.67, -17.13]
            ]
        ] }, "centroid": [34.31, -13.22] }, { "adm0_name": "Mali", "adm0_code": 155, "bbox": { "type": "Polygon", "coordinates": [
            [
                [-12.24, 10.16],
                [-12.24, 25],
                [4.24, 25],
                [4.24, 10.16],
                [-12.24, 10.16]
            ]
        ] }, "centroid": [-3.52, 17.36] }, { "adm0_name": "Mauritania", "adm0_code": 159, "bbox": { "type": "Polygon", "coordinates": [
            [
                [-17.07, 14.72],
                [-17.07, 27.31],
                [-4.83, 27.31],
                [-4.83, 14.72],
                [-17.07, 14.72]
            ]
        ] }, "centroid": [-10.33, 20.26] }, { "adm0_name": "Mauritius", "adm0_code": 160, "bbox": { "type": "Polygon", "coordinates": [
            [
                [56.59, -20.53],
                [56.59, -10.34],
                [63.5, -10.34],
                [63.5, -20.53],
                [56.59, -20.53]
            ]
        ] }, "centroid": [57.89, -20.13] }, { "adm0_name": "Mozambique", "adm0_code": 170, "bbox": { "type": "Polygon", "coordinates": [
            [
                [30.22, -26.87],
                [30.22, -10.47],
                [40.84, -10.47],
                [40.84, -26.87],
                [30.22, -26.87]
            ]
        ] }, "centroid": [35.55, -17.26] }, { "adm0_name": "Namibia", "adm0_code": 172, "bbox": { "type": "Polygon", "coordinates": [
            [
                [11.74, -28.97],
                [11.74, -16.96],
                [25.26, -16.96],
                [25.26, -28.97],
                [11.74, -28.97]
            ]
        ] }, "centroid": [17.22, -22.14] }, { "adm0_name": "Niger", "adm0_code": 181, "bbox": { "type": "Polygon", "coordinates": [
            [
                [0.17, 11.7],
                [0.17, 23.51],
                [16, 23.51],
                [16, 11.7],
                [0.17, 11.7]
            ]
        ] }, "centroid": [9.4, 17.42] }, { "adm0_name": "Nigeria", "adm0_code": 182, "bbox": { "type": "Polygon", "coordinates": [
            [
                [2.67, 4.27],
                [2.67, 13.9],
                [14.68, 13.9],
                [14.68, 4.27],
                [2.67, 4.27]
            ]
        ] }, "centroid": [8.1, 9.59] }, { "adm0_name": "Rwanda", "adm0_code": 205, "bbox": { "type": "Polygon", "coordinates": [
            [
                [28.86, -2.84],
                [28.86, -1.05],
                [30.9, -1.05],
                [30.9, -2.84],
                [28.86, -2.84]
            ]
        ] }, "centroid": [29.92, -2] }, { "adm0_name": "Sao Tome and Principe", "adm0_code": 214, "bbox": { "type": "Polygon", "coordinates": [
            [
                [6.46, -0.01],
                [6.46, 1.73],
                [7.47, 1.73],
                [7.47, -0.01],
                [6.46, -0.01]
            ]
        ] }, "centroid": [6.71, 0.43] }, { "adm0_name": "Senegal", "adm0_code": 217, "bbox": { "type": "Polygon", "coordinates": [
            [
                [-17.53, 12.31],
                [-17.53, 16.69],
                [-11.35, 16.69],
                [-11.35, 12.31],
                [-17.53, 12.31]
            ]
        ] }, "centroid": [-14.46, 14.36] }, { "adm0_name": "Seychelles", "adm0_code": 220, "bbox": { "type": "Polygon", "coordinates": [
            [
                [46.2, -10.23],
                [46.2, -3.71],
                [56.29, -3.71],
                [56.29, -10.23],
                [46.2, -10.23]
            ]
        ] }, "centroid": [51.51, -6.95] }, { "adm0_name": "Sierra Leone", "adm0_code": 221, "bbox": { "type": "Polygon", "coordinates": [
            [
                [-13.3, 6.92],
                [-13.3, 10],
                [-10.27, 10],
                [-10.27, 6.92],
                [-13.3, 6.92]
            ]
        ] }, "centroid": [-11.79, 8.56] }, { "adm0_name": "Somalia", "adm0_code": 226, "bbox": { "type": "Polygon", "coordinates": [
            [
                [40.99, -1.66],
                [40.99, 11.99],
                [51.42, 11.99],
                [51.42, -1.66],
                [40.99, -1.66]
            ]
        ] }, "centroid": [45.87, 6.06] }, { "adm0_name": "South Africa", "adm0_code": 227, "bbox": { "type": "Polygon", "coordinates": [
            [
                [16.45, -46.98],
                [16.45, -22.13],
                [38, -22.13],
                [38, -46.98],
                [16.45, -46.98]
            ]
        ] }, "centroid": [25.09, -29] }, { "adm0_name": "South Sudan", "adm0_code": 74, "bbox": { "type": "Polygon", "coordinates": [
            [
                [24.15, 3.49],
                [24.15, 12.24],
                [35.95, 12.24],
                [35.95, 3.49],
                [24.15, 3.49]
            ]
        ] }, "centroid": [30.33, 7.29] }, { "adm0_name": "Sudan", "adm0_code": 6, "bbox": { "type": "Polygon", "coordinates": [
            [
                [21.81, 8.64],
                [21.81, 22.23],
                [38.58, 22.23],
                [38.58, 8.64],
                [21.81, 8.64]
            ]
        ] }, "centroid": [29.96, 16.02] }, { "adm0_name": "Swaziland", "adm0_code": 235, "bbox": { "type": "Polygon", "coordinates": [
            [
                [30.79, -27.32],
                [30.79, -25.72],
                [32.14, -25.72],
                [32.14, -27.32],
                [30.79, -27.32]
            ]
        ] }, "centroid": [31.5, -26.56] }, { "adm0_name": "Togo", "adm0_code": 243, "bbox": { "type": "Polygon", "coordinates": [
            [
                [-0.15, 6.11],
                [-0.15, 11.14],
                [1.81, 11.14],
                [1.81, 6.11],
                [-0.15, 6.11]
            ]
        ] }, "centroid": [0.98, 8.53] }, { "adm0_name": "Uganda", "adm0_code": 253, "bbox": { "type": "Polygon", "coordinates": [
            [
                [29.57, -1.48],
                [29.57, 4.21],
                [35.04, 4.21],
                [35.04, -1.48],
                [29.57, -1.48]
            ]
        ] }, "centroid": [32.39, 1.28] }, { "adm0_name": "United Republic of Tanzania", "adm0_code": 257, "bbox": { "type": "Polygon", "coordinates": [
            [
                [29.34, -11.76],
                [29.34, -0.98],
                [40.45, -0.98],
                [40.45, -11.76],
                [29.34, -11.76]
            ]
        ] }, "centroid": [34.82, -6.26] }, { "adm0_name": "Zambia", "adm0_code": 270, "bbox": { "type": "Polygon", "coordinates": [
            [
                [22, -18.08],
                [22, -8.19],
                [33.71, -8.19],
                [33.71, -18.08],
                [22, -18.08]
            ]
        ] }, "centroid": [27.79, -13.46] }, { "adm0_name": "Zimbabwe", "adm0_code": 271, "bbox": { "type": "Polygon", "coordinates": [
            [
                [25.24, -22.42],
                [25.24, -15.61],
                [33.06, -15.61],
                [33.06, -22.42],
                [25.24, -22.42]
            ]
        ] }, "centroid": [29.87, -19] }]