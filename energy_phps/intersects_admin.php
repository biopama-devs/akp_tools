<?php
header('Content-Type: application/json');

$json = array(
  'adm_code_name' => null,
  'hospitals_categories' => array(
    'counts' => 0,
    'categories_data' => array()
  ),
  'power_categories' => array(
    'counts' => 0,
    'categories_data' => array()
  )
);
$link = pg_connect("host=myhost dbname=yourUserorPwd user=yourUserorPwd password=yourUserorPwd");

$to_query = $_GET['to_query'];

if ($to_query !== 'bbox') {
  $adm_code_value = $_GET['adm_code_value'];
  $adm_name;

  switch ($to_query) {
    case 'adm0':
      $adm_code_name = 'adm0_code';
      $adm_sql = "select adm0_code_int,adm0_name,area_sqkm from gaul_0_info_all where adm0_code_int=$adm_code_value";
      break;
    case 'adm1':
      $adm_code_name = 'adm1_code';
      $adm_sql = "select adm0_code_int,adm0_name,adm1_code_int,adm1_name,area_sqkm from gaul_1_info_all where adm1_code_int=$adm_code_value";
      break;
    case 'adm2':
      $adm_code_name = 'adm2_code';
      $adm_sql = "select adm0_code,adm0_name,adm1_code,adm1_name,adm2_code,adm2_name,area_sqkm from gaul_2_info_all where adm2_code=$adm_code_value";
      break;

    default:
      # code...
      break;
  }
} else {
  $sw = $_GET['bbox']['sw'];
  $xmin = $sw[0];
  $ymin = $sw[1];

  $ne = $_GET['bbox']['ne'];

  $xmax = $ne[0];
  $ymax = $ne[1];

  //bbox = min Longitude , min Latitude , max Longitude , max Latitude
  //(float xmin, float ymin, float xmax, float ymax, integer srid=unknown);
  $pol = 'POLYGON((' . $xmin . ' ' . $ymin . ',' . $xmin . ' ' . $ymax . ',' . $xmax . ' ' . $ymax . ',' . $xmax . ' ' . $ymin . ',' . $xmin . ' ' . $ymin . '))';
}
if ($to_query == 'bbox') {
  $sql = "select mw,mw_class,gen_type from power_plants_2019_class where ST_Within(power_plants_2019_class.geom, ST_GeomFromText('" . $pol . "',4326))";
} else {
  $sql = "select mw,mw_class,gen_type from power_plants_2019_class where $adm_code_name='$adm_code_value'";
}

//echo $sql;
array_push($json['power_categories']['categories_data'], array(
  'param' => 'mw',
  'cat_arr' => array(),
  'data' => array()
), array(
  'param' => 'gen_type',
  'cat_arr' => array(),
  'data' => array()
));

$mw_arr = array();
$gen_type_arr = array();

$result = pg_query($link, $sql);

$counter_power = 0;
$count_power = pg_num_rows($result);
if ($count_power > 0) {
  $json['power_categories']['counts'] = $count_power;
  // echo 'initial counts are '.$json['power_categories']['counts'];
  while ($data = pg_fetch_array($result)) {
    $all_p_list = array(
      'mw',
      'gen_type'
    );

    for ($i = 0; $i < sizeof($all_p_list); $i++) {
      //'lcoe_h',
      $s = $all_p_list[$i];

      if ($s == 'mw') {
        // array_push($json['power_categories']['categories_data'],array('param'=>$v,'cat_arr'=>array(),'data'=>array()));
        $s_class = 'mw_class';
        $this_class_value = $data['mw_class'];
        $this_data = $data['mw'];

        //position into external array
        $param_pos = array_search($data['mw_class'], $mw_arr);
        // $this_class_pos=array_search($this_class_value, $json['power_categories']['categories_data'][0]['cat_arr']);
        $this_class_pos = array_search($this_class_value, $mw_arr);

        // $p=$json['power_categories']['categories_data'][0]['cat_arr']);
        if ($this_class_pos === false) {
          array_push($mw_arr, $this_class_value);

          array_push($json['power_categories']['categories_data'][0]['cat_arr'], $this_class_value);

          array_push($json['power_categories']['categories_data'][0]['data'], array(
            'cat' => $this_class_value,
            'sum' => $data['mw'],
            'counts' => 1,
            'all_values' => array(
              $this_data
            )
          ));
        } else {
          $param_pos = array_search($data['mw_class'], $mw_arr);

          $json['power_categories']['categories_data'][0]['data'][$this_class_pos]['counts'] += 1;

          // $json['power_categories']['categories_data'][$param_pos]['data'][$cat_pos]['sum']+=$data['mw'];
          array_push($json['power_categories']['categories_data'][0]['data'][$this_class_pos]['all_values'], $this_data);

          if ($counter_power == $count_power - 1) {
            $this_class_pos = array_search($this_class_value, $json['hospitals_categories']['categories_data'][0]['cat_arr']);
          }
        }
      }
    }
    $counter_power++;
  }
}

$c = 0;

$all_p = array(
  'lcoe_h',
  'elctdemand',
  'pv_kw_h',
  'bat_kwh_h',
  'capex',
  'npv_h'
);
//'bat_kwh_m'
$all_p_class = array();

$sql2 = "WITH
        clipped AS (
            SELECT ";
if ($to_query !== 'bbox') {
  $sql2 .= $adm_code_name . ',' . $to_query . '_name,';
}

for ($i = 0; $i < sizeof($all_p); $i++) {
  //array_push($all_p_class, $all_p[$i]);
  if ($i < sizeof($all_p) - 1) {
    $sql2 .= $all_p[$i] . ",$all_p[$i]_class,";
  } else {
    $sql2 .= $all_p[$i] . ",$all_p[$i]_class";
  }
}

if ($to_query == 'bbox') {
  $sql2 .= " from hospitals_no_grid_gaul2 where ST_Within(hospitals_no_grid_gaul2.geom, ST_GeomFromText('" . $pol . "',4326)))
          SELECT ";
} else {
  $sql2 .= " from hospitals_no_grid_gaul2 where $adm_code_name=$adm_code_value)
          SELECT " . $adm_code_name . ',' . $to_query . '_name,';
}

//    $sql2.=" from hospitals_no_grid_gaul2 where $adm_code_name=$adm_code_value)
//       SELECT ".$adm_code_name.','.$to_query.'_name,';
for ($i = 0; $i < sizeof($all_p); $i++) {
  if ($i < sizeof($all_p) - 1) {
    $sql2 .= $all_p[$i] . ",$all_p[$i]_class,";
  } else {
    $sql2 .= $all_p[$i] . ",$all_p[$i]_class";
  }
  $v = $all_p[$i];
  //  echo $v;
  array_push($json['hospitals_categories']['categories_data'], array(
    'param' => $v,
    'cat_arr' => array(),
    'data' => array()
  ));

  $all_p[$i] = array();
  array_push($all_p_class, $all_p[$i]);
}
$sql2 .= ' from clipped';

$hospitals_categories_param = array();

// $all_p_list=array('lcoe_h','elctdemand','pv_kw_h','bat_kwh_h','capex','npv_h');
//  for ($x=0; $x < sizeof($all_p_list); $x++)
//             {
//               //'lcoe_h',
//               $s=$all_p_list[$x];
//               echo $s.' / ';


//               array_push($json['hospitals_categories']['categories_data'],array('param'=>$s,'cat_arr'=>array(),'data'=>array('all_values'=>array())));
//               }
//               var_dump($json['hospitals_categories']['categories_data']);
//   echo $sql2;
$result = pg_query($link, $sql2);

//echo $sql2;
$counter = 0;

$count = pg_num_rows($result);

if ($count > 0) {
  $json['hospitals_categories']['counts'] = $count;

  while ($data = pg_fetch_array($result)) {
    $all_p_list = array(
      'lcoe_h',
      'elctdemand',
      'pv_kw_h',
      'bat_kwh_h',
      'capex',
      'npv_h'
    );
    for ($i = 0; $i < sizeof($all_p_list); $i++) {
      //'lcoe_h',
      $s = $all_p_list[$i];
      $param_pos = array_search($all_p_list[$i], $all_p_list);

      //array_push($json['hospitals_categories']['categories_data'],array('param'=>$v,'cat_arr'=>array(),'data'=>array()));
      //'lcoe_h_class',
      $s_class = $all_p_list[$i] . '_class';

      $this_class_value = $data[$s_class];
      //$t_json=$json['hospitals_categories']['categories_data'][$param_pos];
      $this_class_pos = array_search($this_class_value, $json['hospitals_categories']['categories_data'][$param_pos]['cat_arr']);

      $this_data = $data[$all_p_list[$i]];

      if ($this_class_pos === false) {
        //  echo 'pos  '.$this_class_pos;
        array_push($json['hospitals_categories']['categories_data'][$param_pos]['cat_arr'], $this_class_value);
        array_push($json['hospitals_categories']['categories_data'][$param_pos]['data'], array(
          'cat' => $this_class_value,
          'counts' => 1,
          'all_values' => array(
            $this_data
          )
        ));
      } else {
        $cat_pos = array_search($this_class_value, $json['hospitals_categories']['categories_data'][$param_pos]['cat_arr']);

        $json['hospitals_categories']['categories_data'][$param_pos]['data'][$cat_pos]['counts'] += 1;

        $json['hospitals_categories']['categories_data'][$param_pos]['data'][$cat_pos]['sum'] += $this_data;
        array_push($json['hospitals_categories']['categories_data'][$param_pos]['data'][$cat_pos]['all_values'], $this_data);

        if ($counter == $count - 1) {
          $all_v = $json['hospitals_categories']['categories_data'][$param_pos]['data'][$cat_pos]['all_values'];
          $json['hospitals_categories']['categories_data'][$param_pos]['data'][$cat_pos]['min'] = min($all_v);
          $json['hospitals_categories']['categories_data'][$param_pos]['data'][$cat_pos]['max'] = max($all_v);

          $counts = $json['hospitals_categories']['categories_data'][$param_pos]['data'][$cat_pos]['counts'];
          $sum = $json['hospitals_categories']['categories_data'][$param_pos]['data'][$cat_pos]['sum'];
          $avg = $sum / $counts;
          if ($avg < 0) {
            $avg2 = round($avg, 3);
          } else {
            $avg2 = round($avg, 1);
          }

          $json['hospitals_categories']['categories_data'][$param_pos]['data'][$cat_pos]['avg'] = $avg2;
          // $json['hospitals_categories']['categories_data'][$param_pos]['data'][$cat_pos]['avg']=$sum/$json['hospitals_categories']['categories_data'][$param_pos]['data'][$cat_pos]['counts'];
          // }
        }
      }
    }

    $counter++;
  }
}

//  var_dump($json);

$c = 0;
$json2 = array(
  'adm_code_name' => $adm_code_name,
  'adm_code_value' => $adm_code_value,
  'hospitals_categories' => array(
    'counts' => 0,
    'categories_data' => array()
  ),
  'power_categories' => array(
    'counts' => 0,
    'categories_data' => array()
  ),
  'count_px' => 0,
  'sum_total' => 0,
  'ghs_data' => array()
);

foreach ($json['hospitals_categories']['categories_data'] as $key => $value) {
  foreach ($value['data'] as $key2 => $value2) {
    unset($value2['all_values']);

    // var_dump($value2);
    // array_push($value,$value2);
  }
  array_push($json2['hospitals_categories']['categories_data'], $value);
  $c++;
  # code...
}
$json2['hospitals_categories']['counts'] = $json['hospitals_categories']['counts'];

foreach ($json['power_categories']['categories_data'] as $key => $value) {
  // var_dump($value);
  // array_push($json2['power_categories']['categories_data'],$value);

  foreach ($value['data'] as $key2 => $value2) {

    unset($value2['all_values']);
  }

  array_push($json2['power_categories']['categories_data'], $value);
  $c++;
  # code...
}

$json2['power_categories']['counts'] = $json['power_categories']['counts'];

if ($to_query !== 'bbox') {
  $sql = "select sum(px_val),count(px_class_250m),px_class_250m _class from ghs_all where px_class_250m is not NULL and
                      $adm_code_name=$adm_code_value group by px_class_250m";
} else {
  $sql = "select sum(px_val),count(px_class_250m),px_class_250m _class from ghs_all where px_class_250m is not NULL and ST_Within(ghs_all.geom, ST_GeomFromText('" . $pol . "',4326)) group by px_class_250m";
}

//adm0_code=$adm_code_value group by px_class_250m";
//echo $sql;
$result = pg_query($link, $sql);

$count = pg_num_rows($result);

if ($count > 0) {
  while ($data = pg_fetch_array($result)) {
    $json2['count_px'] += $data['count'];
    $json2['sum_total'] += round($data['sum']);
    array_push($json2['ghs_data'], array(
      'sum' => round(($data['sum'] / 16)),
      'num_px' => $data['count'],
      'px_class' => $data['_class']
    ));
  }
}

if ($to_query !== 'bbox') {
  $adm_code_value = $_GET['adm_code_value'];

  $result = pg_query($link, $adm_sql);

  $count_adm = pg_num_rows($result);
  if ($count_adm > 0) {
    while ($data = pg_fetch_array($result)) {
      $json2['adm_area'] = $data['area_sqkm'];
      switch ($to_query) {
        case 'adm0':
          $json2['adm0_name'] = $data['adm0_name'];
          break;
        case 'adm1':
          $json2['adm0_name'] = $data['adm0_name'];
          $json2['adm1_name'] = $data['adm1_name'];
          break;
        case 'adm2':
          $json2['adm0_name'] = $data['adm0_name'];
          $json2['adm1_name'] = $data['adm1_name'];
          $json2['adm2_name'] = $data['adm2_name'];
          break;
        default:
          break;
      }
    }
  }
}

$main_json = json_encode($json2, JSON_NUMERIC_CHECK);
if ($_GET['callback']) {
  echo $_GET['callback'] . "(" . $main_json . ")";
} else {
  echo $main_json;
};
