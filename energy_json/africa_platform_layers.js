var africa_platform_layers_obj = [
  {
    title: "Infrastructures physical connectivity",
    layer_id: "physicinfrastructureid",
    legend: {
      value:
        '<div class="c-legend-content_stats"><ul class="c-legend-choropleth_stats">\r\n<li><div class="icon-choropleth_stats" style="background-color: #fff4f0!important;  "></div><span class="name"><i>0-1</i></span></li>\r\n<li><div class="icon-choropleth_stats"  style="background-color: #ffe3d7!important;  "></div><span class="name"><i>1-2</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #fdc6af!important;  "></div><span class="name"><i>2-3</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #fca487!important;  "></div><span class="name"><i>3-4</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #fc8161!important;  "></div><span class="name"><i>4-5</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #f85d42!important;  "></div><span class="name"><i>5-6</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #eb362a!important;  "></div><span class="name"><i>6-7</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #cc171c!important;  "></div><span class="name"><i>7-8</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #a90e14!important;  "></div><span class="name"><i>8-9</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #67000d!important;  "></div><span class="name"><i>9-10</i></span></li>\r\n</ul>',
      format: "full_html",
      safe_value:
        '<div class="c-legend-content_stats">\n<ul class="c-legend-choropleth_stats">\n<li>\n<div class="icon-choropleth_stats" style="background-color: #fff4f0!important;  "></div>\n<p><span class="name"><i>0-1</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #ffe3d7!important;  "></div>\n<p><span class="name"><i>1-2</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #fdc6af!important;  "></div>\n<p><span class="name"><i>2-3</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #fca487!important;  "></div>\n<p><span class="name"><i>3-4</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #fc8161!important;  "></div>\n<p><span class="name"><i>4-5</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #f85d42!important;  "></div>\n<p><span class="name"><i>5-6</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #eb362a!important;  "></div>\n<p><span class="name"><i>6-7</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #cc171c!important;  "></div>\n<p><span class="name"><i>7-8</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #a90e14!important;  "></div>\n<p><span class="name"><i>8-9</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #67000d!important;  "></div>\n<p><span class="name"><i>9-10</i></span></p></li>\n</ul>\n</div>'
    },
    wms_name: "africa_platform:inform_indicators",
    wms_style: "physic_infrastructure",
    wms_url:
      "https://geospatial.jrc.ec.europa.eu/geoserver/africa_platform/wms",
    stats_legend: {
      value: "Statistics not yet available",
      format: null,
      safe_value: "Statistics not yet available"
    },
    first_color_legend: "#ffb307",
    second_color_legend: "#5a3607",
    server_type: "Geoserver",
    statistics_type: "nostats"
  },
  {
    title: "Travel time to major cities (Hours)",
    layer_id: "travelid",
    legend: {
      value:
        '<div class="c-legend-content_stats"><ul class="c-legend-choropleth_stats">\r\n<li><div class="icon-choropleth_stats" style="background-color: #ffffd4!important;  "></div><span class="name"><i>0.5h</i></span></li>\r\n<li><div class="icon-choropleth_stats"  style="background-color: #fff5c0!important;  "></div><span class="name"><i>1h</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #ffeaac!important;  "></div><span class="name"><i>1.5h</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #ffdf98!important;  "></div><span class="name"><i>2h</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #fed080!important;  "></div><span class="name"><i>4h</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #febe63!important;  "></div><span class="name"><i>6h</i></span></li>\r\n<li><div class="icon-choropleth_stats"  style="background-color: #feab46!important;  "></div><span class="name"><i>8h</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #fe9929!important;  "></div><span class="name"><i>12h</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #f48821!important;  "></div><span class="name"><i>18h</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #e97819!important;  "></div><span class="name"><i>1d</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #df6711!important;  "></div><span class="name"><i>2d</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #d0590c!important;  "></div><span class="name"><i>3d</i></span></li>\r\n<li><div class="icon-choropleth_stats"  style="background-color: #be4c09!important;  "></div><span class="name"><i>4d</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #ab4006!important;  "></div><span class="name"><i>5d</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #993404!important;  "></div><span class="name"><i>10d</i></span></li>\r\n</ul>\r\n</div>',
      format: "full_html",
      safe_value:
        '<div class="c-legend-content_stats">\n<ul class="c-legend-choropleth_stats">\n<li>\n<div class="icon-choropleth_stats" style="background-color: #ffffd4!important;  "></div>\n<p><span class="name"><i>0.5h</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #fff5c0!important;  "></div>\n<p><span class="name"><i>1h</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #ffeaac!important;  "></div>\n<p><span class="name"><i>1.5h</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #ffdf98!important;  "></div>\n<p><span class="name"><i>2h</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #fed080!important;  "></div>\n<p><span class="name"><i>4h</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #febe63!important;  "></div>\n<p><span class="name"><i>6h</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #feab46!important;  "></div>\n<p><span class="name"><i>8h</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #fe9929!important;  "></div>\n<p><span class="name"><i>12h</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #f48821!important;  "></div>\n<p><span class="name"><i>18h</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #e97819!important;  "></div>\n<p><span class="name"><i>1d</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #df6711!important;  "></div>\n<p><span class="name"><i>2d</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #d0590c!important;  "></div>\n<p><span class="name"><i>3d</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #be4c09!important;  "></div>\n<p><span class="name"><i>4d</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #ab4006!important;  "></div>\n<p><span class="name"><i>5d</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #993404!important;  "></div>\n<p><span class="name"><i>10d</i></span></p></li>\n</ul>\n</div>\n'
    },
    wms_name: "africa_platform:travel_time_sec_africa",
    wms_style: [],
    wms_url: "https://geospatial.jrc.ec.europa.eu/geoserver",
    stats_legend: {
      value: "Statistics not yet available",
      format: null,
      safe_value: "Statistics not yet available"
    },
    first_color_legend: "#ffb307",
    second_color_legend: "#5a3607",
    server_type: "Geoserver",
    statistics_type: "nostats"
  },
  {
    title: "Electricity network existing and planned",
    layer_id: "elnid",
    legend: {
      value:
        '<div class="c-legend-content"><ul class="c-legend-choropleth"><li><div class="country_05m icon-choropleth" style="background-color: #e8bf2b!important;  "></div><span class="name"><i>Electricity network</i></span></li></ul></div>',
      format: "full_html",
      safe_value:
        '<div class="c-legend-content">\n<ul class="c-legend-choropleth">\n<li>\n<div class="country_05m icon-choropleth" style="background-color: #e8bf2b!important;  "></div>\n<p><span class="name"><i>Electricity network</i></span></p></li>\n</ul>\n</div>\n'
    },
    wms_name: "africa_platform:jrc_wb_osm_powergrid",
    wms_style: [],
    wms_url: "https://geospatial.jrc.ec.europa.eu/geoserver",
    stats_legend: {
      value: "Statistics not yet available",
      format: null,
      safe_value: "Statistics not yet available"
    },
    first_color_legend: "#ffb307",
    second_color_legend: "#5a3607",
    server_type: "GeoWebCache",
    statistics_type: "nostats"
  },
  {
    title: "Cost of electricity (USD/kWh) produced by a diesel generator",
    layer_id: "lcoeid",
    legend: {
      value:
        '<div class="c-legend-content_stats"><ul class="c-legend-choropleth_stats">\r\n<li><div class="icon-choropleth_stats" style="background-color: #000004!important;  "></div><span class="name"><i>0</i></span></li>\r\n<li><div class="icon-choropleth_stats"  style="background-color: #1c1046!important;  "></div><span class="name"><i>10</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #50127b!important;  "></div><span class="name"><i>20</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #822581!important;  "></div><span class="name"><i>30</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #b63679!important;  "></div><span class="name"><i>40</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #e75163!important;  "></div><span class="name"><i>50</i></span></li>\r\n<li><div class="icon-choropleth_stats"  style="background-color: #fc8761!important;  "></div><span class="name"><i>60</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #fec387!important;  "></div><span class="name"><i>70</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #fcfdbf!important;  "></div><span class="name"><i>&gt;70</i></span></li>\r\n</ul>\r\n</div>',
      format: "full_html",
      safe_value:
        '<div class="c-legend-content_stats">\n<ul class="c-legend-choropleth_stats">\n<li>\n<div class="icon-choropleth_stats" style="background-color: #000004!important;  "></div>\n<p><span class="name"><i>0</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #1c1046!important;  "></div>\n<p><span class="name"><i>10</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #50127b!important;  "></div>\n<p><span class="name"><i>20</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #822581!important;  "></div>\n<p><span class="name"><i>30</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #b63679!important;  "></div>\n<p><span class="name"><i>40</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #e75163!important;  "></div>\n<p><span class="name"><i>50</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #fc8761!important;  "></div>\n<p><span class="name"><i>60</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #fec387!important;  "></div>\n<p><span class="name"><i>70</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #fcfdbf!important;  "></div>\n<p><span class="name"><i>&gt;70</i></span></p></li>\n</ul>\n</div>\n'
    },
    wms_name: "africa_platform:die_n20161",
    wms_style: [],
    wms_url: "https://geospatial.jrc.ec.europa.eu/geoserver",
    stats_legend: {
      value: "Statistics not yet available",
      format: null,
      safe_value: "Statistics not yet available"
    },
    first_color_legend: "#ffb307",
    second_color_legend: "#5a3607",
    server_type: "GeoWebCache",
    statistics_type: "nostats"
  },
  {
    title: "Photo Voltaic battery (kWh)",
    layer_id: "batteryid",
    legend: {
      value:
        '<div class="c-legend-content_stats"><ul class="c-legend-choropleth_stats">\r\n<li><div class="icon-choropleth_stats" style="background-color: #fcfbfd!important;  "></div><span class="name"><i>144</i></span></li>\r\n<li><div class="icon-choropleth_stats"  style="background-color: #c9cae3!important;  "></div><span class="name"><i>170</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #7c76b6!important;  "></div><span class="name"><i>200</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #3f007d!important;  "></div><span class="name"><i>&gt;400</i></span></li>\r\n</ul>\r\n</div>',
      format: "full_html",
      safe_value:
        '<div class="c-legend-content_stats">\n<ul class="c-legend-choropleth_stats">\n<li>\n<div class="icon-choropleth_stats" style="background-color: #fcfbfd!important;  "></div>\n<p><span class="name"><i>144</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #c9cae3!important;  "></div>\n<p><span class="name"><i>170</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #7c76b6!important;  "></div>\n<p><span class="name"><i>200</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #3f007d!important;  "></div>\n<p><span class="name"><i>&gt;400</i></span></p></li>\n</ul>\n</div>\n'
    },
    wms_name: "africa_platform:batterysize",
    wms_style: [],
    wms_url: "https://geospatial.jrc.ec.europa.eu/geoserver",
    stats_legend: {
      value: "Statistics not yet available",
      format: null,
      safe_value: "Statistics not yet available"
    },
    first_color_legend: "#ffb307",
    second_color_legend: "#5a3607",
    server_type: "Geoserver",
    statistics_type: "nostats"
  },
  {
    title: "Solar PV  vs diesel generator costs",
    layer_id: "pvvsdieselid",
    legend: {
      value:
        '<em>Cost comparison of solar PV and diesel systems (cents USD)</em><br>\r\n<em>How much cheaper PV compared to Diesel</em>\r\n<div class="c-legend-content_stats"><ul class="c-legend-choropleth_stats">\r\n<li><div class="icon-choropleth_stats" style="background-color: #000004!important;  "></div><span class="name"><i>&lt;-50</i></span></li>\r\n<li><div class="icon-choropleth_stats"  style="background-color: #210c49!important;  "></div><span class="name"><i>-25</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #570f6d!important;  "></div><span class="name"><i>0</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #89226a!important;  "></div><span class="name"><i>5</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #bb3754!important;  "></div><span class="name"><i>10</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #e45931!important;  "></div><span class="name"><i>20</i></span></li>\r\n<li><div class="icon-choropleth_stats"  style="background-color: #fa8d0a!important;  "></div><span class="name"><i>40</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #f9ca32!important;  "></div><span class="name"><i>60</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #fcffa4!important;  "></div><span class="name"><i>&gt;60</i></span></li>\r\n</ul>\r\n</div>',
      format: "full_html",
      safe_value:
        '<p><em>Cost comparison of solar PV and diesel systems (cents USD)</em><br /><br />\n<em>How much cheaper PV compared to Diesel</em></p>\n<div class="c-legend-content_stats">\n<ul class="c-legend-choropleth_stats">\n<li>\n<div class="icon-choropleth_stats" style="background-color: #000004!important;  "></div>\n<p><span class="name"><i>&lt;-50</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #210c49!important;  "></div>\n<p><span class="name"><i>-25</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #570f6d!important;  "></div>\n<p><span class="name"><i>0</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #89226a!important;  "></div>\n<p><span class="name"><i>5</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #bb3754!important;  "></div>\n<p><span class="name"><i>10</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #e45931!important;  "></div>\n<p><span class="name"><i>20</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #fa8d0a!important;  "></div>\n<p><span class="name"><i>40</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #f9ca32!important;  "></div>\n<p><span class="name"><i>60</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #fcffa4!important;  "></div>\n<p><span class="name"><i>&gt;60</i></span></p></li>\n</ul>\n</div>\n'
    },
    wms_name: "africa_platform:pvdiesel_africa_2016",
    wms_style: [],
    wms_url: "https://geospatial.jrc.ec.europa.eu/geoserver",
    stats_legend: {
      value: "Statistics not yet available",
      format: null,
      safe_value: "Statistics not yet available"
    },
    first_color_legend: "#ffb307",
    second_color_legend: "#5a3607",
    server_type: "Geoserver",
    statistics_type: "nostats"
  },
  {
    title: "Wind power (W/m2)",
    layer_id: "windid",
    legend: {
      value:
        '<div class="c-legend-content_stats"><ul class="c-legend-choropleth_stats">\r\n<li><div class="icon-choropleth_stats" style="background-color: #440154!important;  "></div><span class="name"><i>0</i></span></li>\r\n<li><div class="icon-choropleth_stats"  style="background-color: #482475!important;  "></div><span class="name"><i>5</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #404387!important;  "></div><span class="name"><i>10</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #345f8d!important;  "></div><span class="name"><i>15</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #29788e!important;  "></div><span class="name"><i>30</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #20908d!important;  "></div><span class="name"><i>50</i></span></li>\r\n<li><div class="icon-choropleth_stats"  style="background-color: #22a884!important;  "></div><span class="name"><i>75</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #43bf70!important;  "></div><span class="name"><i>100</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #7ad251!important;  "></div><span class="name"><i>150</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #bcdf27!important;  "></div><span class="name"><i>200</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #fde725!important;  "></div><span class="name"><i>&gt;200</i></span></li>\r\n</ul>\r\n</div>',
      format: "full_html",
      safe_value:
        '<div class="c-legend-content_stats">\n<ul class="c-legend-choropleth_stats">\n<li>\n<div class="icon-choropleth_stats" style="background-color: #440154!important;  "></div>\n<p><span class="name"><i>0</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #482475!important;  "></div>\n<p><span class="name"><i>5</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #404387!important;  "></div>\n<p><span class="name"><i>10</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #345f8d!important;  "></div>\n<p><span class="name"><i>15</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #29788e!important;  "></div>\n<p><span class="name"><i>30</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #20908d!important;  "></div>\n<p><span class="name"><i>50</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #22a884!important;  "></div>\n<p><span class="name"><i>75</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #43bf70!important;  "></div>\n<p><span class="name"><i>100</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #7ad251!important;  "></div>\n<p><span class="name"><i>150</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #bcdf27!important;  "></div>\n<p><span class="name"><i>200</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #fde725!important;  "></div>\n<p><span class="name"><i>&gt;200</i></span></p></li>\n</ul>\n</div>\n'
    },
    wms_name: "africa_platform:gwa_250_power_d10m_Africa",
    wms_style: [],
    wms_url: "https://geospatial.jrc.ec.europa.eu/geoserver",
    stats_legend: {
      value: "Statistics not yet available",
      format: null,
      safe_value: "Statistics not yet available"
    },
    first_color_legend: "#ffb307",
    second_color_legend: "#5a3607",
    server_type: "GeoWebCache",
    statistics_type: "nostats"
  },
  {
    title: "Solar PV annual yield (kWh/kWp)",
    layer_id: "solarpvid",
    legend: {
      value:
        '<div class="c-legend-content_stats"><ul class="c-legend-choropleth_stats">\r\n<li><div class="icon-choropleth_stats" style="background-color: #000004!important;  "></div><span class="name"><i>1000</i></span></li>\r\n<li><div class="icon-choropleth_stats"  style="background-color: #210c49!important;  "></div><span class="name"><i>1200</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #570f6d!important;  "></div><span class="name"><i>1400</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #89226a!important;  "></div><span class="name"><i>1600</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #bb3754!important;  "></div><span class="name"><i>1800</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #e45931!important;  "></div><span class="name"><i>2000</i></span></li>\r\n<li><div class="icon-choropleth_stats"  style="background-color: #fa8d0a!important;  "></div><span class="name"><i>2200</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #f9ca32!important;  "></div><span class="name"><i>2400</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #fcffa4!important;  "></div><span class="name"><i>&gt;2400</i></span></li>\r\n</ul>\r\n</div>',
      format: "full_html",
      safe_value:
        '<div class="c-legend-content_stats">\n<ul class="c-legend-choropleth_stats">\n<li>\n<div class="icon-choropleth_stats" style="background-color: #000004!important;  "></div>\n<p><span class="name"><i>1000</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #210c49!important;  "></div>\n<p><span class="name"><i>1200</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #570f6d!important;  "></div>\n<p><span class="name"><i>1400</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #89226a!important;  "></div>\n<p><span class="name"><i>1600</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #bb3754!important;  "></div>\n<p><span class="name"><i>1800</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #e45931!important;  "></div>\n<p><span class="name"><i>2000</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #fa8d0a!important;  "></div>\n<p><span class="name"><i>2200</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #f9ca32!important;  "></div>\n<p><span class="name"><i>2400</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #fcffa4!important;  "></div>\n<p><span class="name"><i>&gt;2400</i></span></p></li>\n</ul>\n</div>\n'
    },
    wms_name: "africa_platform:africa_ydc__corrected",
    wms_style: [],
    wms_url: "https://geospatial.jrc.ec.europa.eu/geoserver",
    stats_legend: {
      value: "Statistics not yet available",
      format: null,
      safe_value: "Statistics not yet available"
    },
    first_color_legend: "#ffb307",
    second_color_legend: "#5a3607",
    server_type: "GeoWebCache",
    statistics_type: "nostats"
  },
  {
    title: "Solar global horizontal irradiation (kWh/m2)",
    layer_id: "solarirrid",
    legend: {
      value:
        '<div class="c-legend-content_stats"><ul class="c-legend-choropleth_stats">\r\n<li><div class="icon-choropleth_stats" style="background-color: #ffffd4!important;  "></div><span class="name"><i>900</i></span></li>\r\n<li><div class="icon-choropleth_stats"  style="background-color: #fed98e!important;  "></div><span class="name"><i>2000</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #fe9929!important;  "></div><span class="name"><i>2200</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #d95f0e!important;  "></div><span class="name"><i>2400</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #993404!important;  "></div><span class="name"><i>2700</i></span></li>\r\n</ul>\r\n</div>',
      format: "full_html",
      safe_value:
        '<div class="c-legend-content_stats">\n<ul class="c-legend-choropleth_stats">\n<li>\n<div class="icon-choropleth_stats" style="background-color: #ffffd4!important;  "></div>\n<p><span class="name"><i>900</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #fed98e!important;  "></div>\n<p><span class="name"><i>2000</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #fe9929!important;  "></div>\n<p><span class="name"><i>2200</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #d95f0e!important;  "></div>\n<p><span class="name"><i>2400</i></span></p></li>\n<li>\n<div class="icon-choropleth_stats" style="background-color: #993404!important;  "></div>\n<p><span class="name"><i>2700</i></span></p></li>\n</ul>\n</div>\n'
    },
    wms_name: "africa_platform:gopt",
    wms_style: [],
    wms_url: "https://geospatial.jrc.ec.europa.eu/geoserver",
    stats_legend: {
      value: "Statistics not yet available",
      format: null,
      safe_value: "Statistics not yet available"
    },
    first_color_legend: "#ffffd4",
    second_color_legend: "#993404",
    server_type: "GeoWebCache",
    statistics_type: "nostats"
  },
  {
    title: "Number inhabitants",
    layer_id: "urbanrateid",
    legend: {
      value:
        '<div style="color: #dadada; font-size: 12px; float: left!important;">1K</div><div style="color: #dadada; font-size: 12px; float: right!important;">20M</div><div class="UrbanGradient " style="clear: both;"></div>',
      format: "full_html",
      safe_value:
        '<div style="color: #dadada; font-size: 12px; float: left!important;">1K</div>\n<div style="color: #dadada; font-size: 12px; float: right!important;">20M</div>\n<div class="UrbanGradient " style="clear: both;"></div>\n'
    },
    wms_name: "africa_platform:africapolis",
    wms_style: [],
    wms_url:
      "https://geospatial.jrc.ec.europa.eu/geoserver/africa_platform/wms",
    stats_legend: {
      value:
        '<div id ="title_stats_legend"> Rate of urbanization (% of country area)</div>\r\n<div class="c-legend-content_stats"><ul class="c-legend-choropleth_stats">\r\n<li><div class="icon-choropleth_stats" style="background-color: #ffffd4!important;  "></div><span class="name"><i>15-30</i></span></li>\r\n<li><div class="icon-choropleth_stats"  style="background-color: #fed98e!important;  "></div><span class="name"><i>30-45</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #fe9929!important;  "></div><span class="name"><i>45-60</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #d95f0e!important;  "></div><span class="name"><i>60-75</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #993404!important;  "></div><span class="name"><i>&gt; 75</i></span></li>\r\n</ul>\r\n</div>',
      format: null,
      safe_value:
        "&lt;div id =&quot;title_stats_legend&quot;&gt; Rate of urbanization (% of country area)&lt;/div&gt;\r\n&lt;div class=&quot;c-legend-content_stats&quot;&gt;&lt;ul class=&quot;c-legend-choropleth_stats&quot;&gt;\r\n&lt;li&gt;&lt;div class=&quot;icon-choropleth_stats&quot; style=&quot;background-color: #ffffd4!important;  &quot;&gt;&lt;/div&gt;&lt;span class=&quot;name&quot;&gt;&lt;i&gt;15-30&lt;/i&gt;&lt;/span&gt;&lt;/li&gt;\r\n&lt;li&gt;&lt;div class=&quot;icon-choropleth_stats&quot;  style=&quot;background-color: #fed98e!important;  &quot;&gt;&lt;/div&gt;&lt;span class=&quot;name&quot;&gt;&lt;i&gt;30-45&lt;/i&gt;&lt;/span&gt;&lt;/li&gt;\r\n&lt;li&gt;&lt;div class=&quot;icon-choropleth_stats&quot; style=&quot;background-color: #fe9929!important;  &quot;&gt;&lt;/div&gt;&lt;span class=&quot;name&quot;&gt;&lt;i&gt;45-60&lt;/i&gt;&lt;/span&gt;&lt;/li&gt;\r\n&lt;li&gt;&lt;div class=&quot;icon-choropleth_stats&quot; style=&quot;background-color: #d95f0e!important;  &quot;&gt;&lt;/div&gt;&lt;span class=&quot;name&quot;&gt;&lt;i&gt;60-75&lt;/i&gt;&lt;/span&gt;&lt;/li&gt;\r\n&lt;li&gt;&lt;div class=&quot;icon-choropleth_stats&quot; style=&quot;background-color: #993404!important;  &quot;&gt;&lt;/div&gt;&lt;span class=&quot;name&quot;&gt;&lt;i&gt;&amp;gt; 75&lt;/i&gt;&lt;/span&gt;&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;"
    },
    first_color_legend: "#ffffd4",
    second_color_legend: "#993404",
    server_type: "Geoserver",
    statistics_type: "percentage"
  },
  {
    title: "Tonnes of carbon biomass per km2",
    layer_id: "agbid",
    legend: {
      value:
        '<div style="color: #dadada; font-size: 12px; float: left!important;">1 Mg<sup>2</sup></div><div style="color: #dadada; font-size: 12px; float: right!important;">&gt;55,000 Mg</div><div class="lscGradient " style="clear: both;"></div>',
      format: "full_html",
      safe_value:
        '<div style="color: #dadada; font-size: 12px; float: left!important;">1 Mg<sup>2</sup></div>\n<div style="color: #dadada; font-size: 12px; float: right!important;">&gt;55,000 Mg</div>\n<div class="lscGradient " style="clear: both;"></div>\n'
    },
    wms_name: "dopa_explorer_3:total_carbon",
    wms_style: "total_carbon_africa_platform",
    wms_url: "https://geospatial.jrc.ec.europa.eu/geoserver",
    stats_legend: {
      value:
        '<div id ="title_stats_legend"> Country total carbon (Pg)</div>\r\n<div class="c-legend-content_stats"><ul class="c-legend-choropleth_stats">\r\n<li><div class="icon-choropleth_stats" style="background-color: #1a9641!important;  "></div><span class="name"><i>0</i></span></li>\r\n<li><div class="icon-choropleth_stats"  style="background-color: #58b453!important;  "></div><span class="name"><i>0.5</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #97d265!important;  "></div><span class="name"><i>1</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #c4e687!important;  "></div><span class="name"><i>1.5</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #ecf7ad!important;  "></div><span class="name"><i>2</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #ffedab!important;  "></div><span class="name"><i>5</i></span></li>\r\n<li><div class="icon-choropleth_stats"  style="background-color: #fec981!important;  "></div><span class="name"><i>4</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #f99e59!important;  "></div><span class="name"><i>5</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #e85b3a!important;  "></div><span class="name"><i>6</i></span></li>\r\n<li><div class="icon-choropleth_stats" style="background-color: #d7191c!important;  "></div><span class="name"><i>&gt; 8</i></span></li>\r\n</ul>\r\n</div>',
      format: null,
      safe_value:
        "&lt;div id =&quot;title_stats_legend&quot;&gt; Country total carbon (Pg)&lt;/div&gt;\r\n&lt;div class=&quot;c-legend-content_stats&quot;&gt;&lt;ul class=&quot;c-legend-choropleth_stats&quot;&gt;\r\n&lt;li&gt;&lt;div class=&quot;icon-choropleth_stats&quot; style=&quot;background-color: #1a9641!important;  &quot;&gt;&lt;/div&gt;&lt;span class=&quot;name&quot;&gt;&lt;i&gt;0&lt;/i&gt;&lt;/span&gt;&lt;/li&gt;\r\n&lt;li&gt;&lt;div class=&quot;icon-choropleth_stats&quot;  style=&quot;background-color: #58b453!important;  &quot;&gt;&lt;/div&gt;&lt;span class=&quot;name&quot;&gt;&lt;i&gt;0.5&lt;/i&gt;&lt;/span&gt;&lt;/li&gt;\r\n&lt;li&gt;&lt;div class=&quot;icon-choropleth_stats&quot; style=&quot;background-color: #97d265!important;  &quot;&gt;&lt;/div&gt;&lt;span class=&quot;name&quot;&gt;&lt;i&gt;1&lt;/i&gt;&lt;/span&gt;&lt;/li&gt;\r\n&lt;li&gt;&lt;div class=&quot;icon-choropleth_stats&quot; style=&quot;background-color: #c4e687!important;  &quot;&gt;&lt;/div&gt;&lt;span class=&quot;name&quot;&gt;&lt;i&gt;1.5&lt;/i&gt;&lt;/span&gt;&lt;/li&gt;\r\n&lt;li&gt;&lt;div class=&quot;icon-choropleth_stats&quot; style=&quot;background-color: #ecf7ad!important;  &quot;&gt;&lt;/div&gt;&lt;span class=&quot;name&quot;&gt;&lt;i&gt;2&lt;/i&gt;&lt;/span&gt;&lt;/li&gt;\r\n&lt;li&gt;&lt;div class=&quot;icon-choropleth_stats&quot; style=&quot;background-color: #ffedab!important;  &quot;&gt;&lt;/div&gt;&lt;span class=&quot;name&quot;&gt;&lt;i&gt;5&lt;/i&gt;&lt;/span&gt;&lt;/li&gt;\r\n&lt;li&gt;&lt;div class=&quot;icon-choropleth_stats&quot;  style=&quot;background-color: #fec981!important;  &quot;&gt;&lt;/div&gt;&lt;span class=&quot;name&quot;&gt;&lt;i&gt;4&lt;/i&gt;&lt;/span&gt;&lt;/li&gt;\r\n&lt;li&gt;&lt;div class=&quot;icon-choropleth_stats&quot; style=&quot;background-color: #f99e59!important;  &quot;&gt;&lt;/div&gt;&lt;span class=&quot;name&quot;&gt;&lt;i&gt;5&lt;/i&gt;&lt;/span&gt;&lt;/li&gt;\r\n&lt;li&gt;&lt;div class=&quot;icon-choropleth_stats&quot; style=&quot;background-color: #e85b3a!important;  &quot;&gt;&lt;/div&gt;&lt;span class=&quot;name&quot;&gt;&lt;i&gt;6&lt;/i&gt;&lt;/span&gt;&lt;/li&gt;\r\n&lt;li&gt;&lt;div class=&quot;icon-choropleth_stats&quot; style=&quot;background-color: #d7191c!important;  &quot;&gt;&lt;/div&gt;&lt;span class=&quot;name&quot;&gt;&lt;i&gt;&amp;gt; 8&lt;/i&gt;&lt;/span&gt;&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;"
    },
    first_color_legend: "#a6c370",
    second_color_legend: "#c1603a",
    server_type: "GeoWebCache",
    statistics_type: "quantity"
  }
];
